export default {
    loginhome:{
        login:'เข้าสู่ระบบ',
        login_facebook:'เข้าสู่ระบบด้วย Facebook',
        login_google:'เข้าสู่ระบบด้วย Google',
        login_apple:'ลงชื่อเข้าด้วย Apple',
        register:'สมัครสมาชิก',
        privacy_policy:'นโยบายคุ้มครองข้อมูลส่วนบุคคล',
        or:'หรือ',
        skip:'ข้าม',
        warn_apple:'กรุณาแอปพลิเคชันออกจากรายชื่อ แอปเปิ้ลไอดีของเครื่อง แล้วลองใหม่อีกครั้ง'
    },
    header:{
        back:'กลับ',
        ongoing:'ON GOING',
        comingsoon:'COMING SOON',
        schedule:'SCHEDULE',
        movie:'MOVIES'
    },
    footer:{
        house:'HOUSE',
        wish:'WISH',
        tickets:'TICKETS',
        profile:'PROFILE'
    },
    login:{
        error:{
            username:'กรุณากรอกชื่อผู้ใช้',
            password:'กรุณากรอกรหัสผ่าน'
        },
        username:'อีเมล',
        password:'รหัสผ่าน',
        login:'เข้าสู่ระบบ',
        forget_password:'ลืมรหัสผ่าน',
        
    },
    register:{
        error:{
            email:'กรุณาระบุอีเมลให้ถูกต้อง',
            password:'กรุณากรอกรหัสผ่าน',
            confirm_password:'กรุณากรอกยืนยันรหัสผ่าน',
            mobile:'กรุณาระบุเบอร์โทรศัพท์ให้ถูกต้อง',
            correct_password:'กรุณากรอกรหัสผ่านให้ถูกต้อง',
            check:'กรุณาอ่านและยอมรับข้อตกลงการใช้งาน',
            // validate:'รหัสผ่านต้องมีมากกว่า 8 ตัวอักษร มีตัวใหญ่ ตัวเลขและตัวอักขระพิเศษผสมด้วย'
            validate:'รหัสผ่านต้องมีมากกว่า 8 ตัวอักษร'
        },
        alert:{
            ok:'ตกลง',
            register_success:'สมัครสมาชิกสำเร็จ',
            agree:'ยอมรับ'
        },
        register:'สมัครสมาชิก',
        email:'อีเมล',
        password:'รหัสผ่าน',
        confirm_password:'ยืนยันรหัสผ่าน',
        mobile:'เบอร์โทรศัพท์',
        agree:'คุณยอมรับเงื่อนไขและข้อตกลงในการใช้บริการ',
        register_btn:'สมัครสมาชิก',
        term_of_service:'เงื่อนไขและข้อตกลงในการใช้บริการ'
    },
    tab:{
        house:{
            ongoing:{
                title:'กำลังฉาย'
            },
            comingsoon:{
                title:'เร็วๆนี้'
            },
            schedule:{
                title:'ตางรางฉายหนัง',
                no_item:'ไม่มีรายการฉาย'
            }
        },
        wish:{
            title:'WISH',
            no_list:'ไม่มีรายการ'
        },
        tickets:{
            tickets:'ตั๋วของฉัน',
            history:'ประวัติ',
            tickets_each:{
                movie:'ชื่อหนัง',
                date:'วันที่',
                time:'เวลา',
                cinema:'โรง',
                seat:'ที่นั่ง'
            },
            ticket_buffet:{
                movie:'MOVIE',
                description:'DESCRIPTION',
                days:'DAYS',
            },
            ticket_buffet_movie:{
                event:'THE BUFFET SERIES',
                time:'DATE',
                member_id:'MEMBER ID: ',
                title:'TITLE'
            }
        },
        profile:{
            error:{
                password:'กรุณาระบุรหัสผ่าน',
                confirm_password:'กรุณาระบุยืนยันรหัสผ่าน',
                password_incorrect:'รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน',
                mobile:'กรุณาระบุเบอร์โทรศัพท์ให้ถูกต้อง',
                password_strong:'รหัสผ่านต้องมีมากกว่า 8 ตัวอักษร มีตัวใหญ่ ตัวเลขและตัวอักขระพิเศษผสมด้วย'
            },
            alert:{
                ok:'ตกลง',
                otp_sent:'ระบบได้ส่งรหัส OTP เปลี่ยนแปลงเบอร์โทรไปยังเบอร์โทรที่ระบุ',
                change_password_Success:'เปลี่ยนรหัสผ่านสำเร็จ',
                change_number_Success:'เปลี่ยนเบอร์โทรสำเร็จ',
                cancel:'cancel',
                exchangesuccess:'แลกตั๋วสำหรับชมภาพยนตร์สำเร็จ'
            },
            profile:'ข้อมูลผู้ใช้',
            dont_login:'คุณไม่ได้เข้าสู่ระบบ',
            login_btn:'เข้าสู่ระบบ',
            email:'อีเมล',
            mobile:'เบอร์โทร',
            change_mobile_btn:'เปลี่ยน',
            type_mamber:'ประเภท MEMBER',
            apply_btn:'สมัคร E-MEMBER',
            change_password_btn:'เปลี่ยนรหัสผ่าน',
            delete_user:'ลบบัญชีผู้ใช้งาน',
            point_btn:'คะแนน',
            setting_btn:'ตั้งค่า',
            signout_btn:'ออกจากระบบ',
            edit_proflie:'ดูโปรไฟล์',
            recent_noti:'RECENT NOTIFICATION',
            my_card:'การ์ดของฉัน',
            house_club:'HOUSE CLUB\nMEMBERSHIP',
            
            no_card:'NO.',
            for_life:'FOR LIFE  SINCE',
            other:'OTHERS',

            student_club:'STUDENT\nMEMBERSHIP',
            student_expire:'Expire',

            enter_new_password:'รหัสผ่านใหม่',
            enter_confirm_password:'ยืนยันรหัสผ่าน',
            save_btn:'บันทึก',
            
            change_mobile_number:'เปลี่ยนเบอร์โทรศัพท์',
            mobile_new:'เบอร์โทรใหม่',
            otp:'OTP',
            request_otp:'ส่ง OTP ใหม่',
            enter_otp:'OTP',
            request_new_otp_btn:'REQUEST NEW OTP',

            setting:'ตั้งค่า',
            language:'ภาษา',
            thai:'ไทย',
            ENG:'อังกฤษ',
            notification:'การแจ้งเตือน',
            on:'เปิด',
            off:'ปิด',

            point:'POINT',
            my_voucher:'VOUCHER ที่มี',
            my_point:'คุณมีคะแนน',
            redeem_voucher:'แลก VOUCHER',
            point_unit:'คะแนน',
            voucher_unit:'VOUCHER',
            remark:'10 point แลกได้ 1 voucher',

            ememberprivilege:'E MEMBER PRIVILEGE',
            merchadise:'ของแถมของคุณ',
            receive:'ยังไม่ได้รับ',
            received:'ได้รับแล้ว',
            voucher_p:'VOUCHER',
            buffet_p:'BUFFET',
            discount:'ส่วนลดสำหรับสมาชิก',

            view_profile:'ดูโปรไฟล์',

            delete_user_text:'ยืนยันการลบบัญชีผู้ใช้งาน',
            delete_user_yes:'ตกลง',
            delete_user_no:'ยกเลิก',
            delete_user_text:'การลบบัญชีผู้ใช้งานสำเร็จ',
        }
    },
    EmemberprivilegeScreen:{
        detail_buffet:'การชม FLIM BUFFET ตลอดทั้งโปรแกรม',
        detail_discount:'สิทธิ์ส่วนลดจากราคาตั๋วหนังปกติ',
        discount:'ส่วนลดสำหรับสมาชิก',
        voucher_p:'VOUCHER',
        buffet:'FLIM BUFFET'
    },
    BookingSelectSeatScreen:{
        date:'วันที่',
        screen:'หน้าจอ',
        select_seat:'ที่นั่งที่เลือก',
        total_price:'รวมเป็นเงิน',
        voucher:'คูปอง',
        next:'ถัดไป',
        confirm_booking:'ยืนยันการจอง',
        theater:'โรงที่'
    },
    BookingSelectTimeScreen:{
        no_item:'ไม่มีรายการ',
        trailer:'ตัวอย่างหนัง',
        to_day:'วันนี้',
        mins:'นาที',
        error1:'รอบนี้จะเปิดจองวันที่ ',
        time:' เวลา '
    },
    BookingSuccessScreen:{
        back_main:'กลับสู่หน้าหลัก'
    },
    BookingSummeryScreen:{
        select_seat:'ที่นั่งที่เลือก',
        price:'ราคา',
        unit:'บาท',
        voucher:'คูปอง',
        discont_e:'ส่วนลด E-MEMBER',
        total_price:'ราคาทั้งหมด',
        payment:'ชำระเงิน',
        credit:'CREDIT/DEBIT',
        qr_credit:'QRCODE CREDIT',
        qr_debit:'QRCODE PROMTPAY',
        pay_now:'PAYNOW',
        time_out:'หมดเวลาการทำรายการ',
        select_new:'กรุณาเลือกที่นั่งใหม่',
        ok:'ตกลง',
        error_payment:'จ่ายเงินไม่สำเร็จ',
        error_payment_message:'ข้อมูลการจ่ายเงินไม่ถูกต้อง'
    },
    ConfirmnumberScreen:{
        email:'อีเมล',
        phone:'เบอร์มือถือ',
        back:'กลับ',
        next:'ถัดไป'
    },
    ConfirmMobileScreen:{
        phone:'เบอร์มือถือ',
        back:'กลับ',
        ok:'ตกลง',
        error_phone:'กรุณากรอกเบอร์โทรศัพท์',
        error_number:'กรุณากรอกเบอร์โทรศัพท์',
        email:'อีเมล'
    },
    Econfirm:{
        confirm:'ยืนยัน',
        sent_otp:'ระบบได้ส่งรหัส OTP การสมัครสมาชิก\nไปที่มือถือของท่านแล้ว\nกรุณาตรวจสอบมือถือของท่าน',
        code:'Code',
        back:'BACK',
        next:'NEXT'
    },
    Ehome:{
        btn:'สมัคร E-MEMBER'
    },
    Epayment:{
        payment:'ชำระ',
        credit_card:'Credit Card Number',
        name:'ชื่อ',
        select_month:'เลือกเดือน',
        select_year:'เลือกปี',
        cvv:'CVV',
        back:'ยกเลิก',
        pay_now:'ชำระเงิน',
        remember_card:'จำบัตรเครดิต'
    },
    Epaymenthome:{
       paynow:'ชำระเงิน'
    },
    Eregister:{
        title:{
            mr:'นาย',
            ms:'นาง',
            miss:'นางสาว'
        },
        genderlist:{
            male:'ชาย',
            female:'หญิง',
            other:'อื่นๆ'
        },
        error:{
            title:'กรุณาระบุคำนำหน้า',
            citizen:'กรุณาระบุเลขบัตรประชาชนให้ถูกต้อง',
            name:'กรุณาระบุชื่อ',
            surname:'กรุณาระบุนามสกุล',
            birthday:'กรุณาระบุวันเกิดให้ถูกต้อง',
            mobile:'กรุณาระบุเบอร์โทรศัพท์',
            gender:'กรุณาระบุเพศให้ถูกต้อง',
            age:'กรุณาระบุอายุให้ถูกต้อง',
            merchadise:'กรุณาระบุสินค้าที่จะรับให้ถูกต้อง',
            check:'กรุณายอมรับข้อตกลงการใช้งาน'
        },
        select_tile:'เลือกคำนำหน้า',
        citizen:'เลขที่บัตรประชาชน',
        name:'ชื่อ',
        surname:'นามสกุล',
        birthday:'วันเกิด',
        mobile:'เบอร์โทร',
        Line:'Line ID',
        gender:'เลือกเพศ',
        age:'อายุ',
        merchadise:'เลือกของแถม',
        cancel:'ยกเลิก',
        next:'ถัดไป',
        agreetext:'I agree all statement in term of service?',
        register:'สมัครสมาชิก\n E-MEMBER PRIVILEGE',
        email:'อีเมล',
        is_voucher:'เลือกรับ 3 VOUCHER',
        is_buffet:'เลือกรับ BUFFET',
        term_of_service:'เงื่อนไขและข้อตกลงในการใช้บริการ',
        agree:'ยอมรับ'
    },

    ForgetpasswordScreen:{
        error_email:'กรุณากรอกอีเมลให้ถูกต้อง',
        ok:'ตกลง',
        sent_password:'ส่งลิงค์เปลี่ยนรหัสผ่านไปยัง E-MAIL ของท่านแล้ว',
        back_login:'กลับเข้าสู่หน้า LOGIN',
        forget_password:'ลืมรหัสผ่าน',
        enter_email:'กรอกอีเมลของคุณ',
        email:'อีเมล',
        sent:'ส่ง'
    },
    MycardScreen:{
        error_card:'กรุณาเลือกบัตร',
        add:'เพิ่มบัตร CREDIT/DEBIT',
        payment:'ชำระ',
        confirm:'ยืนยันการชำระเงิน',
        total:'ทั้งหมด',
        unit:'บาท'
    },
    PaymentScreen:{
        error:{
            name:'กรุณาระบุชื่อบัตรให้ถูกต้อง',
            Credit_number:'กรุณาระบุเลขบัตรให้ถูกต้อง',
            exp_month:'กรุณาระบุวันหมดอายุบัตรให้ถูกต้อง',
            exp_year:'กรุณาระบุวันหมดอายุบัตรให้ถูกต้อง',
            cvv:'กรุณาระบุรหัส cvv ให้ถุกต้อง'
        },
        name:'Name',
        card:'Credit Card Number',
        exp_month:'Expire Month',
        exp_year:'Expire Year',
        cvv:'Security Code (CVV)',
        remember_card:'จำบัตรเครดิต',
        submit:'SUBMIT'
    },
    ShowQrcodeScreen:{
        credit:'QRCODE CREDIT',
        debit:'QRCODE PROMTPAY',
        back_home:'BACK TO HOME'
    },
    networkerror:'ไม่สามารถเชื่อมต่อกับระบบได้ กรุณาตรวจสอบอินเตอร์เน็ต',
    try_again:'ลองอีกครั้ง',
    DetailbannerScreen:{
        movie:'MOVIE',
        warn_login:'กรุณาเข้าสู่ะบบเพื่อรับสิทธิ์ซื้อตั๋ว',
        buy_ticket:'ซื้อตั๋วดูภาพยนต์แบบบุฟเฟ่ต์',
        error_buyed_h:'คุณเคยซื้อตั๋วดูภาพยนต์แบบบุฟเฟ่ต์ไปแล้ว',
        error_buyed_d:'โปรดตรวจสอบที่เมนู Ticket'
    },
    PleaseLoginScreen:{
        please:'กรุณาเข้าสู่ระบบ'
    },
    profileView:{
        name:'ชื่อ',
        citizen:'รหัสประจำตัวประชาชน',
        birthday:'วันเกิด',
        age:'อายุ',
        gender:'เพศ',
        telephone:'เบอร์โทรศัพท์',
        email:'อีเมล'
    },
    FromStudentScreen:{
        register:"สมัครสมาชิกสำหรับนักเรียน",
        studentID:"รหัสนักเรียน",
        schoolName:"ชื่อสถานศึกษา",
        ImageCard:"รูปบัตรนักเรียน",
        exp_month:"เดือนที่หมดอายุ",
        exp_year:"ปีที่หมดอายุ",
        error:{
            studentID:"กรุณาระบุรหัสนักเรียน",
            schoolName:"กรุณาระบุชื่อสถานศึกษา",
            image:"กรุณาแนบรูปภาพบัตรนักเรียน",
            register:"ไม่สามารถสมัครสมาชิกนักเรียนได้เนื่องจากได้มีการสมัครไปแล้ว"
        },
        success:"สมัครสมาชิกสำหรับนักเรียนสำเร็จ"
    }


}