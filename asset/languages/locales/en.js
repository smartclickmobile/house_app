export default {
    loginhome:{
        login:'Login',
        login_facebook:'LOGIN WITH FACEBOOK',
        login_google:'LOGIN WITH GOOGLE',
        login_apple:'Sign in with Apple',
        register:'CREATE ACCOUNT',
        privacy_policy:'Privacy Policy',
        or:'OR',
        skip:'SKIP >',
        warn_apple:'Please delete this app in apple id list of this moblie and try again.'
    },
    header:{
        back:'BACK',
        ongoing:'ONGOING',
        comingsoon:'COMING SOON',
        schedule:'SCHEDULE',
        movie:'MOVIES'
    },
    footer:{
        house:'HOUSE',
        wish:'WISH',
        tickets:'TICKETS',
        profile:'PROFILE'
    },
    login:{
        error:{
            username:'Please enter a valid email.',
            password:'Please enter a valid password.'
        },
        username:'Email',
        password:'Password',
        login:'Login',
        forget_password:'Forget Password'
    },
    register:{
        error:{
            email:'Please enter a valid email.',
            password:'Please enter a valid password.',
            confirm_password:'Confirm Password is incorrect',
            mobile:'Please enter a valid mobile.',
            correct_password:'Password and Confirm password is not math',
            check:'Please accept terms and conditons.',
            // validate:'Password must have 8 or more characters;must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character.'
            validate:'Password must have 8 or more characters'
        },
        alert:{
            ok:'OK',
            register_success:'Sign up Success',
            agree:'Agree'
        },
        register:'SIGN UP',
        email:'E-Mail',
        password:'Password',
        confirm_password:'Confirm Password',
        mobile:'Mobile',
        agree:'I agree all statement in',
        register_btn:'SIGN UP',
        term_of_service:'term of service'
    },
    tab:{
        house:{
            ongoing:{
                title:'ONGOING'
            },
            comingsoon:{
                title:'COMING SOON'
            },
            schedule:{
                title:'SCHEDULE',
                no_item:'NOT AVAILABLE'
            }
        },
        wish:{
            title:'WISH',
            no_list:'NO ITEMS'
        },
        tickets:{
            tickets:'TICKETS',
            history:'HISTORY',
            tickets_each:{
                movie:'MOVIE',
                date:'DATE',
                time:'TIME',
                cinema:'CINEMA',
                seat:'SEAT'
            },
            ticket_buffet:{
                movie:'MOVIES',
                description:'DESCRIPTION',
                days:'DAYS',
                
            },
            ticket_buffet_movie:{
                event:'THE BUFFET SERIES',
                time:'DATE',
                member_id:'MEMBER ID: ',
                title:'TITLE'
            }
        },
        profile:{
            error:{
                password:'Please enter a valid password.',
                confirm_password:'Please enter a valid confirm password.',
                password_incorrect:'Password and confirm password are not match.',
                mobile:'Please enter a valid mobile.',
                // password_strong:'Password must have 8 or more characters;must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character.',
                password_strong:'Password must have 8 or more characters',
                
            },
            alert:{
                ok:'OK',
                otp_sent:'ระบบได้ส่งรหัส OTP เปลี่ยนแปลงเบอร์โทรไปยังเบอร์โทรที่ระบุ',
                change_password_Success:'เปลี่ยนรหัสผ่านสำเร็จ',
                change_number_Success:'เปลี่ยนเบอร์โทรสำเร็จ',
                cancel:'CANCEL',
                exchangesuccess:'Get voucher success.'
            },
            profile:'PROFILE',
            dont_login:"You have to login to see profile",
            login_btn:'Login',
            email:'E-MAIL',
            mobile:'MOBILE',
            change_mobile_btn:'CHANGE',
            type_mamber:'TYPE OF MEMBER',
            apply_btn:'APPLY E-MEMBER',
            change_password_btn:'CHANGE PASSWORD',
            delete_user:'DELETE USER',
            point_btn:'POINT',
            setting_btn:'SETTING',
            signout_btn:'SIGNOUT',
            edit_proflie:'VIEW PROFILE',
            recent_noti:'RECENT NOTIFICATION',
            my_card:'MY CARD',
            house_club:'HOUSE CLUB\nMEMBERSHIP',
            no_card:'NO.',
            for_life:'FOR LIFE  SINCE',
            other:'OTHERS',

            student_club:'STUDENT\nMEMBERSHIP',
            student_expire:'Expire',
            
            enter_new_password:'Enter New Password',
            enter_confirm_password:'Confirm Password',
            save_btn:'SAVE',
            
            change_mobile_number:'CHANGE MOBILE NUMBER',
            mobile_new:'Enter New Mobile Number',
            otp:'OTP',
            request_otp:'REQUEST OTP',
            enter_otp:'ENTER OTP',
            request_new_otp_btn:'REQUEST NEW OTP',

            setting:'SETTING',
            language:'LANGUAGE',
            thai:'THAI',
            ENG:'ENGLISH',
            notification:'NOTIFICATION',
            on:'ON',
            off:'OFF',

            point:'POINT',
            my_voucher:'MY VOUCHER',
            my_point:'MY POINT',
            redeem_voucher:'REDEEM VOUCHER',
            point_unit:'POINT',
            voucher_unit:'VOUCHER',
            remark:'10 point แลกได้ 1 voucher',

            ememberprivilege:'E MEMBER PRIVILEGE',
            merchadise:'MY MERCHANDISE',
            receive:'ยังไม่ได้รับ',
            received:'ได้รับแล้ว',
            voucher_p:'VOUCHER',
            buffet_p:'BUFFET',
            discount:'ส่วนลดสำหรับสมาชิก',
            detail_buffet:'การชม FLIM BUFFET ตลอดทั้งโปรแกรม',
            detail_discount:'สิทธิ์ส่วนลดจากราคาตั๋วหนังปกติ',

            view_profile:'VIEW PROFILE',

            delete_user_text:'Account delete confirmation.',
            delete_user_yes:'Confirm',
            delete_user_no:'Cancel',
            delete_user_success:'User account deleted',
        }
    },
    EmemberprivilegeScreen:{
        detail_buffet:'การชม FLIM BUFFET ตลอดทั้งโปรแกรม',
        detail_discount:'สิทธิ์ส่วนลดจากราคาตั๋วหนังปกติ',
        discount:'MEMBER DISCOUNT',
        voucher_p:'VOUCHER',
        buffet:'FLIM BUFFET'
    },
    BookingSelectSeatScreen:{
        date:'DATE',
        screen:'SCREEN',
        select_seat:'SELECT SEAT',
        total_price:'TOTAL PRICE',
        voucher:'VOUCHER',
        next:'NEXT',
        confirm_booking:'CONFIRM BOOKING',
        theater:'THEATER'
    },
    BookingSelectTimeScreen:{
        no_item:'NO ITEM',
        trailer:'TRAILER',
        to_day:'TODAY',
        mins:'MINS',
        error1:'รอบนี้จะเปิดจองวันที่ ',
        time:' เวลา '
    },
    BookingSuccessScreen:{
        back_main:'BACK TO HOME'
    },
    BookingSummeryScreen:{
        select_seat:'SELECT SEAT',
        price:'PRICE',
        unit:'THB',
        voucher:'VOUCHER',
        discont_e:'DISCOUNT E-MEMBER',
        total_price:'TOTAL PRICE',
        payment:'PAYMENT',
        credit:'CREDIT/DEBIT',
        qr_credit:'QRCODE CREDIT',
        qr_debit:'QRCODE PROMTPAY',
        pay_now:'PAYNOW',
        time_out:'Transaction timeout. We are sorry, but your transaction processing is taking too long, please try again.',
        select_new:'กรุณาเลือกที่นั่งใหม่',
        ok:'ตกลง',
        error_payment:'Payment unsuccessful',
        error_payment_message:'The payment infomation is incorrect. Please try again'
    },
    ConfirmnumberScreen:{
        email:'E-mail',
        phone:'Phone Number',
        back:'BACK',
        next:'NEXT'
    },
    ConfirmMobileScreen:{
        phone:'Phone number',
        back:'BACK',
        ok:'OK',
        error_phone:'please input your phone number',
        error_number:'Please enter\nyour phone number',
        email:'Email'
    },
    Econfirm:{
        confirm:'CONFIRM',
        sent_otp:'ระบบได้ส่งรหัส OTP การสมัครสมาชิก\nไปที่มือถือของท่านแล้ว\nกรุณาตรวจสอบมือถือของท่าน',
        code:'Code',
        back:'BACK',
        next:'NEXT'
    },
    Ehome:{
        btn:'REGISTER E-MEMBER'
    },
    Epayment:{
        payment:'PAYMENT',
        credit_card:'Credit Card Number',
        name:'NAME',
        select_month:'SELECT MONTH',
        select_year:'SELECT YEAR',
        cvv:'CVV',
        back:'CANCEL',
        pay_now:'PAY NOW',
        remember_card:'REMENBER CARD'
    },
    Epaymenthome:{
        paynow:'PAY NOW'
     },
    Eregister:{
        title:{
            mr:'Mr.',
            ms:'Mrs.',
            miss:'Miss'
        },
        genderlist:{
            male:'Male',
            female:'Female',
            other:'Other'
        },
        error:{
            title:'Please enter a valid title.',
            citizen:'Please enter a valid citizen id.',
            name:'Please enter a valid name.',
            surname:'Please enter a valid surname.',
            birthday:'Please enter a valid birthday.',
            mobile:'Please enter a valid mobile.',
            gender:'Please enter a valid gender.',
            age:'Please enter a valid age.',
            merchadise:'Please enter a valid merchandise.',
            check:'Please accept terms and conditons.'
        },
        select_tile:'SELECT TITLE',
        citizen:'CITIZEN ID',
        name:'NAME',
        surname:'SURNAME',
        birthday:'BIRTHDAY',
        mobile:'MOBILE',
        Line:'LINE ID',
        gender:'SELECT GENDER',
        age:'AGE',
        merchadise:'SELECT MERCHANDISE',
        cancel:'CANCEL',
        next:'NEXT',
        agreetext:'I agree all statement in term of service?',
        register:'สมัครสมาชิก\n E-MEMBER PRIVILEGE',
        email:'E-MAIL',
        is_voucher:'3 VOUCHER',
        is_buffet:'BUFFET',
        term_of_service:'term of service',
        agree:'Agree'
    },
    ForgetpasswordScreen:{
        error_email:'Please enter a valid email.',
        ok:'OK',
        sent_password:'ส่งลิงค์เปลี่ยนรหัสผ่านไปยัง E-MAIL ของท่านแล้ว',
        back_login:'BACK TO LOGIN',
        forget_password:'Forget Password',
        enter_email:'ENTER YOUR E-MAIL',
        email:'E-MAIL',
        sent:'Sent'
    },
    MycardScreen:{
        error_card:'please select card',
        add:'ADD CREDIT/DEBIT',
        payment:'PAYMENT',
        confirm:'PAY NOW',
        total:'TOTAL',
        unit:'THB'
    },
    PaymentScreen:{
        error:{
            name:'Please enter a valid card name.',
            Credit_number:'Please enter a valid card number.',
            exp_month:'please enter a valid expiration date',
            exp_year:'please enter a valid expiration date',
            cvv:'Please enter a valid cvv.'
        },
        name:'Name',
        card:'Credit Card Number',
        exp_month:'Expire Month',
        exp_year:'Expire Year',
        cvv:'Security Code (CVV)',
        remember_card:'REMEMBER CARD',
        submit:'SUBMIT'
    },
    ShowQrcodeScreen:{
        credit:'QRCODE CREDIT',
        debit:'QRCODE PROMTPAY',
        back_home:'BACK TO HOME'
    },
    networkerror:'Network connectivity is unavailable',
    try_again:"TRY AGAIN",
    DetailbannerScreen:{
        movie:'MOVIE',
        warn_login:'LOGIN TO BUY TICKET',
        buy_ticket:'BUY TICKET BUFFET',
        error_buyed_h:'คุณเคยซื้อตั๋วดูภาพยนต์แบบบุฟเฟ่ต์ไปแล้ว',
        error_buyed_d:'โปรดตรวจสอบที่เมนู Ticket'
    },
    PleaseLoginScreen:{
        please:'PLEASE LOGIN'
    },
    profileView:{
        name:'NAME',
        citizen:'CITIZEN ID',
        birthday:'BIRTHDAY',
        age:'AGE',
        gender:'GENDER',
        telephone:'telephone',
        email:'email'
    },
    FromStudentScreen:{
        register:"REGISTER FOR STUDENT",
        studentID:"STUDENT ID",
        schoolName:"SCHOOL NAME",
        ImageCard:"STUDENT CARD IMAGE",
        exp_month:"EXPIRE MONTH",
        exp_year:"EXPIRE YEAR",
        error:{
            studentID:"Please enter a valid student ID.",
            schoolName:"Please enter a valid school name",
            image:"Please put a valid student card image",
            register:"Can't for student membership because it has already been registered."
        },
        success:"Register for student success"
    }
}