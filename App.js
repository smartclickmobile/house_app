import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/Reducer';
import Routers from './Routers'
import {View, Text} from 'react-native';
export default class App extends Component {
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
       
        <Routers />
      </Provider>
    );
  }
  // render(){
  //     return(<View style={{flex:1,justifyContent:'center',alignContent:'center'}}>
  //     <Text>
  //         {'Hello world!'}
  //     </Text>
  // </View>)
  // }
}
