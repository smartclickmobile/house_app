/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import Tester from './Tester';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
