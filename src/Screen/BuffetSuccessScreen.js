import React, { Component } from "react"
import {
    TouchableOpacity,
    Dimensions,
    View,
    StyleSheet,
    Image,
    Text,
    Platform,

    BackHandler,
    Alert
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import { BG_1, BG_2, BLACK } from '../constant/color'

import Header from '../Component/header'
import { Container, Content} from "native-base";
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { TEXT_BODY_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD, TEXT_HEADER_EN_BOLD,SIZE_3_5,SIZE_2_3} from "../constant/font";
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
class BuffetSuccessScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            membertype:'',
            data : []
        }
    }
    async componentDidMount(){
        var membertype = await AsyncStorage.getItem('membertype')
        this.setState({membertype:membertype})
        BackHandler.addEventListener("back", () => this._handleBack())
        this.props.buffetClearPurchase()

        var data = {
            language:I18n.locale,
            event_buffet_id:this.props.event_buffet_id
        }
        this.props.getPurchaseBuffet_Detail(data)

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.purchase_detail != null && nextProps.purchase_detail != undefined){
            this.setState({data:nextProps.purchase_detail},()=>{
            })
        }
        if(nextProps.error != null && nextProps.error != undefined){
           Alert.alert(I18n.t('BookingSummeryScreen.error_payment'),I18n.t('BookingSummeryScreen.error_payment_message'),[
            {
                text: 'OK',
                onPress: () => {
                    this.props.clear_buffet()
                    Actions.popTo('home')
                }
            }
        ],
        { cancelable: false })
        }
    }
    _handleBack() {
        if(Actions.currentScene == "_BuffetSuccessScreen" || Actions.currentScene == 'BuffetSuccessScreen') {
       
            Actions.popTo('home')
        
          return true
        }
         return false
      }
      _renderItem = (item) => {
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var start_date_buffet = moment(item.start_date,'YYYY-MM-DD')
        var start_date_buffet_day = start_date_buffet.date()
        var start_date_buffet_month = I18n.locale == 'th'?month_th[start_date_buffet.month()]:month_en[start_date_buffet.month()]
        var end_date_buffet = moment(item.end_date,'YYYY-MM-DD')
        var end_date_buffet_day = end_date_buffet.date()
        var end_date_buffet_month = I18n.locale == 'th'?month_th[start_date_buffet.month()]:month_en[start_date_buffet.month()]
        var year = I18n.locale == 'th'?parseInt(start_date_buffet.year())+543:parseInt(start_date_buffet.year())
        var diff_day = end_date_buffet.diff(start_date_buffet, 'days')+1
        if(item.member != undefined){
            var no = item.member.member_id
            var num = 9 - no.length
            var no_text = ''
            for(var i = 0 ; i <= num ; i++){
                if(i == num){
                    no_text += no
                }else{
                    no_text += '0'
                }
            }
            var num1 = no_text.substring(0, 3)
            var num2 = no_text.substring(3, 6)
            var num3 = no_text.substring(6, 9)
            var num_text = num1+' '+num2+' '+num3
        }else{
            var num_text = ''
        }
        
        return(
        <View>
            {this.state.data.length != 0 && this.state.data != null ?<TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#289f8b',width:360,height:255,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                <View style={[{height:85,justifyContent:'center',paddingRight:20,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3,alignItems:'center'}]}>
                    <Image
                        source={require('../../asset/Images/buffet-logo-house-1024-300-tran.png')}
                        style={{
                            width:160,
                            alignSelf:'flex-end',
                            // height:dim.height/100*5,
                        }}
                        resizeMode='contain'
                    />
                </View>
        
                <View style={[{height:85,justifyContent:'flex-start',paddingHorizontal:20}]}>
                    <Text style={[styles.textH,{textAlign:'left',color:'#feec04',fontSize:22,fontFamily:'SpaceMono-Bold'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                    <Text numberOfLines={2} style={{color:'#feec04',fontSize:28,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{`${item.title}`}</Text>        
                </View>
                <View style={[{height:85,justifyContent:'flex-start',paddingHorizontal:20}]}>
                    <Text numberOfLines={1} style={{color:BG_1,fontSize:24,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{`${num_text}`}</Text>        
                    <Text numberOfLines={1} style={{color:BG_1,fontSize:16,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`DATE ${start_date_buffet_day} ${start_date_buffet_month} - ${end_date_buffet_day} ${end_date_buffet_month} ${year}`}</Text>        
                </View>

            </TouchableOpacity>:<View/>}
        {/* //     <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#f9f8f4',width:360,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
        //     <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:'#e7b945',overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
        //         <Image
        //             source={require('../../asset/Images/w-logo-house-1024-300-tran.png')}
        //             style={{
        //                 width:120,
        //                 alignSelf:'center',
        //                 // height:dim.height/100*5,
        //             }}
        //             resizeMode='contain'
        //         />
        //     </View>
    
        //     <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
        //         <Text style={[styles.textH,{textAlign:'center'}]}>EVENT</Text>
        //         <Text numberOfLines={2} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${item.title}`}</Text>        
        //     </View>
    
        //     <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
        //         <View style={{width:160}}>
        //             <View 
        //                 style={{
        //                     height:65,
        //                     borderBottomColor:BG_2,
        //                     borderBottomWidth:1,
        //                     alignItems:'center',
        //                     justifyContent:'center'
        //                 }}
        //             >
        //                 <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
        //                 <Text numberOfLines={2} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${item.total_movie} ${I18n.t('tab.tickets.ticket_buffet.movie')}\n${diff_day} ${I18n.t('tab.tickets.ticket_buffet.days')}`}</Text>        
                        
        //             </View>
        //             <View 
        //                 style={{
        //                     height:65,
        //                     borderBottomColor:BG_2,
        //                     borderBottomWidth:1,
        //                     alignItems:'center',
        //                     justifyContent:'center'
        //                 }}
        //             >
        //                 <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
        //                 <Text numberOfLines={2} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${start_date_buffet_day} ${start_date_buffet_month} - ${end_date_buffet_day} ${end_date_buffet_month} ${year}`}</Text>
                                
        //             </View>
        //         </View>
    
        //         <View style={{width:160,alignItems:'center',justifyContent:'center'}}>
        //             <Image
        //                 source={{uri:item.qrcode_buffet_path}}
        //                 style={{width:130,height:130}}
        //                 resizeMode='contain'
        //             />
        //         </View>
            
        //     </View>
        //     <View style={{height:30,backgroundColor:'#e7b945',overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
        // </TouchableOpacity> */}
        </View>

    );
    }
    _renderTicket(){
        var data = this.state.data
        return(
            this._renderItem(data)
        ) 
    }
    render(){
        return(
            <Container>
                <Header btnleft={true}/>
                <Content>
                {
                    this._renderTicket()
                }
                <TouchableOpacity style={{alignItems:'center',justifyContent:'center',padding:dim.width/100*3,borderColor:BG_1,borderWidth:1,margin:dim.width/100*2,borderRadius:dim.width/100*1.5}} 
                    onPress={()=> {
                        this.props.clear_buffet()
                        Actions.popTo('home')
                    }}
                >
                    <Text style={{color:BLACK,fontSize:SIZE_2_3,
                        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('BookingSuccessScreen.back_main')}</Text>
                </TouchableOpacity>
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        purchase_detail:state.buffet.purchase_detail,
        error:state.buffet.error
    }
  }
  var styles = StyleSheet.create({
    Content_View:{
        padding:dim.width/100*2.5,
        borderBottomColor:BG_2,
        borderBottomWidth:1
    },
    textH:{
        color:BLACK,
        fontSize:SIZE_2_3,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    Content_detail:{
        borderRightColor:BG_2,
        borderRightWidth:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomColor:BG_2,
        borderBottomWidth:1,
        height:dim.height/100*7
    },
    Content_detail1:{
        borderRightColor:BG_2,
        borderRightWidth:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomColor:BG_2,
        borderBottomWidth:1,
        height:dim.height/100*7
    },
    textD:{
        color:BLACK,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    }
  });
export default connect(mapStateToProps,actions)(BuffetSuccessScreen)