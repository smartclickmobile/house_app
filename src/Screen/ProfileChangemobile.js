import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Modal,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content, Icon,Form,Item,Input,Label,Container } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_4,SIZE_3_7,SIZE_6,SIZE_2_5 } from '../constant/font'
import Header from '../Component/header'
const dim = Dimensions.get('window');
class ProfileChangemobile extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalError:false,
            modalSuccess:false,
            textError:'',
            textSuccess:'',
            data:this.props.profile_info,
            telephone:''
        }
    }
    componentDidMount(){
       
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.change_number_num != this.state.changeNumber_num){
            this.setState({changeNumber_num:nextProps.change_number_num},()=>{
                if(nextProps.change_number == true){
                    this.setState({modalSuccess:true,textSuccess:I18n.t('tab.profile.alert.change_number_Success')})
                }
            })
        }
    }
    onChangeNumber(){
        if(this.state.telephone == ''){
          this.setState({textError:I18n.t('tab.profile.error.mobile'),modalError:true})
        }else{
          var data = {
              telephone:this.state.telephone,
              member_type_id:this.state.data.member_type_id,
              language:I18n.locale
            }
            this.props.changeNumber(data)
        }
    }
    render(){
        return(
            <Container>
                <Header btnleft={true}/>
                <Content style={{marginTop:0,padding:dim.width/100*2}}>
                    <Modal visible={this.state.modalError} transparent={true}>
                        <View style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <View style={styles.contain_icon_error}>
                                    <Icon name='exclamation' type='FontAwesome5' style={styles.icon_error}/>
                                </View>
                                <Text style={[styles.modal_text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Modal visible={this.state.modalSuccess} transparent={true}>
                        <View  style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <View style={styles.contain_icon_success}>
                                    <Icon name='check' type='FontAwesome5' style={styles.icon_success}/>
                                </View>
                                <Text style={[styles.modal_text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textSuccess}</Text>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} 
                                    onPress={()=>{
                                        this.setState({modalSuccess:false})
                                        var data = {
                                            language: I18n.locale
                                        }
                                        this.props.getProfileInfo(data)
                                        Actions.pop()
                                    }}
                                >
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_6,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('tab.profile.change_mobile_number')}</Text>
                    <View style={{alignItems:'center',marginTop:dim.height/100*2}}>
                        <Form style={{width:Platform.isPad?dim.width/100*40:dim.width/100*70}}>
                            <Item style={styles.item} floatingLabel>
                                <Label style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.mobile_new')}</Label>
                                <Input 
                                    style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                    onChangeText={(value)=> this.setState({telephone:value})}
                                    keyboardType='phone-pad'
                                    autoCapitalize='none'
                                />
                            </Item>
                        </Form>
                        {/*<TouchableOpacity style={[styles.btn1]} onPress={()=> this.onChangeNumber()}>
                            <Text style={{marginHorizontal:dim.width/100*1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.request_otp')}</Text>
                        </TouchableOpacity>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Form style={{width:dim.width/100*45}}>
                                <Item style={styles.item} floatingLabel>
                                    <Label style={styles.label}>{I18n.t('tab.profile.enter_otp')}</Label>
                                    <Input 
                                        style={styles.label}
                                        secureTextEntry={true}
                                        autoCapitalize='none'
                                    />
                                </Item>
                            </Form>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*20,marginLeft:dim.width/100*5}]}>
                                <Text style={{marginHorizontal:dim.width/100*1,fontSize:SIZE_2_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.request_new_otp_btn')}</Text>
                            </TouchableOpacity>
                                </View>*/}
                        
                        <TouchableOpacity style={[styles.btn1]} onPress={()=> this.onChangeNumber()}>
                            <Text style={{marginHorizontal:dim.width/100*1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.save_btn')}</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    modal_text:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    icon_error:{color:RED_COLOR},
    icon_success:{color:'#26de81'},
    contain_icon_error:{
        borderColor:RED_COLOR,
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    contain_icon_success:{
        borderColor:'#26de81',
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    modal_contain:{
        backgroundColor:'#fff',
        width:dim.width/100*80,
        borderRadius:dim.width/100*3,
        alignItems:'center',
        padding:dim.width/100*5
    },
    modal_container:{
        width: "100%",
        height: "100%",
        position: "absolute",
        backgroundColor:'black',
        opacity:0.7
        },
    item:{
        padding:3
    }
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
        change_number_num:state.profile.change_number_num,
        change_number:state.profile.change_number,
    }
  }

export default connect(mapStateToProps,actions)(ProfileChangemobile)