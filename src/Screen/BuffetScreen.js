import React, { Component } from "react"
import {
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    View,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1,BLACK, RED_COLOR } from '../constant/color'
import { Container, Content,Icon,Form,Label,Input,Item } from 'native-base';
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH, TEXT_BODY_EN, SIZE_3_5, SIZE_4, TEXT_BODY_EN_BOLD} from '../constant/font'
import {CheckBox} from 'react-native-elements'
import { WebView } from 'react-native-webview';
import { URL_IP } from '../constant/constant'
const dim = Dimensions.get('window');
class BuffetScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            checked:false,
            modalAgree: false,
            first_name:'',
            last_name:'',
            mobile:'',
            modalError: false
        }
    }
    componentDidMount(){
        var data1={
            language:I18n.locale
        }
        this.props.GetCreditCard(data1)
        // console.warn('buffet data', this.props.data)
        // console.warn('profile_info',this.props.profile_info)
        // setTimeout(()=>{
            this.setState({
                first_name:this.props.profile_info.first_name,
                last_name:this.props.profile_info.last_name,
                mobile:this.props.profile_info.telephone
            })
        // },1000)
        
    }
    onsubmit(){
        if(this.state.checked == false){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.check')})
        }else if(this.state.first_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.name')})
        }else if(this.state.last_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.surname')})
        }else if(this.state.mobile == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.mobile')})
        }else{
            var data = {
                first_name:this.state.first_name,
                last_name:this.state.last_name,
                mobile:this.state.mobile,
            }
            Actions.buffet_summery({buffet_data:this.props.data,user_data:data})
        }
    }
    render(){
        
        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={[styles.text_modal,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={[styles.text_modal_btn,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal visible={this.state.modalAgree} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*90,height:dim.height/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <TouchableOpacity style={{alignSelf:'flex-end'}}
                                onPress={()=>{
                                    this.setState({modalAgree:false})
                                }}
                            >
                                <Icon name='times'
                                    type='FontAwesome5'
                                    style={{color:BLACK,fontSize:dim.width/100*10,textAlign:'right'}}
                                />
                            </TouchableOpacity>
                            {/*<Text style={[styles.text_modal_bold,{fontSize:SIZE_4_5}]}>{I18n.t('register.term_of_service')}</Text>*/}
                            <WebView
                                style={{width:dim.width/100*80,height:dim.height/100*60,backgroundColor:'transparent',marginTop:dim.height/100*1}}
                                originWhitelist={['*']}
                                source={{ uri: URL_IP+'/Term/buy_buffet' }}
                            />
                            
                        </View>
                    </View>
                </Modal>
                <Header btnleft={true} title={'HOUSE BUFFET'}/>
                <Content>
                    <Image
                        source={{uri:this.props.data.thumbnail_mobile_path}}
                        style={{
                            width:dim.width,
                            height:dim.width
                        }}
                        resizeMode='contain'
                    />
                    <View style={{flexDirection:'row',marginTop:dim.height/100*1,alignItems:'center',justifyContent:'center'}}>
                            <CheckBox
                                ref={(input) => { this.checkb = input; }}
                                // title={I18n.t('register.agree')}
                                checked={this.state.checked}
                                containerStyle={{borderWidth:0,backgroundColor:'transparent'}}
                                textStyle={[styles.label,{fontSize:SIZE_3_5}]}
                                onPress={()=> {
                                    this.setState({checked:!this.state.checked})
                                }}
                                checkedColor={BLACK}
                            />
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.label,{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`${I18n.t('register.agree')} `}</Text>
                                <TouchableOpacity onPress={()=> this.setState({modalAgree:true})}>
                                    <Text style={[styles.label,{fontSize:SIZE_3_5,textDecorationLine:'underline',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.term_of_service')}</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                    <Form style={{width:dim.width/100*80,marginTop:dim.height/100*1,alignSelf:'center'}}>
                        <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} stackedLabel >
                            <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.name')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    value={this.state.first_name}
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.first_name = input; }}
                                    onChangeText={(value)=> this.setState({first_name:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} stackedLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.surname')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    value={this.state.last_name}
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.last_name = input; }}
                                    onChangeText={(value)=> this.setState({last_name:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} stackedLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.mobile')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    value={this.state.mobile}
                                    style={styles.input}
                                    ref={(input) => { this.mobile = input; }}
                                    onChangeText={(value)=> this.setState({mobile:value})}
                                    keyboardType='name-phone-pad'
                                    maxLength={10}
                                    autoCapitalize='none'
                                />
                            </Item>
                    </Form>
                    <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2,alignSelf:'center'}]} onPress={()=> this.onsubmit()}>
                        <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.next')}</Text>
                    </TouchableOpacity>
                    
            </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*1
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_bold:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    input:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        borderRadius:dim.width/100*2,
        paddingLeft:dim.width/100*2,
    },
    input_dis:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        marginVertical:dim.height/100*1,
        borderRadius:dim.width/100*2,
        backgroundColor:'#e1e5e8',
        paddingLeft:dim.width/100*2,
        color:'#888a8c'
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
    }
  }

export default connect(mapStateToProps,actions)(BuffetScreen)