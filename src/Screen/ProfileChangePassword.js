import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Modal,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content, Icon,Form,Item,Input,Label,Container } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_4,SIZE_3_7,SIZE_6 } from '../constant/font'
import Header from '../Component/header'
const dim = Dimensions.get('window');
class ProfileChangePassword extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalError1:false,
            modalSuccess1:false,
            textError:'',
            textSuccess:'',
            data:this.props.profile_info,
            password:'',
            cf_password:'',
            changepassword_num:null
        }
    }
    componentDidMount(){
       
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.change_password_num != this.state.changepassword_num){
            this.setState({changepassword_num:nextProps.change_password_num},()=>{
                
                if(nextProps.change_password == true ){
                    this.setState({modalSuccess1:true,textSuccess:I18n.t('tab.profile.alert.change_password_Success')})
                }
            })
        }
        console.log('nextProps.error',nextProps.error)
        if(nextProps.error != null && nextProps.error != undefined){
            this.setState({modalError1:true,textError:nextProps.error})
        }
    }
    onChangePassword(){
        // var validate = this.validatePassword(this.state.password)
        if(this.state.password==''){
            this.setState({modalError1:true,textError:I18n.t('tab.profile.error.password')})
        }else if(this.state.password.length<8){
            this.setState({modalError1:true,textError:I18n.t('tab.profile.error.password_strong')})
        }else if(this.state.cf_password==''){
            this.setState({modalError1:true,textError:I18n.t('tab.profile.error.confirm_password')})
        }else if(this.state.password != this.state.cf_password){
            this.setState({modalError1:true,textError:I18n.t('tab.profile.error.password_incorrect')})
        }else{
            var data = {
                password:this.state.password,
                language:I18n.locale
            }
            this.props.changePassword(data)
        }
      }
      validatePassword(){
        var SpacialCharacter = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        var upper = /([A-Z]+)/g
        var lower = /([a-z]+)/g
        var num = /([0-9]+)/g
        var check_sp = SpacialCharacter.test(this.state.password);
        var check_upper = upper.test(this.state.password);
        var check_lower = lower.test(this.state.password);
        var check_num = num.test(this.state.password);
        var check = false
        // if(check_sp && check_upper && check_lower && check_num){
        if(this.state.password.length >= 8){
            check = true
            this.setState({password_c:false,validate_password_text:false})
        }else{
            this.setState({password_c:true,validate_password_text:true})
        }
        return check
    }
    render(){
        // console.log('password',this.state.password)
        return(
            <Container>
                <Header btnleft={true}/>
                <Content style={{marginTop:0,padding:dim.width/100*2}}>
                    <Modal visible={this.state.modalError1} transparent={true}>
                        <View  style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <View style={styles.contain_icon_error}>
                                    <Icon name='exclamation' type='FontAwesome5' style={styles.icon_error}/>
                                </View>
                                <Text style={[styles.modal_text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError1:false})}}>
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Modal visible={this.state.modalSuccess1} transparent={true}>
                        <View  style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <View style={styles.contain_icon_success}>
                                    <Icon name='check' type='FontAwesome5' style={styles.icon_success}/>
                                </View>
                                <Text style={styles.modal_text}>{this.state.textSuccess}</Text>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} 
                                    onPress={()=>{
                                        this.setState({modalSuccess1:false,password:'',cf_password:''})
                                        Actions.pop()
                                    }}
                                >
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_6,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('tab.profile.change_password_btn')}</Text>
                    <View style={{alignItems:'center'}}>
                        <Form style={{width:Platform.isPad?dim.width/100*40:dim.width/100*70}}>
                            <Item style={styles.item} floatingLabel>
                                <Label style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('tab.profile.enter_new_password')}</Label>
                                <Input 
                                    value={this.state.password}
                                    style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                    onChangeText={(value)=> this.setState({password:value})}
                                    secureTextEntry={true}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={styles.item} floatingLabel>
                                <Label style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('tab.profile.enter_confirm_password')}</Label>
                                <Input 
                                    value={this.state.cf_password}
                                    style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                    onChangeText={(value)=> this.setState({cf_password:value})}
                                    secureTextEntry={true}
                                    autoCapitalize='none'
                                />
                            </Item>
                        </Form>
                        <TouchableOpacity style={[styles.btn1]} onPress={()=> this.onChangePassword()}>
                            <Text style={{marginHorizontal:dim.width/100*1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.save_btn')}</Text>
                        </TouchableOpacity>
                    </View>
                    
                </Content>
            </Container>
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    modal_text:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    icon_error:{color:RED_COLOR},
    icon_success:{color:'#26de81'},
    contain_icon_error:{
        borderColor:RED_COLOR,
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    contain_icon_success:{
        borderColor:'#26de81',
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    modal_contain:{
        backgroundColor:'#fff',
        width:dim.width/100*80,
        borderRadius:dim.width/100*3,
        alignItems:'center',
        padding:dim.width/100*5
    },
    modal_container:{
        width: "100%",
        height: "100%",
        position: "absolute",
        backgroundColor:'black',
        opacity:0.7
        },
    item:{
        padding:3
    }
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
        change_password_num:state.profile.change_password_num,
        change_password:state.profile.change_password,
        error:state.profile.error
    }
  }

export default connect(mapStateToProps,actions)(ProfileChangePassword)