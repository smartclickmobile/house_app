import React, {Component} from 'react';
import {
  TouchableOpacity,
  Dimensions,
  View,
  StyleSheet,
  Image,
  Text,
  Platform,
  ScrollView,
  FlatList,

  Modal,
  ToastAndroid,
  Alert,
} from 'react-native';
import * as actions from '../Actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {BG_1} from '../constant/color';
import {
  TEXT_BODY_EN,
  TEXT_HEADER_EN,
  TEXT_TH,
  TEXT_TH_BOLD,
  TEXT_BODY_EN_BOLD,
  SIZE_4,
  SIZE_3_5,
  SIZE_5,
  SIZE_6,
  SIZE_3_7,
  SIZE_2_7,
  SIZE_4_5,
  SIZE_3,
  TEXT_HEADER_EN_BOLD,
} from '../constant/font';
import Header1 from '../Component/headerMovie';
import {Container, Header, Tab, Tabs,ScrollableTab} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import AsyncStorage from '@react-native-async-storage/async-storage';
import I18n from '../../asset/languages/i18n';
import moment from 'moment';
import YouTube from 'react-native-youtube';
import {WebView} from 'react-native-webview';

const dim = Dimensions.get('window');
class BookingSelectTimeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timelist: [],
      timeselect: '',
      timeselect_id: '',
      date_list: [],
      dateselect: moment().format('YYYY-MM-DD'),
      is_wish: false,
      token: '',
      membertype: '',
      loading: true,
      height: 215,
      show_time_text: '',
      isPlaying: true,
    };
  }
  async componentDidMount() {
    var membertype = await AsyncStorage.getItem('membertype');
    var token = await AsyncStorage.getItem('token');
    if (token != null && token != '' && token != undefined) {
      var temp = {
        language: I18n.locale,
        telephone: this.props.profile_info.telephone,
      };
      this.props.getVoucher(temp);
    }
    var data = {
      movie_id: this.props.data.movie_id,
      search_date: moment().format('YYYY-MM-DD'),
      language: I18n.locale,
    };
    this.props.getShowtime(data);

    var currentDate = moment();
    var dateArray = [];
    for (var i = 0; i < 31; i++) {
      dateArray.push(moment(currentDate).format('YYYY-MM-DD'));
      currentDate = moment(currentDate).add(1, 'days');
    }
    this.setState({date_list: dateArray});

    this.setState({token: token, membertype: membertype});
    if (this.props.data.is_wish != undefined) {
      this.setState({is_wish: this.props.data.is_wish});
    }
    if (this.props.wish != undefined) {
      this.setState({is_wish: this.props.wish});
    }
    this.convertmin();
  }

  convertmin() {
    var min = parseInt(this.props.data.show_time);
    this.props.convertTime(min).then((resp) => {
      this.setState({show_time_text: resp});
    });
  }

  componentDidUpdate(prvProps) {
    if (
      this.props.showtime != null &&
      prvProps.showtime !== this.props.showtime
    ) {
      this.setState({timelist: this.props.showtime, loading: false});
    }
  }
  onWish(){
      if(this.state.is_wish){
          // remove
          var data1 = {
              movie_id:this.props.data.movie_id,
              language:I18n.locale,
              wish_id:this.props.data.wish_id
          }
          this.setState({is_wish:!this.state.is_wish},()=>{
              this.props.removewish(data1)
          })

      }else{
          // add
          this.setState({is_wish:!this.state.is_wish},()=>{
              var data = {
                  movie_id:this.props.data.movie_id,
                  language:I18n.locale
              }
              this.props.addwish(data)
              if(Platform.OS == 'android'){
                  ToastAndroid.show('YOU ADD "'+this.props.data.title+'"TO WISH', ToastAndroid.SHORT);
              }
          })
      }

  }
  async setTime(data,start){

      this.setState({timeselect:moment(data.start_time,"YYYY-MM-DD HH:mm:ss").format("HH:mm"),timeselect_id:data})
      var token = await AsyncStorage.getItem('token')
      console.warn('getItem token',token)
      if(token != '' && token != null){

          Actions.BookingSelectSeatScreen({data:this.props.data,time_data:data,start:start})
      }else{
          Actions.pleaselogin()
      }

  }
  onSelectDate(data1,index){
      this.scrollToRow(index)
      var data = {
          movie_id:this.props.data.movie_id,
          search_date: data1,
          language:I18n.locale
      }
      this.props.getShowtime(data)
      this.setState({dateselect:data1})
  }
   _renderItemTime = ({item}) => {
       console.warn('item',item)
      var now = moment().subtract({minutes: 30})
      var start = moment(item.start_time,"YYYY-MM-DD HH:mm:ss")
      var booking_date
      if(this.state.membertype == 1){
          booking_date = moment(item.booking_regular_date,"YYYY-MM-DD HH:mm:ss")
      }else if(this.state.membertype == 2){
          booking_date = moment(item.booking_member_date,"YYYY-MM-DD HH:mm:ss")
      }
      var disable_flag = false
      if(item.is_buffet == true){
          if(start <= now){
              disable_flag = true
          }
      }else{
          if(start <= now || booking_date > now){
              disable_flag = true
          }
      }
      return(
      <View style={{marginHorizontal:dim.width/100*2,marginTop:dim.width/100*2}}>
          <View style={{
              height:dim.height/100*6,
              width:dim.width/(3.5),
              backgroundColor:BG_1,
              opacity:  disable_flag == true ? 0.25 : 0.5
          }}/>
          <TouchableOpacity
              style={{
                  height:dim.height/100*6,
                  width:dim.width/(3.5),
                  marginTop:-(dim.height/100*6),
                  justifyContent:'center'
              }}
              onPress={()=>  {
                  if((start <= now || booking_date > now) && item.is_buffet == true){
                      var booking_d = moment(booking_date).format('YYYY-MM-DD')
                      var booking_t = moment(booking_date).format('HH:mm:ss')
                      Alert.alert("",I18n.t('BookingSelectTimeScreen.error1')+booking_d+I18n.t('BookingSelectTimeScreen.time')+booking_t)
                  }else{
                      this.setTime(item,start)
                  }
              }}
              disabled={disable_flag ? true : false}
          >
              <Text style={{fontSize:SIZE_4,color:BG_1,textAlign:'center',fontFamily:TEXT_HEADER_EN,fontWeight:'400'}} numberOfLines={1}>{start.format("HH:mm")}</Text>
          </TouchableOpacity>
      </View>
  )}
  _keyExtractor = (item, index) => item.id;
  scrollToRow(itemIndex) {
      this._scrollView.scrollTo({x:itemIndex * dim.width/3});
    }
  _renderBooking(){
      var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
      var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
      var date = moment(this.props.data.start_release_date,'YYYY-MM-DD HH:mm:ss')

      var day = date.date()
      var month = date.month()
      var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
      var year = date.year()
      var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
      // console.log(this.props.data.show_time)
      return(
          <View style={{marginTop:0}}>
                  <View style={{height:dim.height/100*55,alignItems:'center'}}>
                      <Image
                          source={{uri:this.props.data.poster_mobile_path}}
                          style={{
                              width:Platform.isPad? dim.width/100*30:dim.width/100*40,
                              height:Platform.isPad? dim.width/100*53 : dim.width/100*63,
                              marginTop:dim.height/100*1,
                          }}
                          resizeMode={'cover'}
                      />
                      <View style={{flexDirection:'row',paddingTop:dim.height/100*1}}>
                          <View style={{width:7,height:7,backgroundColor:BG_1,borderRadius:3.5,margin:dim.height/100*1}}/>
                          <View style={{width:7,height:7,backgroundColor:BG_1,borderRadius:3.5,margin:dim.height/100*1,opacity:0.5}}/>
                      </View>
                      <View style={{flexDirection:'row',marginTop:dim.height/100*0.25,alignItems:'center'}}>
                          <Text numberOfLines={2} style={{color:BG_1,fontSize:SIZE_4_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,width:dim.width/100*60,textAlign:'center'}}>{this.props.data.title}</Text>
                          {this.state.token!=null&&this.state.token!=''?<TouchableOpacity onPress={()=> this.onWish()}>
                              <Icon solid={this.state.is_wish} name='star' type='FontAwesome5' style={{fontSize:SIZE_6,marginHorizontal:dim.width/100*6,color:BG_1}}/>
                          </TouchableOpacity>:<View/>}
                      </View>
                      <View style={{marginVertical:dim.height/100*0.25,flexDirection:'row'}}>
                          {this.props.data.movie_category_name !=null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                              <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.movie_category_name.toUpperCase()}`}</Text>
                          </View>}
                          {this.props.data.show_time != null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                              <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.show_time} ${I18n.t('BookingSelectTimeScreen.mins')}`}</Text>
                          </View>}
                          {this.props.data.language!=null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                              <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.language}`}</Text>
                          </View>}
                          {this.props.data.rate_name !=null &&<View style={{paddingHorizontal:dim.width/100*1}}>
                              <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`R : ${this.props.data.rate_name}`}</Text>
                          </View>}
                      </View>
                  </View>
                  {<View style={{height:dim.height/100*5}}>
                  <View style={{backgroundColor:BG_1,opacity:0.5,height:dim.height/100*5,width:dim.width}}/>
                      <ScrollView ref={view => this._scrollView = view} showsHorizontalScrollIndicator={false} horizontal style={{height:dim.height/100*5,marginTop:-(dim.height/100*5)}}>
                          <View style={{flexDirection:'row',alignItems:'center'}}>
                              {
                                  this.state.date_list.map((item,index)=>{
                                      var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
                                      var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
                                      var day_th = ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."]
                                      var day_en = ["SUN","MON","TUE","WED","THU","FRI","SAT"]
                                      var date = moment(item,'YYYY-MM-DD')
                                      var day = date.date()
                                      var days = date.day()
                                      var month = date.month()

                                      var days_txt = I18n.locale == 'th'?day_th[days]:day_en[days]
                                      var month_txt = I18n.locale == 'th'?month_th[month]:month_en[month]
                                      return(
                                          <TouchableOpacity key={index} style={{width:dim.width/3,alignItems:'center',flexDirection:'row',justifyContent:'center',marginLeft:index==0?dim.width/3:0}} onPress={()=> this.onSelectDate(item,index)}>
                                              <Text style={item == this.state.dateselect?[styles.text_btn_a,{color:BG_1,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]:[styles.text_btn,{color:BG_1}]}>{index==0?"TODAY":days_txt}</Text>
                                              <Text style={item == this.state.dateselect?[styles.text_btn_a,{color:BG_1,fontFamily:TEXT_HEADER_EN_BOLD}]:[styles.text_btn,{color:BG_1}]}> {day}</Text>
                                              <Text style={item == this.state.dateselect?[styles.text_btn_a,{color:BG_1,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]:[styles.text_btn,{color:BG_1}]}> {month_txt}</Text>
                                          </TouchableOpacity>
                                      )
                                  })
                              }
                          </View>
                      </ScrollView>
                  </View>}
                  <View style={{alignItems:'center'}}>
                  {
                      this.state.timelist.length == 0?
                      <View style={{padding:dim.height/100*1}}>
                          <Text style={[styles.text_btn_a,{color:BG_1,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('BookingSelectTimeScreen.no_item')}</Text>
                      </View>
                      :
                      <FlatList
                          data={this.state.timelist}
                          extraData={this.state}
                          keyExtractor={this._keyExtractor}
                          renderItem={this._renderItemTime}
                          numColumns={3}
                      />
                  }
                  </View>
          </View>
      )
  }
  handleReady = () => {
      setTimeout(() => this.setState({ height: Platform.isPad? dim.width/100*53 : dim.width/100*63 }), 500);
  }
  _renderDetail(){
      return(
          <View style={{marginTop:0}}>
              <View style={{height:dim.height/100*55,alignItems:'center'}}>
                  <YouTube
                      play={this.state.isPlaying==true?true:false}
                      videoId={this.props.data.trailer_mobile_path}   // The YouTube video ID
                      // play={true}             // control playback of video with true/false
                      // fullscreen={true}       // control whether the video should play in fullscreen or inline
                      apiKey={'AIzaSyA0T02ovtW32lO19GS3lB9PiJgI9xGZ7Mk'}
                      onReady={this.handleReady}
                      controls={2}
                      style={{ alignSelf: 'stretch', height: Platform.isPad? dim.width/100*52 : dim.width/100*62,marginTop:dim.height/100*1, }}
                  />
                  {<View style={{flexDirection:'row',paddingTop:dim.height/100*1}}>
                      <View style={{width:7,height:7,backgroundColor:BG_1,borderRadius:3.5,margin:dim.height/100*1,opacity:0.5}}/>
                      <View style={{width:7,height:7,backgroundColor:BG_1,borderRadius:3.5,margin:dim.height/100*1}}/>
                  </View>}
                  <View style={{flexDirection:'row',marginTop:dim.height/100*0.25,alignItems:'center'}}>
                      <Text numberOfLines={2} style={{color:BG_1,fontSize: SIZE_4_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,width:dim.width/100*60,textAlign:'center'}}>{this.props.data.title}</Text>
                      {this.state.token!=null&&this.state.token!=''?<TouchableOpacity onPress={()=> this.onWish()}>
                          <Icon solid={this.state.is_wish} name='star' type='FontAwesome5' style={{fontSize:SIZE_6,marginHorizontal:dim.width/100*6,color:BG_1}}/>
                      </TouchableOpacity>:<View/>}
                  </View>
                  <View style={{marginVertical:dim.height/100*0.5,flexDirection:'row'}}>
                      {this.props.data.movie_category_name !=null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                      <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.movie_category_name.toUpperCase()}`}</Text>
                      </View>}
                      {this.props.data.show_time != null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                          <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.show_time} ${I18n.t('BookingSelectTimeScreen.mins')}`}</Text>
                      </View>}
                      {this.props.data.language!=null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                          <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.language}`}</Text>
                      </View>}
                      {this.props.data.rate_name !=null &&<View style={{paddingHorizontal:dim.width/100*1}}>
                          <Text style={{color:BG_1,fontSize:SIZE_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`R : ${this.props.data.rate_name}`}</Text>
                      </View>}

                  </View>
              </View>
                  <View style={{backgroundColor:BG_1,width:dim.width,height:dim.height/100*5,opacity:0.5}}/>
                  <View style={{alignItems:'center',width:dim.width,height:dim.height/100*5,marginTop:-(dim.height/100*5),justifyContent:'center'}}>
                      <Text style={{fontSize:SIZE_4,color:BG_1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontWeight:'400'}}>{I18n.t('BookingSelectTimeScreen.trailer')}</Text>
                  </View>
              <ScrollView>
                  <View style={{flex:1,alignItems:'center',justifyContent:'center',paddingHorizontal:dim.width/100*5}}>
                      <Text style={{fontSize:SIZE_3_5,color:BG_1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontWeight:'400'}}>{this.props.data.short_description}</Text>
                  </View>
                  {this.props.data.description!=null&&this.props.data.description!=undefined&&this.props.data.description!=''?<WebView
                      style={{width:dim.width,height:dim.height/100*30,backgroundColor:'transparent',marginTop:dim.height/100*1}}
                      originWhitelist={['*']}
                      source={{ html: `<div style="color: white; text-align: center;font-size:2rem;">` + this.props.data.description + '</div>' }}
                  />:<View/>}
              </ScrollView>

          </View>
      )
  }
  ontabChange(i){
      console.log('ontabChange',i)
      if(i == 0){
          this.setState({isPlaying:false},()=>{
              console.log('success')
          })
      }else{
          this.setState({isPlaying:true})
      }
  }
  render() {
    console.log('this.props.data', this.props.data);
    return (
      <Container>
        <Image
          source={{
            uri:
              this.props.data.background_mobile_path == null
                ? this.props.data.poster_mobile_path
                : this.props.data.background_mobile_path,
          }}
          style={{
            position: 'absolute',
            width: (dim.width / 100) * 300,
            height: (dim.height / 100) * 300,
            alignSelf: 'center',
            marginTop: -((dim.height / 100) * 100),
            backgroundColor: '#000000',
          }}
          blurRadius={Platform.OS == 'ios' ? 7 : 4}
        />
        <Header
          style={{
            backgroundColor: BG_1,
            borderBottomWidth: 1,
            borderBottomColor: 'transparent',
            paddingTop: (dim.height / 100) * 3,
            height: (dim.height / 100) * 11,
            opacity: 0.3,
            position: 'absolute',
          }}
        />
        {<Header1 leftbutton={true} transparent1={true} />}
        {
          <Tabs
            onChangeTab={({i}) => this.ontabChange(i)}
            renderTabBar={()=> <ScrollableTab style={{height:0}} />}
            style={{backgroundColor: 'transparent'}}>
            <Tab heading="" style={{backgroundColor: 'transparent'}}>
              {this._renderBooking()}

            </Tab>
            <Tab heading="" style={{backgroundColor: 'transparent'}}>
              {this._renderDetail()}

            </Tab>
            {/* <Tab heading="" style={{backgroundColor: 'transparent'}}>
              {this._renderTestA()}

            </Tab> */}
          </Tabs>
        }
      </Container>
    );
  }
  _renderTestA = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'red',
        }}>
        <Text>{'Hello world!'}</Text>
      </View>
    );
  };
  _renderTestB = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'blue',
        }}>
        <Text>{'Hello world!'}</Text>
      </View>
    );
  };
}
const mapStateToProps = (state) => {
  return {
    checkIpnoe: state.check.checkIpnoe,
    showtime: state.booking.showtime,
    profile_info: state.profile.profile_info,
  };
};
var styles = StyleSheet.create({
  text_btn_a: {
    textAlign: 'center',
    fontSize: SIZE_3_7,
    color: BG_1,
    fontFamily: I18n.locale == 'th' ? TEXT_TH_BOLD : TEXT_BODY_EN_BOLD,
  },
  text_btn: {
    textAlign: 'center',
    fontSize: SIZE_3_7,
    color: BG_1,
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
  },
});
export default connect(mapStateToProps, actions)(BookingSelectTimeScreen);
