import React, { Component } from "react"
import {
    TouchableOpacity,
    Dimensions,
    View,
    Image,
    Text,
    Platform,
    Alert,
    StyleSheet,

    BackHandler,
    ActivityIndicator,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import { BG_1, BLACK } from '../constant/color'
import { TEXT_BODY_EN, TEXT_HEADER_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD, TEXT_HEADER_EN_BOLD,SIZE_3_5,SIZE_4,SIZE_5 } from '../constant/font'
import Header1 from '../Component/headerMovie'
import { Container, Content,Header, Icon } from "native-base";
import CountDown from 'react-native-countdown-component';
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
class BookingSummeryScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            membertype:'',
            voucher_amount:parseInt(this.props.price.voucher_amount),
            voucher_max:parseInt(this.props.price.voucher_max),
            selectSeat:[],
            discount_voucher:[],
            total:parseInt(this.props.price.total_price_regular),
            total_voucher:0,
            discount_member:0,
            loading:false
        }
    }
    async componentDidMount(){
        BackHandler.addEventListener("back", () => this._handleBack())
        var membertype = await AsyncStorage.getItem('membertype')
        
        this.setState({membertype:membertype})
        var seat_array = this.props.seat.selectSeat
        seat_array.sort(function(a, b){return parseInt(a.normal_price)-parseInt(b.normal_price)});
        this.setState({selectSeat:seat_array},()=>{
            var discount_voucher = []
            for(var i = 0;i <this.state.voucher_amount;i++){
                discount_voucher.push(this.state.selectSeat[(this.state.selectSeat.length-1)-discount_voucher.length])
            }
            var total_voucher = 0
            for(var j = 0; j < discount_voucher.length ; j++){
                total_voucher += parseInt(discount_voucher[j].normal_price)
            }
            
            var discount_member = 0
            
            if(membertype == 2 && this.props.showtime_detail.is_use_member == true){
                for(var k = (this.state.selectSeat.length-1)-discount_voucher.length; k>=0;k--){
                    if(this.state.selectSeat[k].different > discount_member){
                        discount_member = this.state.selectSeat[k].different
                    }
                }
            }
            console.log(discount_member,this.props.showtime_detail.is_use_member,this.state.selectSeat)

            this.setState({total:parseInt(this.props.price.total_price_regular)-parseInt(total_voucher)-parseInt(discount_member),total_voucher:total_voucher,discount_voucher:discount_voucher,discount_member:discount_member})
        })
        
       
    }
    _handleBack() {
        if(Actions.currentScene == "_BookingSummeryScreen" || Actions.currentScene == "BookingSummeryScreen") {
            var data = {
                purchase_transaction_id:this.props.purchase_transaction_id,
                language:I18n.locale
            }
            this.props.bookingCancel(data)
        
          return true
        }
        // return false
      }
    async removeVoucher(){
        if(parseInt(this.state.voucher_amount) != 0){
            this.setState({voucher_amount:parseInt(this.state.voucher_amount)-1})
            var discount_voucher = this.state.discount_voucher
            discount_voucher.sort(function(a, b){return parseInt(a.normal_price)-parseInt(b.normal_price)});
            discount_voucher.pop()
            var total_voucher = 0
            for(var j = 0; j < discount_voucher.length ; j++){
                total_voucher += parseInt(discount_voucher[j].normal_price)
            }
            var discount_member = 0
            if(this.state.membertype == 2 && this.props.showtime_detail.is_use_member == true){
                for(var k = (this.state.selectSeat.length-1)-discount_voucher.length; k>=0;k--){
                    if(this.state.selectSeat[k].different > discount_member){
                        discount_member = this.state.selectSeat[k].different
                    }
                }
            }
            this.setState({discount_voucher:discount_voucher,total:parseInt(this.props.price.total_price_regular)-parseInt(total_voucher)-parseInt(discount_member),total_voucher:total_voucher,discount_member:discount_member})
            
        }
    }
    async addVoucher(){
        if(parseInt(this.state.voucher_amount) < parseInt(this.state.voucher_max)){
            this.setState({voucher_amount:parseInt(this.state.voucher_amount)+1})
            var discount_voucher = this.state.discount_voucher
            discount_voucher.push(this.state.selectSeat[(this.state.selectSeat.length-1)-discount_voucher.length])
            discount_voucher.sort(function(a, b){return parseInt(a.normal_price)-parseInt(b.normal_price)});
            var total_voucher = 0
            for(var j = 0; j < discount_voucher.length ; j++){
                total_voucher += parseInt(discount_voucher[j].normal_price)
            }
            var discount_member = 0
            if(this.state.membertype == 2 && this.props.showtime_detail.is_use_member == true){
                for(var k = (this.state.selectSeat.length-1)-discount_voucher.length; k>=0;k--){
                    if(this.state.selectSeat[k].different > discount_member){
                        discount_member = this.state.selectSeat[k].different
                    }
                }
            }
            this.setState({discount_voucher:discount_voucher,total:parseInt(this.props.price.total_price_regular)-parseInt(total_voucher)-parseInt(discount_member),total_voucher:total_voucher,discount_member:discount_member})
            
        }
    }
    onTimeout(){
        Alert.alert(I18n.t('BookingSummeryScreen.time_out'),I18n.t('BookingSummeryScreen.select_new'),[
            {
                text: I18n.t('BookingSummeryScreen.ok'),
                onPress: () => {
                    var data = {
                        purchase_transaction_id:this.props.purchase_transaction_id,
                        language:I18n.locale
                    }
                    this.props.bookingCancel(data)
                }
            }
        ],
        { cancelable: false }
    )
    }
    confirmPaymentFree(){
        var array_voucher = this.props.price.voucher_list_all
        var array_voucher_s = array_voucher.slice(0, parseInt(this.state.voucher_amount))
        var data = {
            language:I18n.locale,
            purchase_transaction_id:this.props.purchase_transaction_id,
            voucher_ids:array_voucher_s,
            price:this.state.total
        }
        // console.log('confirmPaymentFree',data)
        this.props.confirmPaymentFree(data)
        
    }
    confirmPaymentbyCredit(){
        var array_voucher = this.props.price.voucher_list_all
        var array_voucher_s = array_voucher.slice(0, parseInt(this.state.voucher_amount))
        var data = {
            purchase_transaction_id:this.props.purchase_transaction_id,
            voucher_ids:array_voucher_s,
            price:this.state.total
        }
       
        if(this.props.my_card.length > 0){
            Actions.mycard({data:data})
        }else{
            // console.log('confirmPaymentbyCredit',data)
            Actions.payment({data:data})
        }
       
    }
    confirmPaymentbyQRcredit(){
        this.setState({loading:true},()=>{
            setTimeout(() => {
                this.setState({loading:false})
            },60000)
        })
        var array_voucher = this.props.price.voucher_list_all
        var array_voucher_s = array_voucher.slice(0, parseInt(this.state.voucher_amount))
        var data = {
            language:I18n.locale,
            purchase_transaction_id:this.props.purchase_transaction_id,
            voucher_ids:array_voucher_s,
            price:this.state.total
        }
        this.props.getQRcredit(data)
    }
    confirmPaymentbyQRdebit(){
        this.setState({loading:true},()=>{
            setTimeout(() => {
                this.setState({loading:false})
            },60000)
        })
        var array_voucher = this.props.price.voucher_list_all
        var array_voucher_s = array_voucher.slice(0, parseInt(this.state.voucher_amount))
        var data = {
            language:I18n.locale,
            purchase_transaction_id:this.props.purchase_transaction_id,
            voucher_ids:array_voucher_s,
            price:this.state.total
        }
        this.props.getQRdebit(data)
    }

    _renderBooking(){
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date = moment(this.props.showtime_detail.start_time,'YYYY-MM-DD HH:mm:ss')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)

        var height = 0
        if(this.state.membertype==2){
            if(this.props.data.is_voucher==1){
                height = dim.height/100*12
            }else{
                height = dim.height/100*8
            }
        }else{
            if(this.props.data.is_voucher==1){
                height = dim.height/100*8
            }else{
                height = dim.height/100*4
            }
        }
        return(
            <Content>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:1}}>
                        <View style={{width:dim.width/100*49,height:dim.height/100*15,backgroundColor:BG_1,opacity:0.3}}/>
                        <View style={{width:dim.width/100*49,height:dim.height/100*15,marginTop:-(dim.height/100*15),flexDirection:'row'}}>
                            <Image
                                source={{uri:this.props.data.poster_mobile_path}}
                                style={{width:dim.width/100*17,height:dim.height/100*15}}
                            />
                            
                            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{textAlign:'center',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BG_1}}>{this.props.data.title}</Text>
                                <View style={{marginVertical:dim.height/100*0.5,flexDirection:'row'}}>
                                    <View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                                        <Text style={{color:BG_1,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{this.props.data.showtime}</Text>
                                    </View>
                                    <View style={{paddingHorizontal:dim.width/100*1}}>
                                        <Text style={{color:BG_1,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`R : ${this.props.data.rate_name}`}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                        <View style={{width:dim.width/100*49,height:dim.height/100*15,backgroundColor:BG_1,opacity:0.3}}/>
                        <View style={{width:dim.width/100*49,height:dim.height/100*15,marginTop:-(dim.height/100*15),alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:BG_1,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`DATE - ${day} ${monthtxt} ${yeartxt}`}</Text>
                            <Text style={{color:BG_1,fontSize:SIZE_5,fontFamily:TEXT_HEADER_EN_BOLD,marginTop:dim.height/100*1}}>{moment(this.props.time_data.start_time,"YYY-MM-DD HH:mm:ss").format("HH:mm")}</Text>
                        </View>
                    </View>
                </View>
                
                <View>
                    <View style={{height:dim.height/100*10,backgroundColor:BG_1,opacity:0.3,marginTop:dim.height/100*2}}/>
                    <View style={{marginTop:-(dim.height/100*10),height:dim.height/100*10,alignItems:'center',justifyContent:'center'}}>
                        <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.select_seat')}</Text>
                        <Text style={[styles.text_btn_pay_bold,{fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{this.props.seat.select_string}</Text>
                    </View>
                </View>

                <View>
                    <View style={{height:height,backgroundColor:BG_1,opacity:0.3,marginTop:dim.height/100*2}}/>
                    <View style={{marginTop:-height,height:height,padding:dim.width/100*2}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={[styles.text_btn_pay,{flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.price')}</Text>
                            <Text style={[styles.text_btn_pay,{textAlign:'right',flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`${this.props.price.total_price_regular} ${I18n.t('BookingSummeryScreen.unit')}`}</Text>
                        </View>
                        {this.props.data.is_voucher==1&&<View style={{flexDirection:'row',marginTop:dim.height/100*1}}>
                            <Text style={[styles.text_btn_pay,{flex:2,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.voucher')}</Text>
                            <TouchableOpacity onPress={()=> this.removeVoucher()} disabled={this.state.voucher_amount==0} style={{flex:0,alignItems:'center',justifyContent:'center',opacity:this.state.voucher_amount!=0?1:0.5}}>
                                <View style={{width:dim.width/100*5,height:dim.width/100*5,borderRadius:dim.width/100*2.5,backgroundColor:BG_1,opacity:0.3}}/>
                                <View style={{width:dim.width/100*5,height:dim.width/100*5,borderColor:BG_1,borderWidth:1,borderRadius:dim.width/100*2.5,alignItems:'center',justifyContent:'center',marginTop:-(dim.width/100*5)}}>
                                    <Icon name='minus' type='FontAwesome5' style={{color:BG_1,fontSize:SIZE_4}}/>
                                </View>
                            </TouchableOpacity>
                            <Text style={{flex:0,color:BG_1,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,textAlign:'center',marginHorizontal:dim.width/100*5}}>{this.state.voucher_amount}</Text>
                            <TouchableOpacity onPress={()=> this.addVoucher()} disabled={parseInt(this.state.voucher_amount)==parseInt(this.state.voucher_max) || parseInt(this.state.voucher_amount)== parseInt(this.state.selectSeat.length)} style={{flex:0,alignItems:'center',justifyContent:'center',opacity:parseInt(this.state.voucher_amount)==parseInt(this.state.voucher_max)|| parseInt(this.state.voucher_amount)== parseInt(this.state.selectSeat.length)?0.5:1}}>
                                <View style={{width:dim.width/100*5,height:dim.width/100*5,borderRadius:dim.width/100*2.5,backgroundColor:BG_1,opacity:0.3}}/>
                                <View style={{width:dim.width/100*5,height:dim.width/100*5,borderColor:BG_1,borderWidth:1,borderRadius:dim.width/100*2.5,alignItems:'center',justifyContent:'center',marginTop:-(dim.width/100*5)}}>
                                    <Icon name='plus' type='FontAwesome5' style={{color:BG_1,fontSize:SIZE_4}}/>
                                </View>
                            </TouchableOpacity>
                            <Text style={[styles.text_btn_pay,{textAlign:'right',flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`-${this.state.total_voucher} THB`}</Text>
                            
                        </View>}
                        {this.state.membertype==2?<View style={{flexDirection:'row',marginTop:dim.height/100*1}}>
                            <Text style={[styles.text_btn_pay,{flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.discont_e')}</Text>
                            <Text style={[styles.text_btn_pay,{textAlign:'right',flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`-${this.state.discount_member} THB`}</Text>
                        </View>:<View/>}
                    </View>
                </View>

                <View>
                    <View style={{height:dim.height/100*8,backgroundColor:BG_1,opacity:0.3,marginTop:dim.height/100*2}}/>
                    <View style={{marginTop:-(dim.height/100*8),height:dim.height/100*8,alignItems:'center',justifyContent:'center'}}>
                        <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.total_price')}</Text>
                        <Text style={[styles.text_btn_pay_bold,{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{`${this.state.total} ${I18n.t('BookingSummeryScreen.unit')}`}</Text>
                    </View>
                </View>

                {<View>
                    <View style={{height:dim.height/100*6,backgroundColor:BG_1,opacity:0.5,marginTop:dim.height/100*2}}/>
                    <View style={{marginTop:-(dim.height/100*6),height:dim.height/100*6,alignItems:'center',justifyContent:'center'}}>
                        <Text style={[styles.text_btn_pay_bold,{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('BookingSummeryScreen.payment')}</Text>
                    </View>
                </View>}
                
                {this.state.total != 0?
                <View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BG_1,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentbyCredit()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.credit')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BG_1,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentbyQRcredit()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.qr_credit')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BG_1,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentbyQRdebit()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.qr_debit')}</Text>
                        </TouchableOpacity>
                        <View style={{margin:dim.width/100*1,flex:1,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} />
                    </View>
                </View>
                :
                <View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BG_1,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentFree()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.pay_now')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>}
            </Content>
        )
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View  style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        backgroundColor:'black',
                        opacity:0.7
                        }}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={{backgroundColor:'#fff',width:dim.width/100*50,height:dim.width/100*50,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5,justifyContent:'center'}}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                        </View>
                </Modal>
                <Image 
                    source={{uri:this.props.data.background_mobile_path==null?this.props.data.poster_mobile_path:this.props.data.background_mobile_path}}
                    style={{
                        position:'absolute',
                        width:dim.width/100*300,
                        height:dim.height/100*300,
                        alignSelf:'center',
                        marginTop:-(dim.height/100*100),
                        backgroundColor:'#000000'
                    }}
                    blurRadius={Platform.OS == 'ios'?7:4}
                />
                <Header
                    style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'transparent',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11,
                        opacity:0.3,
                        position:'absolute'
                    }}
                />
                {<Header1 leftbutton={true} transparent1={true} purchase_transaction_id={this.props.purchase_transaction_id}/>}
                <CountDown
                    until={420}
                    onFinish={() => this.onTimeout()}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: '', s: ''}}
                    separatorStyle={{color: BG_1, opacity:0.4}}
                    showSeparator
                    digitStyle={{backgroundColor: BG_1, opacity:0.4,marginVertical:dim.height/100*1}}
                    digitTxtStyle={{fontFamily:TEXT_HEADER_EN,color:BLACK}}
                    size={17}
                />
                {this._renderBooking()}
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        my_card:state.profile.my_card
    }
  }
  var styles = StyleSheet.create({
    text_btn_pay:{
        color:BG_1,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_btn_pay_bold:{
        color:BG_1,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    }
  });
  export default connect(mapStateToProps,actions)(BookingSummeryScreen)