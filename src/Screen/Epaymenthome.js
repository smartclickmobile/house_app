import React, { Component } from "react"
import {
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    View,
    ActivityIndicator,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, RED_COLOR } from '../constant/color'
import { Container, Content ,Icon} from 'native-base';
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH, TEXT_BODY_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3_7,SIZE_5_5,SIZE_4 } from '../constant/font'
const dim = Dimensions.get('window');
class Epaymenthome extends Component {
    constructor(props){
        super(props)
        this.state = {
           loading:false,
           error_num:null,
           modalError:false,
           textError:''
        }
    }
    componentDidMount(){
        var data1={
            language:I18n.locale
        }
        this.props.GetCreditCard(data1)
    }
    componentWillReceiveProps(nextProp){
        if(nextProp.credit_success == true){
            this.setState({loading:false})
        }
        console.log('testtest',nextProp.error,nextProp.error_num,this.state.error_num)
        if(nextProp.error != null && nextProp.error_num != this.state.error_num){
            this.setState({loading:false,error_num:nextProp.error_num,modalError:true,textError:nextProp.error})
        }
    }
    onPayFree(){
        var data = {
            language:I18n.locale,
            title:this.props.data.title,
            first_name:this.props.data.first_name,
            last_name:this.props.data.last_name,
            telephone:this.props.data.telephone,
            citizen_number:this.props.data.citizen_number,
            birth_date:this.props.data.birth_date,
            lineid:this.props.data.lineid,
            gender:this.props.data.gender,
            age:this.props.data.age,
            gift_id:this.props.data.gift_id,
            is_voucher:this.props.data.is_voucher,
            is_buffet:this.props.data.is_buffet,
            package_id:this.props.data.package_id,
            price:this.props.data.price,
            email:this.props.data.email,
            payment_id:1
        }
        this.props.upgradeEmember(data)
    }
    onPaybyCredit(){
        // console.log('my_card',this.props.my_card.length)
        if(this.props.my_card.length > 0){
            Actions.MycradEmemberscreen({data:this.props.data})
        }else{
            Actions.Epayment({data:this.props.data})
        }
    }
    onPaybyqrcredit(){
        this.setState({loading:true})
        var data = {
            language:I18n.locale,
            title:this.props.data.title,
            first_name:this.props.data.first_name,
            last_name:this.props.data.last_name,
            telephone:this.props.data.telephone,
            citizen_number:this.props.data.citizen_number,
            birth_date:this.props.data.birth_date,
            lineid:this.props.data.lineid,
            gender:this.props.data.gender,
            age:this.props.data.age,
            gift_id:this.props.data.gift_id,
            is_voucher:this.props.data.is_voucher,
            is_buffet:this.props.data.is_buffet,
            package_id:this.props.data.package_id,
            price:this.props.data.price,
            email:this.props.data.email,
            payment_id:4
        }
        this.props.upgradeEmember(data)
        
    }
    onPaybyqrdebit(){
        this.setState({loading:true})
        var data = {
            language:I18n.locale,
            title:this.props.data.title,
            first_name:this.props.data.first_name,
            last_name:this.props.data.last_name,
            telephone:this.props.data.telephone,
            citizen_number:this.props.data.citizen_number,
            birth_date:this.props.data.birth_date,
            lineid:this.props.data.lineid,
            gender:this.props.data.gender,
            age:this.props.data.age,
            gift_id:this.props.data.gift_id,
            is_voucher:this.props.data.is_voucher,
            is_buffet:this.props.data.is_buffet,
            package_id:this.props.data.package_id,
            price:this.props.data.price,
            email:this.props.data.email,
            payment_id:3
        }
        this.props.upgradeEmember(data)
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View  style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        backgroundColor:'black',
                        opacity:0.7
                        }}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={{backgroundColor:'#fff',width:dim.width/100*50,height:dim.width/100*50,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5,justifyContent:'center'}}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                        </View>
                </Modal>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={styles.text_modal}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={styles.text_modal_btn}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Header btnleft={true}/>
                <Content>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_5_5,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('Epayment.payment')}</Text>
                    {
                        this.props.data.price!=0
                        ?
                        <View>
                            <TouchableOpacity style={[styles.btn1,{alignSelf:'center',justifyContent:'center'}]} onPress={()=> this.onPaybyCredit()}>
                                <Text style={{marginHorizontal:dim.width/100*1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('BookingSummeryScreen.credit')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn1,{alignSelf:'center',justifyContent:'center'}]} onPress={()=> this.onPaybyqrdebit()}>
                                <Text style={{marginHorizontal:dim.width/100*1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('BookingSummeryScreen.qr_debit')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn1,{alignSelf:'center',justifyContent:'center'}]} onPress={()=> this.onPaybyqrcredit()}>
                                <Text style={{marginHorizontal:dim.width/100*1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('BookingSummeryScreen.qr_credit')}</Text>
                            </TouchableOpacity>
                        </View>
                    :
                    <TouchableOpacity style={[styles.btn1,{alignSelf:'center',justifyContent:'center'}]} onPress={()=> this.onPaybyCredit()}>
                        <Text style={{marginHorizontal:dim.width/100*1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('BookingSummeryScreen.credit')}</Text>
                    </TouchableOpacity>
                }
            </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        my_card:state.profile.my_card,
        credit_success:state.eMember.credit_success,
        credit_success_num:state.eMember.credit_success_num,
        error:state.eMember.error,
        error_num:state.eMember.error_num
    }
  }

export default connect(mapStateToProps,actions)(Epaymenthome)