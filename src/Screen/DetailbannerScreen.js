import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Modal,
    ScrollView,
    Platform,

    Alert
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1,BG_2, BLACK } from '../constant/color'
import {  Content, Container } from 'native-base';
import I18n from '../../asset/languages/i18n'
import Header from '../Component/header'
import MovieList from '../Component/movieList'

import {TEXT_BODY_EN,TEXT_BODY_EN_BOLD,TEXT_HEADER_EN,TEXT_HEADER_EN_BOLD,SIZE_3,SIZE_4,TEXT_TH,TEXT_TH_BOLD,SIZE_3_5,SIZE_7,SIZE_3_7,SIZE_4_5} from '../constant/font'
import { Actions } from "react-native-router-flux"
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
class DetailbannerScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            loading:false,
            data:null,
            token:false,
            buffet:null,
            buffet_random:null
        }
    }
    async componentDidMount(){
    var lower = this.props.data.detail_type.toLowerCase()
    if(lower == 'emember_privilege'){
        lower = 'member_privilege'
    }
     var data = {
        language:I18n.locale,
        event_id:this.props.data.detail_id,
        event_type_name:lower
     }
     console.log('data barner',data)
     this.props.getEventDetail(data)

     var token = await AsyncStorage.getItem('token');
     console.warn('token',token)
     this.setState({token:token})

    }

    componentWillReceiveProps(nextProps){
       if(nextProps.event_detail != null && nextProps.event_detail != undefined){
            this.setState({data:nextProps.event_detail},()=>{
                if(this.props.data.detail_type == 'MOVIE'){
                    Actions.pop()
                    Actions.BookingSelectTimeScreen({data:this.state.data,flaq_detail:true})
                }
                console.warn('buffet check', this.state.data.is_buffet,this.state.buffet_random,nextProps.buffet_random,this.state.data.is_buffet == true && this.state.buffet_random == null)
                if(this.state.data.is_buffet == true && this.state.buffet_random == null){
                    var data = {
                        language:I18n.locale,
                        token:this.state.token
                    }
                    this.props.getBuffet(data)
                }
            })
       }
       
       if(nextProps.buffet != null && nextProps.buffet != undefined){
           console.warn('set buffet_random')
            this.setState({
                buffet:nextProps.buffet,
                buffet_random:nextProps.buffet_random
            })
       }
    }
    isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    render(){
        var flag_buybuffet = false
        var btn_buy = false
        
        if(this.state.data != null){
            if(this.state.token != null && this.state.token != undefined && this.state.data.is_buffet == true){
                // console.warn('flag_buybuffet',!this.isEmpty(this.state.buffet), this.state.buffet != null , this.state.buffet != undefined,this.state.buffet,this.isEmpty(this.state.buffet))
                if(!this.isEmpty(this.state.buffet) && this.state.buffet != null && this.state.buffet != undefined){
                    flag_buybuffet = true
                }  
                btn_buy = false
            }else{
                if(this.state.data.is_buffet == true){
                    if(this.state.buffet != {} && this.state.buffet != null && this.state.buffet != undefined){
                        btn_buy = true
                    }  
                }
            }
            
        }
        
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View 
                        style={{ width: "100%",
                            height: "100%",
                            position: "absolute",
                            backgroundColor:'black',
                            opacity:0.7}}>
                    </View>
                    <View style={{width: "100%",height: "100%",alignItems:'center',justifyContent:'center'}}>
                        {/* <Bubbles size={20} color="#FFF" />     */}
                    </View>
                </Modal>
                <Header btnleft={true}/>
                <Content>
                   
                        {<Image 
                            source={{uri:this.state.data != null?this.state.data.thumbnail_mobile_path:''}}
                            style={{
                                width:dim.width,
                                height:dim.width,
                                // position:'absolute'
                            }}
                            resizeMode='cover'
                        />}

                    
                        <Text style={{fontSize:SIZE_4_5,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,textAlign:'center'}}>{this.state.data!=null?this.state.data.title:''}</Text>
                   
                   
                        <Text style={{minHeight:dim.height/100*15,fontSize:SIZE_3_5,color:BLACK,textAlign:'center',margin:dim.width/100*2.5,fontFamily:TEXT_TH_BOLD}}>{this.state.data!=null?this.state.data.short_description:''}</Text>
                        
                        <View>
                            {btn_buy &&<TouchableOpacity 
                                style={{
                                    flexDirection:'row',
                                    alignItems:'center',
                                    justifyContent:'center',
                                    width:Platform.isPad?dim.width/100*50:dim.width/100*65,
                                    // backgroundColor:"#ccd1d5",
                                    paddingVertical:dim.height/100*1.5,
                                    borderRadius:dim.width/100*1,
                                    borderColor:BG_2,
                                    borderWidth:1,
                                    marginTop:dim.height/100*2.5,
                                    height:dim.height/100*6,
                                    alignSelf:'center'
                                }} 
                                onPress={()=> {
                                    // Actions.pop()
                                    this.props.clear_buffet()
                                    Actions.popTo('loginhome',{fromdetail:true,data_detail:this.props.data})
                                    Actions.loginhome({fromdetail:true,data_detail:this.props.data})
                                }}
                            >
                                <Text style={{
                                    marginHorizontal:dim.width/100*2,
                                    fontSize:SIZE_4,
                                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN
                                }}
                                >{I18n.t('DetailbannerScreen.warn_login')}</Text>
                            </TouchableOpacity>}
                            {flag_buybuffet &&<TouchableOpacity 
                                style={{
                                    flexDirection:'row',
                                    alignItems:'center',
                                    justifyContent:'center',
                                    width:Platform.isPad?dim.width/100*50:dim.width/100*65,
                                    backgroundColor:"#e7b945",
                                    paddingVertical:dim.height/100*1.5,
                                    borderRadius:dim.width/100*1,
                                    borderColor:BG_2,
                                    borderWidth:1,
                                    marginTop:dim.height/100*2.5,
                                    height:dim.height/100*6,
                                    alignSelf:'center'
                                }} 
                                onPress={()=> {
                                    console.warn('is_canbuy',this.state.buffet)
                                    if(this.state.buffet != null){
                                        if(this.state.buffet.is_canbuy == true){
                                            Actions.buffet_home({data:this.state.buffet})
                                        }else{
                                            Alert.alert(I18n.t('DetailbannerScreen.error_buyed_h'),I18n.t('DetailbannerScreen.error_buyed_d'))
                                        }
                                    }
                                    // Actions.loginhome({fromdetail:true,data_detail:this.props.data})
                                }}
                            >
                                <Text style={{
                                    marginHorizontal:dim.width/100*2,
                                    fontSize:SIZE_4,
                                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN
                                }}
                                >{I18n.t('DetailbannerScreen.buy_ticket')}</Text>
                            </TouchableOpacity>}
                        </View>

                    
                    {
                        this.state.data != null && this.state.data.movies != undefined &&<Text style={[styles.text_btn_a,{fontSize:SIZE_7,marginTop:dim.height/100*1,flex:1}]}>{I18n.t('DetailbannerScreen.movie')}</Text>
                    }
                   
                    
                   
                    {
                        this.state.data != null && <MovieList data={this.state.data.movies}/>
                    }
                   
                   
                    
                </Content>
                
            </Container>
        
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,  
        event_detail:state.event.event_detail,
        buffet:state.buffet.buffet,
        buffet_random:state.buffet.buffet_success_num
    }
  }
  var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:TEXT_BODY_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:TEXT_BODY_EN_BOLD
    }
  });
  

export default connect(mapStateToProps,actions)(DetailbannerScreen)