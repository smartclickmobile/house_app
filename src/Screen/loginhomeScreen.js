import React, {Component} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  BackHandler,
  Alert,
  Platform,
  Linking,
} from 'react-native';
import * as actions from '../Actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {BG_1, BG_2, BLACK} from '../constant/color';
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Container} from 'native-base';
import {Icon} from 'react-native-elements';
import I18n from '../../asset/languages/i18n';
import {
  TEXT_BODY_EN,
  TEXT_TH,
  TEXT_TH_BOLD,
  TEXT_BODY_EN_BOLD,
  TEXT_HEADER_EN,
  TEXT_HEADER_EN_BOLD,
  SIZE_3,
  SIZE_2_7,
  SIZE_2_5,
  SIZE_4,
} from '../constant/font';
import {GoogleSignin, statusCodes} from 'react-native-google-signin';
import {LoginButton, AccessToken, LoginManager} from 'react-native-fbsdk';
import DeviceInfo from 'react-native-device-info';
import {appleAuth} from '@invertase/react-native-apple-authentication';
import axios from 'axios';
import {API_IP, CONFIG} from '../constant/constant';

const dim = Dimensions.get('window');
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
    };
  }
  componentDidMount() {
    this.checkPermission();
    this.props.resetProfile(0);
    this.props.setindexmenu(1);
    this.props.setindexfooter(1);
    BackHandler.addEventListener('back', () => this._handleBack());
  }
  _handleBack() {
    if (
      Actions.currentScene == '_loginhome' ||
      Actions.currentScene == 'loginhome'
    ) {
      BackHandler.exitApp();

      return true;
    }
    return false;
  }
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }
  getToken() {
    firebase
      .messaging()
      .getToken()
      .then((fcmToken) => {
        if (fcmToken) {
          // user has a device token
          this.setState({fcmToken: fcmToken}, () => {
            AsyncStorage.setItem('fcmToken', fcmToken, () => {
              console.log('fcmToken is ' + fcmToken);
            });
          });
        } else {
          console.log("user doesn't have a device token yet");
        }
      });
  }
  testngo() {
    console.log('testngo');
  }
  async onLoginWithFacebook() {
    // LoginManager.logOut()
    if (Platform.OS == 'android') {
      LoginManager.logOut();

      LoginManager.logInWithPermissions(['public_profile']).then(
        function (result) {
          if (result.isCancelled) {
            console.log('Login Cancelled');
          } else {
            console.log(
              'Login Success permission granted:' + result.grantedPermissions,
            );
            console.log('result = null', result);
            AccessToken.getCurrentAccessToken().then(async (data) => {
              const {accessToken} = data;
              console.log('accessToken', accessToken);
              var token_fcm = await AsyncStorage.getItem('fcmToken');

              fetch(
                'https://graph.facebook.com/v2.5/me?fields=email,id&access_token=' +
                  accessToken,
              )
                .then((response) => response.json())
                .then((json) => {
                  var data = {
                    email: json.email,
                    token_facebook: accessToken,
                    device_id: DeviceInfo.getDeviceId(),
                    device_name: DeviceInfo.getDeviceName(),
                    language: I18n.locale,
                    type: 1,
                    token_firebase: token_fcm,
                    facebook_id: json.id,
                  };
                  console.log('initUser', data);
                  var data1 = new FormData();
                  data1.append('email', data.email);
                  data1.append('device_id', data.device_id);
                  data1.append('device_name', data.device_name);
                  data1.append('platform_id', 2);
                  data1.append('language', data.language);
                  data1.append('token_firebase', data.token_firebase);

                  var type = '';
                  if (data.type == 1) {
                    data1.append(
                      'telephone',
                      data.telephone != undefined ? data.telephone : '',
                    );
                    data1.append('token_facebook', data.token_facebook);
                    data1.append('member_type_id', 1);
                    data1.append('facebook_id', data.facebook_id);
                    type = 'fb';
                  } else if (data.type == 2) {
                    data1.append(
                      'telephone',
                      data.telephone != undefined ? data.telephone : '',
                    );
                    data1.append('token_gmail', data.token_gmail);
                    data1.append('member_type_id', 1);
                    type = 'gm';
                  } else if (data.type == 3) {
                    data1.append('password', data.password);
                    data1.append('member_type_id', 1);
                    type = 'r';
                  }
                  console.log('login new', data1);
                  axios
                    .post(API_IP + 'member/login', data1, CONFIG)
                    .then(function (response) {
                      console.log('member/login', response.data.code);
                      if (response.data.code == '0x0000-00000') {
                        AsyncStorage.setItem('token', response.data.data);
                        AsyncStorage.setItem('login_type', type);
                        if (data.type == 1) {
                          AsyncStorage.setItem('token_fb', data.token_facebook);
                        } else if (data.type == 2) {
                          AsyncStorage.setItem('token_gm', data.token_gmail);
                        }
                        try {
                          if (this.props.fromdetail) {
                            console.warn('in if');
                            Actions.home({
                              login_flag: true,
                              fromdetail: this.props.fromdetail,
                              data_detail: this.props.data_detail,
                            });
                          } else {
                            console.warn('else');
                            if (data.login) {
                              Actions.popTo('loginhome', {login_flag: true});
                              Actions.home({login_flag: true});
                            } else {
                              Actions.home({login_flag: true});
                            }
                          }
                        } catch (error) {
                          if (data.login) {
                            Actions.popTo('loginhome', {login_flag: true});
                            Actions.home({login_flag: true});
                          } else {
                            Actions.home({login_flag: true});
                          }
                        }
                      } else if (response.data.code == '0x0API-00004') {
                        if (data.type == 1) {
                          var val = {
                            email: data.email,
                            token_facebook: data.token_facebook,
                          };
                        } else if (data.type == 2) {
                          var val = {
                            email: data.email,
                            token_gmail: data.token_gmail,
                          };
                        }
                        Actions.confirmmobile({
                          data: val,
                          login_t: type,
                          facebook_id: data.facebook_id,
                        });
                      } else {
                        // GoogleSignin.revokeAccess();
                        GoogleSignin.signOut();
                        LoginManager.logOut();
                        Alert.alert('ERROR', response.data.message);
                      }
                    })
                    .catch(function (error) {
                      console.log('login error', error);
                      Alert.alert('', I18n.t('networkerror'));
                    });
                })
                .catch(() => {
                  console.log('reject');
                });
            });
          }
        },
        function (error) {
          console.log('some error occurred!!');
        },
      );
    } else {
      let result;
      try {
        LoginManager.setLoginBehavior('native');
        result = await LoginManager.logInWithPermissions([
          'public_profile',
          'email',
        ]);
      } catch (nativeError) {
        try {
          LoginManager.setLoginBehavior('web');
          result = await LoginManager.logInWithPermissions([
            'public_profile',
            'email',
          ]);
        } catch (webError) {
          // show error message to the user if none of the FB screens
          // did not open
        }
      }
      // handle the case that users clicks cancel button in Login view
      if (result != null && result.isCancelled) {
      } else {
        // Create a graph request asking for user information
        AccessToken.getCurrentAccessToken().then((data) => {
          const {accessToken} = data;
          console.log('accessToken', accessToken);
          this.initUser(accessToken);
        });
      }
    }
  }
  async onLoginWithGoogle() {
    var token_fcm = await AsyncStorage.getItem('fcmToken');
    GoogleSignin.configure({
      scopes: ['profile', 'email'],
      webClientId:
        '742559247810-o4h2e6m43abbkcf9e36mqftosqnmp84p.apps.googleusercontent.com',
      iosClientId:
        '742559247810-0div9gkj5k0cjd7n8olugtcn9jogjeet.apps.googleusercontent.com',
    });

    try {
      // await GoogleSignin.hasPlayServices({
      //   showPlayServicesUpdateDialog: true,
      // });
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });

      const issignin = await GoogleSignin.isSignedIn();
      console.log('issignin', issignin);
      if (issignin == true) {
        const currentUser = await GoogleSignin.getCurrentUser();
        this.setState({userInfo: currentUser}, async () => {
          if (currentUser == null) {
            const currentUser1 = await GoogleSignin.signIn();
            var data = {
              email: currentUser1.user.email,
              token_gmail: currentUser1.idToken,
              device_id: DeviceInfo.getDeviceId(),
              device_name: DeviceInfo.getDeviceName(),
              language: I18n.locale,
              type: 2,
              token_firebase: token_fcm,
              fromdetail: this.props.fromdetail,
              data_detail: this.props.data_detail,
            };
            this.props.login(data);
            console.log('userInfo is sign', data);
          } else {
            var data = {
              email: currentUser.user.email,
              token_gmail: currentUser.idToken,
              device_id: DeviceInfo.getDeviceId(),
              device_name: DeviceInfo.getDeviceName(),
              language: I18n.locale,
              type: 2,
              token_firebase: token_fcm,
              fromdetail: this.props.fromdetail,
              data_detail: this.props.data_detail,
            };
            this.props.login(data);
            console.log('userInfo is unsign', data);
          }
        });
      } else {
        const userInfo1 = await GoogleSignin.signIn();
        this.setState({userInfo: userInfo1}, () => {
          var data = {
            email: userInfo1.user.email,
            token_gmail: userInfo1.idToken,
            device_id: DeviceInfo.getDeviceId(),
            device_name: DeviceInfo.getDeviceName(),
            language: I18n.locale,
            type: 2,
            token_firebase: token_fcm,
            fromdetail: this.props.fromdetail,
            data_detail: this.props.data_detail,
          };
          this.props.login(data);
        });
      }
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  }
  async onLoginWithApple() {
    var token_fcm = await AsyncStorage.getItem('fcmToken');
    try {
      if (appleAuth.isSupported) {
        const appleAuthRequestResponse = await appleAuth.performRequest({
          requestedOperation: appleAuth.Operation.LOGIN,
          requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
        });

        var data = {
          email: appleAuthRequestResponse.email,
          token_appleid: appleAuthRequestResponse.identityToken,
          device_id: DeviceInfo.getDeviceId(),
          device_name: DeviceInfo.getDeviceName(),
          language: I18n.locale,
          type: 4,
          token_firebase: token_fcm,
          fromdetail: this.props.fromdetail,
          data_detail: this.props.data_detail,
        };
        if (appleAuthRequestResponse.email !== null) {
          AsyncStorage.setItem(
            'apple_id_email',
            appleAuthRequestResponse.email,
          );
        } else {
          var Apple_email = await AsyncStorage.getItem('apple_id_email');
          if (Apple_email) {
            data.email = Apple_email;
          } else {
            Alert.alert('', I18n.t('loginhome.warn_apple'));
            return;
          }
        }
        this.props.login(data);
        console.log('userInfo is sign', data);
      }
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  }
  async onSkip() {
    Actions.home();
  }
  async initUser(token) {
    var token_fcm = await AsyncStorage.getItem('fcmToken');

    fetch(
      'https://graph.facebook.com/v2.5/me?fields=email,id&access_token=' +
        token,
    )
      .then((response) => response.json())
      .then((json) => {
        var data = {
          email: json.email,
          token_facebook: token,
          device_id: DeviceInfo.getDeviceId(),
          device_name: DeviceInfo.getDeviceName(),
          language: I18n.locale,
          type: 1,
          token_firebase: token_fcm,
          facebook_id: json.id,
          fromdetail: this.props.fromdetail,
          data_detail: this.props.data_detail,
        };
        console.log('initUser', json);
        this.props.login(data);
      })
      .catch(() => {
        console.log('reject');
      });
  }
  // render(){
  //   return(
  //     <View style={{backgroundColor:'red',fle}}></View>
  //   )
  // }
  render() {
    return (
      <Container>
        <LinearGradient
          colors={[BG_1, BG_1, BG_2]}
          style={[styles.linearGradient]}>
          <View
            style={{
              marginTop: (dim.height / 100) * 5,
              padding: (dim.width / 100) * 5,
            }}>
            <TouchableOpacity onPress={() => this.onSkip()}>
              <Text
                style={{
                  textAlign: 'right',
                  fontSize: I18n.locale == 'th' ? SIZE_4 : SIZE_3,
                  fontFamily:
                    I18n.locale == 'th' ? TEXT_TH_BOLD : TEXT_HEADER_EN_BOLD,
                }}>
                {I18n.t('loginhome.skip')}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end'}}>
            <Image
              source={require('../../asset/Images/logo.png')}
              style={{
                width: Platform.isPad
                  ? (dim.width / 100) * 40
                  : (dim.width / 100) * 60,
                height: (dim.height / 100) * 10,
              }}
              resizeMode="contain"
            />
          </View>

          <View
            style={{
              flex: 3,
              alignItems: 'center',
              paddingTop: (dim.height / 100) * 10,
            }}>
            <TouchableOpacity
              style={[styles.btn]}
              onPress={() => this.onLoginWithGoogle()}>
              <View style={{flex: 1.5, alignItems: 'flex-end'}}>
                <Image
                  source={require('../../asset/Images/search.png')}
                  style={{
                    width: (dim.width / 100) * 5,
                    height: (dim.width / 100) * 5,
                  }}
                />
              </View>
              <Text style={styles.text1}>
                {I18n.t('loginhome.login_google')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.btn]}
              onPress={() => this.onLoginWithFacebook()}>
              <View
                style={{
                  flex: 1.5,
                  alignItems: 'flex-end',
                }}>
                <Image
                  source={require('../../asset/Images/facebook.png')}
                  style={{
                    width: (dim.width / 100) * 5,
                    height: (dim.width / 100) * 5,
                  }}
                />
              </View>
              <Text style={styles.text1}>
                {I18n.t('loginhome.login_facebook')}
              </Text>
            </TouchableOpacity>
            {Platform.OS === 'ios' && (
              <TouchableOpacity
                style={[styles.btn, {backgroundColor: 'black'}]}
                onPress={() => {
                  this.onLoginWithApple();
                }}>
                <View
                  style={{
                    flex: 1.5,
                    alignItems: 'flex-end',
                  }}>
                  <Icon
                    name="apple"
                    type="font-awesome"
                    color={'white'}
                    containerStyle={{
                      width: (dim.width / 100) * 5,
                      height: (dim.width / 100) * 5,
                      alignItems:'center',
                      justifyContent:'center'
                    }}
                    size={Platform.isPad?(dim.width / 100) * 3:(dim.width / 100) * 5}
                  />
                </View>

                <Text style={[styles.text2, {color: 'white'}]}>
                  {I18n.t('loginhome.login_apple')}
                </Text>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              style={[styles.btn]}
              onPress={() =>
                Actions.login({
                  fromdetail: this.props.fromdetail,
                  data_detail: this.props.data_detail,
                })
              }>
              <Text
                style={[
                  styles.text,
                  {
                    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_HEADER_EN,
                    fontSize: I18n.locale == 'th' ? SIZE_4 : SIZE_2_5,
                  },
                ]}>
                {I18n.t('loginhome.login')}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.btn,
                {
                  marginTop: (dim.height / 100) * 3.5,
                  backgroundColor: '#565455',
                },
              ]}
              onPress={() => Actions.register()}>
              <Text
                style={[
                  styles.text,
                  {
                    color: '#ffffff',
                    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_HEADER_EN,
                    fontSize: I18n.locale == 'th' ? SIZE_4 : SIZE_2_5,
                  },
                ]}>
                {I18n.t('loginhome.register')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.btn,{borderWidth:0}]}
              onPress={() =>{
                let url = 'https://www.housesamyan.com/site/Contact/privacy_policy'
                Linking.canOpenURL(url)
                .then((supported) => {
                  if (!supported) {
                    console.log("Can't handle url: " + url);
                  } else {
                    return Linking.openURL(url);
                  }
                })
                .catch((err) => console.error('An error occurred', err));
              }}>
              <Text
                style={[
                  styles.text,
                  {
                    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_HEADER_EN,
                    fontSize: I18n.locale == 'th' ? SIZE_4 : SIZE_2_5,
                  },
                ]}>
                {I18n.t('loginhome.privacy_policy')}
              </Text>
            </TouchableOpacity>
          </View>
          {/* <View
            style={{
              flex: 3,
              alignItems: 'center',
              paddingTop: (dim.height / 100) * 10,
            }}>
           
            </View> */}
        </LinearGradient>
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    checkIpnoe: state.check.checkIpnoe,
  };
};
var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: Platform.isPad ? (dim.width / 100) * 50 : (dim.width / 100) * 65,
    // backgroundColor:"#ccd1d5",
    paddingVertical: (dim.height / 100) * 1.5,
    borderRadius: (dim.width / 100) * 1,
    borderColor: BG_2,
    borderWidth: 1,
    marginTop: (dim.height / 100) * 2.5,
    height: (dim.height / 100) * 6,
  },
  text: {
    marginHorizontal: (dim.width / 100) * 2,
    fontSize: I18n.locale == 'th' ? SIZE_4 : SIZE_2_5,
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_HEADER_EN,
  },
  text1: {
    flex: 8,
    textAlign: 'left',
    marginLeft: (dim.width / 100) * 7,
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_HEADER_EN,
    fontSize: SIZE_2_5,
  },
  text2: {
    flex: 8,
    textAlign: 'left',
    marginLeft: (dim.width / 100) * 7,
    fontSize: Platform.isPad?dim.width/360*12:dim.width/360*14,
  },
});
export default connect(mapStateToProps, actions)(Login);
