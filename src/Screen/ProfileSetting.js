import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content,Container } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3,SIZE_3_7,SIZE_2_9,SIZE_6, SIZE_3_5 } from '../constant/font'
import Header from '../Component/header'
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
class ProfileSetting extends Component {
    constructor(props){
        super(props)
        this.state = {
            data:this.props.profile_info,
            language:I18n.locale,
            notification:true
            
        }
    }
    async componentDidMount(){
        const noti = await AsyncStorage.getItem('noti')
        if(noti!= undefined && noti != null && noti != ''){
          if(noti == '0'){
            this.setState({notification:false})
          }else{
            this.setState({notification:true})
          }
        }else{
            this.setState({notification:true})
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.change_number_num != this.state.changeNumber_num){
            this.setState({changeNumber_num:nextProps.change_number_num},()=>{
                if(nextProps.change_number == true){
                    this.setState({modalSuccess:true,textSuccess:I18n.t('tab.profile.alert.change_number_Success')})
                }
            })
        }
    }
    async onChangeLanguage(language){
        if(language == 'th'){
            I18n.locale = 'th'
            this.setState({language:I18n.locale})
            await AsyncStorage.setItem('language',I18n.locale)
        }else if(language == 'en'){
            I18n.locale = 'en'
            this.setState({language:I18n.locale})
            await AsyncStorage.setItem('language',I18n.locale)
        }
      }
    async onchangeNotification(noti){
        if(noti == true){
            this.setState({notification:noti})
            firebase.messaging().subscribeToTopic('All')
            await AsyncStorage.setItem('noti','1')
        }else if(noti == false){
            this.setState({notification:noti})
            firebase.messaging().unsubscribeFromTopic('All')
            await AsyncStorage.setItem('noti','0')
        }
      }
    render(){
        return(
            <Container>
                <Header btnleft={true}/>
                <Content style={{marginTop:0,padding:dim.width/100*2}}>
                    <Text style={
                        [
                            {
                                textAlign:'center',
                                fontSize:SIZE_3_7,
                                color:BLACK,
                                fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
                            },
                            {fontSize:SIZE_6,marginTop:dim.height/100*1.5}
                        ]}>{I18n.t('tab.profile.setting')}</Text>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                        <View style={[styles.Container_row,{flex:0,width:dim.width/100*40,padding:dim.width/100*3}]}>
                            <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,color:BLACK}}>{I18n.t('tab.profile.language')}</Text>
                        </View>
                        <TouchableOpacity style={{flex:1,padding:dim.width/100*3}} onPress={()=> this.onChangeLanguage('th')}>
                            <Text style={this.state.language=='th'? 
                                {
                                    fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                    color:BLACK,
                                    fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
                                }
                                :
                                {
                                    fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                    color:BLACK,
                                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
                                }
                            }>{I18n.t('tab.profile.thai')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1,padding:dim.width/100*3}} onPress={()=> this.onChangeLanguage('en')}>
                            <Text style={this.state.language=='en'? 
                            {
                                fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                color:BLACK,
                                fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
                            }
                            :
                            {
                                fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                color:BLACK,
                                fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
                            }
                            }>{I18n.t('tab.profile.ENG')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                        <View style={[styles.Container_row,{flex:0,width:dim.width/100*40,padding:dim.width/100*3}]}>
                            <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,color:BLACK}}>{I18n.t('tab.profile.notification')}</Text>
                        </View>
                        <TouchableOpacity style={{flex:1,padding:dim.width/100*3}} onPress={()=> this.onchangeNotification(true)}>
                            <Text style={this.state.notification==true? 
                                {
                                    fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                    color:BLACK,
                                    fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
                                }
                                :
                                {
                                    fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                    color:BLACK,
                                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
                                }
                            }>{I18n.t('tab.profile.on')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1,padding:dim.width/100*3}} onPress={()=> this.onchangeNotification(false)}>
                            <Text style={this.state.notification==false? 
                            {
                                fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                color:BLACK,
                                fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
                            }
                            :
                            {
                                fontSize:I18n.locale=='th'?SIZE_3_5:SIZE_2_9,
                                color:BLACK,
                                fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
                            }
                            }>{I18n.t('tab.profile.off')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity style={[styles.btn1]} 
                        onPress={()=> 
                            {
                                Actions.popTo('home',{re:true})
                                setTimeout(() => Actions.refresh({ re:true , re_num:Math.floor((Math.random() * 1000) + 1)}),1000);
                            }
                        }>
                            <Text style={{marginHorizontal:dim.width/100*1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.save_btn')}</Text>
                        </TouchableOpacity>
                    </View>
                    
                    
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    txt_d_select:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    txt_h:{
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN,
        fontSize:SIZE_3,
        color:BLACK
    },
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
    }
  }

export default connect(mapStateToProps,actions)(ProfileSetting)