import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Modal,

    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK,BG_2,RED_COLOR } from '../constant/color'
import { Container, Content, Icon,Form,Item,Input,Label,Picker,ListItem,Left,Right,Radio } from 'native-base';
import {CheckBox} from 'react-native-elements'
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
// import DateTimePicker from "react-native-modal-datetime-picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment'
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN,TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3,SIZE_3_5,SIZE_4,SIZE_3_7,SIZE_2_9,SIZE_4_5,SIZE_5_5 } from '../constant/font'
import DeviceInfo from "react-native-device-info"
import { URL_IP } from '../constant/constant'
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
import { WebView } from 'react-native-webview';
class Eregister extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalError:false,
            textError:'',
            titleList:[
                {
                    value:1,
                    name:I18n.t('Eregister.title.mr'),
                    key:'mr'
                },
                {
                    value:2,
                    name:I18n.t('Eregister.title.miss'),
                    key:'ms'
                },
                {
                    value:3,
                    name:I18n.t('Eregister.title.ms'),
                    key:'mrs'
                },
            ],
            MerchandiseList:[],
            GenderList:[
                {
                    value:1,
                    name:I18n.t('Eregister.genderlist.male'),
                    key:'male'
                },
                {
                    value:2,
                    name:I18n.t('Eregister.genderlist.female'),
                    key:'female'
                },
                {
                    value:3,
                    name:I18n.t('Eregister.genderlist.other'),
                    key:'other'
                },
            ],
            selected_Merchandise:0,
            Merchandise:'',
            selected:0,
            selected_name:'',
            selected_gender:0,
            gender:'',

            first_name:'',
            last_name:'',
            mobile:this.props.profile_info.telephone,
            line:'',
            citizen_id:'',
            age:'0',
            Birthday:'',

            checked:false,
            isDateTimePickerVisible:false,
            packet:[],
            price:0,
            is_voucher:true,
            is_buffet:false,
            modalAgree:false,

            title_key:'',
            gender_key:'',

            voucher_flag:false,
            buffet_flag:false
        }
    }
    componentDidMount(){
        var data = {
            language: I18n.locale
        }
        this.props.getGift(data)
        this.props.getPacket(data)
        if(Platform.OS == 'android'){
            this.setState({
                selected_name:this.state.titleList[0].name,
                gender:this.state.GenderList[0].name,
                selected:this.state.titleList[0].value,
                selected_gender:this.state.GenderList[0].value,
                title_key:this.state.titleList[0].key,
                gender_key:this.state.GenderList[0].key
            })
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.gitfList != null && nextProps.gitfList != undefined){
            this.setState({MerchandiseList:nextProps.gitfList})
        }
        if(nextProps.packetList != null && nextProps.packetList != undefined){
            this.setState({packet:nextProps.packetList,price:nextProps.packetList.price},()=>{
            //    console.log('packet flag',this.state.packet.voucher_flag, this.state.packet.buffet_flag)
               this.setState({voucher_flag:this.state.packet.voucher_flag,buffet_flag:this.state.packet.buffet_flag},()=>{
                   if(this.state.voucher_flag == true && this.state.buffet_flag == true){

                   }else{
                       this.setState({is_voucher:this.state.voucher_flag,is_buffet:this.state.buffet_flag})
                   }
               })
            })
        }
    }
    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
      };
    
      hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
      };
    
      handleDatePicked = date => {
        
        var date_now = moment()
        var date_select = moment(date)
        var date_now1 = moment(moment().format('YYYY-MM-DD'))
        var date_select1 = moment(moment(date).format('YYYY-MM-DD'))
        console.log("A date has been picked: ", date_now1<date_select1);
        if(date_now1<date_select1){
            
            this.hideDateTimePicker();
            setTimeout(() => this.setState({modalError:true,textError:I18n.t('Eregister.error.birthday')}),500)
            
        }else{
            var age = date_now.diff(date_select,'years')
            console.log('age',age)
            this.setState({Birthday:moment(date).format('DD/MM/YYYY'),age:age.toString()})
            this.hideDateTimePicker();
        }
        
        
      };
    onValueChangetitle(value: string) {
        this.setState({
          selected: value
        },()=>{
            for(var i = 0 ; i<this.state.titleList.length ; i++){
                if(this.state.selected == this.state.titleList[i].value){
                    this.setState({selected_name:this.state.titleList[i].name,title_key:this.state.titleList[i].key})
                }
            }
        });
    }
    onValueChangegender(value: string) {
        this.setState({
            selected_gender: value
        },()=>{
            for(var i = 0 ; i<this.state.GenderList.length ; i++){
                if(this.state.selected_gender == this.state.GenderList[i].value){
                    this.setState({gender:this.state.GenderList[i].name,gender_key:this.state.GenderList[i].key})
                }
            }
        });
    }
    onValueChangeMerchandise(value: string) {
        this.setState({
            selected_Merchandise: value
        },()=>{
            for(var i = 0 ; i<this.state.MerchandiseList.length ; i++){
                if(this.state.selected_Merchandise == this.state.MerchandiseList[i].gift_id){
                    this.setState({Merchandise:this.state.MerchandiseList[i].name})
                }
            }
        });
    }
    onAgree(){
        // this.setState({checked:!this.state.checked})
        if(this.state.checked){
            this.setState({checked:!this.state.checked})
        }else{
            this.setState({modalAgree:true})
        }
        
    }
    async onsubmit(){
        if(this.state.selected_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.title')})
        }else if(this.state.first_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.name')})
        }else if(this.state.last_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.surname')})
        }else if(this.state.Birthday == '' && this.state.age == 0){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.birthday')})
        }else if(this.state.mobile == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.mobile')})
        }else if(this.state.citizen_id == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.citizen')})
        }else if(this.state.gender == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.gender')})
        }else if(this.state.age == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.age')})
        }else if(this.state.Merchandise == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.merchadise')})
        }else if(!this.state.checked){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.check')})
        }
        else{
            var data = {
                language:I18n.locale,
                title:this.state.title_key,
                first_name:this.state.first_name,
                last_name:this.state.last_name,
                telephone:this.state.mobile,
                citizen_number:this.state.citizen_id,
                birth_date:this.state.Birthday,
                lineid:this.state.line,
                gender:this.state.gender_key,
                age:this.state.age,
                gift_id:this.state.selected_Merchandise,
                is_voucher:this.state.is_voucher,
                is_buffet:this.state.is_buffet,
                package_id:this.state.packet.member_package_id,
                price:this.state.price,
                email:this.props.profile_info.email,
            }
            console.log('data regis emember',data)
            Actions.Epaymenthome({data: data})
            
        }
        
    }
    render(){
        console.log('packet',this.state.packet)
        return(
            <Container>
                <DateTimePickerModal
                    isVisible={this.state.isDateTimePickerVisible}
                    mode="date"
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                />
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={[styles.text_modal,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={[styles.text_modal_btn,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal visible={this.state.modalAgree} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*90,height:dim.height/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <TouchableOpacity style={{alignSelf:'flex-end'}}
                                onPress={()=>{
                                    this.setState({modalAgree:false})
                                }}
                            >
                                <Icon name='times'
                                    type='FontAwesome5'
                                    style={{color:BLACK,fontSize:dim.width/100*10,textAlign:'right'}}
                                />
                            </TouchableOpacity>
                            {/*<Text style={[styles.text_modal_bold,{fontSize:SIZE_4_5}]}>{I18n.t('register.term_of_service')}</Text>*/}
                            <WebView
                                style={{width:dim.width/100*80,height:dim.height/100*60,backgroundColor:'transparent',marginTop:dim.height/100*1}}
                                originWhitelist={['*']}
                                source={{ uri: URL_IP+'/term/member' }}
                            />
                            
                        </View>
                    </View>
                </Modal>
                <Header btnleft={true}/>
                <Content>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_5_5,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.register')}</Text>
                    <Text style={[styles.text_btn,{marginTop:dim.height/100*2,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`${I18n.t('Eregister.email')} : ${this.props.profile_info.email}`}</Text>
                    <View style={{alignItems:'center'}}>
                        <Form style={{width:dim.width/100*80,marginTop:dim.height/100*3}}>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.select_tile')}<Text style={{color:'red'}}> *</Text></Label>
                            <Picker
                                placeholder={I18n.t('Eregister.select_tile')}
                                mode="dropdown"
                                iosHeader={I18n.t('Eregister.select_tile')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}

                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChangetitle.bind(this)}
                                >
                                {
                                    this.state.titleList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                    })
                                }
                            </Picker>
                            
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.name')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.first_name = input; }}
                                    onChangeText={(value)=> this.setState({first_name:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.surname')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.last_name = input; }}
                                    onChangeText={(value)=> this.setState({last_name:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <TouchableOpacity onPress={()=> this.showDateTimePicker()} style={{borderColor:BG_2,borderWidth:1,flexDirection:'row',marginLeft:dim.width/100*3,padding:dim.height/100*1.2,borderRadius:dim.width/100*1,marginTop:dim.height/100*3}}>
                                <Text style={{flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{this.state.Birthday==''?I18n.t('Eregister.birthday'):this.state.Birthday}<Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:'red'}}>{this.state.Birthday==''?' *':''}</Text></Text>
                                <Icon name='calendar' type='FontAwesome5' style={{color:BG_2,fontSize:SIZE_4}}/>
                            </TouchableOpacity>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.mobile')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    disabled={true}
                                    value={this.state.mobile}
                                    style={styles.input_dis}
                                    ref={(input) => { this.mobile = input; }}
                                    onChangeText={(value)=> this.setState({mobile:value})}
                                    keyboardType='name-phone-pad'
                                    maxLength={10}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel 
                                // style={{marginTop:-(dim.height/100*1)}}
                            >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.citizen')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.citizen_id = input; }}
                                    onChangeText={(value)=> this.setState({citizen_id:value})}
                                    keyboardType='numeric'
                                    maxLength={13}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.Line')}</Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.line = input; }}
                                    onChangeText={(value)=> this.setState({line:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.gender')}<Text style={{color:'red'}}> *</Text></Label>
                            <Picker
                                placeholder={I18n.t('Eregister.gender')}
                                mode="dropdown"
                                iosHeader={I18n.t('Eregister.gender')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected_gender}
                                onValueChange={this.onValueChangegender.bind(this)}
                                >
                                {
                                    this.state.GenderList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                    })
                                }
                            </Picker>
                            {/*<Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel 
                            // style={{marginTop:-(dim.height/100*1)}}
                            >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.age')}</Label>
                                <Input
                                    disabled={true}
                                    value={this.state.age}
                                    style={styles.input_dis}
                                    ref={(input) => { this.age = input; }}
                                    onChangeText={(value)=> this.setState({age:value})}
                                    autoCapitalize='none'
                                />
                            </Item>*/}
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.merchadise')}<Text style={{color:'red'}}> *</Text></Label>
                            <Picker
                                placeholder={I18n.t('Eregister.merchadise')}
                                mode="dropdown"
                                iosHeader={I18n.t('Eregister.merchadise')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected_Merchandise}
                                onValueChange={this.onValueChangeMerchandise.bind(this)}
                                >
                                {
                                    this.state.MerchandiseList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.gift_id} />
                                        )
                                    })
                                }
                            </Picker>
                            {this.state.voucher_flag==true&&<ListItem onPress={()=> this.setState({is_buffet:false,is_voucher:true})}>
                                <Left>
                                    <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.is_voucher')}</Text>
                                </Left>
                                <Right style={{alignItems:'center'}}>
                                    <Radio onPress={()=>this.setState({is_buffet:false,is_voucher:true})}  selected={this.state.is_voucher} />
                                </Right>
                            </ListItem>}
                            {this.state.buffet_flag==true&&<ListItem onPress={()=> this.setState({is_buffet:true,is_voucher:false})}>
                                <Left>
                                    <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.is_buffet')}</Text>
                                </Left>
                                <Right style={{alignItems:'center'}}>
                                    <Radio onPress={()=>this.setState({is_buffet:true,is_voucher:false})} selected={this.state.is_buffet} />
                                </Right>
                            </ListItem>}
                        </Form>
                        <View style={{flexDirection:'row',marginTop:dim.height/100*3,alignItems:'center',width:dim.width/100*80,justifyContent:'center'}}>
                            <CheckBox
                                ref={(input) => { this.checkb = input; }}
                                // title={I18n.t('register.agree')}
                                checked={this.state.checked}
                                containerStyle={{borderWidth:0,backgroundColor:'transparent'}}
                                textStyle={[styles.label,{fontSize:SIZE_3_5}]}
                                onPress={()=> {
                                    
                                    this.setState({checked:!this.state.checked})
                                }}
                                checkedColor={BLACK}
                            />
                            <View style={{flex:0}}>
                                <Text style={[styles.label,{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`${I18n.t('register.agree')} `}</Text>
                                <TouchableOpacity  onPress={()=> this.setState({modalAgree:true})}>
                                    <Text style={[styles.label,{fontSize:SIZE_3_5,textDecorationLine:'underline',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.term_of_service')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> Actions.pop()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.cancel')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> this.onsubmit()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.next')}</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    Container_row:{
        padding:dim.width/100*3,width:dim.width/100*35
    },
    txt_h:{
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
    },
    btn:{
        padding:dim.width/100*1.5,
        alignItems:'center',
        borderColor:BG_2,
        borderWidth:1,
        borderRadius:dim.width/100*1.5,
        margin:dim.width/100*1.5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_bold:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    input:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        borderRadius:dim.width/100*2,
        paddingLeft:dim.width/100*2,
    },
    input_dis:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        marginVertical:dim.height/100*1,
        borderRadius:dim.width/100*2,
        backgroundColor:'#e1e5e8',
        paddingLeft:dim.width/100*2,
        color:'#888a8c'
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        gitfList:state.eMember.GitfList,
        packetList:state.eMember.packetList,
        my_card:state.profile.my_card
    }
  }

export default connect(mapStateToProps,actions)(Eregister)