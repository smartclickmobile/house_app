import React, {Component} from 'react';
import {
  Dimensions,
  StyleSheet,
  Image,
  Platform,
  ToastAndroid,
  TouchableOpacity,
  Text,
  BackHandler,
} from 'react-native';
import * as actions from '../Actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {BG_1, BG_2, BLACK} from '../constant/color';
import {Container, Content} from 'native-base';
import Header from '../Component/header';
import I18n from '../../asset/languages/i18n';
import {
  TEXT_BODY_EN,
  TEXT_TH,
  TEXT_BODY_EN_BOLD,
  TEXT_TH_BOLD,
} from '../constant/font';
// import RNFetchBlob from 'react-native-fetch-blob';
import CameraRoll from '@react-native-community/cameraroll';
import RNFS from 'react-native-fs';
const dim = Dimensions.get('window');
class ShowQrcodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qr: '',
    };
  }

  componentDidMount() {
    // console.log('qr',this.props.qr)
    this.setState({qr: this.props.qr});
    this.downloadimage(this.props.qr);
    BackHandler.addEventListener('back', () => this._handleBack());
  }
  _handleBack() {
    if (
      Actions.currentScene == '_ShowQrcodeScreen' ||
      Actions.currentScene == 'ShowQrcodeScreen'
    ) {
      Actions.popTo('home');

      return true;
    }
    return false;
  }
  extention(filename) {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  }
  downloadimage(url) {
    if (Platform.OS == 'ios') {
      CameraRoll.saveToCameraRoll(url, 'photo')
        .then(function (result) {
          console.log('save succeeded ' + result);
          // alert(result);
        })
        .catch(function (error) {
          console.log('save failed ' + error);
          // alert(error);
        });
    } else {
    
        // CameraRoll.saveToCameraRoll(url, 'photo')
        // .then(function (result) {
        //   console.log('save succeeded ' + result);
        //   // alert(result);
        // })
        // .catch(function (error) {
        //   console.log('save failed ' + error);
        //   // alert(error);
        // });
    //   try {
    //     const {config, fs} = RNFetchBlob;
    //     var date = new Date();
    //     var ext = this.extention(url);
    //     ext = '.' + ext[0];
    //     let options = {
    //       fileCache: false,
    //       addAndroidDownloads: {
    //         useDownloadManager: true,
    //         notification: true,
    //         path:
    //           RNFS.ExternalStorageDirectoryPath +
    //           '/house/' +
    //           Math.floor(date.getTime() + date.getSeconds() / 2) +
    //           ext,
    //         description: 'Image',
    //       },
    //     };
    //     config(options)
    //       .fetch('GET', url)
    //       .then((res) => {
    //         ToastAndroid.show('Success Downloaded', ToastAndroid.SHORT);
    //       });
    //   } catch (error) {
    //     console.log('catch',error);
    //   }
    }
  }
  render() {
    console.warn('this.state.qr', this.state.qr);
    return (
      <Container>
        <LinearGradient
          colors={[BG_1, BG_1, BG_2]}
          style={[styles.linearGradient]}>
          <Header btnleft={false} title={this.props.title} />
          <Image
              source={{uri: this.state.qr}}
              style={{width: dim.width, height: dim.width}}
              resizeMode="cover"
            />
          <TouchableOpacity
            style={{
              padding: (dim.width / 100) * 3,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Actions.popTo('home')}>
            <Text
              style={{
                color: BLACK,
                fontFamily:
                  I18n.locale == 'th' ? TEXT_TH_BOLD : TEXT_BODY_EN_BOLD,
              }}>
              {I18n.t('ShowQrcodeScreen.back_home')}
            </Text>
          </TouchableOpacity>
        </LinearGradient>
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    checkIpnoe: state.check.checkIpnoe,
    forget_success: state.auth.forget_success,
    forget_success_random: state.auth.forget_success_random,
  };
};
var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: Platform.isPad ? (dim.width / 100) * 35 : (dim.width / 100) * 50,
    backgroundColor: '#ccd1d5',
    paddingVertical: (dim.height / 100) * 1.5,
    borderRadius: (dim.width / 100) * 1.5,
    borderColor: BG_1,
    borderWidth: 1,
    marginTop: (dim.height / 100) * 5,
  },
  text: {
    marginHorizontal: (dim.width / 100) * 1,
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
  },
  input: {
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
  },
});
export default connect(mapStateToProps, actions)(ShowQrcodeScreen);
