import React, { Component } from "react"
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2 } from '../constant/color'
import { Container,Form,Item,Input,Label } from 'native-base';
import Header from '../Component/header'
import { TEXT_TH, TEXT_BODY_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_6 } from "../constant/font";
import I18n from '../../asset/languages/i18n'
const dim = Dimensions.get('window');
class ConfirmnumberScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            checked:false
        }
    }
    render(){
        return(
            <Container>
                <LinearGradient colors={[BG_1,BG_1, BG_2]} style={[styles.linearGradient]}>
                    <Header btnleft={true}/>
                    <View style={{flex:1,alignItems:'center',padding:dim.width/100*10}}>
                        <Text style={{fontSize:SIZE_6,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`Please enter \nyour phone number`}</Text>
                        <Form style={{width:Platform.isPad?dim.width/100*50:dim.width/100*80}}>
                            <Item style={styles.item} floatingLabel>
                                <Label style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ConfirmnumberScreen.email')}</Label>
                                <Input 
                                    secureTextEntry={true}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={styles.item} floatingLabel>
                                <Label style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ConfirmnumberScreen.phone')}</Label>
                                <Input 
                                    autoCapitalize='none'
                                />
                            </Item>
                        </Form>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity style={[styles.btn]} onPress={()=> Actions.pop()}>
                                <Text style={styles.text}>{I18n.t('ConfirmnumberScreen.back')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn]} onPress={()=> Actions.home()}>
                                <Text style={styles.text}>{I18n.t('ConfirmnumberScreen.next')}</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    btn:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:Platform.isPad?dim.width/100*20:dim.width/100*30,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5,
        margin:dim.height/100*2
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
export default connect(mapStateToProps,actions)(ConfirmnumberScreen)