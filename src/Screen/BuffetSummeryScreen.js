import React, { Component } from "react"
import {
    TouchableOpacity,
    Dimensions,
    View,
    Image,
    Text,
    Platform,
    Alert,
    StyleSheet,

    BackHandler,
    ActivityIndicator,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import { BG_1, BLACK,BG_2 } from '../constant/color'
import { TEXT_BODY_EN, TEXT_HEADER_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD, TEXT_HEADER_EN_BOLD,SIZE_3_5,SIZE_4,SIZE_5 } from '../constant/font'
import Header from '../Component/header'
import { Container, Content, Icon } from "native-base";
import CountDown from 'react-native-countdown-component';
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import DeviceInfo from "react-native-device-info"
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
class BuffetSummeryScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            membertype:'',
            total:0,
            discount_member:0,
            loading:false
        }
    }
    async componentDidMount(){
        var membertype = await AsyncStorage.getItem('membertype')
        var total = 0
         if(membertype == 2){
            total = this.props.buffet_data.member_price
        }else{
            total = this.props.buffet_data.normal_price
         }
        this.setState({membertype:membertype,total:total})
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.success == true && nextProps.success_num != this.state.success_num){
            this.setState({loading:false})
        }
    }
    confirmPaymentbyCredit(){
       
        var data = {
            language:I18n.locale,
            price:this.state.total,
            device_id:DeviceInfo.getDeviceId(),
            first_name:this.props.user_data.first_name,
            last_name:this.props.user_data.last_name,
            telephone:this.props.user_data.mobile,
            event_buffet_id:this.props.buffet_data.event_buffet_id
        }
       
        if(this.props.my_card.length > 0){
            Actions.MycardBuffet({data:data})
        }else{
            // console.log('confirmPaymentbyCredit',data)
            Actions.BuffetPayment({data:data})
        }
       
    }
    confirmPaymentbyQRcredit(){
        this.setState({loading:true},()=>{
            setTimeout(() => {
                this.setState({loading:false})
            },30000)
        })
        var data = {
            language:I18n.locale,
            price:this.state.total,
            device_id:DeviceInfo.getDeviceId(),
            first_name:this.props.user_data.first_name,
            last_name:this.props.user_data.last_name,
            telephone:this.props.user_data.mobile,
            event_buffet_id:this.props.buffet_data.event_buffet_id
        }
        this.props.getQRcredit_buffet(data)
    }
    confirmPaymentbyQRdebit(){
        this.setState({loading:true},()=>{
            setTimeout(() => {
                this.setState({loading:false})
            },30000)
        })
        var data = {
            language:I18n.locale,
            price:this.state.total,
            device_id:DeviceInfo.getDeviceId(),
            first_name:this.props.user_data.first_name,
            last_name:this.props.user_data.last_name,
            telephone:this.props.user_data.mobile,
            event_buffet_id:this.props.buffet_data.event_buffet_id
        }
        this.props.getQRdebit_buffet(data)
    }

    _renderBooking(){
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date_start = moment(this.props.buffet_data.start_date,'YYYY-MM-DD HH:mm:ss')
        var day_start = date_start.date()
        var month_start = date_start.month()
        var monthtxt_start = I18n.locale == 'th'?month_th[month_start]:month_en[month_start]
        var year_start = date_start.year()
        var yeartxt_start =  I18n.locale == 'th'?parseInt(year_start)+543:parseInt(year_start)

        var date_end = moment(this.props.buffet_data.end_date,'YYYY-MM-DD HH:mm:ss')
        var day_end = date_end.date()
        var month_end = date_end.month()
        var monthtxt_end = I18n.locale == 'th'?month_th[month_end]:month_en[month_end]
        var year_end = date_end.year()
        var yeartxt_end =  I18n.locale == 'th'?parseInt(year_end)+543:parseInt(year_end)
        console.warn('buffet_data',this.props.buffet_data)
         var discount_member = 0
         var total = 0
         if(this.state.membertype == 2){
            discount_member = this.props.buffet_data.normal_price - this.props.buffet_data.member_price
            total = this.props.buffet_data.member_price
        }else{
            discount_member = 0
            total = this.props.buffet_data.normal_price
         }
        return(
            <Content>
                <View>
                    <View style={{flexDirection:'row',paddingHorizontal:7,backgroundColor:BG_2,width:dim.width,height:dim.height/100*5,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{flex:1,textAlign:'left',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>HOUSE BUFFET</Text>
                        <Text style={{flex:2,textAlign:'right',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>{`${day_start} ${monthtxt_start} ${yeartxt_start} - ${day_end} ${monthtxt_end} ${yeartxt_end}`}</Text>
                    </View>

                    <View style={{marginTop:dim.height/100*1,paddingHorizontal:7,backgroundColor:BG_2,width:dim.width,height:dim.height/100*15,alignItems:'center',justifyContent:'center'}}>
                        <View style={{flexDirection:'row',height:dim.height/100*5,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{flex:1,textAlign:'left',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>PRICE</Text>
                            <Text style={{flex:1,textAlign:'right',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>{`${this.props.buffet_data.normal_price} THB`}</Text>
                        </View>
                        <View style={{flexDirection:'row',height:dim.height/100*5,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{flex:1,textAlign:'left',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>MEMBER DISCOUNT</Text>
                            <Text style={{flex:1,textAlign:'right',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>{`${this.state.membertype==2?'- ':''}${discount_member} THB`}</Text>
                        </View>
                        <View style={{flexDirection:'row',height:dim.height/100*5,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{flex:1,textAlign:'left',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>TOTAL</Text>
                            <Text style={{flex:1,textAlign:'right',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BLACK}}>{`${total} THB`}</Text>
                        </View>
                    </View>
                    
                    <View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BLACK,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentbyCredit()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.credit')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BLACK,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentbyQRcredit()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.qr_credit')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{margin:dim.width/100*1,flex:1,borderColor:BLACK,borderWidth:dim.width/100*0.5,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} onPress={()=> this.confirmPaymentbyQRdebit()}>
                            <Text style={[styles.text_btn_pay,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('BookingSummeryScreen.qr_debit')}</Text>
                        </TouchableOpacity>
                        <View style={{margin:dim.width/100*1,flex:1,alignItems:'center',justifyContent:'center',paddingVertical:dim.height/100*4}} />
                    </View>
                </View>
                </View>
            </Content>
        )
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View  style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        backgroundColor:'black',
                        opacity:0.7
                        }}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={{backgroundColor:'#fff',width:dim.width/100*50,height:dim.width/100*50,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5,justifyContent:'center'}}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                        </View>
                </Modal>
                <Header btnleft={true}/>
                {this._renderBooking()}
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        my_card:state.profile.my_card,
        success:state.buffet.qr_success,
        success_num:state.buffet.qr_success_num,
    }
  }
  var styles = StyleSheet.create({
    text_btn_pay:{
        color:BLACK,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_btn_pay_bold:{
        color:BG_1,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    }
  });
  export default connect(mapStateToProps,actions)(BuffetSummeryScreen)