import React, {Component} from 'react';
import {
  View,
  Image,
  Dimensions,
  Alert,
  Modal,
  Platform,
  PermissionsAndroid,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import * as actions from '../Actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import {getAppstoreAppVersion} from 'react-native-appstore-version-checker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';
import {
  TEXT_HEADER_EN_BOLD,
  SIZE_6,
  SIZE_5,
  TEXT_BODY_EN,
  SIZE_4,
} from '../constant/font';
import {BLACK, BG_1} from '../constant/color';

import I18n from '../../asset/languages/i18n';
const dim = Dimensions.get('window');
class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updateVer: false,
      appVersion_data: '',
    };
  }
  // render() {
  //   return <View style={{backgroundColor: 'red', flex: 1}} />;
  // }
  async componentDidMount() {
    this.props.checkiphonex(dim);
    this.checkVersionapp();
    this.requestCameraPermission();
    this.setPermissions();
    this.createNotificationListeners();

    const token = await AsyncStorage.getItem('token');
    // firebase.messaging().subscribeToTopic('All')
    firebase.messaging().subscribeToTopic('Test');
    const language = await AsyncStorage.getItem('language');
    if (language != undefined && language != null && language != '') {
      I18n.locale = language;
    } else {
      I18n.locale = 'en';
    }
  }
  async nextto() {
    const token = await AsyncStorage.getItem('token');
    if (token != undefined && token != null && token != '') {
      if (Platform.OS == 'ios') {
        Actions.loginhome();
        Actions.home();
      } else {
        setTimeout(() => {
          Actions.loginhome();
          Actions.home();
        }, 1000);
      }
    } else {
      if (Platform.OS == 'ios') {
        Actions.loginhome();
      } else {
        setTimeout(() => {
          Actions.loginhome();
        }, 1000);
      }
    }
  }
  async checkVersionapp() {
    var appVersion_data = '';
    if (Platform.OS == 'ios') {
      getAppstoreAppVersion('1478113485') //put any apps id here
        .then((appVersion) => {
          console.log('ios app version on appstore', appVersion);
          appVersion_data = appVersion;
          var myappVersion_data = DeviceInfo.getVersion();
          var appVersion_data_s = appVersion_data.split('.');
          var myappVersion_data_s = myappVersion_data.split('.');
          var storeV_1 = parseInt(appVersion_data_s[0]);
          var storeV_2 = parseInt(appVersion_data_s[1]);
          var storeV_3 = parseInt(appVersion_data_s[2]);

          var myV_1 = parseInt(myappVersion_data_s[0]);
          var myV_2 = parseInt(myappVersion_data_s[1]);
          var myV_3 = parseInt(myappVersion_data_s[2]);

          if (storeV_1 > myV_1) {
            this.setState({updateVer: true, appVersion_data: appVersion_data});
          } else if (storeV_2 > myV_2 && storeV_1 == myV_1) {
            this.setState({updateVer: true, appVersion_data: appVersion_data});
          } else if (storeV_3 > myV_3 && storeV_2 == myV_2) {
            this.setState({updateVer: true, appVersion_data: appVersion_data});
          } else {
            console.log('nextto');
            this.nextto();
          }
        })
        .catch((err) => {
          // console.log("error occurred", err);
        });
    } else {

      let appVer = DeviceInfo.getVersion()
      let newVer = await this.props.checkappversion()
      let arrAppVer = appVer.split('.')
      let arrNewVer = newVer.split('.')
      
      if(arrAppVer[0]==arrNewVer[0]){
        if(arrNewVer[1]==arrAppVer[1]){
          if(arrNewVer[2]==arrAppVer[2]){
            this.nextto();
          }else if(arrNewVer[2]>arrAppVer[2]){
            this.setState({updateVer: true, appVersion_data: newVer});
          }else{
            this.nextto();
          }
        }else if(arrNewVer[1]>arrAppVer[1]){
          this.setState({updateVer: true, appVersion_data: newVer});
        }else{
          this.nextto();
        }
      }else if(arrNewVer[0]>arrAppVer[0]){
        this.setState({updateVer: true, appVersion_data: newVer});
      }else{
        this.nextto();
      }

      //#######
      // this.nextto();
      // getAppstoreAppVersion('com.house_app') //put any apps id here
      //   .then((appVersion) => {
      //     console.log('android app version on appstore', appVersion);
      //     appVersion_data = appVersion;
      //     var myappVersion_data = DeviceInfo.getVersion();
      //     var appVersion_data_s = appVersion_data.split('.');
      //     var myappVersion_data_s = myappVersion_data.split('.');
      //     var storeV_1 = parseInt(appVersion_data_s[0]);
      //     var storeV_2 = parseInt(appVersion_data_s[1]);
      //     var storeV_3 = parseInt(appVersion_data_s[2]);

      //     var myV_1 = parseInt(myappVersion_data_s[0]);
      //     var myV_2 = parseInt(myappVersion_data_s[1]);
      //     var myV_3 = parseInt(myappVersion_data_s[2]);

      //     if (storeV_1 > myV_1) {
      //       this.setState({updateVer: true, appVersion_data: appVersion_data});
      //     } else if (storeV_2 > myV_2 && storeV_1 == myV_1) {
      //       this.setState({updateVer: true, appVersion_data: appVersion_data});
      //     } else if (storeV_3 > myV_3 && storeV_2 == myV_2) {
      //       this.setState({updateVer: true, appVersion_data: appVersion_data});
      //     } else {
      //       console.log('nextto');
      //       this.nextto();
      //     }
      //   })
      //   .catch((err) => {
      //     // console.log("error occurred", err);
      //   });
    }
  }
  componentWillUnmount() {
    // this.notificationDisplayedListener()
    // this.notificationListener()
  }
  async requestCameraPermission() {
    if (Platform.OS == 'android') {
      try {
        const granted = PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        ]).then((result) => {
          // console.log('result', JSON.stringify(result));
        });
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
        } else {
          console.log('Camera permission denied');
        }
      } catch (err) {
        console.log(err);
      }
    }
  }
  async setPermissions() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      // user has permissions
      console.log('user has permissions');
    } else {
      // user doesn't have permission
      // console.log('user doesnt have permission')
      try {
        await firebase.messaging().requestPermission();
        // User has authorised
        console.log('User has authorised');
      } catch (error) {
        // User has rejected permissions
        console.log('User has rejected permissions');
      }
    }
  }
  async createNotificationListeners() {
    // firebase.messaging().subscribeToTopic('alert');
    // console.log('createNotificationListeners')
    // this.notificationListener = messaging().onNotification((notification: Notification) => {
    //   console.log('notificationListener',notification.data)
    //   notification.android.setChannelId('alert');
    //   firebase.notifications().displayNotification(notification)
    // });
    // this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
    //   console.log('notificationDisplayedListener')
    // });
    // this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //   console.log('notificationOpenedListener')
    // });
    // const notificationOpen = await firebase.notifications().getInitialNotification();
    // if (notificationOpen) {
    // }
    // this.messageListener = firebase.messaging().onMessage((message) => {
    //   //process data message
    //   console.log('messageListener',message)
    //   console.log(JSON.stringify(message));
    // });
  }
  showAlert(title, body) {
    Alert.alert(
      title,
      body,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  }
  // notification() {
  //   if(!this.checkPermission()) {
  //     firebase.messaging().requestPermission()
  //       .then(() => {
  //         // User has authorised
  //       })
  //       .catch(error => {
  //         // User has rejected permissions
  //         return
  //       })
  //   }

  //   firebase.messaging().onMessage((message: RemoteMessage) => {
  //     console.log(message.data)
  //   })
  // }

  //     checkPermission() {
  //       firebase.messaging().hasPermission()
  //         .then(enabled => {
  //           if(enabled) {
  //             return true
  //           } else {
  //             return false
  //           }
  //         })
  //     }

  //     createNotificationChannel() {
  //       // Build a channel
  //       const channel = new firebase.notifications.Android.Channel('alert', 'Alert', firebase.notifications.Android.Importance.Max)
  //         .setDescription('Alert')
  //       console.log('createNotificationChannel',channel)
  //       // Create the channel
  //       firebase.notifications().android.createChannel(channel)
  //     }
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Modal
          visible={this.state.updateVer}
          transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              backgroundColor: 'black',
              opacity: 0.7,
            }}></View>
          <View
            style={{
              width: '100%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: (dim.width / 100) * 90,
                borderRadius: (dim.width / 100) * 2,
                alignItems: 'center',
                padding: (dim.width / 100) * 5,
                height: (dim.height / 100) * 90,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: SIZE_6,
                  color: BLACK,
                  fontFamily: TEXT_HEADER_EN_BOLD,
                }}>
                HOUSE CINEMA
              </Text>
              <Text
                style={{
                  fontSize: SIZE_5,
                  color: BLACK,
                  fontFamily: TEXT_BODY_EN,
                }}>{`Update version ${this.state.appVersion_data}`}</Text>

              <Text
                style={{
                  fontSize: SIZE_4,
                  color: BLACK,
                  fontFamily: TEXT_BODY_EN,
                  marginTop: (dim.height / 100) * 10,
                }}>{`Please update your application`}</Text>

              <TouchableOpacity
                style={{
                  marginTop: (dim.height / 100) * 10,
                  backgroundColor: '#565455',
                  width: (dim.width / 100) * 50,
                  padding: (dim.width / 100) * 3,
                  alignItems: 'center',
                }}
                onPress={() => {
                  if (Platform.OS == 'ios') {
                    var url =
                      'https://apps.apple.com/us/app/house-cinema/id1478113485?ls=1';
                  } else {
                    var url =
                      'https://play.google.com/store/apps/details?id=com.house_app';
                  }
                  Linking.canOpenURL(url)
                    .then((supported) => {
                      if (!supported) {
                        console.log("Can't handle url: " + url);
                      } else {
                        return Linking.openURL(url);
                      }
                    })
                    .catch((err) => console.error('An error occurred', err));
                }}>
                <Text
                  style={{
                    fontSize: SIZE_4,
                    color: BG_1,
                    fontFamily: TEXT_HEADER_EN_BOLD,
                  }}>{`UPDATE`}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        {Platform.OS == 'ios' ? (
          <View />
        ) : (
          <Image
            source={require('../../asset/Images/logo.png')}
            style={{
              width: (dim.width / 100) * 50,
              height: (dim.width / 100) * 50,
            }}
            resizeMode="contain"
          />
        )}
        {/* <Image
          source={require('../../asset/Images/logo.png')}
          style={{
            width: (dim.width / 100) * 50,
            height: (dim.width / 100) * 50,
          }}
          resizeMode="contain"
        /> */}
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    checkIpnoe: state.check.checkIpnoe,
  };
};
export default connect(mapStateToProps, actions)(Splash);
