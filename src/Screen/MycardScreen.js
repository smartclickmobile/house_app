import React, { Component } from "react"
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    Alert,
    StyleSheet,
    ActivityIndicator,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2 ,BLACK} from '../constant/color'
import { Container,Icon, Content, Card, CardItem,Body, Right } from 'native-base';
import I18n from '../../asset/languages/i18n'
import Header from '../Component/header'
import { SwipeListView } from 'react-native-swipe-list-view';
import { TEXT_TH, TEXT_BODY_EN, SIZE_4, SIZE_3_5 } from '../constant/font'
const dim = Dimensions.get('window');
class MycardScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
           list_card:this.props.my_card,
           selected_card:null,
           loading:false,
           success_num:null
        }
    }
    componentDidMount(){
        var array = []
        for(var i = 0 ; i < this.props.my_card.length; i++){
            var data = {
                creditcard_id:this.props.my_card[i].creditcard_id,
                number:this.props.my_card[i].number,
                expiration_month:this.props.my_card[i].expiration_month,
                expiration_year:this.props.my_card[i].expiration_year,
                name:this.props.my_card[i].name,
                remember_card:this.props.my_card[i].remember_card,
                selected:false
            }
            array.push(data)
        }
        this.setState({list_card:array})
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.my_card != null && nextProps.my_card != undefined){
            this.setState({list_card:nextProps.my_card})
        }
        if(nextProps.success == true && nextProps.success_num != this.state.success_num){
            this.setState({loading:false})
        }
    }
    onSelect(item){
        // console.log('item',item)
        if(this.state.selected_card == null){
            var array = []
            for(var i = 0 ; i <this.state.list_card.length; i++){
                var check = false
                if(this.state.list_card[i].creditcard_id == item.creditcard_id){
                    check = true
                }
                var data = {
                    creditcard_id:this.state.list_card[i].creditcard_id,
                    number:this.state.list_card[i].number,
                    expiration_month:this.state.list_card[i].expiration_month,
                    expiration_year:this.state.list_card[i].expiration_year,
                    name:this.state.list_card[i].name,
                    remember_card:this.state.list_card[i].remember_card,
                    selected:check
                }
                array.push(data)
            }
            this.setState({list_card:array,selected_card:item})
        }else{
            if(this.state.selected_card.creditcard_id == item.creditcard_id){
                this.setState({selected_card:null},()=>{
                    var array = []
                    for(var i = 0 ; i < this.state.list_card.length; i++){
                        var data = {
                            creditcard_id:this.state.list_card[i].creditcard_id,
                            number:this.state.list_card[i].number,
                            expiration_month:this.state.list_card[i].expiration_month,
                            expiration_year:this.state.list_card[i].expiration_year,
                            name:this.state.list_card[i].name,
                            remember_card:this.state.list_card[i].remember_card,
                            selected:false
                        }
                        array.push(data)
                    }
                    this.setState({list_card:array})
                })
            }

        }
        
    }
    onPay(){
        // console.log('onPay')
        if(this.state.selected_card == null){
            Alert.alert('',I18n.t('MycardScreen.error_card'))
        }else{
            this.setState({loading:true})
            var data = { 
                language:I18n.locale,
                purchase_transaction_id:this.props.data.purchase_transaction_id,
                voucher_ids:this.props.data.voucher_ids,
                price:this.props.data.price,
                creditcard_id:this.state.selected_card.creditcard_id
            }
            this.props.purchaseBycredit(data)
        }
        
    }
    onDelete(item){
        // console.log('onDelete',item)
        var data = { 
            language:I18n.locale,
            creditcard_id:item.creditcard_id
        }
        this.props.deletecredit(data)
    }
    render(){
        console.log(this.state.list_card)
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View  style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        backgroundColor:'black',
                        opacity:0.7
                        }}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={{backgroundColor:'#fff',width:dim.width/100*50,height:dim.width/100*50,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5,justifyContent:'center'}}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                        </View>
                    </Modal>
                <LinearGradient colors={[BG_1,BG_1, BG_2]} style={[styles.linearGradient]}>
                    <Header btnleft={true} title={I18n.t('MycardScreen.payment')}/>
                    <Content>
                        <SwipeListView
                            data={this.state.list_card}
                            style={{margin:dim.width/100*3}}
                            renderItem={ (item, index) => (
                                <Card key={index} style={{borderRadius:dim.width/100*2.5}}>
                                    <CardItem style={{borderRadius:dim.width/100*2.5}}>
                                        <TouchableOpacity style={{flexDirection:'row'}} onPress={()=> this.onSelect(item.item)}>
                                            <Body>
                                                <Text style={{flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,margin:dim.width/100*5,fontSize:SIZE_3_5}}>{item.item.number}</Text>
                                            </Body>
                                            <Right>
                                                {item.item.selected?<Icon name='check' type='FontAwesome5' style={{color:BLACK,fontSize:SIZE_4}}/>:<View/>}
                                            </Right>
                                        </TouchableOpacity>
                                    </CardItem>
                                </Card>
                            )}
                            renderHiddenItem={ (item, index) => (
                                <TouchableOpacity style={styles.rowBack} onPress={()=> this.onDelete(item.item)}>
                                    <Icon name='trash' type='FontAwesome5' style={{color:BLACK}}/>
                                </TouchableOpacity>
                            )}
                            // leftOpenValue={75}
                            rightOpenValue={-(dim.width/100*12)}
                        />
                        <TouchableOpacity style={[styles.botton]} onPress={()=> Actions.payment({data:this.props.data})}>
                            <Icon name='plus' type='FontAwesome5' style={{color:BLACK,fontSize:SIZE_4,marginRight:dim.width/100*2}}/>
                            <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3_5}}>{I18n.t('MycardScreen.add')}</Text>
                        </TouchableOpacity>
                        <View style={{height:dim.height/100*7,backgroundColor:'#565455',alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:BG_1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3_5}}>{I18n.t('MycardScreen.payment')}</Text>
                        </View>
                        <TouchableOpacity style={[styles.botton,{flexDirection:'column'}]} onPress={()=> this.onPay()}>
                            <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3_5}}>{I18n.t('MycardScreen.confirm')}</Text>
                            <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3_5}}>{`${I18n.t('MycardScreen.total')} ${this.props.data.price} ${I18n.t('MycardScreen.unit')}`}</Text>
                        </TouchableOpacity>
                    </Content>
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        my_card:state.profile.my_card,
        success:state.purchase.credit_success,
        success_num:state.purchase.credit_success_num
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    botton:{
        borderColor:BLACK,
        borderRadius:dim.width/100*1,
        borderWidth:0.7,
        margin:dim.width/100*2.5,
        padding:dim.width/100*5,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        backgroundColor:BG_2,
    },
    rowBack: {
		alignItems: 'center',
		// backgroundColor: '#DDD',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
        // paddingLeft: 15,
        paddingRight:dim.width/100*4
	},
  });
export default connect(mapStateToProps,actions)(MycardScreen)