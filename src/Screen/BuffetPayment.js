import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Platform,
    ActivityIndicator,
    Modal,
    Alert
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK,BG_2, RED_COLOR } from '../constant/color'
import { Container, Content, Footer, FooterTab, Button, Icon,Form,Item,Input,Label,Picker } from 'native-base';
import {CheckBox} from 'react-native-elements'
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
import moment from 'moment'
import I18n from '../../asset/languages/i18n'
import { TEXT_TH, TEXT_BODY_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3_5,SIZE_3_7,SIZE_5_5,SIZE_4 } from '../constant/font'
const dim = Dimensions.get('window');
class BuffetPayment extends Component {
    constructor(props){
        super(props)
        this.state = {
            monthList : [
                {
                    value:1,
                    name:'01'
                },
                {
                    value:2,
                    name:'02'
                },
                {
                    value:3,
                    name:'03'
                },
                {
                    value:4,
                    name:'04'
                },
                {
                    value:5,
                    name:'05'
                },
                {
                    value:6,
                    name:'06'
                },
                {
                    value:7,
                    name:'07'
                },
                {
                    value:8,
                    name:'08'
                },
                {
                    value:9,
                    name:'09'
                },
                {
                    value:10,
                    name:'10'
                },
                {
                    value:11,
                    name:'11'
                },
                {
                    value:12,
                    name:'12'
                },
            ],
            yearList:[],
            checked:false,
            remember_card:false,
            Name:'',
            Credit_number:'',
            Credit_number_str:'',
            exp_month:'',
            exp_year:'',
            cvv:'',
            selected_month:'',
            selected_year:'',
            loading:false,
            success_num:null,
            modalError:false,
            error_num:null
        }
    }
    componentDidMount(){
        var currentDate = moment()
        var year = currentDate.year()-1
        var array_year = []
        var y = year
        for(var i=0 ;i<21;i++){
            var data = {
                value:i+1,
                name:((parseInt(y) + 1) -2000).toString()
            }
            array_year.push(data)
            y = parseInt(y) + 1
        }
        this.setState({yearList:array_year},()=>{
            if(Platform.OS == 'android'){
                this.setState({exp_month:this.state.monthList[0].name,exp_year:this.state.yearList[0].name,selected_month:this.state.monthList[0].value,selected_year:this.state.yearList[0].value})
            }
        })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.success == true && nextProps.success_num != this.state.success_num){
            this.setState({loading:false})
        }
        if(Actions.currentScene == 'BuffetPayment'){
            if(nextProps.error != null && nextProps.error_num != this.state.error_num){
                this.setState({error_num:nextProps.error_num},()=>{
                    Alert.alert('ERROR',nextProps.error,[
                        {
                            text: 'OK',
                                onPress: () => {
                                    this.setState({loading:false},()=>{
                                        console.warn('loading',this.state.loading)
                                    })
                                }
                        }
                    ])
                })
               
            }
        }
        
    }
    onChangecreditcard(value){
        var temp = value.replace(/-/gi,'')
        this.props.creditcardFormat(temp).then(resp=>{
            this.setState({Credit_number:temp,Credit_number_str:resp})
        })
    }
    onValueChangeMonth(value: string) {
        this.setState({
            selected_month: value
        },()=>{
            for(var i = 0 ; i<this.state.monthList.length ; i++){
                if(this.state.selected_month == this.state.monthList[i].value){
                    this.setState({exp_month:this.state.monthList[i].name})
                }
            }
        });
    }
    onValueChangeYear(value: string) {
        this.setState({
            selected_year: value
        },()=>{
            for(var i = 0 ; i<this.state.yearList.length ; i++){
                if(this.state.selected_year == this.state.yearList[i].value){
                    this.setState({exp_year:this.state.yearList[i].name})
                }
            }
        });
    }
    onPayment(){
        var Credit = this.state.Credit_number_str.replace(/-/gi,'')
        if(this.state.Name == ''){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.name')})
        }else if(Credit == '' || Credit.length != 16){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.Credit_number')})
        }else if(this.state.exp_month == '' ){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.exp_month')})
        }else if(this.state.exp_year == ''){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.exp_year')})
        }else if(this.state.cvv == '' || this.state.cvv.length < 3){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.cvv')})
        }
        else{
            this.setState({loading:true})
            var data = {
                language:I18n.locale,
                first_name:this.props.data.first_name,
                last_name:this.props.data.last_name,
                telephone:this.props.data.telephone,
                price:this.props.data.price,
                platform_id:2,
                event_buffet_id:this.props.data.event_buffet_id,
                device_id:this.props.data.device_id,
                
                number:Credit,
                expiration_month:this.state.exp_month,
                expiration_year:this.state.exp_year,
                security_code:this.state.cvv,
                name:this.state.Name,
                remember_card:this.state.remember_card
            }
            this.props.payBycreditcard(data)
        }
        
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                        onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                        <View  style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        backgroundColor:'black',
                        opacity:0.7
                        }}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={{backgroundColor:'#fff',width:dim.width/100*50,height:dim.width/100*50,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5,justifyContent:'center'}}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                        </View>
                </Modal>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={[styles.text_modal,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={[styles.text_modal_btn,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Header btnleft={true}/>
                <Content>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_5_5,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.payment')}</Text>
                    <View style={{alignItems:'center'}}>
                        <Form style={{width:Platform.isPad?dim.width/100*40:dim.width/100*70}}>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel >
                                <Label style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.name')}</Label>
                                <Input 
                                    style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    onChangeText={(value)=>{this.setState({Name:value})}}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel >
                                <Label style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.credit_card')}</Label>
                                <Input 
                                    style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    value={this.state.Credit_number_str}
                                    onChangeText={(value)=>{this.onChangecreditcard(value)}}
                                    autoCapitalize='none'
                                    maxLength={19}
                                />
                            </Item>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.select_month')}</Label>
                            <Picker
                                placeholder="MM"
                                mode="dropdown"
                                iosHeader={I18n.t('Epayment.select_month')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected_month}
                                onValueChange={this.onValueChangeMonth.bind(this)}
                                >
                                {
                                    this.state.monthList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                        
                                    })
                                }
                            </Picker>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.select_year')}</Label>
                            <Picker
                                placeholder="YYYY"
                                mode="dropdown"
                                iosHeader={I18n.t('Epayment.select_year')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected_year}
                                onValueChange={this.onValueChangeYear.bind(this)}
                                >
                                {
                                    this.state.yearList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                        
                                    })
                                }
                            </Picker>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel style={{marginTop:-4}}>
                                <Label style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.cvv')}</Label>
                                <Input 
                                    style={[styles.label,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    onChangeText={(value)=>{this.setState({cvv:value})}}
                                    autoCapitalize='none'
                                    secureTextEntry={true}
                                    maxLength={3}
                                />
                            </Item>
                        </Form>
                        <CheckBox
                            ref={(input) => { this.checkb = input; }}
                            title={I18n.t('Epayment.remember_card')}
                            checked={this.state.remember_card}
                            containerStyle={{borderWidth:0,backgroundColor:'transparent',marginTop:dim.height/100*3}}
                            textStyle={[styles.label,{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                            onPress={()=> this.setState({remember_card:!this.state.remember_card})}
                            checkedColor={BLACK}
                            
                        />
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> Actions.pop()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.back')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> this.onPayment()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Epayment.pay_now')}</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                
                </Content>
            </Container> 
        )
        
    }


}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        success:state.buffet.credit_success,
        success_num:state.buffet.credit_success_num,
        error:state.buffet.error,
        error_num:state.buffet.error_num
    }
  }

export default connect(mapStateToProps,actions)(BuffetPayment)