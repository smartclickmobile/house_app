import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Modal,
  StyleSheet,

  Platform,
} from 'react-native';
import * as actions from '../Actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BG_1, BG_2, BLACK, RED_COLOR} from '../constant/color';
import {Container, Form, Item, Input, Label, Icon, Content} from 'native-base';
import {CheckBox} from 'react-native-elements';
import Header from '../Component/header';
import I18n from '../../asset/languages/i18n';
import DeviceInfo from 'react-native-device-info';
import {
  TEXT_BODY_EN,
  TEXT_TH,
  TEXT_TH_BOLD,
  TEXT_BODY_EN_BOLD,
  SIZE_3,
  SIZE_3_5,
  SIZE_4,
  SIZE_7,
  SIZE_4_5,
} from '../constant/font';
import {WebView} from 'react-native-webview';
import {URL_IP} from '../constant/constant';
const dim = Dimensions.get('window');
class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      email: '',
      password: '',
      cf_password: '',
      mobile: '',

      email_c: false,
      password_c: false,
      cf_password_c: false,
      mobile_c: false,

      modalError: false,
      textError: '',

      modalSuccess: false,
      modalAgree: false,
      token_fcm: null,
    };
  }
  async componentDidMount() {
    var token_fcm = await AsyncStorage.getItem('fcmToken');
    this.setState({token_fcm: token_fcm});
  }
  componentDidUpdate(prvProps) {
    if (
      this.props.register !== null &&
      prvProps.register !== this.props.register
    ) {
      if (this.props.register == true) {
        this.setState({modalSuccess: true});
      }
    }
  }
//   componentWillReceiveProps(nextProps) {
//     if (nextProps.register != null && nextProps.register != undefined) {
//       if (nextProps.register == true) {
//         this.setState({modalSuccess: true});
//       }
//     }
//   }
  validatePassword() {
    var SpacialCharacter = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var upper = /([A-Z]+)/g;
    var lower = /([a-z]+)/g;
    var num = /([0-9]+)/g;
    var check_sp = SpacialCharacter.test(this.state.password);
    var check_upper = upper.test(this.state.password);
    var check_lower = lower.test(this.state.password);
    var check_num = num.test(this.state.password);
    var check = false;
    // if(check_sp && check_upper && check_lower && check_num){
    if (this.state.password.length >= 8) {
      check = true;
      this.setState({password_c: false, validate_password_text: false});
    } else {
      this.setState({password_c: true, validate_password_text: true});
    }
    return check;
  }
  onRegister() {
    // var validate = this.validatePassword(this.state.password)
    if (
      this.state.email == '' ||
      !this.state.email.includes('@') ||
      !this.state.email.includes('.')
    ) {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.email'),
        email_c: true,
      });
    } else if (this.state.password == '') {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.password'),
        email_c: false,
        password_c: true,
      });
    } else if (this.state.password.length < 8) {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.validate'),
        email_c: false,
        password_c: true,
      });
    } else if (this.state.cf_password == '') {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.confirm_password'),
        email_c: false,
        password_c: false,
        cf_password_c: true,
      });
    } else if (this.state.password != this.state.cf_password) {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.correct_password'),
        email_c: false,
        password_c: true,
        cf_password_c: true,
      });
    } else if (this.state.mobile == '') {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.mobile'),
        email_c: false,
        password_c: false,
        cf_password_c: false,
        mobile_c: true,
      });
    } else if (this.state.mobile.length < 9) {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.mobile'),
        email_c: false,
        password_c: false,
        cf_password_c: false,
        mobile_c: true,
      });
    } else if (!this.state.checked) {
      this.setState({
        modalError: true,
        textError: I18n.t('register.error.check'),
        email_c: false,
        password_c: false,
        cf_password_c: false,
        mobile_c: false,
      });
    } else {
      var data = {
        email: this.state.email,
        telephone: this.state.mobile,
        device_id: DeviceInfo.getDeviceId(),
        device_name: DeviceInfo.getDeviceName(),
        password: this.state.password,
        member_type_id: 1,
        language: I18n.locale,
        type: 3,
      };
      this.props.registerUser(data);
    }
  }
  onAgree() {
    // this.setState({checked:!this.state.checked})
    if (this.state.checked) {
      this.setState({checked: !this.state.checked});
    } else {
      this.setState({modalAgree: true});
    }
  }
  // render(){
  //   return(
  //     <View style={{backgroundColor:'red',flex:1}}>

  //     </View>
  //   )
  // }
  render() {
    return (
      <Container>
        <Modal
          visible={this.state.modalError}
          transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              backgroundColor: 'black',
              opacity: 0.7,
            }}
          />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: (dim.width / 100) * 80,
                borderRadius: (dim.width / 100) * 3,
                alignItems: 'center',
                padding: (dim.width / 100) * 5,
              }}>
              <View
                style={{
                  borderColor: RED_COLOR,
                  borderWidth: (dim.width / 100) * 1,
                  width: (dim.width / 100) * 15,
                  height: (dim.width / 100) * 15,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: (dim.width / 100) * 7.5,
                }}>
                <Icon
                  name="exclamation"
                  type="FontAwesome5"
                  style={{color: RED_COLOR}}
                />
              </View>
              <Text
                style={[
                  styles.text_modal,
                  {fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN},
                ]}>
                {this.state.textError}
              </Text>
              <TouchableOpacity
                style={{
                  borderColor: BG_1,
                  borderWidth: (dim.width / 100) * 0.5,
                  padding: (dim.width / 100) * 2,
                  backgroundColor: '#ccd1d5',
                  borderRadius: (dim.width / 100) * 2,
                  width: (dim.width / 100) * 20,
                }}
                onPress={() => {
                  this.setState({modalError: false});
                }}>
                <Text
                  style={[
                    styles.text_modal_btn,
                    {fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN},
                  ]}>
                  {I18n.t('register.alert.ok')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          visible={this.state.modalSuccess}
          transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              backgroundColor: 'black',
              opacity: 0.7,
            }}
          />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: (dim.width / 100) * 80,
                borderRadius: (dim.width / 100) * 3,
                alignItems: 'center',
                padding: (dim.width / 100) * 5,
              }}>
              <View
                style={{
                  borderColor: '#26de81',
                  borderWidth: (dim.width / 100) * 1,
                  width: (dim.width / 100) * 15,
                  height: (dim.width / 100) * 15,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: (dim.width / 100) * 7.5,
                }}>
                <Icon
                  name="check"
                  type="FontAwesome5"
                  style={{color: '#26de81'}}
                />
              </View>
              <Text
                style={[
                  styles.text_modal,
                  {fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN},
                ]}>
                {I18n.t('register.alert.register_success')}
              </Text>
              <TouchableOpacity
                style={{
                  borderColor: BG_1,
                  borderWidth: (dim.width / 100) * 0.5,
                  padding: (dim.width / 100) * 2,
                  backgroundColor: '#ccd1d5',
                  borderRadius: (dim.width / 100) * 2,
                  width: (dim.width / 100) * 20,
                }}
                onPress={() => {
                  this.setState({modalSuccess: false});
                  var data = {
                    email: this.state.email,
                    password: this.state.password,
                    device_id: DeviceInfo.getDeviceId(),
                    device_name: DeviceInfo.getDeviceName(),
                    language: I18n.locale,
                    type: 3,
                    token_firebase: this.state.token_fcm,
                  };
                  this.props.login(data);
                  // Actions.replace('login')
                }}>
                <Text
                  style={[
                    styles.text_modal_btn,
                    {fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN},
                  ]}>
                  {I18n.t('register.alert.ok')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          visible={this.state.modalAgree}
          transparent={true}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              backgroundColor: 'black',
              opacity: 0.7,
            }}
          />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: (dim.width / 100) * 90,
                height: (dim.height / 100) * 80,
                borderRadius: (dim.width / 100) * 3,
                alignItems: 'center',
                padding: (dim.width / 100) * 5,
              }}>
              <TouchableOpacity
                style={{alignSelf: 'flex-end'}}
                onPress={() => {
                  this.setState({modalAgree: false});
                }}>
                <Icon
                  name="times"
                  type="FontAwesome5"
                  style={{
                    color: BLACK,
                    fontSize: (dim.width / 100) * 10,
                    textAlign: 'right',
                  }}
                />
              </TouchableOpacity>
              {/*<Text style={[styles.text_modal_bold,{fontSize:SIZE_4_5}]}>{I18n.t('register.term_of_service')}</Text>*/}
              <WebView
                style={{
                  width: (dim.width / 100) * 80,
                  height: (dim.height / 100) * 60,
                  backgroundColor: 'transparent',
                  marginTop: (dim.height / 100) * 1,
                }}
                originWhitelist={['*']}
                source={{uri: URL_IP + '/term/user'}}
              />
            </View>
          </View>
        </Modal>
        <LinearGradient
          colors={[BG_1, BG_1, BG_2]}
          style={[styles.linearGradient]}>
          <Header btnleft={true} />
          <Content>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                padding: (dim.width / 100) * 10,
              }}>
              <Text
                style={[
                  styles.label_bold,
                  {
                    fontSize: SIZE_7,
                    fontFamily:
                      I18n.locale == 'th' ? TEXT_TH_BOLD : TEXT_BODY_EN_BOLD,
                  },
                ]}>
                {I18n.t('register.register')}
              </Text>
              <Form
                style={{
                  width: Platform.isPad
                    ? (dim.width / 100) * 50
                    : (dim.width / 100) * 80,
                }}>
                <Item
                  style={styles.item}
                  floatingLabel
                  error={this.state.email_c}>
                  <Label
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}>
                    {I18n.t('register.email')}
                  </Label>
                  <Input
                    ref={(input) => {
                      this.email_text = input;
                    }}
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}
                    onChangeText={(value) => this.setState({email: value})}
                    autoCapitalize="none"
                    keyboardType="email-address"
                  />
                </Item>
                <Item
                  style={styles.item}
                  floatingLabel
                  error={this.state.password_c}>
                  <Label
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}>
                    {I18n.t('register.password')}
                  </Label>
                  <Input
                    ref={(input) => {
                      this.password_text = input;
                    }}
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}
                    onChangeText={(value) => this.setState({password: value})}
                    onEndEditing={(value) => this.validatePassword(value)}
                    secureTextEntry={true}
                    autoCapitalize="none"
                  />
                </Item>
                {this.state.validate_password_text ? (
                  <Text
                    style={{
                      marginLeft: (dim.width / 100) * 5,
                      color: RED_COLOR,
                      fontSize: SIZE_3,
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}>
                    {'*' + I18n.t('register.error.validate')}
                  </Text>
                ) : (
                  <View />
                )}
                <Item
                  style={styles.item}
                  floatingLabel
                  error={this.state.cf_password_c}>
                  <Label
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}>
                    {I18n.t('register.confirm_password')}
                  </Label>
                  <Input
                    ref={(input) => {
                      this.cf_password_text = input;
                    }}
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}
                    onChangeText={(value) =>
                      this.setState({cf_password: value})
                    }
                    secureTextEntry={true}
                    autoCapitalize="none"
                  />
                </Item>
                <Item
                  style={styles.item}
                  floatingLabel
                  error={this.state.mobile_c}>
                  <Label
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}>
                    {I18n.t('register.mobile')}
                  </Label>
                  <Input
                    ref={(input) => {
                      this.mobile_text = input;
                    }}
                    style={{
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    }}
                    onChangeText={(value) => this.setState({mobile: value})}
                    autoCapitalize="none"
                    maxLength={10}
                  />
                </Item>
              </Form>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: (dim.height / 100) * 3,
                  alignItems: 'center',
                }}>
                <CheckBox
                  ref={(input) => {
                    this.checkb = input;
                  }}
                  // title={I18n.t('register.agree')}
                  checked={this.state.checked}
                  containerStyle={{
                    borderWidth: 0,
                    backgroundColor: 'transparent',
                  }}
                  textStyle={[styles.label, {fontSize: SIZE_3_5}]}
                  onPress={() => {
                    this.setState({checked: !this.state.checked});
                  }}
                  checkedColor={BLACK}
                />
                <View>
                  <Text
                    style={[
                      styles.label,
                      {
                        fontSize: SIZE_3_5,
                        fontFamily:
                          I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                      },
                    ]}>{`${I18n.t('register.agree')} `}</Text>
                  <TouchableOpacity
                    onPress={() => this.setState({modalAgree: true})}>
                    <Text
                      style={[
                        styles.label,
                        {
                          fontSize: SIZE_3_5,
                          textDecorationLine: 'underline',
                          fontFamily:
                            I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                        },
                      ]}>
                      {I18n.t('register.term_of_service')}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <TouchableOpacity
                style={[styles.btn]}
                onPress={() => {
                  this.onRegister();
                }}>
                <Text
                  style={[
                    styles.label,
                    {
                      fontSize: SIZE_3_5,
                      fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
                    },
                  ]}>
                  {I18n.t('register.register_btn')}
                </Text>
              </TouchableOpacity>
            </View>
          </Content>
        </LinearGradient>
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    checkIpnoe: state.check.checkIpnoe,
    register: state.auth.register,
    error_register: state.auth.error_register,
    register_random: state.auth.register_random,
  };
};
var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: Platform.isPad ? (dim.width / 100) * 35 : (dim.width / 100) * 50,
    backgroundColor: '#ccd1d5',
    paddingVertical: (dim.height / 100) * 1.5,
    borderRadius: (dim.width / 100) * 1.5,
    borderColor: BG_1,
    borderWidth: 1,
    marginTop: (dim.height / 100) * 5,
  },
  label: {
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
  },
  label_bold: {
    fontFamily: I18n.locale == 'th' ? TEXT_TH_BOLD : TEXT_BODY_EN_BOLD,
  },
  text_modal: {
    marginVertical: (dim.height / 100) * 1.5,
    fontSize: SIZE_4,
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
  },
  text_modal_bold: {
    marginVertical: (dim.height / 100) * 1.5,
    fontSize: SIZE_4,
    fontFamily: I18n.locale == 'th' ? TEXT_TH_BOLD : TEXT_BODY_EN_BOLD,
  },
  text_modal_btn: {
    textAlign: 'center',
    fontSize: SIZE_4,
    fontFamily: I18n.locale == 'th' ? TEXT_TH : TEXT_BODY_EN,
  },
  item: {
    padding: 3,
  },
});
export default connect(mapStateToProps, actions)(RegisterScreen);
