import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content, Icon,Container } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN, TEXT_BODY_EN_BOLD, TEXT_TH_BOLD,SIZE_3,SIZE_3_5,SIZE_4,SIZE_3_7,SIZE_2_9,SIZE_6 } from '../constant/font'
import Header from '../Component/header'
const dim = Dimensions.get('window');
class ProfilePoint extends Component {
    constructor(props){
        super(props)
        this.state = {
            data:this.props.profile_info,
            voucher_num:null,
            voucher:[],
            maxVoucher:0,
            voucher_select:0,
            exchange_success_num:null,
            modalRedeem:false,
            modalSuccess:false,
        }
    }
    componentDidMount(){
        var temp = {
            language:I18n.locale,
            telephone:this.state.data.telephone
        }
        this.props.getVoucher(temp)
        if(this.state.data.score != null){
            var mx_voucher = parseInt(this.state.data.score)/10
            var int_m = parseInt(mx_voucher)
            this.setState({maxVoucher:int_m,voucher_select:int_m})
        }
       
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.voucher != null && nextProps.voucher != undefined && this.state.voucher_num != nextProps.voucher_num){
            this.setState({voucher:nextProps.voucher,voucher_num:nextProps.voucher_num})
        }
        console.log('exchange_success',nextProps.exchange_success,nextProps.exchange_success_num,this.state.exchange_success_num)
        if(nextProps.exchange_success_num != this.state.exchange_success_num){
            this.setState({exchange_success_num:nextProps.exchange_success_num},()=>{
                
                if(nextProps.exchange_success == true ){
                    var datanew = {
                        language: I18n.locale
                    }
                    this.props.getProfileInfo(datanew)
                   this.setState({modalRedeem:false,modalSuccess:true})
                }
            })
        }
        if(nextProps.profile_info != null && nextProps.profile_info != undefined){
            this.setState({data:nextProps.profile_info})
        }
    }
    onRedeemVoucher(){
        this.setState({modalRedeem:true})
    }
    removeVoucher(){
        if(parseInt(this.state.voucher_select) != 0){
            this.setState({voucher_select:parseInt(this.state.voucher_select)-1})
        }
      }
    addVoucher(){
        if(parseInt(this.state.voucher_select) < parseInt(this.state.maxVoucher)){
            this.setState({voucher_select:parseInt(this.state.voucher_select)+1})
        }
    }
    onexchange(){
        var data = {
            language:I18n.locale,
            total_voucher:this.state.voucher_select
        }
        // console.log('onexchange',data)
        this.props.exchangePoint(data)
    }
    render(){
        return(
            <Container>
                <Header btnleft={true}/>
                <Content style={{marginTop:0,padding:dim.width/100*2}}>
                    <Modal visible={this.state.modalRedeem} transparent={true}>
                        <View style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.redeem_voucher')}</Text>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.my_point')}</Text>
                                    <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,marginHorizontal:dim.width/100*3}}>{this.state.data.score==null||this.state.data.score==''?0:this.state.data.score}</Text>
                                    <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.point_unit')}</Text>
                                </View>
                                <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:RED_COLOR}}>{I18n.t('tab.profile.remark')}</Text>
                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <TouchableOpacity disabled={this.state.voucher_select == 0} style={{flex:1,justifyContent:'center',alignItems:'center',opacity:this.state.voucher_select == 0?0.5:1}} onPress={()=> this.removeVoucher()}>
                                        <Icon name='caret-left' type='FontAwesome5' style={{color:BLACK}}/>
                                    </TouchableOpacity>
                                    <Text style={{flex:1,textAlign:'center',marginVertical:dim.height/100*1.5,fontSize:SIZE_6,marginHorizontal:dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{this.state.voucher_select}</Text>
                                    <TouchableOpacity disabled={this.state.voucher_select == this.state.maxVoucher} style={{flex:1,justifyContent:'center',opacity:this.state.voucher_select == this.state.maxVoucher?0.5:1,alignItems:'center'}} onPress={()=> this.addVoucher()}>
                                        <Icon name='caret-right' type='FontAwesome5' style={{color:BLACK}}/>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*30}} onPress={()=>{this.setState({modalRedeem:false})}}>
                                        <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.cancel')}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity disabled={this.state.voucher_select == 0} style={{borderColor:BG_1,opacity:this.state.voucher_select == 0?0.5:1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*30}} onPress={()=>{this.onexchange()}}>
                                        <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                    </TouchableOpacity>
                                </View>
                                
                            </View>
                        </View>
                    </Modal>
                    <Modal visible={this.state.modalSuccess} transparent={true}>
                        <View  style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <View style={styles.contain_icon_success}>
                                    <Icon name='check' type='FontAwesome5' style={styles.icon_success}/>
                                </View>
                                <Text style={styles.modal_text}>{I18n.t('tab.profile.alert.exchangesuccess')}</Text>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} 
                                    onPress={()=>{
                                        this.setState({modalSuccess:false})
                                        var data = {
                                            language: I18n.locale
                                        }
                                        this.props.getProfileInfo(data)
                                    }}
                                >
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_6,marginTop:dim.height/100*1.5}]}>{I18n.t('tab.profile.point')}</Text>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                        <View style={[styles.Container_row,{flex:0,width:dim.width/100*35,padding:dim.width/100*3}]}>
                            <Text style={[styles.txt_h,{fontSize:SIZE_3_5}]}>{I18n.t('tab.profile.my_voucher')}</Text>
                        </View>
                        <View style={{flex:1,padding:dim.width/100*3}} onPress={()=> this.onChangeLanguage('th')}>
                            <Text style={[styles.txt_d,{textAlign:'left',fontSize:SIZE_4}]}>{this.state.voucher.length}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={[styles.Container_row,{flex:0,width:dim.width/100*35,padding:dim.width/100*3}]}>
                            <Text style={[styles.txt_h,{fontSize:SIZE_3_5}]}>{I18n.t('tab.profile.my_point')}</Text>
                        </View>
                        <TouchableOpacity style={{flex:1,padding:dim.width/100*3}} >
                            <Text style={[styles.txt_d,{textAlign:'left',fontSize:SIZE_4}]}>{this.state.data.score==null||this.state.data.score==''?0:this.state.data.score}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn,{flex:3}]} onPress={()=> this.onRedeemVoucher()}>
                            <Text numberOfLines={1} style={[styles.txt_d]}>{I18n.t('tab.profile.redeem_voucher')}</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    modal_text:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    icon_error:{color:RED_COLOR},
    icon_success:{color:'#26de81'},
    contain_icon_error:{
        borderColor:RED_COLOR,
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    contain_icon_success:{
        borderColor:'#26de81',
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    modal_contain:{
        backgroundColor:'#fff',
        width:dim.width/100*80,
        borderRadius:dim.width/100*3,
        alignItems:'center',
        padding:dim.width/100*5
    },
    modal_container:{
        width: "100%",
        height: "100%",
        position: "absolute",
        backgroundColor:'black',
        opacity:0.7
    },
    btn:{
        padding:dim.width/100*1.5,
        alignItems:'center',
        borderColor:BG_2,
        borderWidth:1,
        borderRadius:dim.width/100*1.5,
        margin:dim.width/100*1.5
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    txt_h:{
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
        
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
        exchange_success:state.voucher.exchange_success,
        exchange_success_num:state.voucher.exchange_success_num,
        voucher:state.profile.voucher,
        voucher_num:state.profile.voucher_num,
    }
  }

export default connect(mapStateToProps,actions)(ProfilePoint)