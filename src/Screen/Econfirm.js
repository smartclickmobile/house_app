import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK,BG_2 } from '../constant/color'
import { Container, Content,Form,Item,Input,Label,Picker } from 'native-base';
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH, TEXT_BODY_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3_7,SIZE_5,SIZE_5_5 } from '../constant/font'
const dim = Dimensions.get('window');
class Econfirm extends Component {
    constructor(props){
        super(props)
        this.state = {
           
        }
    }

    render(){
        return(
            <Container>
                <Header btnleft={true}/>
                <Content>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_5_5,marginTop:dim.height/100*1.5}]}>{I18n.t('Econfirm.confirm')}</Text>
                    <Text style={[styles.text_btn_a,{marginTop:dim.height/100*1.5,fontSize:SIZE_5}]}>{I18n.t('Econfirm.sent_otp')}</Text>
                    <Text style={[styles.text_btn,{marginTop:dim.height/100*3}]}>{`E-MAIL : SIRIYAKON.THAIKON@GMAIL.COM`}</Text>
                    <View style={{alignItems:'center'}}>
                        <Form style={{width:Platform.isPad?dim.width/100*40:dim.width/100*70,marginTop:dim.height/100*3}}>
                            <Item style={styles.item} floatingLabel >
                                <Label style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Econfirm.code')}</Label>
                                <Input autoCapitalize='none' style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}/>
                            </Item>
                        </Form>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> Actions.pop()}>
                                <Text style={styles.text}>{I18n.t('Econfirm.back')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> Actions.Epayment()}>
                                <Text style={styles.text}>{I18n.t('Econfirm.next')}</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn:{
        padding:dim.width/100*1.5,
        alignItems:'center',
        borderColor:BG_2,
        borderWidth:1,
        borderRadius:dim.width/100*1.5,
        margin:dim.width/100*1.5
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }

export default connect(mapStateToProps,actions)(Econfirm)