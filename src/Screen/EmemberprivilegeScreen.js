import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content, Icon,Container, CardItem, Left, Body, Right, Thumbnail,Card } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { 
    TEXT_TH,
    TEXT_BODY_EN,
    TEXT_HEADER_EN, 
    TEXT_TH_BOLD, 
    TEXT_BODY_EN_BOLD,
    SIZE_3,
    SIZE_3_5,
    SIZE_4,
    SIZE_3_7,
    SIZE_2_9,
    SIZE_6,
    SIZE_4_5
 } from '../constant/font'
import Header from '../Component/header'
const dim = Dimensions.get('window');
class EmemberprivilegeScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            data:this.props.profile_info.gift,
           
        }
    }
    componentDidMount(){
      
    }
    componentWillReceiveProps(nextProps){
        
    }
    
    render(){
        // console.log('is_receive',this.state.data.is_receive)
        return(
            <Container>
                <Header btnleft={true}/>
                <Content style={{marginTop:0,padding:dim.width/100*2}}>
                    
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_6,marginTop:dim.height/100*1.5}]}>{I18n.t('tab.profile.ememberprivilege')}</Text>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                        <Text style={[styles.txt_h,{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('tab.profile.merchadise')}</Text>
                    </View>
                    <Card>
                        <CardItem>
                            <Left style={{flex:0}}>
                                {
                                    this.state.data.thumbnail_mobile_path!=null&&this.state.data.thumbnail_mobile_path!=''&&this.state.data.thumbnail_mobile_path!=undefined?
                                    <Thumbnail large source={{uri: this.state.data.thumbnail_mobile_path}} />
                                    :
                                    <Thumbnail large source={require('../../asset/Images/no.png')} />
                                    
                                }
                            </Left>

                            <Body style={{paddingVertical:dim.height/100*1,paddingHorizontal:dim.width/100*2}}>
                                <Text style={[styles.txt_h,{fontSize:SIZE_4_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{this.state.data.name}</Text>
                                <Text style={[styles.txt_h,{fontSize:SIZE_3_7,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.data.description}</Text>
                            </Body>

                            <Right style={{flex:0}}>
                            {
                                this.state.data.is_receive?<Text style={[styles.txt_h,{fontSize:SIZE_4}]}>{I18n.t('tab.profile.received')}</Text>:<Text style={[styles.txt_h,{fontSize:SIZE_4}]}>{I18n.t('tab.profile.receive')}</Text>
                            }
                                
                            </Right>
                        </CardItem>
                    </Card>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                        <Text style={[styles.txt_h,{fontSize:SIZE_3_5}]}>{I18n.t('tab.profile.voucher_p')}</Text>
                    </View>
                    <Card>
                        <CardItem>
                            <Left style={{flex:0}}> 
                                <Thumbnail large source={require('../../asset/Images/vo.png')} />
                            </Left>

                            <Body style={{paddingVertical:dim.height/100*1,paddingHorizontal:dim.width/100*2,justifyContent:'center'}}>
                                <Text style={[styles.txt_h,{fontSize:SIZE_4_5}]}>{I18n.t('EmemberprivilegeScreen.voucher_p')}</Text>
                            </Body>

                            <Right style={{flex:0}}>
                            </Right>
                        </CardItem>
                    </Card>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                        <Text style={[styles.txt_h,{fontSize:SIZE_3_5}]}>{I18n.t('tab.profile.buffet_p')}</Text>
                    </View>
                    <Card>
                        <CardItem>
                            <Left style={{flex:0}}>
                                {
                                    <Thumbnail large source={require('../../asset/Images/buf.png')} />
                                }
                            </Left>

                            <Body style={{paddingVertical:dim.height/100*1,paddingHorizontal:dim.width/100*2}}>
                                <Text style={[styles.txt_h,{fontSize:SIZE_4_5}]}>{I18n.t('EmemberprivilegeScreen.buffet')}</Text>
                                <Text style={[styles.txt_h,{fontSize:SIZE_3_7,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('EmemberprivilegeScreen.detail_buffet')}</Text>
                            </Body>

                            <Right style={{flex:0}}>
                            </Right>
                        </CardItem>
                    </Card>
                    <View style={{flexDirection:'row',marginTop:dim.height/100*3}}>
                    <Text style={[styles.txt_h,{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('tab.profile.discount')}</Text>
                </View>
                <Card>
                    <CardItem>
                        <Left style={{flex:0}}>
                            <Thumbnail large source={require('../../asset/Images/dif.png')} />
                        </Left>

                        <Body style={{paddingVertical:dim.height/100*1,paddingHorizontal:dim.width/100*2}}>
                            <Text style={[styles.txt_h,{fontSize:SIZE_4_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}]}>{I18n.t('EmemberprivilegeScreen.discount')}</Text>
                            <Text style={[styles.txt_h,{fontSize:SIZE_3_7,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('EmemberprivilegeScreen.detail_discount')}</Text>
                        </Body>

                        <Right style={{flex:0}}>
                        </Right>
                    </CardItem>
                </Card>
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    modal_text:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    icon_error:{color:RED_COLOR},
    icon_success:{color:'#26de81'},
    contain_icon_error:{
        borderColor:RED_COLOR,
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    contain_icon_success:{
        borderColor:'#26de81',
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    modal_contain:{
        backgroundColor:'#fff',
        width:dim.width/100*80,
        borderRadius:dim.width/100*3,
        alignItems:'center',
        padding:dim.width/100*5
    },
    modal_container:{
        width: "100%",
        height: "100%",
        position: "absolute",
        backgroundColor:'black',
        opacity:0.7
    },
    btn:{
        padding:dim.width/100*1.5,
        alignItems:'center',
        borderColor:BG_2,
        borderWidth:1,
        borderRadius:dim.width/100*1.5,
        margin:dim.width/100*1.5
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    txt_h:{
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
        
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
        exchange_success:state.voucher.exchange_success,
        exchange_success_num:state.voucher.exchange_success_num,
        voucher:state.profile.voucher,
        voucher_num:state.profile.voucher_num
    }
  }

export default connect(mapStateToProps,actions)(EmemberprivilegeScreen)