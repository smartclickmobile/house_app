import React, { Component } from "react"
import {
    TouchableOpacity,
    Dimensions,
    View,
    StyleSheet,
    Image,
    Text,
    Platform,
    ScrollView,

    Modal,
    Alert
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BG_2, BLACK } from '../constant/color'
import Header1 from '../Component/headerMovie'
import { Container,Header, Icon } from "native-base";
import DeviceInfo from "react-native-device-info"
import AsyncStorage from '@react-native-async-storage/async-storage';
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { TEXT_BODY_EN, TEXT_TH, TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD, TEXT_HEADER_EN_BOLD,SIZE_3,SIZE_3_5,SIZE_4,SIZE_5, SIZE_2_3 } from "../constant/font";

const dim = Dimensions.get('window');
class BookingSelectSeatScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            showtime_detail:null,
            seat_row_num:0,
            seat_col_num:0,
            selectSeat : [],
            select_string:'',
            total_price_regular:0,
            membertype:'',

            walkway_row:[],
            walkway_col:[],

            voucher_amount:0,
            voucher_max:this.props.voucher==null?0:this.props.voucher.length,
            voucher_list_all:this.props.voucher==null?[]:this.props.voucher,

            loading:false,
            check:false,
            re_num:null
        }
    }
    async componentDidMount(){
        console.warn('this.props.time_data.showtimes_id',this.props.time_data.showtimes_id)
        var membertype = await AsyncStorage.getItem('membertype')
        this.setState({membertype:membertype})
        var temp = {
            showtimes_id:this.props.time_data.showtimes_id,
            language:I18n.locale
        }
        this.props.getShowtimeDetail(temp)
        var data={
            language:I18n.locale
        }
        this.props.GetCreditCard(data)
    }
    componentDidUpdate(){
        if(this.props.re && this.props.re_num != this.state.re_num){
            this.setState({select_string:'',selectSeat:[],total_price_regular:0,voucher_amount:0,check:true,loading:true,re_num:this.props.re_num},()=>{
                var temp = {
                    showtimes_id:this.props.time_data.showtimes_id,
                    language:I18n.locale
                }
                this.props.getShowtimeDetail(temp)
            })
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.showtimedetail != null && nextProps.showtimedetail){
            this.setState({showtime_detail:nextProps.showtimedetail,seat_row_num:nextProps.showtimedetail.row_seats.length,loading:false},()=>{
                var num = 0
                for(var i = 0 ; i < nextProps.showtimedetail.row_seats.length ; i++){
                    if(nextProps.showtimedetail.row_seats[i].seats.length > num){
                        num = nextProps.showtimedetail.row_seats[i].seats.length
                    }
                }
                this.setState({seat_col_num:num})
                var row = nextProps.showtimedetail.walkway_row
                var col = nextProps.showtimedetail.walkway_col
                if(row!=''){
                    var row_re = row.replace(" ", "")
                    var row_split = row_re.split(",")
                    this.setState({walkway_row:row_split})
                }
                if(col!=''){
                    var col_re = col.replace(" ", "")
                    var col_split = col_re.split(",")
                    this.setState({walkway_col:col_split})
                }
            })
        }
    }
    onReserve(){
        var arr = []
            for(var i = 0 ; i<this.state.selectSeat.length; i++){
                arr.push(this.state.selectSeat[i].showtimes_seat_id)
            }
            var price = {
                total_price_regular:this.state.total_price_regular,
                voucher_amount:this.state.voucher_amount,
                voucher_max:this.state.voucher_max,
                voucher_list_all:this.state.voucher_list_all
            }
            var seat = {
                select_string: this.state.select_string,
                selectSeat: this.state.selectSeat
            }
        if(this.props.time_data.is_buffet == false){
            var data = {
                showtime_detail:this.state.showtime_detail,
                showtimes_seat_id:arr,
                platform_id:2,
                device_id:DeviceInfo.getDeviceId(),
                language:I18n.locale,
    
                data:this.props.data,
                time_data:this.props.time_data,
                price:price,
                seat:seat
            }
            this.setState({check:false},()=>{
                this.props.bookingReserve(data)
            })
        }else{
            var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
            var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
            var date = moment(this.props.start,'YYYY-MM-DD HH:mm:ss')
            var day = date.date()
            var month = date.month()
            var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
            var year = date.year()
            var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)

            Alert.alert("Confirm Booking",`${this.props.data.title} ${day} ${monthtxt} ${yeartxt} ${moment(this.props.time_data.start_time,"YYY-MM-DD HH:mm:ss").format("HH:mm")}`,[
                {
                    text: 'CONFIRM',
                        onPress: () => {
                            var data = {
                                showtime_detail:this.state.showtime_detail,
                                showtimes_seat_id:arr,
                                platform_id:2,
                                device_id:DeviceInfo.getDeviceId(),
                                language:I18n.locale
                            }
                            this.setState({check:false},()=>{
                                this.props.bookingReserveBuffet(data)
                            })
                        }
                },
                {
                    text: 'CANCEL'
                }
            ])
     
        }
    }
    removeVoucher(){
        if(parseInt(this.state.voucher_amount) != 0){
            this.setState({voucher_amount:parseInt(this.state.voucher_amount)-1})
        }
    }
    addVoucher(){
        if(parseInt(this.state.voucher_amount) < parseInt(this.state.voucher_max)){
            this.setState({voucher_amount:parseInt(this.state.voucher_amount)+1})
        }
    }
    selectSeat(seat){
        console.warn('seat',seat)
        var data_seat = {
            showtimes_seat_id:seat.showtimes_seat_id,
            seat_id:seat.seat_id,
            row:seat.row,
            col_no:seat.col_no,
            normal_price:seat.normal_price,
            member_price:seat.member_price,
            different:parseInt(seat.normal_price) - parseInt(seat.member_price),
            status:seat.status
        }
        var seat_list = this.state.selectSeat
        var index_delete = 0
        var check_delete = false
        if(this.props.time_data.is_buffet == true){
            seat_list = []
            seat_list.push(data_seat)
        }else{
            if(seat_list.length == 0){
                seat_list.push(data_seat)
            }else{
                for(var i = 0;i<seat_list.length;i++){
                    if(seat.showtimes_seat_id == seat_list[i].showtimes_seat_id){
                        // seat_list.splice(i,1)
                        index_delete = i
                        check_delete = true
                    }
                }
                if(check_delete){
                    seat_list.splice(index_delete,1)
                }else{
                    seat_list.push(data_seat)
                }
            }
        }
        
        
        this.setState({selectSeat:seat_list},()=>{
            var select_string = ''
            var price = 0
            for(var j = 0 ; j < this.state.selectSeat.length ; j++){
                if(j == 0){
                    select_string += this.state.selectSeat[j].row + this.state.selectSeat[j].col_no
                    price += parseFloat(this.state.selectSeat[j].normal_price)
                }else{
                    select_string += ","+this.state.selectSeat[j].row + this.state.selectSeat[j].col_no
                    price += parseFloat(this.state.selectSeat[j].normal_price)
                }
            }
            this.setState({select_string:select_string,total_price_regular:price})
        })
        // seat_list.push(seat.showtimes_seat_id)
    }
    
    _renderBooking(){
        console.warn('_renderBooking',this.props.time_data.is_buffet)
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date = moment(this.props.start,'YYYY-MM-DD HH:mm:ss')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        return(
            <View style={{marginTop:0,flex:1}}>
            <View style={{flexDirection:'row',marginTop:dim.height/100*2}}>
                <View style={{flex:1}}>
                    <View style={{width:dim.width/100*49,height:dim.height/100*15,backgroundColor:BG_1,opacity:0.3}}/>
                    <View style={{width:dim.width/100*49,height:dim.height/100*15,marginTop:-(dim.height/100*15),flexDirection:'row'}}>
                        <Image
                            source={{uri:this.props.data.poster_mobile_path}}
                            style={{width:dim.width/100*17,height:dim.height/100*15}}
                        />
                        
                        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{textAlign:'center',fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BG_1}}>{this.props.data.title}</Text>
                            <View style={{marginVertical:dim.height/100*0.5,flexDirection:'row'}}>
                                <View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                                    <Text style={{color:BG_1,fontSize:SIZE_2_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.show_time} ${I18n.t('BookingSelectTimeScreen.mins')}`}</Text>
                                </View>
                                {this.props.data.language!= null &&<View style={{borderRightColor:BG_1,borderRightWidth:1,paddingHorizontal:dim.width/100*1}}>
                                    <Text style={{color:BG_1,fontSize:SIZE_2_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${this.props.data.language}`}</Text>
                                </View>}
                                <View style={{paddingHorizontal:dim.width/100*1}}>
                                    <Text style={{color:BG_1,fontSize:SIZE_2_3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`R : ${this.props.data.rate_name}`}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{flex:1,alignItems:'flex-end'}}>
                    <View style={{width:dim.width/100*49,height:dim.height/100*15,backgroundColor:BG_1,opacity:0.3}}/>
                    <View style={{width:dim.width/100*49,height:dim.height/100*15,marginTop:-(dim.height/100*15),alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:BG_1,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${I18n.t('BookingSelectSeatScreen.date')} - ${day} ${monthtxt} ${yeartxt}`}</Text>
                        <Text style={{color:BG_1,fontSize:SIZE_5,fontFamily:TEXT_HEADER_EN_BOLD,marginTop:dim.height/100*1}}>{moment(this.props.time_data.start_time,"YYY-MM-DD HH:mm:ss").format("HH:mm")}</Text>
                    </View>
                </View>
            </View>
            
            <ScrollView style={{flex:3,padding:dim.width/100*5}}>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1,color:BG_1,fontSize:SIZE_3_5,marginTop:dim.height/100*0.5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}} numberOfLines={4}>{this.state.showtime_detail!=null?this.state.showtime_detail.theater_name:''}</Text>
                    <Text style={{flex:1,color:BG_1,fontSize:SIZE_3_5,marginTop:dim.height/100*0.5,textAlign:'right',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}} numberOfLines={4}>{I18n.t('BookingSelectSeatScreen.screen')}</Text>
                </View>
                <View style={{height:dim.height/100*1,backgroundColor:BG_1,opacity:0.3,marginBottom:dim.height/100*5}}/>
                {
                    this.state.showtime_detail!=null&&this.state.showtime_detail.row_seats.map((item,index)=>{
                        // render row
                        var check_row = false
                        for(var r = 0 ; r < this.state.walkway_row.length; r++){
                            if( this.state.walkway_row[r]==item.row){
                                check_row = true
                            }
                        }
                        return(
                            <View key={index} style={{flexDirection:'row',justifyContent:'center',marginTop:check_row?5:0}}>
                                <View style={{width:dim.width/(this.state.seat_col_num+6.2),alignItems:'center',height:index==0?2*(dim.width/(this.state.seat_col_num+6.2)):dim.width/(this.state.seat_col_num+6.2),justifyContent:'center',marginTop:3,marginRight:3}}>
                                    <Text style={{flex:1,color:BG_1,fontFamily:TEXT_BODY_EN,fontSize:SIZE_3,marginTop:index==0?dim.width/(this.state.seat_col_num+6.2):0}}>{item.row}</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    {
                                        item.seats.map((item2,index2)=>{
                                            var color = BG_1
                                            var disabled = false
                                            if(item2.status == 'ACTIVATE'){
                                                for(var j = 0 ;j<this.state.selectSeat.length;j++){
                                                    if(this.state.selectSeat[j].showtimes_seat_id === item2.showtimes_seat_id){
                                                        color = "#efad0e"
                                                    }
                                                }
                                                if(this.props.time_data.is_buffet == true && this.state.selectSeat.length >= 2){
                                                    disabled = true
                                                }
                                            }else if(item2.status == 'SUSPEND'){
                                                color = ""
                                                disabled = true
                                            }
                                            else{
                                                // color = "#555"
                                                color = "#555"
                                                disabled = true
                                            }
                                            var check_col = false
                                            for(var c = 0 ; c < this.state.walkway_col.length; c++){
                                                if( this.state.walkway_col[c]==item2.col_no){
                                                    check_col = true
                                                }
                                            }
                                            return(
                                                <View>
                                                    {index==0?<Text style={{textAlign:'center',color:BG_1,fontFamily:TEXT_HEADER_EN,fontSize:SIZE_3,height:dim.width/(this.state.seat_col_num+6.2),width:dim.width/(this.state.seat_col_num+6.2),marginRight:3}}>{item2.col_no}</Text>:<View/>}
                                                    <TouchableOpacity disabled={disabled} key={index2} onPress={()=> this.selectSeat(item2)} style={{opacity:0.5,width:dim.width/(this.state.seat_col_num+6.2),backgroundColor:color,alignItems:'center',height:dim.width/(this.state.seat_col_num+6.2),justifyContent:'center',marginTop:3,marginRight:3,marginLeft:check_col?5:0}}>
                                                        <Text></Text>
                                                    </TouchableOpacity>
                                                    {index==this.state.showtime_detail.row_seats.length-1?<Text style={{marginTop:12,textAlign:'center',color:BG_1,fontFamily:TEXT_HEADER_EN,fontSize:SIZE_3,height:dim.width/(this.state.seat_col_num+6.2),width:dim.width/(this.state.seat_col_num+6.2),marginRight:3}}>{item2.col_no}</Text>:<View/>}
                                                </View>
                                                
                                            )
                                        })
                                    }
                                </View>
                                <View style={{width:dim.width/(this.state.seat_col_num+6.2),alignItems:'center',height:index==0?2*(dim.width/(this.state.seat_col_num+6.2)):dim.width/(this.state.seat_col_num+6.2),justifyContent:'center',marginTop:3,marginRight:3}}>
                                    <Text style={{flex:1,color:BG_1,fontFamily:TEXT_BODY_EN,fontSize:SIZE_3,marginTop:index==0?dim.width/(this.state.seat_col_num+6.2):0}}>{item.row}</Text>
                                </View>
                            </View>
                        )
                    })
                }
            </ScrollView>
            {this.state.selectSeat.length>0 && this.props.time_data.is_buffet == false?<View>
                <View style={{backgroundColor:BG_1,height:dim.height/100*20,opacity:0.3}}/>
                <View style={{height:dim.height/100*20,marginTop:-(dim.height/100*20),padding:dim.width/100*2.5}}>
                    <View style={{flexDirection:'row',borderBottomColor:BG_1,borderBottomWidth:1,paddingVertical:dim.height/100*1,height:dim.height/100*10}}>
                        <View style={{width:dim.width/100*60}}>
                            <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{I18n.t('BookingSelectSeatScreen.select_seat')}</Text>
                            <View style={{height:dim.height/100*5,justifyContent:'center',width:dim.width/100*60}}>
                                <Text style={{fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BG_1}}>{this.state.select_string}</Text>
                            </View>
                        </View>
                        <View style={{width:dim.width/100*30}}>
                            <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1,textAlign:'right'}}>{I18n.t('BookingSelectSeatScreen.total_price')}</Text>
                            <View style={{height:dim.height/100*5,justifyContent:'center'}}>
                                <Text style={{fontSize:SIZE_5,fontFamily:TEXT_HEADER_EN_BOLD,color:BG_1,textAlign:'right'}}>{this.state.total_price_regular}</Text>
                            </View>
                        </View>
                    </View>
                    {this.props.data.is_voucher == 1 ?<View style={{flexDirection:'row',alignItems:'center',height:dim.height/100*10}}>
                        <Text style={{flex:3,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{I18n.t('BookingSelectSeatScreen.voucher')}</Text>
                        <TouchableOpacity onPress={()=> this.removeVoucher()} disabled={this.state.voucher_amount==0} style={{flex:1,alignItems:'center',opacity:this.state.voucher_amount!=0?1:0.5}}>
                            <View style={{width:dim.width/100*7,height:dim.width/100*7,borderRadius:dim.width/100*3.5,backgroundColor:BG_1,opacity:0.3}}/>
                            <View style={{width:dim.width/100*7,height:dim.width/100*7,borderColor:BG_1,borderWidth:1,borderRadius:dim.width/100*3.5,alignItems:'center',justifyContent:'center',marginTop:-(dim.width/100*7)}}>
                                <Icon name='minus' type='FontAwesome5' style={{color:BG_1,fontSize:SIZE_4}}/>
                            </View>
                        </TouchableOpacity>
                        <View style={{alignItems:'center',justifyContent:'center',flex:1}}>
                            <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{this.state.voucher_amount}</Text>
                        </View>
                        <TouchableOpacity onPress={()=> this.addVoucher()} disabled={this.state.voucher_amount==this.state.voucher_max || this.state.voucher_amount== this.state.selectSeat.length} style={{flex:1,alignItems:'center',opacity:this.state.voucher_amount==this.state.voucher_max || this.state.voucher_amount== this.state.selectSeat.length?0.5:1}}>
                            <View style={{width:dim.width/100*7,height:dim.width/100*7,borderRadius:dim.width/100*3.5,backgroundColor:BG_1,opacity:0.3}}/>
                            <View style={{width:dim.width/100*7,height:dim.width/100*7,borderColor:BG_1,borderWidth:1,borderRadius:dim.width/100*3.5,alignItems:'center',justifyContent:'center',marginTop:-(dim.width/100*7)}}>
                                <Icon name='plus' type='FontAwesome5' style={{color:BG_1,fontSize:SIZE_4}}/>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:2,alignItems:'center',justifyContent:'center',borderColor:BG_1,borderWidth:1,paddingVertical:dim.height/100*1}} onPress={()=> this.onReserve()}>
                            <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{I18n.t('BookingSelectSeatScreen.next')}</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <TouchableOpacity style={{flex:2,alignItems:'center',justifyContent:'center',borderColor:BG_1,borderWidth:1,paddingVertical:dim.height/100*1}} onPress={()=> this.onReserve()}>
                        <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{I18n.t('BookingSelectSeatScreen.next')}</Text>
                    </TouchableOpacity>
                    }
                    
                </View>
            </View>:<View/>}

            {this.state.selectSeat.length>0 && this.props.time_data.is_buffet == true?<View>
                <View style={{backgroundColor:BG_1,height:dim.height/100*20,opacity:0.3}}/>
                <View style={{height:dim.height/100*20,marginTop:-(dim.height/100*20),padding:dim.width/100*2.5}}>
                    <View style={{flexDirection:'row',borderBottomColor:BG_1,borderBottomWidth:1,paddingVertical:dim.height/100*1,height:dim.height/100*10}}>
                        <View style={{width:dim.width/100*60}}>
                            <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{I18n.t('BookingSelectSeatScreen.select_seat')}</Text>
                            <View style={{height:dim.height/100*5,justifyContent:'center',width:dim.width/100*60}}>
                                <Text style={{fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,color:BG_1}}>{this.state.select_string}</Text>
                            </View>
                        </View>
                    </View>
                    {
                    <TouchableOpacity style={{flex:2,alignItems:'center',justifyContent:'center',borderColor:BG_1,borderWidth:1,paddingVertical:dim.height/100*1}} onPress={()=> this.onReserve()}>
                        <Text style={{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:BG_1}}>{I18n.t('BookingSelectSeatScreen.confirm_booking')}</Text>
                    </TouchableOpacity>
                    }
                    
                </View>
            </View>:<View/>}
        </View>
        )
    }
    render(){
       
        return(
            <Container>
                <Modal visible={this.state.loading} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View 
                        style={{ width: "100%",
                            height: "100%",
                            position: "absolute",
                            backgroundColor:'black',
                            opacity:0.7}}>
                    </View>
                    <View style={{width: "100%",height: "100%",alignItems:'center',justifyContent:'center'}}>

                    </View>
                    
                </Modal>
                <Image 
                    source={{uri:this.props.data.background_mobile_path==null?this.props.data.poster_mobile_path:this.props.data.background_mobile_path}}
                    style={{
                        position:'absolute',
                        width:dim.width/100*300,
                        height:dim.height/100*300,
                        alignSelf:'center',
                        marginTop:-(dim.height/100*100),
                        backgroundColor:'#000000'
                    }}
                    blurRadius={Platform.OS == 'ios'?7:4}
                />
                <Header
                    style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'transparent',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11,
                        opacity:0.3,
                        position:'absolute'
                    }}
                />
                {<Header1 leftbutton={true} transparent1={true}/>}
                {this._renderBooking()}
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        booking:state.check.booking,

        showtime:state.booking.showtime,
        showtimedetail:state.booking.showtimedetail,

        voucher:state.profile.voucher,
    }
  }
  var styles = StyleSheet.create({
  
  });
export default connect(mapStateToProps,actions)(BookingSelectSeatScreen)