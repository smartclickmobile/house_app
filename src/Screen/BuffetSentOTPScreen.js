import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content,Container } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD } from '../constant/font'
import Header from '../Component/header'
import { WebView } from 'react-native-webview';
import { URL_IP,URL_PROD_IP } from '../constant/constant'
const dim = Dimensions.get('window');
class BuffetSentOTPScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            data:this.props.profile_info,
            language:I18n.locale,
            
        }
    }
    componentDidMount(){
       
    }
    componentWillReceiveProps(nextProps){
        
    }
    _onNavigationStateChange(webViewState){
        console.log(webViewState.url)
        if(webViewState.url == URL_PROD_IP || webViewState.url == URL_IP){
            Actions.popTo('home') 
            Actions.BuffetSuccessScreen({
                event_buffet_id:this.props.event_buffet_id
            })
        }
      }
    render(){
        var data = this.props.data
        
        var data_h = data.split("<!DOCTYPE html>")
        var data_t = data_h[1].split("</html>")
        
        var data_html = `${data_t[0]}</html>`
        var data_html_n = data_html.replace(/\\n|\\/gi,'')
        var temp = data_html_n.replace(/'/gi,'"')
       
        console.log('temp',temp)
        return(
         
                <WebView 
                    source={{html:temp}}
                    originWhitelist={['*']}
                    onNavigationStateChange={this._onNavigationStateChange.bind(this)}
                    domStorageEnabled={true}
                    javaScriptEnabled={true}
                    onError={(error)=> console.log('error',error)}
                    onLoad={() => console.log('load webview')}
                    javaScriptEnabledAndroid={true}
                />
           
        )
        
    }
}
var styles = StyleSheet.create({
   
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        
    }
  }

export default connect(mapStateToProps,actions)(BuffetSentOTPScreen)