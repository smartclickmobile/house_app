import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../constant/color'
import { Content,Container, Card, CardItem, Body } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3,SIZE_3_5,SIZE_3_7,SIZE_2_9,SIZE_6 } from '../constant/font'
import Header from '../Component/header'
import moment from 'moment'
const dim = Dimensions.get('window');
class Profilepage extends Component {
    constructor(props){
        super(props)
        this.state = {
            data:this.props.profile_info,
            language:I18n.locale,
            
        }
    }
    componentDidMount(){
       
    }
    componentWillReceiveProps(nextProps){
         
    }
    render(){
        console.log('data',this.state.data)
        var month_th = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พิศจิการยน","ธันวาคม"]
        var month_en = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]
        var date = moment(this.state.data.birth_date,'YYYY-MM-DD')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        return(
            <Container>
                <Header btnleft={true}/>
                <Content style={{marginTop:0,padding:dim.width/100*2}}>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_6,marginVertical:dim.height/100*1.5}]}>PROFILE</Text>
                    <Card>
                        <CardItem>
                            <Body>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.name')}:  `}</Text>
                                    {<Text style={[styles.txt_d]}>{this.state.data.title == null?'':this.state.data.title.toUpperCase()}</Text>}
                                    <Text style={[styles.txt_d]}>   </Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.first_name== null?'':this.state.data.first_name.toUpperCase()}</Text>
                                    <Text style={[styles.txt_d]}>   </Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.last_name== null?'':this.state.data.last_name.toUpperCase()}</Text>
                                </View>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.citizen')}:  `}</Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.citizen_number}</Text>
                                </View>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.birthday')}:  `}</Text>
                                    <Text style={[styles.txt_d]}>{day}</Text>
                                    <Text style={[styles.txt_d]}>   </Text>
                                    <Text style={[styles.txt_d]}>{monthtxt}</Text>
                                    <Text style={[styles.txt_d]}>   </Text>
                                    <Text style={[styles.txt_d]}>{yeartxt}</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <Body>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.age')}:  `}</Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.age}</Text>
                                </View>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.gender')}:  `}</Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.gender==null?'':this.state.data.gender.toUpperCase()}</Text>
                                </View>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.telephone')}:  `}</Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.telephone}</Text>
                                </View>
                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginVertical:dim.height/100*1}}>
                                    <Text style={[styles.txt_h]}>{`${I18n.t('profileView.email')}:  `}</Text>
                                    <Text style={[styles.txt_d]}>{this.state.data.email==null?'':this.state.data.email.toUpperCase()}</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>
                    {/*<TouchableOpacity style={{marginVertical:dim.height/100*1,alignItems:'center'}}>
                        <Text style={[styles.txt_h,{fontSize:SIZE_3_5}]}>EDIT PROFILE</Text>
                    </TouchableOpacity>*/}
                    
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    txt_d_select:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    txt_h:{
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
    }
  }

export default connect(mapStateToProps,actions)(Profilepage)