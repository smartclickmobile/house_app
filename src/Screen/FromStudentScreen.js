import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Modal,

    Platform,
    Image
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK,BG_2,RED_COLOR } from '../constant/color'
import { Container, Content, Icon,Form,Item,Input,Label,Picker,ListItem,Left,Right,Radio } from 'native-base';
import {CheckBox} from 'react-native-elements'
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN,TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3,SIZE_3_5,SIZE_4,SIZE_3_7,SIZE_2_9,SIZE_4_5,SIZE_5_5 } from '../constant/font'
import DeviceInfo from "react-native-device-info"
import { URL_IP } from '../constant/constant'
const dim = Dimensions.get('window');
import { WebView } from 'react-native-webview';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
class FromStudentScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalError:false,
            modalSuccess:false,
            textError:'',
            titleList:[
                {
                    value:1,
                    name:I18n.t('Eregister.title.mr'),
                    key:'mr'
                },
                {
                    value:2,
                    name:I18n.t('Eregister.title.miss'),
                    key:'ms'
                },
                {
                    value:3,
                    name:I18n.t('Eregister.title.ms'),
                    key:'mrs'
                },
            ],
            
            selected:0,
            selected_name:'',
            first_name:'',
            last_name:'',
            mobile:this.props.profile_info.telephone,
            citizen_id:'',
            student_id:'',
            school_name:'',
            checked:false,
            modalAgree:false,
            image:[],
            image_display:[],
            title_key:'',
            gender_key:'',
            expire_month:'',
            expire_year:'',
            monthList : [
                {
                    value:1,
                    name:'01'
                },
                {
                    value:2,
                    name:'02'
                },
                {
                    value:3,
                    name:'03'
                },
                {
                    value:4,
                    name:'04'
                },
                {
                    value:5,
                    name:'05'
                },
                {
                    value:6,
                    name:'06'
                },
                {
                    value:7,
                    name:'07'
                },
                {
                    value:8,
                    name:'08'
                },
                {
                    value:9,
                    name:'09'
                },
                {
                    value:10,
                    name:'10'
                },
                {
                    value:11,
                    name:'11'
                },
                {
                    value:12,
                    name:'12'
                },
            ],
            yearList:[],
            selected_month:'',
            selected_year:'',
            error_num:null,
            success_num:null
        }
    }
    componentDidMount(){
        if(Platform.OS == 'android'){
            this.setState({
                selected_name:this.state.titleList[0].name,
                selected:this.state.titleList[0].value,
                title_key:this.state.titleList[0].key,
            })
        }
        var currentDate = moment()
        var year = currentDate.year()-1
        var array_year = []
        var y = year
        for(var i=0 ;i<21;i++){
            var data = {
                value:i+1,
                name:((parseInt(y) + 1) ).toString()
            }
            array_year.push(data)
            y = parseInt(y) + 1
        }
        this.setState({yearList:array_year},()=>{
            if(Platform.OS == 'android'){
                this.setState({expire_month:this.state.monthList[0].name,expire_year:this.state.yearList[0].name,selected_month:this.state.monthList[0].value,selected_year:this.state.yearList[0].value})
            }
        })
    }
    componentDidUpdate(pevProps){
        if(this.props.student_error!==null&& pevProps.student_error!==this.props.student_error){
            if(this.props.student_error != null && this.props.student_error != undefined && this.props.student_error != '' && this.props.student_error_num != this.state.error_num){
                this.setState({error_num:this.props.student_error_num},()=>{
                    this.setState({modalError:true,textError:this.props.student_error})
                })
            }
        }
       
        if(this.props.student_success!==null&& pevProps.student_success!==this.props.student_success){
            if(this.props.student_success != null && this.props.student_success != undefined && this.props.student_success != '' && this.props.student_success_num != this.state.success_num){
                this.setState({success_num:this.props.student_success_num},()=>{
                    this.setState({modalSuccess:true})
                })
            }
        }
        
    }
    onValueChangeMonth(value: string) {
        this.setState({
            selected_month: value
        },()=>{
            for(var i = 0 ; i<this.state.monthList.length ; i++){
                if(this.state.selected_month == this.state.monthList[i].value){
                    this.setState({expire_month:this.state.monthList[i].name})
                }
            }
        });
    }
    onValueChangeYear(value: string) {
        this.setState({
            selected_year: value
        },()=>{
            for(var i = 0 ; i<this.state.yearList.length ; i++){
                if(this.state.selected_year == this.state.yearList[i].value){
                    this.setState({expire_year:this.state.yearList[i].name})
                }
            }
        });
    }
    
      handleDatePicked = date => {
        
        var date_now = moment()
        var date_select = moment(date)
        var date_now1 = moment(moment().format('YYYY-MM-DD'))
        var date_select1 = moment(moment(date).format('YYYY-MM-DD'))
        console.log("A date has been picked: ", date_now1<date_select1);
        if(date_now1<date_select1){
            
            this.hideDateTimePicker();
            setTimeout(() => this.setState({modalError:true,textError:I18n.t('Eregister.error.birthday')}),500)
            
        }else{
            var age = date_now.diff(date_select,'years')
            console.log('age',age)
            this.setState({Birthday:moment(date).format('DD/MM/YYYY'),age:age.toString()})
            this.hideDateTimePicker();
        }
        
        
      };
    onValueChangetitle(value: string) {
        this.setState({
          selected: value
        },()=>{
            for(var i = 0 ; i<this.state.titleList.length ; i++){
                if(this.state.selected == this.state.titleList[i].value){
                    this.setState({selected_name:this.state.titleList[i].name,title_key:this.state.titleList[i].key})
                }
            }
        });
    }
    
    onAgree(){
        if(this.state.checked){
            this.setState({checked:!this.state.checked})
        }else{
            this.setState({modalAgree:true})
        }
        
    }
    async onsubmit(){
        if(this.state.selected_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.title')})
        }else if(this.state.first_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.name')})
        }else if(this.state.last_name == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.surname')})
        }
        else if(this.state.mobile == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.mobile')})
        }
        else if(this.state.citizen_id == ''){
            this.setState({modalError:true,textError:I18n.t('Eregister.error.citizen')})
        }
        else if(this.state.student_id == ''){
            this.setState({modalError:true,textError:I18n.t('FromStudentScreen.error.studentID')})
        }
        else if(this.state.school_name == ''){
            this.setState({modalError:true,textError:I18n.t('FromStudentScreen.error.schoolName')})
        }
        else if(this.state.image.length <= 0){
            this.setState({modalError:true,textError:I18n.t('FromStudentScreen.error.image')})
        }
        else if(this.state.expire_month == ''){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.exp_month')})
        }
        else if(this.state.expire_year == ''){
            this.setState({modalError:true,textError:I18n.t('PaymentScreen.error.expire_year')})
        }
        // else if(!this.state.checked){
        //     this.setState({modalError:true,textError:I18n.t('Eregister.error.check')})
        // }
        else{

            var data = {
                language:I18n.locale,
                title:this.state.title_key,
                first_name:this.state.first_name,
                last_name:this.state.last_name,
                citizen_number:this.state.citizen_id,
                student_number:this.state.student_id,
                school_name:this.state.school_name,
                expire_month:this.state.expire_month,
                expire_year:this.state.expire_year,
                document_file:this.state.image
            }

            this.props.registerStudent(data)
        }
        
    }
    selectImage(){
        ImagePicker.showImagePicker( (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
                var temp_image = this.state.image
                var temp_image_display = this.state.image_display
                temp_image.push(response.uri)
                temp_image_display.push('data:image/jpeg;base64,' + response.data)
                this.setState({
                    image:temp_image,
                    image_display:temp_image_display
                })
            }
          });
    }
    deleteImage(index){
        var array_image = this.state.image
        var array_image_display= this.state.image_display
        array_image.splice(index, 1);
        array_image_display.splice(index, 1);
        this.setState({
            image:array_image,
            image_display:array_image_display
        })
    }
    render(){

        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={[styles.text_modal,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={[styles.text_modal_btn,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal visible={this.state.modalSuccess} transparent={true}>
                        <View  style={styles.modal_container}/>
                        <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                            <View style={styles.modal_contain}>
                                <View style={styles.contain_icon_success}>
                                    <Icon name='check' type='FontAwesome5' style={styles.icon_success}/>
                                </View>
                                <Text style={[styles.modal_text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t("FromStudentScreen.success")}</Text>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} 
                                    onPress={()=>{
                                        this.setState({modalSuccess:false})
                                        this.props.clearStudent()
                                        Actions.pop()
                                    }}
                                >
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                <Modal visible={this.state.modalAgree} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*90,height:dim.height/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <TouchableOpacity style={{alignSelf:'flex-end'}}
                                onPress={()=>{
                                    this.setState({modalAgree:false})
                                }}
                            >
                                <Icon name='times'
                                    type='FontAwesome5'
                                    style={{color:BLACK,fontSize:dim.width/100*10,textAlign:'right'}}
                                />
                            </TouchableOpacity>
                            {/*<Text style={[styles.text_modal_bold,{fontSize:SIZE_4_5}]}>{I18n.t('register.term_of_service')}</Text>*/}
                            <WebView
                                style={{width:dim.width/100*80,height:dim.height/100*60,backgroundColor:'transparent',marginTop:dim.height/100*1}}
                                originWhitelist={['*']}
                                source={{ uri: URL_IP+'/term/member' }}
                            />
                            
                        </View>
                    </View>
                </Modal>
                <Header btnleft={true}/>
                <Content>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_5_5,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('FromStudentScreen.register')}</Text>
                    <Text style={[styles.text_btn,{marginTop:dim.height/100*2,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`${I18n.t('Eregister.email')} : ${this.props.profile_info.email}`}</Text>
                    <View style={{alignItems:'center'}}>
                        <Form style={{width:dim.width/100*80,marginTop:dim.height/100*3}}>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.select_tile')}<Text style={{color:'red'}}> *</Text></Label>
                            <Picker
                                placeholder={I18n.t('Eregister.select_tile')}
                                mode="dropdown"
                                iosHeader={I18n.t('Eregister.select_tile')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}

                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChangetitle.bind(this)}
                                >
                                {
                                    this.state.titleList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                    })
                                }
                            </Picker>
                            
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.name')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.first_name = input; }}
                                    onChangeText={(value)=> this.setState({first_name:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.surname')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.last_name = input; }}
                                    onChangeText={(value)=> this.setState({last_name:value})}
                                    autoCapitalize='none'
                                />
                            </Item>
                            
                            {/* <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.mobile')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    disabled={true}
                                    value={this.state.mobile}
                                    style={styles.input_dis}
                                    ref={(input) => { this.mobile = input; }}
                                    onChangeText={(value)=> this.setState({mobile:value})}
                                    keyboardType='name-phone-pad'
                                    maxLength={10}
                                    autoCapitalize='none'
                                />
                            </Item> */}
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel 
                                // style={{marginTop:-(dim.height/100*1)}}
                            >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Eregister.citizen')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.citizen_id = input; }}
                                    onChangeText={(value)=> this.setState({citizen_id:value})}
                                    keyboardType='numeric'
                                    maxLength={13}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel 
                                // style={{marginTop:-(dim.height/100*1)}}
                            >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('FromStudentScreen.studentID')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.student_id = input; }}
                                    onChangeText={(value)=> this.setState({student_id:value})}
                                    keyboardType='numeric'
                                    maxLength={13}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={[styles.item,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]} floatingLabel 
                                // style={{marginTop:-(dim.height/100*1)}}
                            >
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('FromStudentScreen.schoolName')}<Text style={{color:'red'}}> *</Text></Label>
                                <Input
                                    style={[styles.input,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}
                                    ref={(input) => { this.school_name = input; }}
                                    onChangeText={(value)=> this.setState({school_name:value})}
                                    maxLength={13}
                                    autoCapitalize='none'
                                />
                            </Item>
                            
                            <View style={{margin:5,borderColor:BG_2,borderWidth:1,padding:10,borderRadius:5,marginLeft:15}}>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('FromStudentScreen.ImageCard')}<Text style={{color:'red'}}> *</Text></Label>
                                {<TouchableOpacity onPress={()=> this.selectImage()}
                                style={{margin:5,borderColor:BG_2,borderWidth:1,padding:5,borderRadius:5,marginLeft:15,paddingVertical:15,alignItems:'center',justifyContent:'center'}}>
                                    <Text style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>SELECT IMAGE</Text>
                                </TouchableOpacity>}
                                {
                                    this.state.image_display.map((item,index)=>{
                                        return(
                                            <View key={index} style={{alignItems:'center',margin:5}}>
                                                <TouchableOpacity style={{position:'absolute',zIndex:1,backgroundColor:'#ffffff',borderRadius:20,marginTop:-5,alignSelf:'flex-end'}} onPress={()=> this.deleteImage(index)}>
                                                    <Icon name='times-circle' type='FontAwesome5' style={{color:RED_COLOR}}/>
                                                </TouchableOpacity>
                                                <Image
                                                    
                                                    source={{uri:item}}
                                                    style={{
                                                        width:250,
                                                        height:250,
                                                        zIndex:0
                                                    }}
                                                />
                                            </View>
                                        )
                                    })
                                }
                                
                                
                            </View>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('FromStudentScreen.exp_month')}</Label>
                            <Picker
                                placeholder="MM"
                                mode="dropdown"
                                iosHeader={I18n.t('Epayment.select_month')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected_month}
                                onValueChange={this.onValueChangeMonth.bind(this)}
                                >
                                {
                                    this.state.monthList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                        
                                    })
                                }
                            </Picker>
                            <Label style={[styles.label,{marginTop:dim.height/100*2,marginLeft: dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('FromStudentScreen.exp_year')}</Label>
                            <Picker
                                placeholder="YYYY"
                                mode="dropdown"
                                iosHeader={I18n.t('Epayment.select_year')}
                                iosIcon={
                                    <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                                }
                                headerStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                textStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                itemTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                placeholderStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerTitleStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                headerBackButtonTextStyle={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                style={{ width: '95%' ,marginTop:dim.height/100*1,marginLeft: dim.width/100*3,borderWidth:1,borderColor:BG_2}}
                                selectedValue={this.state.selected_year}
                                onValueChange={this.onValueChangeYear.bind(this)}
                                >
                                {
                                    this.state.yearList.map((item,index)=>{
                                        return(
                                            <Picker.Item key={index} label={item.name} value={item.value} />
                                        )
                                        
                                    })
                                }
                            </Picker>
                            
                        </Form>
                        {/* <View style={{flexDirection:'row',marginTop:dim.height/100*3,alignItems:'center',width:dim.width/100*80,justifyContent:'center'}}>
                            <CheckBox
                                ref={(input) => { this.checkb = input; }}
                                // title={I18n.t('register.agree')}
                                checked={this.state.checked}
                                containerStyle={{borderWidth:0,backgroundColor:'transparent'}}
                                textStyle={[styles.label,{fontSize:SIZE_3_5}]}
                                onPress={()=> {
                                    
                                    this.setState({checked:!this.state.checked})
                                }}
                                checkedColor={BLACK}
                            />
                            <View style={{flex:0}}>
                                <Text style={[styles.label,{fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{`${I18n.t('register.agree')} `}</Text>
                                <TouchableOpacity  onPress={()=> this.setState({modalAgree:true})}>
                                    <Text style={[styles.label,{fontSize:SIZE_3_5,textDecorationLine:'underline',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.term_of_service')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View> */}
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> Actions.pop()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.cancel')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn1,{width:dim.width/100*30,margin:dim.width/100*2}]} onPress={()=> this.onsubmit()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('Eregister.next')}</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    Container_row:{
        padding:dim.width/100*3,width:dim.width/100*35
    },
    txt_h:{
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
    },
    btn:{
        padding:dim.width/100*1.5,
        alignItems:'center',
        borderColor:BG_2,
        borderWidth:1,
        borderRadius:dim.width/100*1.5,
        margin:dim.width/100*1.5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_bold:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    input:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        borderRadius:dim.width/100*2,
        paddingLeft:dim.width/100*2,
    },
    input_dis:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        marginVertical:dim.height/100*1,
        borderRadius:dim.width/100*2,
        backgroundColor:'#e1e5e8',
        paddingLeft:dim.width/100*2,
        color:'#888a8c'
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    },
    icon_success:{color:'#26de81'},
    contain_icon_success:{
        borderColor:'#26de81',
        borderWidth:dim.width/100*1,
        width:dim.width/100*15,
        height:dim.width/100*15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:dim.width/100*7.5
    },
    modal_contain:{
        backgroundColor:'#fff',
        width:dim.width/100*80,
        borderRadius:dim.width/100*3,
        alignItems:'center',
        padding:dim.width/100*5
    },
    modal_container:{
        width: "100%",
        height: "100%",
        position: "absolute",
        backgroundColor:'black',
        opacity:0.7
    },
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        student_success:state.student.success,
        student_success_num:state.student.success_num,
        student_error:state.student.error,
        student_error_num:state.student.error_num,
    }
  }

export default connect(mapStateToProps,actions)(FromStudentScreen)