import React, { Component } from "react"
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Dimensions,
    Modal,
    StyleSheet,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2, RED_COLOR } from '../constant/color'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Container,Form,Item,Input,Label,Icon, Content } from 'native-base';
import DeviceInfo from "react-native-device-info"
import I18n from '../../asset/languages/i18n'
import Header from '../Component/header'
import { TEXT_BODY_EN, TEXT_TH,SIZE_3_5,SIZE_4 } from '../constant/font'
const dim = Dimensions.get('window');
class loginScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            username:'',
            password:'',

            username_c:false,
            password_c:false,

            modalError:false,
            textError:''
        }
    }   
 
    
   async onLogin(){
        if(this.state.username == ''){
            this.setState({modalError:true,textError:I18n.t('login.error.username'),username_c:true})
        }else if(this.state.password == ''){
            this.setState({modalError:true,textError:I18n.t('login.error.password'),username_c:false,password_c:true})
        }else{
            var token_fcm = await AsyncStorage.getItem('fcmToken')
            var data = {
                "email":this.state.username,
                "password":this.state.password,
                "device_id":DeviceInfo.getDeviceId(),
                "device_name":DeviceInfo.getDeviceName(),
                "language":I18n.locale,
                "type":3,
                "token_firebase": token_fcm,
                "fromdetail":this.props.fromdetail,
                "data_detail":this.props.data_detail
            }
            this.props.login(data)
        }
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={[styles.text_modal,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={[styles.text_modal_btn,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <LinearGradient colors={[BG_1,BG_1, BG_2]} style={[styles.linearGradient]}>
                    <Header btnleft={true}/>

                    <Content>
                        <View style={{alignItems:'center',justifyContent:'flex-end',flex:1}}>
                            <Image
                                source={require('../../asset/Images/logo.png')}
                                style={{
                                    width:Platform.isPad? dim.width/100*40 : dim.width/100*50,
                                    height:dim.height/100*10,
                                    marginTop:dim.height/100*2
                                }}
                                resizeMode='contain'
                            />
                        </View>

                        <View style={{flex:3,alignItems:'center',marginTop:dim.height/100*3}}>
                            <Form style={{width:Platform.isPad?dim.width/100*50:dim.width/100*80}}>
                                <Item style={styles.item} floatingLabel>
                                    <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('login.username')}</Label>
                                    <Input 
                                        ref={(input) => { this.username = input; }} 
                                        style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                        onChangeText={(value)=>{this.setState({username:value})}}
                                        autoCapitalize='none'
                                        // onSubmitEditing={()=> this.password.focus()}
                                    />
                                </Item>
                                <Item style={styles.item} floatingLabel>
                                    <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('login.password')}</Label>
                                    <Input 
                                        ref={(input) => { this.password = input; }} 
                                        style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                        onChangeText={(value)=>{this.setState({password:value})}}
                                        secureTextEntry={true}
                                        autoCapitalize='none'
                                    />
                                </Item>
                            </Form>
                            <TouchableOpacity style={[styles.btn]} onPress={()=> {this.onLogin()}}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('login.login')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{marginTop:dim.height/100*3}} onPress={()=> Actions.forgetpassword()}>
                                <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('login.forget_password')}</Text>
                            </TouchableOpacity>
                        </View>
                    </Content>
                    
                    
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    btn:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:Platform.isPad?dim.width/100*35:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*2,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
export default connect(mapStateToProps,actions)(loginScreen)