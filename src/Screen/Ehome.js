import React, { Component } from "react"
import {
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1 } from '../constant/color'
import { Container, Content } from 'native-base';
import Header from '../Component/header'
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_TH, TEXT_BODY_EN } from '../constant/font'
const dim = Dimensions.get('window');
class Ehome extends Component {
    constructor(props){
        super(props)
        this.state = {
           
        }
    }
    componentDidMount(){
        var data1={
            language:I18n.locale
        }
        this.props.GetCreditCard(data1)
    }
    render(){
        return(
            <Container>
                <Header btnleft={true}/>
                <Content>
                    <Image
                        source={{uri:this.props.emember_privilege!=null?this.props.emember_privilege[0].thumbnail_mobile_path:''}}
                        style={{
                            width:dim.width,
                            height:dim.width
                        }}
                        resizeMode='contain'
                    />
                    <TouchableOpacity style={[styles.btn1,{alignSelf:'center',justifyContent:'center'}]} onPress={()=> Actions.Eregister()}>
                        <Text style={{marginHorizontal:dim.width/100*1,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('Ehome.btn')}</Text>
                    </TouchableOpacity>
            </Content>
            </Container> 
        )
        
    }
}
var styles = StyleSheet.create({
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    }
  });
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        emember_privilege:state.profile.emember_privilege
    }
  }

export default connect(mapStateToProps,actions)(Ehome)