import React, { Component } from "react"
import {
    Dimensions,
    Image,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,

    Modal,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2, BLACK, RED_COLOR } from '../constant/color'
import { Container, Content,Form,Item,Label,Input,Icon,Title } from "native-base";
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3_5,SIZE_4,TEXT_HEADER_EN,SIZE_3 } from '../constant/font'
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk';

import DeviceInfo from "react-native-device-info"
import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
const dim = Dimensions.get('window');
class PleaseLoginScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            username:'',
            password:'',

            username_c:false,
            password_c:false,

            modalError:false,
            textError:''
        }
    }
 
    async onLoginWithFacebook(){
      // LoginManager.logOut()
      if(Platform.OS=='android'){
        LoginManager.logOut()
        // let result;
        // // try { 
        // //   LoginManager.setLoginBehavior('native_only');
        // //   result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        // // } catch (nativeError) {
        //   try {
        //     // LoginManager.setLoginBehavior('web_only');
        //     LoginManager.setLoginBehavior('web_only');
        //     result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        //   } catch (webError) {
        //     // show error message to the user if none of the FB screens
        //     // did not open
        //   }
        // // }
        // // handle the case that users clicks cancel button in Login view
        
        // if (result!= null && result.isCancelled) {
        //   console.log('result != null',result)
        // } else {
        //   // Create a graph request asking for user information
        //   console.log('result = null',result)
        //   AccessToken.getCurrentAccessToken().then(
        //     (data) => {
        //         const { accessToken } = data
        //         console.log('accessToken',accessToken)
        //         this.initUser(accessToken)
        //     }
        //     )
        // }
        LoginManager.logInWithPermissions(['public_profile']).then(function(result) {
          if (result.isCancelled) {
            console.log("Login Cancelled");
          } else {
            console.log("Login Success permission granted:" + result.grantedPermissions);
               console.log('result = null',result)
                AccessToken.getCurrentAccessToken().then(
                  async (data) => {
                      const { accessToken } = data
                      console.log('accessToken',accessToken)
                      var token_fcm = await AsyncStorage.getItem('fcmToken')
      
                      fetch('https://graph.facebook.com/v2.5/me?fields=email,id&access_token=' + accessToken)
                      .then((response) => response.json())
                      .then((json) => {
                        var data = {
                          "email":json.email,
                          "token_facebook":accessToken,
                          "device_id":DeviceInfo.getDeviceId(),
                          "device_name":DeviceInfo.getDeviceName(),
                          "language":I18n.locale,
                          "type":1,
                          "token_firebase": token_fcm,
                          "facebook_id":json.id
                      }
                      console.log('initUser',data)
                      // this.props.login(data)
                      var data1 = new FormData()
                      data1.append("email", data.email)
                      data1.append("device_id", data.device_id)
                      data1.append("device_name", data.device_name)
                      data1.append("platform_id", 2)
                      data1.append("language", data.language)
                      data1.append("token_firebase", data.token_firebase)
                      var type = ''
                      if(data.type == 1){
                          data1.append("telephone", data.telephone != undefined?data.telephone:'')
                          data1.append("token_facebook", data.token_facebook)
                          data1.append("member_type_id", 1)
                          type = 'fb'
                      }else if(data.type == 2){
                          data1.append("telephone", data.telephone != undefined?data.telephone:'')
                          data1.append("token_gmail", data.token_gmail)
                          data1.append("member_type_id", 1)
                          type = 'gm'
                      }else if(data.type == 3){
                          data1.append("password", data.password)
                          data1.append("member_type_id", 1)
                          type = 'r'
                      }
                      console.log('login new',data1)
                      axios
                          .post(API_IP + "member/login",data1, CONFIG)
                          .then(function (response) {
                              console.log('member/login',response.data.code)
                              if(response.data.code == '0x0000-00000'){
                                  AsyncStorage.setItem('token', response.data.data);
                                  AsyncStorage.setItem('login_type', type);
                                  if(data.type == 1){
                                      AsyncStorage.setItem('token_fb', data.token_facebook);
                                  }else if(data.type == 2){
                                      AsyncStorage.setItem('token_gm', data.token_gmail);
                                  }
                                  if(data.login){
                                      Actions.popTo('loginhome',{login_flag:true})
                                      Actions.home({login_flag:true})
                                  }else{
                                      Actions.home({login_flag:true})
                                  }
                                  
                                  
                              }else if(response.data.code == '0x0API-00004'){
                                  if(data.type == 1){
                                      var val = {
                                          email:data.email,
                                          token_facebook:data.token_facebook
                                      }
                                  }else if(data.type == 2){
                                      var val = {
                                          email:data.email,
                                          token_gmail:data.token_gmail
                                      }
                                  }
                                  Actions.confirmmobile({data:val,login_t:type})
                              }
                              else{
                                  // GoogleSignin.revokeAccess();
                                  GoogleSignin.signOut();
                                  LoginManager.logOut()
                                  Alert.alert('ERROR',response.data.message)
                                  
                              }
                          })
                          .catch(function (error) {
                              console.log('login error',error)
                              Alert.alert('',I18n.t('networkerror'))
                          })
                      })
                      .catch(() => {
                        console.log('reject')
                      })
                  }
                  )
          }
        }, function(error) {
           console.log("some error occurred!!");
        })
      }else{
        let result;
      try { 
        LoginManager.setLoginBehavior('native');
        result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
      } catch (nativeError) {
        try {
          LoginManager.setLoginBehavior('web');
          result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
        } catch (webError) {
          // show error message to the user if none of the FB screens
          // did not open
        }
      }
      // handle the case that users clicks cancel button in Login view
      if (result!= null &&  result.isCancelled) {
        
      } else {
        // Create a graph request asking for user information
        AccessToken.getCurrentAccessToken().then(
          (data) => {
              const { accessToken } = data
              console.log('accessToken',accessToken)
              this.initUser(accessToken)
          }
          )
      }
      }
      

    }
      async onLoginWithGoogle(){
        var token_fcm = await AsyncStorage.getItem('fcmToken')
        GoogleSignin.configure({
            scopes: ['profile','email'],
            webClientId:'742559247810-o4h2e6m43abbkcf9e36mqftosqnmp84p.apps.googleusercontent.com',
            iosClientId:'742559247810-0div9gkj5k0cjd7n8olugtcn9jogjeet.apps.googleusercontent.com'
            
          });

          try {
            await GoogleSignin.hasPlayServices({
              //Check if device has Google Play Services installed.
              //Always resolves to true on iOS.
              showPlayServicesUpdateDialog: true,
            });
            if (GoogleSignin.isSignedIn()) {
              GoogleSignin.signOut()
              }
            const issignin = await GoogleSignin.isSignedIn();
            console.log('issignin',issignin)
            if(issignin == true){
              const currentUser = await GoogleSignin.getCurrentUser()
              this.setState({ userInfo: currentUser }, async ()=>{
                if(currentUser == null){
                  const currentUser1 = await GoogleSignin.signIn();
                  var data = {
                    "email":currentUser1.user.email,
                    "token_gmail":currentUser1.idToken,
                    "device_id":DeviceInfo.getDeviceId(),
                    "device_name":DeviceInfo.getDeviceName(),
                    "language":I18n.locale,
                    "type":2,
                    "token_firebase": token_fcm,
                    "login":true
                  }
                  this.props.login(data)
                  console.log('userInfo is sign',data)
                }else{
                  var data = {
                    "email":currentUser.user.email,
                    "token_gmail":currentUser.idToken,
                    "device_id":DeviceInfo.getDeviceId(),
                    "device_name":DeviceInfo.getDeviceName(),
                    "language":I18n.locale,
                    "type":2,
                    "token_firebase": token_fcm,
                    "login":true
                  }
                  this.props.login(data)
                  console.log('userInfo is unsign',data)
                }
             
                
              });
            }else{
              const userInfo1 = await GoogleSignin.signIn();
              this.setState({ userInfo: userInfo1 },()=>{
                var data = {
                  "email":userInfo1.user.email,
                  "token_gmail":userInfo1.idToken,
                  "device_id":DeviceInfo.getDeviceId(),
                  "device_name":DeviceInfo.getDeviceName(),
                  "language":I18n.locale,
                  "type":2,
                  "token_firebase": token_fcm,
                  "login":true
              }
              this.props.login(data)
                  
              });
            }
           
          } catch (error) {
            console.log('Message', error.message);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
              console.log('User Cancelled the Login Flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
              console.log('Signing In');

            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
              console.log('Play Services Not Available or Outdated');
            } else {
              console.log('Some Other Error Happened');
            }
          }
    }
    async initUser(token) {
      var token_fcm = await AsyncStorage.getItem('fcmToken')
      
        fetch('https://graph.facebook.com/v2.5/me?fields=email,id&access_token=' + token)
        .then((response) => response.json())
        .then((json) => {
          var data = {
            "email":json.email,
            "token_facebook":token,
            "device_id":DeviceInfo.getDeviceId(),
            "device_name":DeviceInfo.getDeviceName(),
            "language":I18n.locale,
            "type":1,
            "token_firebase": token_fcm,
            "login":true,
            "facebook_id":json.id
        }
        console.log('initUser',data)
        this.props.login(data)
        })
        .catch(() => {
          console.log('reject')
        })
      }
    async onLogin(){
        console.log('onLogin',this.state.username,this.state.password)
        if(this.state.username == ''){
            this.setState({modalError:true,textError:I18n.t('login.error.username'),username_c:true})
        }else if(this.state.password == ''){
            this.setState({modalError:true,textError:I18n.t('login.error.password'),username_c:false,password_c:true})
        }else{
            var token_fcm = await AsyncStorage.getItem('fcmToken')
            var data = {
                "email":this.state.username,
                "password":this.state.password,
                "device_id":DeviceInfo.getDeviceId(),
                "device_name":DeviceInfo.getDeviceName(),
                "language":I18n.locale,
                "type":3,
                "token_firebase": token_fcm,
                "login":true
            }
            this.props.login(data)
        }
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={[styles.text_modal,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={[styles.text_modal_btn,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <LinearGradient colors={[BG_1,BG_1, BG_2]} style={[styles.linearGradient]}>
                    <TouchableOpacity style={{margin:dim.width/100*5,flexDirection:'row',alignItems:'center',marginTop:dim.height/100*5}} onPress={()=> Actions.pop()}>
                      <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                      <Title style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3}}>{I18n.t('header.back')}</Title>
                    </TouchableOpacity>
                    <View style={{alignItems:'center',justifyContent:'flex-end'}}>
                        <Image
                            source={require('../../asset/Images/logo.png')}
                            style={{
                                width:Platform.isPad? dim.width/100*40 : dim.width/100*60,
                                height:dim.height/100*10
                            }}
                            resizeMode='contain'
                        />
                    </View>
                    <View style={{alignItems:'center',marginTop:dim.height/100*3}}>
                        <Text style={{fontSize:SIZE_4,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{I18n.t('PleaseLoginScreen.please')}</Text>
                        <Form style={{width:Platform.isPad?dim.width/100*50:dim.width/100*80}}>
                            <Item style={styles.item} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('login.username')}</Label>
                                <Input 
                                    ref={(input) => { this.username = input; }} 
                                    style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                    onChangeText={(value)=>{this.setState({username:value})}}
                                    autoCapitalize='none'
                                />
                            </Item>
                            <Item style={styles.item} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('login.password')}</Label>
                                <Input 
                                    ref={(input) => { this.password = input; }} 
                                    style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                    onChangeText={(value)=>{this.setState({password:value})}}
                                    secureTextEntry={true}
                                    autoCapitalize='none'
                                />
                            </Item>
                        </Form>
                        
                    </View>  
                    <TouchableOpacity style={[styles.btn,{width:dim.width/100*30,alignSelf:'center'}]} onPress={()=> {this.onLogin()}}>
                        <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('login.login')}</Text>
                    </TouchableOpacity>      
                    <View style={{flex:3,alignItems:'center',paddingTop:dim.height/100*10}}>
                        <TouchableOpacity style={[styles.btn]} onPress={()=> this.onLoginWithGoogle()}>
                            <View style={{flex:1.5,alignItems:'flex-end'}}>
                                <Image 
                                    source={require('../../asset/Images/search.png')}
                                    style={{
                                        width:dim.width/100*5,
                                        height:dim.width/100*5
                                    }}
                                />
                            </View>
                            <Text style={styles.text1}>{I18n.t('loginhome.login_google')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn]} onPress={()=> this.onLoginWithFacebook()}>
                            <View style={{flex:1.5,alignItems:'flex-end'}}>
                                <Image 
                                    source={require('../../asset/Images/facebook.png')}
                                    style={{
                                        width:dim.width/100*5,
                                        height:dim.width/100*5
                                    }}
                                />
                            </View>
                            <Text style={styles.text1}>{I18n.t('loginhome.login_facebook')}</Text>
                                  </TouchableOpacity>
                        
                        <TouchableOpacity style={[styles.btn,{marginTop:dim.height/100*3.5,backgroundColor:'#565455'}]} onPress={()=> Actions.register()}>
                            <Text style={[styles.text,{color:'#ffffff',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('loginhome.register')}</Text>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
            </Container>
            
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    btn:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:Platform.isPad?dim.width/100*50:dim.width/100*65,
        // backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1,
        borderColor:BG_2,
        borderWidth:1,
        marginTop:dim.height/100*2.5,
        height:dim.height/100*6
    },
    text:{
        marginHorizontal:dim.width/100*2,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text1:{
        flex:8,
        textAlign:'left',
        marginLeft:dim.width/100*7,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    },
    text_modal:{
      marginVertical:dim.height/100*1.5,
      fontSize:SIZE_4,
      fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
  },
  text_modal_btn:{
      textAlign:'center',
      fontSize:SIZE_4,
      fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
  },
  });
export default connect(mapStateToProps,actions)(PleaseLoginScreen)