import React, { Component } from "react"
import {
    Dimensions,
    View,
    StyleSheet,
    Image,
    Platform,
    BackHandler,
    Alert
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { BG_1, BG_2, BLACK } from '../constant/color'
import { SIZE_3_7 } from '../constant/font'
import Header1 from '../Component/headerhome'
import Footer1 from '../Component/footer'
import { Container,Header,Footer } from "native-base";
// // tab menu top
import Ongoing from '../Component/ongoing'
import Comingsoon from '../Component/comingsoon'
import Schedule from '../Component/schedule'
// main screen
import Wish from '../Component/Main/Wish'
import Tickets from '../Component/Main/Tickets'
import Profile from '../Component/Main/Profile'

import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
const dim = Dimensions.get('window');
class HomeScreen extends Component {
 
    constructor(props){
        super(props)
        this.state = {
            subTab:1,
            Tickets:[],
            History:[],
            branner:null,
            token:null,
            re_num:null
        }
    }
    async componentDidMount(){
        var data = {
            language:I18n.locale
        }
        // this.props.getBraner(data)
        var token = await AsyncStorage.getItem('token')
        this.setState({token:token})
        if(token != null && token != undefined){
            this.props.getProfileInfo(data)
            this.props.GetCreditCard(data)
            this.props.getbuffetTicket(data)
            // this.props.getTicket(data)
        }
        BackHandler.addEventListener("back", () => this._handleBack())
        console.warn('fromdetail home',this.props.fromdetail)
        if(this.props.fromdetail == true){
            Actions.detailbaner({data:this.props.data_detail})
        }
    }
    async componentWillReceiveProps(nextProps){
        if(nextProps.branner != null && nextProps.branner){
            this.setState({branner:nextProps.branner})
        }
        if(nextProps.ticket_list != null && nextProps.ticket_list != undefined){
            this.setState({Tickets:nextProps.ticket_list,loading:false})
        }
        console.warn('nextProps.error_profile',nextProps.error_profile)
        if(nextProps.error_profile != null && nextProps.error_profile != undefined){
            await AsyncStorage.setItem('token','')
            Actions.popTo('loginhome')
            // Actions.login()
        }
        
    }
    async componentDidUpdate(){
        var token = await AsyncStorage.getItem('token')
        
        if(this.props.re && this.state.re_num != this.props.re_num){
            console.log('componentDidUpdate',this.props.re,this.props.re_num,I18n.locale)
            this.setState({re_num:this.props.re_num})
            var data = {
                language:I18n.locale
            }
            if(token != null && token != undefined){
                this.props.getProfileInfo(data)
                this.props.GetCreditCard(data)
                this.props.getbuffetTicket(data)
                // this.props.getTicket(data)
            }
        }
    }
    _handleBack() {
        if(Actions.currentScene == "_home" || Actions.currentScene == 'home') {
            BackHandler.exitApp()
          return true
        }
         return false
      }

    onChangeSubtab(index){
        this.setState({subTab:index})
    }
    _renderHome(){
        return(
            <Ongoing/>
        )
    }
    _renderWish(){
        return(
            <Wish/>
        )
    }
    _renderTickets(){
        return(
            <Tickets/>
        )
    }
    _renderProfile(){
        return(
            <Profile/>
        )
    }
    render(){
            if(this.props.index_menu==0&&this.props.index_footer==3){
                if(this.state.Tickets){
                    var background_mobile = ''
                    if(this.props.index_ticket==1){
                        if(this.state.Tickets.length != 0 && this.state.Tickets[0] != null){
                            if(this.state.Tickets[1] != undefined){
                                if(this.state.Tickets[1].movie.background_mobile_path != null){
                                    background_mobile = this.state.Tickets[1].movie.background_mobile_path
                                }else{
                                    background_mobile = this.state.Tickets[1].movie.poster_mobile_path
                                }
                            }
                           
                        }
                    }else{
                        if(this.state.History.length != 0){
                            if(this.state.History[0].movie.background_mobile_path != null){
                                background_mobile = this.state.History[0].movie.background_mobile_path
                            }else{
                                background_mobile = this.state.History[0].movie.poster_mobile_path
                            }
                        }
                    }
                    
                    return(
                        <Container>
                            <Image 
                                source={{uri:background_mobile}} 
                                style={{
                                    position:'absolute',
                                    width:dim.width/100*300,
                                    height:dim.height/100*300,
                                    alignSelf:'center',
                                    marginTop:-(dim.height/100*150),
                                    backgroundColor:'#000000'
                                }}
                                blurRadius={Platform.OS == 'ios'?7:4}
                            />
                            <Header
                                style={{
                                    backgroundColor:BG_1,
                                    borderBottomWidth:1,
                                    borderBottomColor:'transparent',
                                    paddingTop:dim.height/100*3,
                                    height:dim.height/100*11,
                                    opacity:0.3,
                                    position:'absolute'
                                }}
                            />
                            {<Header1 transparent1={true}/>}
                                {this._renderTickets()}
                            <Footer 
                                style={{
                                    opacity:0.5,
                                    backgroundColor:BG_1,
                                    marginBottom:this.props.checkIpnoe == 'xr' || this.props.checkIpnoe =='x'?-dim.height/100*8.8:-dim.height/100*8,
                                    height:this.props.checkIpnoe == 'xr' || this.props.checkIpnoe =='x'?dim.height/100*5:dim.height/100*8,
                                }}
                            />
                            <Footer1 transparent1={true}/>
                            
                        </Container>
                        
                    )
                }
                
            }else{
                return(
                    <Container>
                        <Header1/>
                        <LinearGradient colors={[BG_1,BG_1, BG_2]} style={this.props.index_menu==0?styles.linearGradient1:styles.linearGradient}>
                            {/* when on press footer */}
                            {this.props.index_menu==1&&this.props.index_footer==1?this._renderHome():<View/>}
                            {this.props.index_menu==0&&this.props.index_footer==2?this._renderWish():<View/>}
                            {this.props.index_menu==0&&this.props.index_footer==4?this._renderProfile():<View/>}

                            {/* when on press menu top */}
                            {this.props.index_menu==2?<Comingsoon/>:<View/>}
                            {this.props.index_menu==3?<Schedule/>:<View/>}
                        </LinearGradient>
                        <Footer1/>
                    </Container>
                    
                )
            }
            
    
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        index_menu:state.check.index_menu,
        index_footer:state.check.index_footer,
        index_ticket:state.check.index_ticket,
        booking_flag:state.check.booking_flag,
        booking_flag_num:state.check.booking_flag_num,
        booking:state.check.booking,
        error:state.home.error,
        branner:state.home.branner,
        ticket_list:state.ticket.tickets,
        error_num:state.home.error_num,
        error_profile:state.profile.error,
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
      paddingHorizontal:0
    },
    linearGradient1: {
        flex: 1,
      },
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK
    }
  });
export default connect(mapStateToProps,actions)(HomeScreen)