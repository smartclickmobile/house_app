import React, { Component } from "react"
import {
    TouchableOpacity,
    Dimensions,
    View,
    StyleSheet,
    Image,
    Text,
    Platform,

    BackHandler,
    Alert
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import { BG_1, BG_2, BLACK } from '../constant/color'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Header1 from '../Component/headerMovie'
import { Container, Content,Header} from "native-base";
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { TEXT_BODY_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD, TEXT_HEADER_EN_BOLD,SIZE_3_5,SIZE_2_3} from "../constant/font";
const dim = Dimensions.get('window');
class BookingSuccessScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            membertype:'',
            data : []
        }
    }
    async componentDidMount(){
        var membertype = await AsyncStorage.getItem('membertype')
        this.setState({membertype:membertype})
        BackHandler.addEventListener("back", () => this._handleBack())
        this.props.clearPurchase()
        if(this.props.buffet_movie == true){
            this.setState({data:this.props.data_success})
        }else{
            var data = {
                language:I18n.locale,
                purchase_transaction_id:this.props.purchase_transaction_id
            }
            this.props.getPurchase_Detail(data)
        }
        

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.purchase_detail != null && nextProps.purchase_detail != undefined){
            this.setState({data:nextProps.purchase_detail},()=>{
                if(this.state.data.showtimes.seats[0].qrcode_path == null || this.state.data.showtimes.seats[0].qrcode_path == '' || this.state.data.showtimes.seats[0].qrcode_path == undefined){
                    // Actions.pop()
                    Alert.alert(I18n.t('BookingSummeryScreen.error_payment'),I18n.t('BookingSummeryScreen.error_payment_message'),[
                        {
                            text: 'OK',
                            onPress: () => {
                                this.props.clear_buffet()
                                Actions.popTo('home')
                            }
                        }])
                }
            })
        }
    }
    _handleBack() {
        if(Actions.currentScene == "_BookingSuccessScreen" || Actions.currentScene == 'BookingSuccessScreen') {
       
            Actions.popTo('home')
        
          return true
        }
         return false
      }
      _renderItem = (item,item1,index,index1) => {
        // console.log(item)
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss')
        var time = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss').format('HH:mm')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        
        if(this.props.buffet_movie ==  true){
            var date_start = moment(item.event.start_date,'YYYY-MM-DD HH:mm:ss')
            var day_start = date_start.date()
            var month_start = date_start.month()
            var monthtxt_start = I18n.locale == 'th'?month_th[month_start]:month_en[month_start]
            var year_start = date_start.year()
            var yeartxt_start =  I18n.locale == 'th'?parseInt(year_start)+543:parseInt(year_start)

            var date_end = moment(item.event.end_date,'YYYY-MM-DD HH:mm:ss')
            var day_end = date_end.date()
            var month_end = date_end.month()
            var monthtxt_end = I18n.locale == 'th'?month_th[month_end]:month_en[month_end]
            var year_end = date_end.year()
            var yeartxt_end =  I18n.locale == 'th'?parseInt(year_end)+543:parseInt(year_end)

            var no = item.member.member_id
            var num = 9 - no.length
            var no_text = ''
            for(var i = 0 ; i <= num ; i++){
                if(i == num){
                    no_text += no
                }else{
                    no_text += '0'
                }
            }
            var num1 = no_text.substring(0, 3)
            var num2 = no_text.substring(3, 6)
            var num3 = no_text.substring(6, 9)
            var num_text = num1+' '+num2+' '+num3
        }
        return(
            <View>
                {this.props.buffet_movie ==  true ? 
                <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#fff8eb',width:360,height:270,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*2}}>
                    <View style={{flexDirection:'row'}}>
                        <View style={{width:145,backgroundColor:'#fff8eb',borderRightWidth:0.5,borderRightColor:BLACK,height:270,borderTopLeftRadius:dim.width/100*2,borderBottomLeftRadius:dim.width/100*2,padding:10}}>
                            <View style={{height:50}}>
                                <Image
                                    source={require('../../asset/Images/g-logo-house-1024-300-tran.png')}
                                    style={{
                                        width:120,
                                        height:54,
                                        alignSelf:'flex-start'
                                    }}
                                    resizeMode='contain'
                                />
                            </View>
                            <View style={{height:50,justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'left',color:BLACK,fontSize:10}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:14,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{item.event.title}</Text>        
                            </View>
                            <View style={{height:50,justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'left',color:BLACK,fontSize:10}]}>{I18n.t('tab.tickets.ticket_buffet_movie.time')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`${day_start} ${monthtxt_start} - ${day_end} ${monthtxt_end} ${yeartxt_end}`}</Text>        
                            </View>
                            <View style={{height:50,justifyContent:'center'}}>
                                <Text numberOfLines={2} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`${item.member.first_name}\n${item.member.last_name}`}</Text>        
                            </View>
                            <View style={{height:50,justifyContent:'center'}}>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${num_text}`}</Text>        
                            </View>
                        </View>
                        <View style={{width:215,height:270,borderBottomRightRadius:dim.width/100*3,borderBottomLeftRadius:dim.width/100*1,paddingTop:10,paddingHorizontal:5}}>
                            <View style={{height:50,alignItems:'flex-end',justifyContent:'flex-end',flexDirection:'row'}}>
                                <View style={{backgroundColor:'#78bfb5',height:50,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,alignItems:'center',justifyContent:'center'}}>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${item.event.title}`}</Text>        
                                </View>
                            </View>
                            <View style={{height:50,alignItems:'center',justifyContent:'center'}}>
                                <View style={{width:205,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                    <Text style={[styles.textH,{textAlign:'left'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.title')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{item.movie.title}</Text>        
                                </View>
                            </View>
                            <View style={{height:50,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                <View style={{width:100,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                    <Text style={[styles.textH,{textAlign:'left'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${day} ${monthtxt}`}</Text>        
                                </View>
                                <View style={{width:100,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center',marginLeft:5}}>
                                    <Text style={[styles.textH,{textAlign:'left'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{item.theater.name}</Text>        
                                </View>
                            </View>
                            <View style={{width:215,flexDirection:'row',height:100}}>
                                <View style={{height:100,width:53,alignItems:'center',justifyContent:'center'}}>
                                    <Image
                                        source={{uri:item.movie.poster_mobile_path}}
                                        style={{width:53,height:95,backgroundColor:'#fbc618'}}
                                        resizeMode='contain'
                                    />
                                </View>
                                <View style={{height:100,width:60,marginLeft:2,alignItems:'center',justifyContent:'center'}}>
                                    <View style={{width:60,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                        <Text style={[styles.textH,{textAlign:'left'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                        <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{time}</Text>        
                                    </View>
                                    <View style={{width:60,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center',marginTop:5}}>
                                        <Text style={[styles.textH,{textAlign:'left'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                        <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${item1.row}${item1.col_no}`}</Text>        
                                    </View>
                                </View>
                                <View style={{height:90,width:90,alignItems:'center',justifyContent:'center'}}>
                                    <Image
                                        source={{uri:item1.qrcode_path}}
                                        style={{width:90,height:90}}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                                
                        </View>
                    </View>
                </TouchableOpacity>
                :
                <TouchableOpacity key={`${index}${index1}`} style={{flex:1,alignSelf:'center',backgroundColor:BG_1,width:320,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                    <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:item.movie.ticket_color==null?BLACK:item.movie.ticket_color,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
                        {item.movie.ticket_mobile_path!=null && item.movie.ticket_mobile_path != undefined?<View style={{height:dim.width/100*20,overflow:'hidden',width:dim.width/100*80,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}>
                            <Image 
                                source={{uri:item.movie.ticket_mobile_path}}
                                style={{height:60,width:280,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}
                                resizeMode='stretch'
                                
                            />
                        </View>:
                        <Image
                            source={require('../../asset/Images/w-logo-house-1024-300-tran.png')}
                            style={{
                                width:120,
                                alignSelf:'center',
                                // height:dim.height/100*5,
                            }}
                            resizeMode='contain'
                        />}
                    </View>
            
                    <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
                        <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.movie')}</Text>
                        <Text numberOfLines={1} style={{color:BLACK,fontSize:20,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.movie.title}</Text>
                    </View>
            
                    <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
                        <View style={{width:160}}>
                            <View 
                                style={{
                                    height:65,
                                    borderBottomColor:BG_2,
                                    borderBottomWidth:1,
                                    flexDirection:'row',
                                    alignItems:'center'
                                }}
                            >
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.theater.name}</Text>        
                                </View>
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${item1.row}${item1.col_no}`}</Text>        
                                </View>
                            </View>
                            <View 
                                style={{
                                    height:65,
                                    borderBottomColor:BG_2,
                                    borderBottomWidth:1,
                                    flexDirection:'row',
                                    alignItems:'center'
                                }}
                            >
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${day} ${monthtxt}`}</Text>        
                                </View>
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:TEXT_HEADER_EN_BOLD}}>{`${time}`}</Text>        
                                </View>
                            </View>
                        </View>
            
                        <View style={{width:160,alignItems:'center',justifyContent:'center'}}>
                            <Image
                                source={{uri:item1.qrcode_path}}
                                style={{width:130,height:130}}
                                resizeMode='contain'
                            />
                        </View>
                    
                    </View>
                    <View style={{height:30,backgroundColor:item.movie.ticket_color==null?BLACK:item.movie.ticket_color,overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
                </TouchableOpacity>}
            </View>
        
    );
    }
    _renderTicket(){
        var data = this.state.data
        return(
            data.showtimes.seats.map((item1,index1)=>{
                return(
                    this._renderItem(data,item1,index1)
                )
            }) 
        )
    }
    render(){
        console.warn('this.state.data.movie.background_mobile_path',this.state.data.length,this.state.data)
        return(
            <Container>
                <Image 
                    source={{uri:this.state.data.length==0?'':this.state.data.movie.background_mobile_path==null?this.state.data.movie.poster_mobile_path:this.state.data.movie.background_mobile_path}}
                    style={{
                        position:'absolute',
                        width:dim.width/100*300,
                        height:dim.height/100*300,
                        alignSelf:'center',
                        marginTop:-(dim.height/100*100),
                        backgroundColor:'#000000'
                    }}
                    blurRadius={Platform.OS == 'ios'?7:4}
                />
                <Header
                    style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'transparent',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11,
                        opacity:0.3,
                        position:'absolute'
                    }}
                />
                {<Header1 leftbutton={false} transparent1={true}/>}
                <Content>
                {
                    this.state.data.length != 0 && this._renderTicket()
                }
                <TouchableOpacity style={{alignItems:'center',justifyContent:'center',padding:dim.width/100*3,borderColor:BG_1,borderWidth:1,margin:dim.width/100*2,borderRadius:dim.width/100*1.5}} onPress={()=> Actions.popTo('home')}>
                    <Text style={{color:BG_1,fontSize:SIZE_2_3,
                        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('BookingSuccessScreen.back_main')}</Text>
                </TouchableOpacity>
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        purchase_detail:state.purchase.purchase_detail,
        error:state.purchase.error
    }
  }
  var styles = StyleSheet.create({
    Content_View:{
        padding:dim.width/100*2.5,
        borderBottomColor:BG_2,
        borderBottomWidth:1
    },
    textH:{
        color:BLACK,
        fontSize:SIZE_2_3,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    Content_detail:{
        borderRightColor:BG_2,
        borderRightWidth:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomColor:BG_2,
        borderBottomWidth:1,
        height:dim.height/100*7
    },
    Content_detail1:{
        borderRightColor:BG_2,
        borderRightWidth:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomColor:BG_2,
        borderBottomWidth:1,
        height:dim.height/100*7
    },
    textD:{
        color:BLACK,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    }
  });
export default connect(mapStateToProps,actions)(BookingSuccessScreen)