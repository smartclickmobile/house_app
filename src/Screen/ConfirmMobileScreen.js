import React, { Component } from "react"
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    Modal,
    StyleSheet,

    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2, BLACK,RED_COLOR } from '../constant/color'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Container,Form,Item,Input,Label,Icon } from 'native-base';
import DeviceInfo from "react-native-device-info"
import I18n from '../../asset/languages/i18n'
import Header from '../Component/header'
import { TEXT_BODY_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3,SIZE_3_5,SIZE_4,SIZE_7 } from '../constant/font'
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager } from 'react-native-fbsdk';
const dim = Dimensions.get('window');
class ConfirmMobileScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            number:'',
            number_c:false,
            modalError:false,
            textError:''
        }
    }
    async onsignout(){
        if(this.props.login_t == 'gm'){
            try {
                // await GoogleSignin.revokeAccess();
                await GoogleSignin.signOut();
                Actions.pop()
              } catch (error) {
                console.error(error);
              }
        }else{
            LoginManager.logOut()
            Actions.pop()
        }
        
    }
   async onConfirmNum(){
        if(this.state.number == '' && this.state.number.length < 9){
            this.setState({modalError:true,textError:I18n.t('ConfirmMobileScreen.error_phone'),username_c:true})
        }else{
            var token_fcm = await AsyncStorage.getItem('fcmToken')
            var data_user = this.props.data
            if(this.props.login_t == 'gm'){
                
                var data = {
                    "email":data_user.email,
                    "token_gmail":data_user.token_gmail,
                    "device_id":DeviceInfo.getDeviceId(),
                    "device_name":DeviceInfo.getDeviceName(),
                    "language":I18n.locale,
                    "type":2,
                    "token_firebase": token_fcm,
                    "telephone":this.state.number,
                    "fromdetail":this.props.fromdetail,
                    "data_detail":this.props.data_detail
                    
                }
            }else if(this.props.login_t =='ap'){
                var data = {
                    "email":data_user.email,
                    "token_appleid":data_user.token_appleid,
                    "device_id":DeviceInfo.getDeviceId(),
                    "device_name":DeviceInfo.getDeviceName(),
                    "language":I18n.locale,
                    "type":4,
                    "token_firebase": token_fcm,
                    "telephone":this.state.number,
                    "fromdetail":this.props.fromdetail,
                    "data_detail":this.props.data_detail
                    
                }
            }else{
                var data = {
                    "email":data_user.email,
                    "token_facebook":data_user.token_facebook,
                    "device_id":DeviceInfo.getDeviceId(),
                    "device_name":DeviceInfo.getDeviceName(),
                    "language":I18n.locale,
                    "type":1,
                    "token_firebase": token_fcm,
                    "telephone":this.state.number,
                    "facebook_id": this.props.facebook_id,
                    "fromdetail":this.props.fromdetail,
                    "data_detail":this.props.data_detail
                }
            }
            console.log('data',data)
            this.props.login(data)  
        }
    }
    render(){
        var email = ''
        console.warn('confirm email data ', this.props.data)
        if(this.props.login_t == 'gm'){
            email = this.props.data.email
        }else{
            email = this.props.data.email
        }
        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={styles.text_modal}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={styles.text_modal_btn}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <LinearGradient colors={[BG_1,BG_1, BG_2]} style={[styles.linearGradient]}>
                    <Header btnleft={true}/>
                    <Text style={{fontSize:SIZE_7,color:BLACK,marginTop:dim.height/100*2,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{I18n.t('ConfirmMobileScreen.error_number')}</Text>
                    <Text style={{fontSize:SIZE_4,color:BLACK,marginTop:dim.height/100*2,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${I18n.t('ConfirmMobileScreen.email')} : ${email=='undefined'|| email == null || email == undefined || email == 'null'?'-':email}`}</Text>
                    <View style={{flex:3,alignItems:'center',marginTop:dim.height/100*3}}>
                        <Form style={{width:Platform.isPad?dim.width/100*50:dim.width/100*80}}>
                            <Item style={styles.item} floatingLabel>
                                <Label style={styles.label}>{I18n.t('ConfirmMobileScreen.phone')}</Label>
                                <Input 
                                    style={styles.label}
                                    onChangeText={(value)=>{this.setState({number:value})}}
                                    keyboardType='phone-pad'
                                    autoCapitalize='none'
                                />
                            </Item>
                        </Form>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity style={[styles.btn]} 
                                onPress={()=> {
                                    this.onsignout()
                                   
                                   
                                }}>
                                <Text style={styles.text}>{I18n.t('ConfirmMobileScreen.back')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn]} onPress={()=> {this.onConfirmNum()}}>
                                <Text style={styles.text}>{I18n.t('ConfirmMobileScreen.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                        
                      
                    </View>
                    
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    btn:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:Platform.isPad?dim.width/100*20:dim.width/100*30,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5,
        marginHorizontal:dim.height/100*1
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*2,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:SIZE_4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
export default connect(mapStateToProps,actions)(ConfirmMobileScreen)