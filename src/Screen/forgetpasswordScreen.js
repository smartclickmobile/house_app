import React, { Component } from "react"
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    Modal,
    StyleSheet,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2, BLACK,RED_COLOR } from '../constant/color'
import { Container,Form,Item,Input,Label,Icon } from 'native-base';
import Header from '../Component/header'
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_4,SIZE_7,SIZE_3_7 } from '../constant/font'
const dim = Dimensions.get('window');
class ForgetpasswordScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            checked:false,
            email:'',
            modalError:false,
            textError:'',
            success:false,
            forget_error_num:null
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.forget_success != null && nextProps.forget_success != undefined){
            if(nextProps.forget_success == true){
                this.setState({success:true})
            }
        }
        if(nextProps.forget_error != null && nextProps.forget_error != undefined && nextProps.forget_error_num != this.state.forget_error_num){
            this.setState({modalError:true,textError:nextProps.forget_error,forget_error_num:nextProps.forget_error_num})
        }
    }
    onSent(){
        if(this.state.email == '' || !this.state.email.includes("@") || !this.state.email.includes(".")){
            this.setState({modalError:true,textError:I18n.t('ForgetpasswordScreen.error_email')})
        }else{
            var data = {
                "email":this.state.email,
                "language":I18n.locale
            }
            this.props.forgetpassword(data)
        }

    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:RED_COLOR,borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:RED_COLOR}}/>
                            </View>
                            <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ForgetpasswordScreen.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal visible={this.state.success} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:'#26de81',borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='check' type='FontAwesome5' style={{color:'#26de81'}}/>
                            </View>
                            <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ForgetpasswordScreen.sent_password')}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2}} 
                                onPress={()=>{
                                    this.setState({success:false})
                                    Actions.popTo('login')
                                }}
                            >
                                <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ForgetpasswordScreen.back_login')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <LinearGradient colors={[BG_1,BG_1, BG_2]} style={[styles.linearGradient]}>
                    <Header btnleft={true}/>
                    <View style={{flex:1,alignItems:'center',padding:dim.width/100*10}}>
                        <Text style={{fontSize:SIZE_7,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{I18n.t('ForgetpasswordScreen.forget_password')}</Text>
                        <Text style={{fontSize:SIZE_3_7,color:BLACK,marginTop:dim.height/100*4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ForgetpasswordScreen.enter_email')}</Text>
                        <Form style={{width:Platform.isPad?dim.width/100*50:dim.width/100*80}}>
                            <Item style={styles.item} floatingLabel>
                                <Label style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('ForgetpasswordScreen.email')}</Label>
                                <Input 
                                    style={{ fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}
                                    onChangeText={(value)=>{this.setState({email:value})}}
                                    autoCapitalize='none'
                                />
                            </Item>
                        </Form>
                        
                        <TouchableOpacity style={[styles.btn]} onPress={()=> {this.onSent()}}>
                            <Text style={[styles.text,{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}]}>{I18n.t('ForgetpasswordScreen.sent')}</Text>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        forget_success:state.auth.forget_success,
        forget_error:state.auth.forget_error,
        forget_success_random:state.auth.forget_success_random,
        forget_error_num:state.auth.forget_error_num
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    btn:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:Platform.isPad?dim.width/100*35:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    text:{
        marginHorizontal:dim.width/100*1,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,
        fontSize:SIZE_4
    },
    input:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    item:{
        padding:3
    }
  });
export default connect(mapStateToProps,actions)(ForgetpasswordScreen)