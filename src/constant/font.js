import {
    Dimensions,
    Platform
  } from "react-native"
  const dim = Dimensions.get('window');
export const TEXT_TH = 'Prompt-Regular'
export const TEXT_BODY_EN = 'Montserrat-Regular'
export const TEXT_HEADER_EN = 'Syncopate-Regular'
export const TEXT_TH_BOLD = 'Prompt-Bold'
export const TEXT_BODY_EN_BOLD = 'Montserrat-Bold'
export const TEXT_HEADER_EN_BOLD = 'Syncopate-Bold'

export const SIZE_2_3 = Platform.isPad?dim.width/100*1.3:dim.width/100*2.3
export const SIZE_2_5 = Platform.isPad?dim.width/100*1.5:dim.width/100*2.5
export const SIZE_2_7 = Platform.isPad?dim.width/100*1.7:dim.width/100*2.7
export const SIZE_2_9 = Platform.isPad?dim.width/100*1.9:dim.width/100*2.9

export const SIZE_3 = Platform.isPad?dim.width/100*2:dim.width/100*3
export const SIZE_3_5 = Platform.isPad?dim.width/100*2.5:dim.width/100*3.5
export const SIZE_3_7 = Platform.isPad?dim.width/100*2.7:dim.width/100*3.7

export const SIZE_4 = Platform.isPad?dim.width/100*3:dim.width/100*4
export const SIZE_4_5 = Platform.isPad?dim.width/100*3.5:dim.width/100*4.5

export const SIZE_5 = Platform.isPad?dim.width/100*4:dim.width/100*5
export const SIZE_5_5 = Platform.isPad?dim.width/100*4.5:dim.width/100*5.5

export const SIZE_6 = Platform.isPad?dim.width/100*5:dim.width/100*6

export const SIZE_7 = Platform.isPad?dim.width/100*6:dim.width/100*7