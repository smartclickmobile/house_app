export const resultCard = {
    '00':{
        message:'Approved',
        data:'Payment Completed'
    },
    '01':{
        message:'Refer to Card Issuer',
        data:'Give cardholder contacts issuer bank'
    },
    '03':{
        message:'Invalid Merchant ID',
        data:'Please contact GBPrimePay'
    },
    '05':{
        message:'Do Not Honour',
        data:'Cardholder input invalid card information. Ex. Expiry date, CVV2 or card number. Give cardholder contacts issuer bank.'
    },
    '12':{
        message:'Invalid Transaction',
        data:'Please contact GBPrimePay'
    },
    '13':{
        message:'Invalid Amount',
        data:'Payment amount must more than 0.1'
    },
    '14':{
        message:'Invalid Card Number',
        data:'Please check all digits of card no.'
    },
    '17':{
        message:'Customer Cancellation',
        data:'Customers click at cancel button in payment page when they make transaction. Customers have to make new payment transaction.'
    },
    '19':{
        message:'Re-enter Transaction',
        data:'Duplicate payment. Please contact GBPrimePay'
    },
    '30':{
        message:'Format Error',
        data:'Transaction format error. Please contact GBPrimePay'
    },
    '41':{
        message:'Lost Card -Pick Up',
        data:'Lost Card and Cardholder give up.'
    },
    '43':{
        message:'Stolen Card -Pick Up',
        data:'Stolen Card and Cardholder give up.'
    },
    '50':{
        message:'Invalid Payment Condition',
        data:'Ex. Session time out or invalid VbV Password : ask cardholders to try ma again and complete transaction within 15 minutes with correct card information.'
    },
    '51':{
        message:'Insufficient Funds',
        data:'Not enough credit limit to pay. Please contact issuer'
    },
    '54':{
        message:'Expired Card',
        data:'Cardholder key in invalid expiry date'
    },
    '55':{
        message:'Wrong PIN',
        data:'Declined - Wrong PIN Entered by Card Holder'
    },
}