import base64 from 'react-native-base64'

//pro

export const API_IP = 'https://www.housesamyan.com/houseapi/'
export const URL_IP =  'https://www.housesamyan.com/site'

//uat

// export const API_IP = 'https://www.housesamyan.com/uat/houseapi/'
// export const URL_IP =  'https://www.housesamyan.com/uat/site'


export const URL_PROD_IP =  'https://www.housesamyan.com/site'
export const GB_API_IP = 'https://api.gbprimepay.com/v1/tokens'
export const GB_API_3D = 'https://api.gbprimepay.com/v1/tokens/3d_secured'
export const PUBLIC_KEY = "aqPOJuFUGGytfeYCB9d3IrMjyICBkzDB"
export const CONFIG = {
    headers: {
      "content-type": "multipart/form-data",
      "Authorization": "Basic "+base64.encode("HouseVsSmartClick:")
    },
    timeout: 10000
}
export const GB_CONFIG = {
  headers: {
    "content-type": "application/json",
    // "Authorization": "Basic "+base64.encode("rCS27x4vzQULoeyyw6uKJ1mKrKxe1g3X:")
    "Authorization": "Basic "+base64.encode("aqPOJuFUGGytfeYCB9d3IrMjyICBkzDB:")
  },
  timeout: 10000
}
export const GB_CONFIG_H = {
  headers: {
    "content-type": "application/x-www-form-urlencoded",
  },
  timeout: 10000
}

export const HOUSE_CARD = [
  {
    point:0,
    img:require('../../asset/Images/housecard/House_Card-00.png')
  },
  {
    point:1,
    img:require('../../asset/Images/housecard/House_Card-01.png')
  },
  {
    point:2,
    img:require('../../asset/Images/housecard/House_Card-02.png')
  },
  {
    point:3,
    img:require('../../asset/Images/housecard/House_Card-03.png')
  },
  {
    point:4,
    img:require('../../asset/Images/housecard/House_Card-04.png')
  },
  {
    point:5,
    img:require('../../asset/Images/housecard/House_Card-05.png')
  },
  {
    point:6,
    img:require('../../asset/Images/housecard/House_Card-06.png')
  },
  {
    point:7,
    img:require('../../asset/Images/housecard/House_Card-07.png')
  },
  {
    point:8,
    img:require('../../asset/Images/housecard/House_Card-08.png')
  },
  {
    point:9,
    img:require('../../asset/Images/housecard/House_Card-09.png')
  },
  {
    point:10,
    img:require('../../asset/Images/housecard/House_Card-10.png')
  },
]