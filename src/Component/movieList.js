import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    Image,
    FlatList,
    TouchableOpacity,
    Modal
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BLACK } from '../constant/color'
import { Content } from 'native-base';
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { Actions } from "react-native-router-flux";
import { TEXT_TH,TEXT_HEADER_EN, TEXT_BODY_EN,TEXT_BODY_EN_BOLD,TEXT_HEADER_EN_BOLD,TEXT_TH_BOLD,SIZE_3,SIZE_3_5 } from '../constant/font'

const dim = Dimensions.get('window');
class movieList extends Component {
    constructor(props){
        super(props)
        this.state = {
            movieList:this.props.data,
        }
    }
    componentDidMount(){
       
    }
    componentWillReceiveProps(nextProps){
       
    }
    _renderItem=({item})=>{
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date = moment(item.start_release_date,'YYYY-MM-DD HH:mm:ss')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        return(
            <TouchableOpacity style={{alignItems:'center',width:dim.width/100*45,paddingVertical:dim.height/100*1}} onPress={()=> {Actions.BookingSelectTimeScreen({data:item})}}>
                <Image
                    source={{uri:item.poster_mobile_path}}
                    style={{
                        width:dim.width/100*40,
                        height:dim.width/100*60,
                    }}
                    resizeMode='cover'
                />
                <Text style={{fontSize:SIZE_3_5,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,marginTop:dim.height/100*1}} numberOfLines={1}>{item.title}</Text>
                {<Text style={{fontSize:SIZE_3,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${day} ${monthtxt} ${yeartxt}`}</Text>}
            </TouchableOpacity>
        );
    } 
    _keyExtractor = (item, index) => index;
    render(){
        return(
            <Content>
                <FlatList
                    data={this.state.movieList}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    numColumns={2}
                    style={{alignSelf:'center'}}
                />
            </Content>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }

export default connect(mapStateToProps,actions)(movieList)