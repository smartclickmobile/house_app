import React, { Component } from "react"
import {
    Dimensions,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2 } from '../constant/color'
import { Header, Left, Body } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { TEXT_HEADER_EN, TEXT_HEADER_EN_BOLD,SIZE_2_7 } from '../constant/font'
const dim = Dimensions.get('window');
class headercustom extends Component {
    constructor(props){
        super(props)
        this.state= {
            index:1
        }
    }
    onselectmenu(index){
        this.props.setindexmenu(index)
        
        this.props.clearBooking()
        if(index == 1){
            this.props.setindexfooter(1)
        }else{
            this.props.setindexfooter(0)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.index_menu != null && nextProps.index_menu != undefined){
            this.setState({index:nextProps.index_menu})
        }
    }
    render(){
        if(this.props.transparent1){
            return(
                <Header noShadow
                    style={{
                        backgroundColor:'transparent',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11,
                        borderColor:'transparent'
                    }} 
                    androidStatusBarColor={BG_2}
                >
                    <Left style={{flex:1}}>
                       <Image 
                            source={require('../../asset/Images/logo_shot.png')}
                            style={{
                                width:dim.width/100*12
                            }}
                            resizeMode='contain'
                        />
                    </Left>
                    <Body style={{flex:9,flexDirection:'row'}}>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.onselectmenu(1)}>
                            <Text style={this.props.index_menu==1?styles.text_btnT_a:styles.text_btnT}>{I18n.t('header.ongoing')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.onselectmenu(2)}>
                            <Text style={this.props.index_menu==2?styles.text_btnT_a:styles.text_btnT}>{I18n.t('header.comingsoon')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.onselectmenu(3)}>
                            <Text style={this.props.index_menu==3?styles.text_btnT_a:styles.text_btnT}>{I18n.t('header.schedule')}</Text>
                        </TouchableOpacity>
                    </Body>
                </Header>
            )
        }else{
            return(
                <Header 
                    style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'#ddd',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11
                    }} 
                    androidStatusBarColor={BG_2}
                >
                    <Left style={{flex:1}}>
                       <Image 
                            source={require('../../asset/Images/logo_shot.png')}
                            style={{
                                width:dim.width/100*12
                            }}
                            resizeMode='contain'
                        />
                    </Left>
                    <Body style={{flex:9,flexDirection:'row'}}>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.onselectmenu(1)}>
                            <Text style={this.props.index_menu==1?styles.text_btn_a:styles.text_btn}>{I18n.t('header.ongoing')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.onselectmenu(2)}>
                            <Text style={this.props.index_menu==2?styles.text_btn_a:styles.text_btn}>{I18n.t('header.comingsoon')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1}} onPress={()=>this.onselectmenu(3)}>
                            <Text style={this.props.index_menu==3?styles.text_btn_a:styles.text_btn}>{I18n.t('header.schedule')}</Text>
                        </TouchableOpacity>
                    </Body>
                </Header>
            )
        }
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        index_menu:state.check.index_menu
    }
  }
  var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_2_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_2_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN_BOLD:TEXT_HEADER_EN_BOLD
    },
    text_btnT:{
        textAlign:'center',
        fontSize:SIZE_2_7,
        color:BG_1,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN
    },
    text_btnT_a:{
        textAlign:'center',
        fontSize:SIZE_2_7,
        color:BG_1,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN_BOLD:TEXT_HEADER_EN_BOLD
    }
  });

export default connect(mapStateToProps,actions)(headercustom)