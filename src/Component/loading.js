import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    ActivityIndicator
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2 } from '../constant/color'
import { Content } from 'native-base';
import { TEXT_HEADER_EN , TEXT_TH, TEXT_HEADER_EN_BOLD,SIZE_3,SIZE_5 } from '../constant/font'
const dim = Dimensions.get('window');
class loading extends Component {
    constructor(props){
        super(props)
        this.state= {
            index:1
        }
    }
    render(){
            return(
                <Content>
                    <View style={{alignItems:'center',justifyContent:'center',height:dim.height/100*80}}>
                        <ActivityIndicator size="large" color={this.props.black==false?BG_1:BLACK} />
                        <Text style={{fontSize:SIZE_3,fontFamily:TEXT_HEADER_EN,color:this.props.black==false?BG_1:BLACK}}>loading...</Text>
                    </View>
                </Content>
            )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }

export default connect(mapStateToProps,actions)(loading)