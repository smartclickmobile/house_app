import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2 } from '../constant/color'
import { Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_HEADER_EN , TEXT_TH, TEXT_HEADER_EN_BOLD,SIZE_3,SIZE_5 } from '../constant/font'
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk';
const dim = Dimensions.get('window');
class header extends Component {
    constructor(props){
        super(props)
        this.state= {
            index:1
        }
    }
    onselectmenu(index){
        this.props.setindexmenu(index)
       
        if(index == 1){
            this.props.setindexfooter(1)
        }else{
            this.props.setindexfooter(0)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.index_menu != null && nextProps.index_menu != undefined){
            this.setState({index:nextProps.index_menu})
        }
    }
    onback(){
        console.warn(Actions.currentScene)
        if(Actions.currentScene == 'confirmmobile'){
            // GoogleSignin.revokeAccess();
            GoogleSignin.signOut();
            LoginManager.logOut()
        }
        if(Actions.currentScene == 'detailbaner'){
            this.props.clear_buffet()
        }
        if(Actions.currentScene == 'FromStudentScreen'){
            this.props.clearStudent()
        }
       
        Actions.pop()
    }
    render(){
            return(
                <Header 
                    style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'#ddd',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11
                    }} 
                    androidStatusBarColor={BG_2}
                >
                    <Left style={{flex:this.props.title!=undefined?0:2}}>
                        {this.props.btnleft?<Button transparent onPress={()=> this.onback()}>
                            <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                            {this.props.title!=undefined?<View/>:<Title style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3}}>{I18n.t('header.back')}</Title>}
                        </Button>:<View/>}
                    </Left>
                    <Body style={{flex:9,flexDirection:'row',justifyContent:'center'}}>
                        {this.props.title!=undefined?<Text style={[styles.text_btn_a,{fontSize:SIZE_5}]}>{this.props.title}</Text>:<View/>}
                    </Body>
                    <Right>
                    </Right>
                </Header>
            )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        index_menu:state.check.index_menu
    }
  }
  var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN_BOLD:TEXT_HEADER_EN_BOLD
    }
  });

export default connect(mapStateToProps,actions)(header)