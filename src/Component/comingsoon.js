import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    Image,
    FlatList,
    TouchableOpacity,
    Modal,
    RefreshControl,
    Alert,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BLACK, BG_1 } from '../constant/color'
import { Content } from 'native-base';
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { 
    TEXT_TH, 
    TEXT_BODY_EN, 
    TEXT_HEADER_EN, 
    TEXT_TH_BOLD, 
    TEXT_BODY_EN_BOLD, 
    TEXT_HEADER_EN_BOLD,

    SIZE_3,
    SIZE_3_5,
    SIZE_4
} from '../constant/font'
import { Actions } from "react-native-router-flux";
import Carousel from 'react-native-banner-carousel';
import Loading from '../Component/loading'
const dim = Dimensions.get('window');
class comingsoon extends Component {
    constructor(props){
        super(props)
        this.state = {
            movieList:[],
            branner:null,
            loading:true
        }
    }
    
    componentDidMount(){
        this.fetchData()
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.comingsoonList != null && nextProps.comingsoonList != undefined){
            this.setState({movieList:nextProps.comingsoonList,loading:false})
        }

        if(nextProps.branner != null && nextProps.branner != undefined){
            this.setState({branner:nextProps.branner})
        }

        if(nextProps.error != null && nextProps.error != undefined){
            Alert.alert('',nextProps.error,[
                {
                    text: I18n.t('try_again'),
                    onPress: () => {
                        this.fetchData()
                    }
                }
            ],
            { cancelable: false })
        }
    }
    _onRefresh = () => {
        this.setState({loading: true});
        this.fetchData()
      }
    fetchData(){
        var data = {
            language:I18n.locale
        }
        this.props.getComingsoon(data)
        this.props.getBraner(data)
    }
    _renderItem=({item})=>{
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var day_th = ["อ.","จ.","อ.","พ.","พฤ.","ศ.","ส.","อ."]
        var day_en = ["SUN","MON","TUE","WED","THU","FRI","SAT","SUN"]
        var date = moment(item.start_release_date,'YYYY-MM-DD HH:mm:ss')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        var days = I18n.locale == 'th'?day_th[date.day()]:day_en[date.day()]
        var days_t = date.format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')?I18n.t('BookingSelectTimeScreen.to_day'):days
        return(
            <TouchableOpacity style={{alignItems:'center',width:dim.width/100*50,paddingVertical:dim.height/100*1}} onPress={()=> {Actions.BookingSelectTimeScreen({data:item,flaq_detail:true})}}>
                <Image
                    source={{uri:item.poster_mobile_path}}
                    style={{
                        width:dim.width/100*48,
                        height:dim.width/100*70,
                    }}
                    resizeMode='cover'
                />
                <Text style={{fontSize:SIZE_3_5,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,marginTop:dim.height/100*1}} numberOfLines={1}>{item.title}</Text>
                {<Text style={{fontSize:SIZE_3,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${days_t} ${day} ${monthtxt}`}</Text>}
            </TouchableOpacity>
        );
    } 
    _keyExtractor = (item, index) => item.id;
    renderPage(image, index) {
        return (
            <TouchableOpacity key={index} onPress={()=> Actions.detailbaner({data:image})}>
                <Image 
                style={{ 
                    width: dim.width, 
                    height: Platform.isPad ? dim.width/100*20 :dim.width
                }} resizeMode='cover' source={{ uri: Platform.isPad? image.image_web_path : image.image_mobile_path }} />
            </TouchableOpacity>
        );
    }
    render(){
        if(this.state.loading == true){
            return(
                <Loading/>
            )
        }else{
            return(
                <Content refreshControl={
                    <RefreshControl
                      refreshing={this.state.loading}
                      onRefresh={this._onRefresh}
                    />
                  }>
                    <Carousel
                        autoplay
                        autoplayTimeout={5000}
                        loop
                        index={0}
                        pageSize={dim.width}
                    >
                        {this.state.branner!= null && this.state.branner.map((image, index) => this.renderPage(image, index))}
                    </Carousel>
                    <Image 
                        source={require('../../asset/Images/logo.png')}
                        style={{
                            width:Platform.isPad? dim.width/100*20 : dim.width/100*30,
                            height:dim.height/100*7,
                            margin:dim.height/100*2,
                            alignSelf:'center'
                        }}
                        resizeMode='contain'
                    />
                    <View style={{backgroundColor:'#565455',paddingVertical:dim.height/100*1.5}}>
                        <Text style={{fontSize:SIZE_3_5,textAlign:'center',color:BG_1,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD}}>{I18n.t('tab.house.comingsoon.title')}</Text>
                    </View>
                    
                    <FlatList
                        data={this.state.movieList}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        numColumns={2}
                        style={{alignSelf:'center'}}
                    />
                </Content>
            )
        }
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        comingsoonList:state.home.comingsoon_list,
        branner:state.home.branner,
        error:state.home.error,
        error_num:state.home.error_num
    }
  }

export default connect(mapStateToProps,actions)(comingsoon)