import React, { Component } from "react"
import {
    Dimensions,
    StatusBar,
    Text,
    View,
    Image,
    StyleSheet,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK } from '../constant/color'
import { Footer, FooterTab, Button} from 'native-base';
import { identifier } from "@babel/types";
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_HEADER_EN, TEXT_TH, TEXT_HEADER_EN_BOLD,SIZE_2_7 } from '../constant/font'
const dim = Dimensions.get('window');
class footercustom extends Component {
  constructor(props){
    super(props)
    this.state = {
      index:1,
    }
  }
  onselectmenu(index){
    this.props.setindexfooter(index)
    this.props.resetProfile(0)
    this.props.clearBooking()
    this.props.resetInfoProfile()
    if(index == 1){
      this.props.setindexmenu(1)
    }else{
      this.props.setindexmenu(0)
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.index_footer != null && nextProps.index_footer != undefined){
      this.setState({index:nextProps.index_footer})
    }
  }
    render(){
      if(this.props.transparent1){
        return(
          <Footer style={[this.props.checkIpnoe == 'xr' || this.props.checkIpnoe =='x'?styles.footerX:styles.footer,{backgroundColor:'transparent'}]}>
            <FooterTab style={{backgroundColor:'transparent'}}>
              <Button vertical onPress={()=> this.onselectmenu(1)}>
                <Image 
                  source={require('../../asset/Images/w-icon-Home.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==1?styles.textT_a:styles.textT}>{I18n.t('footer.house')}</Text>
              </Button>
              <Button vertical onPress={()=> this.onselectmenu(2)}>
                <Image 
                  source={require('../../asset/Images/w-icon-Wish-List.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==2?styles.textT_a:styles.textT}>{I18n.t('footer.wish')}</Text>
              </Button>
              <Button vertical onPress={()=> this.onselectmenu(3)}>
                <Image 
                  source={require('../../asset/Images/w-icon-Ticket.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==3?styles.textT_a:styles.textT}>{I18n.t('footer.tickets')}</Text>
              </Button>
              <Button vertical onPress={()=> this.onselectmenu(4)}>
                <Image 
                  source={require('../../asset/Images/w-icon-Profile.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==4?styles.textT_a:styles.textT}>{I18n.t('footer.profile')}</Text>
              </Button>
            </FooterTab>
          </Footer>
      )
      }else{
        return(
          <Footer style={[this.props.checkIpnoe == 'xr' || this.props.checkIpnoe =='x'?styles.footerX:styles.footer,{backgroundColor:BG_1}]}>
            <FooterTab style={{backgroundColor:BG_1}}>
              <Button vertical onPress={()=> this.onselectmenu(1)}>
                <Image 
                  source={require('../../asset/Images/icon_home.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==1?styles.text_a:styles.text}>{I18n.t('footer.house')}</Text>
              </Button>
              <Button vertical onPress={()=> this.onselectmenu(2)}>
                <Image 
                  source={require('../../asset/Images/icon_star.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==2?styles.text_a:styles.text}>{I18n.t('footer.wish')}</Text>
              </Button>
              <Button vertical onPress={()=> this.onselectmenu(3)}>
                <Image 
                  source={require('../../asset/Images/icon_s.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==3?styles.text_a:styles.text}>{I18n.t('footer.tickets')}</Text>
              </Button>
              <Button vertical onPress={()=> this.onselectmenu(4)}>
                <Image 
                  source={require('../../asset/Images/icon_people.png')}
                  style={styles.icon}
                  resizeMode='contain'
                />
                <Text style={this.props.index_footer==4?styles.text_a:styles.text}>{I18n.t('footer.profile')}</Text>
              </Button>
            </FooterTab>
          </Footer>
      )
      }
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        index_menu:state.check.index_menu,
        index_footer:state.check.index_footer,
    }
  }
  var styles = StyleSheet.create({
    text_a:{
      color:BLACK,
      fontFamily:I18n.locale=='th'?TEXT_HEADER_EN_BOLD:TEXT_HEADER_EN_BOLD,
      fontSize:SIZE_2_7,
      marginTop:dim.height/100*1
    },
    text:{
      color:BLACK,
      fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN,
      fontSize:SIZE_2_7,
      marginTop:dim.height/100*1
    },
    textT_a:{
      color:BG_1,
      fontFamily:I18n.locale=='th'?TEXT_HEADER_EN_BOLD:TEXT_HEADER_EN_BOLD,
      fontSize:SIZE_2_7,
      marginTop:dim.height/100*1
    },
    textT:{
      color:BG_1,
      fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN,
      fontSize:SIZE_2_7,
      marginTop:dim.height/100*1
    },
    icon:{
      width:Platform.isPad?dim.width/100*4:dim.width/100*5,
      height:Platform.isPad?dim.width/100*4:dim.width/100*5,
      marginBottom:2
    },
    footer:{
      height:dim.height/100*8
    },
    footerX:{
      height:dim.height/100*5
    }
  })

export default connect(mapStateToProps,actions)(footercustom)