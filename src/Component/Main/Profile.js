import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Modal,

    Image,
    ScrollView,
    Platform,
    Alert
  } from "react-native"
import * as actions from "../../Actions"
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2, RED_COLOR } from '../../constant/color'
import { Content, Card, CardItem, Body, Left, List,Icon } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../../asset/languages/i18n'
import { 
    TEXT_TH,
    TEXT_BODY_EN,
    TEXT_HEADER_EN,
    TEXT_TH_BOLD,
    TEXT_BODY_EN_BOLD,
    TEXT_HEADER_EN_BOLD,
    SIZE_3,SIZE_3_5,
    SIZE_4,SIZE_7,
    SIZE_3_7,
    SIZE_5,
    SIZE_2_9,
    SIZE_6,
    SIZE_4_5,
    SIZE_2_5
} from '../../constant/font'
import { HOUSE_CARD } from '../../constant/constant'
import Loading from '../loading'
import moment from 'moment'
import DeviceInfo from "react-native-device-info"
const dim = Dimensions.get('window');
class Proflie extends Component {
  constructor(props){
    super(props)
    this.state = {
       data:null,
       token:null,
       type:'',
       profile_info_num:null,
       loading:true,
       voucher_num:null,
       voucher:[],
       maxVoucher:0,
       voucher_select:0,
       exchange_success_num:null,
       modalRedeem:false,
       modalSuccess:false,

       card_point:[],
       member_id:''
    }
  }
  async componentDidMount(){
     
      var token = await AsyncStorage.getItem('token')
      var type = await AsyncStorage.getItem('login_type')
    this.setState({token:token,type:type},()=>{
        if(this.state.token == null || this.state.token == ''){
            this.setState({loading:false})
        }else{
            var data = {
                language: I18n.locale
            }
            this.props.getProfileInfo(data)
            this.props.GetCreditCard(data)
            this.props.member_privilege(data)
        }
    })

    // this.convertTextno('13273')
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.profile_info != null && nextProps.profile_info != undefined){
        this.setState({loading:false})
    }
    if(nextProps.profile_info != null && nextProps.profile_info != undefined && this.state.profile_info_num != nextProps.profile_info_num){
        this.setState({data:nextProps.profile_info,profile_info_num:nextProps.profile_info_num,loading:false},()=>{
            var temp = {
                language:I18n.locale,
                telephone:this.state.data.telephone
            }
            this.props.getVoucher(temp)
            if(this.state.data.score != null){
                var mx_voucher = parseInt(this.state.data.score)/10
                var int_m = parseInt(mx_voucher)
                this.setState({maxVoucher:int_m,voucher_select:int_m},()=>{
                    var data_score_mod = parseInt(this.state.data.score)%10
                    var length = this.state.maxVoucher
                    var data_score = []
                    if(data_score_mod != 0){
                        length += 1
                    }
                    for(var i = 0 ; i < length ; i++){
                        if(i == length-1){
                            if(data_score_mod != 0){
                                var temp = {
                                    no:i,
                                    point:data_score_mod
                                }
                            }else{
                                var temp = {
                                    no:i,
                                    point:10
                                }
                            }
                           
                        }else{
                            var temp = {
                                no:i,
                                point:10
                            }
                        }
                        data_score.push(temp)
                    }
                    this.setState({card_point:data_score})
                })
            }
            this.convertTextno(this.state.data.member_id)
        })
    }
    if(nextProps.voucher != null && nextProps.voucher != undefined && this.state.voucher_num != nextProps.voucher_num){
        this.setState({voucher:nextProps.voucher,voucher_num:nextProps.voucher_num})
    }
    if(nextProps.exchange_success_num != this.state.exchange_success_num){
        this.setState({exchange_success_num:nextProps.exchange_success_num},()=>{
            
            if(nextProps.exchange_success == true ){
                var datanew = {
                    language: I18n.locale
                }
                this.props.getProfileInfo(datanew)
               this.setState({modalRedeem:false,modalSuccess:true})
            }
        })
    }
    if(nextProps.delete_user != null && nextProps.delete_user != undefined && this.state.delete_user_num != nextProps.delete_user_num){
    //    this.successDeleteduser()
    }
    
  }
    onLogout(){
        var data = {
            language:I18n.locale
        }
        this.props.logout(data)
    }
    onRedeemVoucher(){
        this.setState({modalRedeem:true})
    }
    removeVoucher(){
        if(parseInt(this.state.voucher_select) != 0){
            this.setState({voucher_select:parseInt(this.state.voucher_select)-1})
        }
    }
    addVoucher(){
        if(parseInt(this.state.voucher_select) < parseInt(this.state.maxVoucher)){
            this.setState({voucher_select:parseInt(this.state.voucher_select)+1})
        }
    }
    onexchange(){
        var data = {
            language:I18n.locale,
            total_voucher:this.state.voucher_select
        }
        console.log('onexchange',data)
        this.props.exchangePoint(data)
    }
    convertTextno(no){
        var num = 9 - no.length
        var no_text = ''
        for(var i = 0 ; i <= num ; i++){
            if(i == num){
                no_text += no
            }else{
                no_text += '0'
            }
        }
        var num1 = no_text.substring(0, 3)
        var num2 = no_text.substring(3, 6)
        var num3 = no_text.substring(6, 9)
        var num_text = num1+' '+num2+' '+num3
        console.log('convertTextno',num_text)
        this.setState({member_id:num_text})
    }
    goToRegisterStudent(){
        if(this.props.profile_info.student_info == null || this.props.profile_info.student_info == {} || this.props.profile_info.student_info == undefined){
            Actions.FromStudentScreen()
        }else{
            Alert.alert("",I18n.t("FromStudentScreen.error.register"))
        }
    }

    deleteUser(){
        Alert.alert("", I18n.t('tab.profile.delete_user_text'), [
            {
              text: I18n.t('tab.profile.delete_user_yes'),
              onPress: () => {
                var temp = {
                    language:I18n.locale,
                }
                 this.props.deleteUser(temp)
              },
            },
            ,
            {
              text:  I18n.t('tab.profile.delete_user_no'),
              onPress: () => {console.log("Cancel Pressed")},
              style: "cancel"
            },
          ]);
    }
    async successDeleteduser(){
       
        Alert.alert("", I18n.t('tab.profile.delete_user_success'), [
            {
              text: I18n.t('tab.profile.delete_user_yes'),
              onPress: () => {
                 
              },
            }
             
          ]);
    }

  _renderMainProflie(){
      var data = this.state.data
      var apply = true
      let student = this.props.profile_info.student_info != null && this.props.profile_info.student_info != {} && this.props.profile_info.student_info != undefined 
      if(data != null){
          if(data.member_type_id == 1){
            apply = true
          }else{
            apply = false
          }
          var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
          var date = moment(data.created_date,'YYYY-MM-DD HH:mm:ss')
        //   let stu_date = student? moment(data.created_date,'YYYY-MM-DD HH:mm:ss') : null
            
          var day = date.date()
          var month = date.month()
          var monthtxt = month_en[month]
          var year = date.year()
          var yeartxt =  parseInt(year)
          var text = `${day} ${monthtxt} ${yeartxt}`

          if(student){
            // student_number
            let month2 = this.props.profile_info.student_info.expire_month-1
            let monthtxt2 = month_en[month2]

            let yeartxt2 =  this.props.profile_info.student_info.expire_year
            var text2 = `${monthtxt2} ${yeartxt2}`
            var student_number = this.props.profile_info.student_info.student_number
          }
      }
      console.warn('profile info',this.props.profile_info)
    
    return(
        <Content style={{marginTop:0}}>
            <Modal visible={this.state.modalRedeem} transparent={true}>
                <View style={styles.modal_container}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={styles.modal_contain}>
                            <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.redeem_voucher')}</Text>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.my_point')}</Text>
                                <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,marginHorizontal:dim.width/100*3}}>{this.state.data != null?this.state.data.score==null||this.state.data.score==''?0:this.state.data.score : ''}</Text>
                                <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.point_unit')}</Text>
                            </View>
                            <Text style={{marginVertical:dim.height/100*1.5,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:RED_COLOR}}>{I18n.t('tab.profile.remark')}</Text>
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <TouchableOpacity disabled={this.state.voucher_select == 0} style={{flex:1,justifyContent:'center',alignItems:'center',opacity:this.state.voucher_select == 0?0.5:1}} onPress={()=> this.removeVoucher()}>
                                    <Icon name='caret-left' type='FontAwesome5' style={{color:BLACK}}/>
                                </TouchableOpacity>
                                <Text style={{flex:1,textAlign:'center',marginVertical:dim.height/100*1.5,fontSize:SIZE_6,marginHorizontal:dim.width/100*3,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{this.state.voucher_select}</Text>
                                <TouchableOpacity disabled={this.state.voucher_select == this.state.maxVoucher} style={{flex:1,justifyContent:'center',opacity:this.state.voucher_select == this.state.maxVoucher?0.5:1,alignItems:'center'}} onPress={()=> this.addVoucher()}>
                                    <Icon name='caret-right' type='FontAwesome5' style={{color:BLACK}}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*30}} onPress={()=>{this.setState({modalRedeem:false})}}>
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.cancel')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity disabled={this.state.voucher_select == 0} style={{borderColor:BG_1,opacity:this.state.voucher_select == 0?0.5:1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*30}} onPress={()=>{this.onexchange()}}>
                                    <Text style={{textAlign:'center',fontSize:SIZE_4,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.profile.alert.ok')}</Text>
                                </TouchableOpacity>
                            </View>   
                        </View>
                    </View>
            </Modal>
            {/* Image + Name */}
            <View style={{flexDirection:'row',padding:dim.width/100*3}}>
                <View style={{width:Platform.isPad?dim.width/100*20:dim.width/100*25,height:Platform.isPad?dim.width/100*20:dim.width/100*25,alignItems:'center',justifyContent:'center',borderRadius:Platform.isPad?dim.width/100*10:dim.width/100*12.5,borderWidth:0.5,padding:dim.width/100*2}}>
                    <Image
                        source={require('../../../asset/Images/NewProject.png')}
                        style={{
                            width:Platform.isPad?dim.width/100*15:dim.width/100*20,
                            height:Platform.isPad?dim.width/100*15:dim.width/100*20,
                            borderRadius:Platform.isPad?dim.width/100*7.5:dim.width/100*10,
                            
                            // padding:20
                        }}
                    />
                </View>
                
                <View style={{flex:2,justifyContent:'center',padding:dim.width/100*3}}>
                    {apply&&<Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,fontSize:SIZE_4,color:BLACK,marginVertical:dim.height/100*0.5}}>{data==null?'':data.email==''?data.telephone:data.email.toUpperCase()}</Text>}
                    {!apply&&<Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,fontSize:SIZE_4,color:BLACK,marginVertical:dim.height/100*0.5}}>{data==null?'':data.first_name.toUpperCase() + '  ' + data.last_name.toUpperCase()}</Text>}
                    {!apply&&<Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3,color:BLACK,marginVertical:dim.height/100*0.5}}>{data==null?'':`HOUSE ${data.member_type_name.toUpperCase()}`}</Text>}
                    {!apply&&<TouchableOpacity style={{marginVertical:dim.height/100*0.5}} onPress={()=> Actions.Profilepage()}>
                        <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.edit_proflie')}</Text>
                    </TouchableOpacity>}
                </View>
            </View>

            


            {/* Recent Notitfication */}
            {/*<View style={{padding:dim.width/100*3}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.recent_noti')}</Text>
                <Card style={{borderRadius:dim.width/100*2.5}}>
                    <CardItem style={{borderRadius:dim.width/100*2.5}}>
                        <Left style={{flex:2}}>
                            <Image
                                source={require('../../../asset/Images/logo_shot.png')}
                                style={{
                                    width:dim.width/100*10,
                                    height:dim.width/100*10
                                }}
                            />
                        </Left>
                        <Body style={{flex:8,justifyContent:'center'}}>
                            <Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>testtesttestsetetestsetetsetststetsetsetsetsetsetsetset</Text>
                        </Body>
                    </CardItem>
                </Card>
            </View>*/}
            {/* My Card */}
            {!apply?
            <View style={{padding:dim.width/100*3}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.my_card')}</Text>
                 {/* Member Card */}
                <View style={{
                    height:Platform.isPad?dim.height/100*30:dim.height/100*25,
                    width:Platform.isPad?dim.width/100*70:dim.width/100*90,
                    backgroundColor:'#565455',
                    borderRadius:dim.width/100*2.5,
                    alignSelf:'center'
                }}>
                    <View style={{flex:1,alignItems:'flex-end',padding:dim.width/100*3}}>
                        <Image
                            source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                            style={{
                                width:dim.width/100*30,
                                height:dim.height/100*6,
                            }}
                            resizeMode='contain'
                        />
                    </View>
                    <View style={{flex:1,padding:dim.width/100*3,justifyContent:'center'}}>
                        <Text style={{fontFamily:TEXT_HEADER_EN,fontSize:SIZE_4_5,color:BG_1,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.house_club')}</Text>
                    </View>
                    <View style={{flex:1,padding:dim.width/100*3,justifyContent:'flex-end'}}>
                        <Text style={{fontFamily:TEXT_HEADER_EN,fontSize:SIZE_3_5,color:BG_1}}>{`${I18n.t('tab.profile.no_card')} ${this.state.member_id}`}</Text>
                        <Text style={{fontFamily:TEXT_BODY_EN,fontSize:SIZE_3,color:BG_1}}>{`${I18n.t('tab.profile.for_life')} ${text}`}</Text>
                    </View>
                </View>
            </View>
            :
            <View style={{padding:dim.width/100*3}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.my_card')}</Text>
                <TouchableOpacity style={{borderWidth:dim.width/100*0.2,borderColor:BG_2,width:dim.width/100*70,alignSelf:'center',padding:dim.width/100*3,borderRadius:dim.width/100*3,alignItems:'center',justifyContent:'center'}} onPress={()=> Actions.Ehome()}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.apply_btn')}</Text>
                </TouchableOpacity>
            </View>
            }
            {student&&this.props.profile_info.student_info.status ==='APPROVE' &&
               <View style={{padding:dim.width/100*3}}>
                {/* student Card */}
               <View style={{
                   height:Platform.isPad?dim.height/100*30:dim.height/100*25,
                   width:Platform.isPad?dim.width/100*70:dim.width/100*90,
                   backgroundColor:'#1288d5',
                   borderRadius:dim.width/100*2.5,
                   alignSelf:'center'
               }}>
                   <View style={{flex:1,alignItems:'flex-end',padding:dim.width/100*3}}>
                       <Image
                           source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                           style={{
                               width:dim.width/100*30,
                               height:dim.height/100*6,
                           }}
                           resizeMode='contain'
                       />
                   </View>
                   <View style={{flex:1,padding:dim.width/100*3,justifyContent:'center'}}>
                       <Text style={{fontFamily:TEXT_HEADER_EN,fontSize:SIZE_4_5,color:BG_1,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.student_club')}</Text>
                   </View>
                   <View style={{flex:1,padding:dim.width/100*3,justifyContent:'flex-end'}}>
                       <Text style={{fontFamily:TEXT_HEADER_EN,fontSize:SIZE_3_5,color:BG_1}}>{`${I18n.t('tab.profile.no_card')} ${student_number}`}</Text>
                       <Text style={{fontFamily:TEXT_BODY_EN,fontSize:SIZE_3,color:BG_1}}>{`${I18n.t('tab.profile.student_expire')} ${text2}`}</Text>
                   </View>
               </View>
           </View>
            }
            {/* Voucher Card */}
            <View style={{padding:dim.width/100*3}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{`${I18n.t('tab.profile.point_unit')}`}</Text>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3_5,color:BLACK}}>{`${I18n.t('tab.profile.my_voucher')}  ${this.state.voucher.length}`}</Text>
                <View style={{flexDirection:'row',marginVertical:dim.height/100*0.5,alignItems:'center'}}>
                    <Text numberOfLines={1} style={{flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{`${I18n.t('tab.profile.my_point')}  ${data==null?0:data.score == 'null'||data.score == null?0:data.score}`}</Text>
                    <TouchableOpacity style={{
                        padding:dim.width/100*1.5,
                        alignItems:'center',
                        borderColor:'#565455',
                        borderWidth:1,
                        borderRadius:dim.width/100*1.5,
                        margin:dim.width/100*1.5,
                        backgroundColor:'#565455'
                    }}
                    onPress={()=> this.onRedeemVoucher()}
                    >
                        <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BG_1,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.redeem_voucher')}</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView horizontal style={{flexDirection:'row'}}>
                    {
                        this.state.card_point.map((item,index)=>{
                            // console.log(item.point)
                            return(
                                <Image
                                key={index}
                                source={HOUSE_CARD[item.point].img}
                                style={{
                                    width:Platform.isPad?dim.width/100*46:dim.width/100*92,
                                    height:Platform.isPad?dim.width/100*30:dim.width/100*60,
                                    marginLeft:index==0?0:dim.width/100*2
                                }}
                                resizeMode='contain'
                            />
                            )
                        })
                    }
                </ScrollView>
            </View>
            {/* Other */}
            <View style={{padding:dim.width/100*3}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD,fontSize:SIZE_3_5,color:BLACK,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.other')}</Text>

                <TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> this.goToRegisterStudent()}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>STUDENT REGISTER</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> Actions.ProfileChangemobile()}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.change_mobile_number')}</Text>
                </TouchableOpacity>

                {!apply&&<TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> Actions.Ememberprivilege()}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.ememberprivilege')}</Text>
                </TouchableOpacity>}

                <TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> Actions.ProfileChangePassword()}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.change_password_btn')}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> {this.deleteUser()}}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.delete_user')}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> Actions.ProfileSetting()}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.setting_btn')}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{backgroundColor:BG_1,borderWidth:dim.width/100*0.2,borderColor:BG_2,padding:dim.width/100*4}} onPress={()=> {this.onLogout()}}>
                    <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{I18n.t('tab.profile.signout_btn')}</Text>
                </TouchableOpacity>
            </View>
            <View style={{padding:dim.width/100*3,alignItems:'flex-end'}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{'v.'+DeviceInfo.getVersion()}</Text>
            </View>
        </Content>
    )
  }
  _renderNoLogin(){
    return(
        <Content style={{marginTop:0}}>
            <View style={{flexDirection:'row',padding:dim.width/100*3}}>
                <View style={{width:Platform.isPad?dim.width/100*20:dim.width/100*25,height:Platform.isPad?dim.width/100*20:dim.width/100*25,alignItems:'center',justifyContent:'center',borderRadius:Platform.isPad?dim.width/100*10:dim.width/100*12.5,borderWidth:0.5,padding:dim.width/100*2}}>
                    <Image
                        source={require('../../../asset/Images/NewProject.png')}
                        style={{
                            width:Platform.isPad?dim.width/100*15:dim.width/100*20,
                            height:Platform.isPad?dim.width/100*15:dim.width/100*20,
                            borderRadius:Platform.isPad?dim.width/100*7.5:dim.width/100*10,
                            
                            // padding:20
                        }}
                    />
                </View>
                
                <View style={{flex:2,justifyContent:'center',padding:dim.width/100*3}}>
                    {<Text style={{fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,fontSize:SIZE_4,color:BLACK,marginVertical:dim.height/100*0.5}}>{I18n.t('tab.profile.dont_login')}</Text>}
                </View>
            </View>
            {/*<Text style={[styles.text_btn_a,{fontSize:SIZE_7,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN}]}>{I18n.t('tab.profile.profile')}</Text>
                    <Text style={[styles.text_btn,{fontSize:SIZE_5,marginTop:dim.height/100*1.5}]}>{I18n.t('tab.profile.dont_login')}</Text>*/}
            <TouchableOpacity 
                style={{
                    flexDirection:'row',
                    alignItems:'center',
                    justifyContent:'center',
                    width:Platform.isPad?dim.width/100*50:dim.width/100*65,
                    // backgroundColor:"#ccd1d5",
                    paddingVertical:dim.height/100*1.5,
                    borderRadius:dim.width/100*1,
                    borderColor:BG_2,
                    borderWidth:1,
                    marginTop:dim.height/100*2.5,
                    height:dim.height/100*6,
                    alignSelf:'center'
                }} 
                onPress={()=> {
                    Actions.popTo('loginhome')
                    Actions.replace('loginhome')
                }}
            >
                <Text style={{
                    marginHorizontal:dim.width/100*2,
                    fontSize:SIZE_3,
                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN
                }}
                >{I18n.t('tab.profile.login_btn')}</Text>
            </TouchableOpacity>
            <View style={{padding:dim.width/100*3,alignItems:'flex-end',marginTop:dim.height/100*50}}>
                <Text numberOfLines={1} style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:SIZE_3,color:BLACK}}>{'v.'+DeviceInfo.getVersion()}</Text>
            </View>
        </Content>
    )
  }
    render(){
        if(this.state.loading == true){
            return(
                <Loading/>
            )
        }else{
            if(this.state.token == null || this.state.token == ''){
                return(
                    this._renderNoLogin()
                )
            }else{
                return(
                    this._renderMainProflie()
                )
            }
        }
       
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile_info:state.profile.profile_info,
        profile_info_num:state.profile.profile_info_num,
        voucher:state.profile.voucher,
        voucher_num:state.profile.voucher_num,
        exchange_success:state.voucher.exchange_success,
        exchange_success_num:state.voucher.exchange_success_num,
        delete_user:state.profile.delete_user,
        delete_user_num:state.profile.delete_user_num,
    }
  }
  var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    Container_row:{
        padding:dim.width/100*3,width:dim.width/100*35
    },
    txt_h:{
        fontSize:SIZE_3,
        color:BLACK,

        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    txt_d:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    txt_d_select:{
        fontSize:SIZE_2_9,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    },
    btn:{
        padding:dim.width/100*1.5,
        alignItems:'center',
        borderColor:BG_2,
        borderWidth:1,
        borderRadius:dim.width/100*1.5,
        margin:dim.width/100*1.5
    },
    btn1:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    modal_container:{
        width: "100%",
        height: "100%",
        position: "absolute",
        backgroundColor:'black',
        opacity:0.7
    },
    modal_contain:{
        backgroundColor:'#fff',
        width:dim.width/100*80,
        borderRadius:dim.width/100*3,
        alignItems:'center',
        padding:dim.width/100*5
    },

});
  

export default connect(mapStateToProps,actions)(Proflie)