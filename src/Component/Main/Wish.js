import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    FlatList,

    Modal,
    RefreshControl,
    Alert
  } from "react-native"
import * as actions from "../../Actions"
import { connect } from "react-redux"
import { BLACK } from '../../constant/color'
import {  Content } from 'native-base';
import I18n from '../../../asset/languages/i18n'
import { TEXT_TH,TEXT_BODY_EN,TEXT_HEADER_EN,TEXT_BODY_EN_BOLD,TEXT_HEADER_EN_BOLD,TEXT_TH_BOLD,SIZE_3_5,SIZE_4,SIZE_7,SIZE_3_7 } from '../../constant/font'
import { Actions } from 'react-native-router-flux'
import moment from 'moment'
import AsyncStorage from '@react-native-async-storage/async-storage';
const dim = Dimensions.get('window');
import Loading from '../loading'
class Wish extends Component {
    constructor(props){
        super(props)
        this.state = {
            movieList:[],
            loading:true
        }
    }
    async componentDidMount(){
       this.fetchData()
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.wishlist != null && nextProps.wishlist != null){
            this.setState({movieList:nextProps.wishlist,loading:false})
        }
        if(nextProps.error != null && nextProps.error != undefined){
            Alert.alert('',nextProps.error,[
                {
                    text: I18n.t('try_again'),
                    onPress: () => {
                        this.fetchData()
                    }
                }
            ],
            { cancelable: false })
        }
    }
    _onRefresh = () => {
        this.setState({loading: true});
        this.fetchData()
    }
    async fetchData(){
        var token = await AsyncStorage.getItem('token')
        if(token == '' || token == null){
            this.setState({loading:false})
        }else{
            var data={
                language:I18n.locale
            }
            this.props.getwish(data)
        }
    }
  _renderItem_old = ({item}) => {
      var flaq_detail = false
      var date_now = moment()
      var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date = moment(item.movie.start_release_date,'YYYY-MM-DD HH:mm:ss')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        var data = item
        if(date > date_now){
            flaq_detail = true
        }
    return(
        <TouchableOpacity style={{flex:1,flexDirection:'row',marginVertical:dim.height/100*1}} onPress={()=> Actions.BookingSelectTimeScreen({data:item.movie,flaq_detail:flaq_detail,wish:true})}>
            <Image
                source={{uri:item.movie.poster_mobile_path}}
                style={{
                    width:dim.width/100*40,
                    height:dim.height/100*34,
                    marginLeft:dim.width/100*4
                }}
                resizeMode='contain'
            />
            <View style={{flex:1,marginTop:dim.height/100*2.5,marginLeft:dim.height/100*2.5}}>
                <Text style={{fontSize:SIZE_4,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.movie.title}</Text>
                <Text style={{fontSize:SIZE_3_5,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{`${day} ${monthtxt} ${yeartxt}`}</Text>
            </View>
            
        </TouchableOpacity>
    );
  }
  _renderItem = ({item}) => {
    var flaq_detail = false
    var date_now = moment()
    var date = moment(item.movie.start_release_date,'YYYY-MM-DD HH:mm:ss')
    if(date > date_now){
        flaq_detail = true
    }
    var data = {
        movie_id:item.movie.movie_id,
        movie_category_id:item.movie.movie_category_id,
        movie_category_name:item.movie.movie_category_name,
        rate_id:item.movie.rate_id,
        rate_name:item.movie.rate_name,
        title:item.movie.title,
        short_description:item.movie.short_description,
        description:item.movie.description,
        show_time:item.movie.show_time,
        start_release_date:item.movie.start_release_date,
        end_release_date:item.movie.end_release_date,
        poster_web_path:item.movie.poster_web_path,
        poster_mobile_path:item.movie.poster_mobile_path,
        trailer_web_path:item.movie.trailer_web_path,
        trailer_mobile_path:item.movie.trailer_mobile_path,
        wish_id:item.wish_id
    }
  return(
      <TouchableOpacity style={{flex:1,alignItems:'center'}} onPress={()=> Actions.BookingSelectTimeScreen({data:item.movie,flaq_detail:flaq_detail,wish:true})}>
          <Image
              source={{uri:item.movie.poster_mobile_path}}
              style={dim.width <= 320?styles.img_2:styles.img_3}
              resizeMode='contain'
          />
      </TouchableOpacity>
  );
}
  
_keyExtractor = (item, index) => index;
    render(){
        if(this.state.loading == true){
            return(
                <Loading/>
            )
        }else{
            return(
                <Content style={{marginTop:0}} refreshControl={
                    <RefreshControl
                      refreshing={this.state.loading}
                      onRefresh={this._onRefresh}
                    />
                  }>
                    <Text style={[styles.text_btn_a,{fontSize:SIZE_7,marginTop:dim.height/100*1.5,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD}]}>{I18n.t('tab.wish.title')}</Text>
                    {
                        this.state.movieList.length == 0 ?
                        <Text style={{textAlign:'center',marginTop:dim.height/100*2,fontSize:SIZE_3_5,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.wish.no_list')}</Text>
                        :
                        <FlatList
                            data={this.state.movieList}
                            extraData={this.state}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem}
                            numColumns={dim.width <= 320?2:3}
                            
                        />
                    }
                    
                </Content>
            )
        }
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        wishlist:state.wish.wishlist,
        error:state.wish.error
    }
  }
  var styles = StyleSheet.create({
    text_btn_a:{
        textAlign:'center',
        fontSize:SIZE_3_7,
        color:BLACK,
    },
    img_3:{
        width:dim.width/3.5,
        height:dim.height/3.7,
    },
    img_2:{
        width:dim.width/2.5,
        height:dim.height/2.7,
    }
  });
  

export default connect(mapStateToProps,actions)(Wish)