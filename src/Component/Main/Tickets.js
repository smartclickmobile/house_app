import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Modal,
    RefreshControl,
    Alert
  } from "react-native"
import * as actions from "../../Actions"
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2 } from '../../constant/color'
import { Content } from 'native-base';
import I18n from '../../../asset/languages/i18n'
import { 
    TEXT_TH, 
    TEXT_BODY_EN, 
    TEXT_HEADER_EN,
    TEXT_BODY_EN_BOLD,
    TEXT_HEADER_EN_BOLD,
    TEXT_TH_BOLD,
    SIZE_3_5,
    SIZE_7,
    SIZE_5
} from '../../constant/font'
import moment from 'moment'
import Loading from '../loading'
const dim = Dimensions.get('window');
class Tickets extends Component {
    constructor(props){
        super(props)
        this.state = {
            Tickets:[],
            History:[],
            tab_index:1,
            loading:true
        }
    }
    componentDidMount(){
    this.fetchData()
    }
  
    componentWillReceiveProps(nextProps){
        console.log('history_list',nextProps.history_list)
        if(nextProps.ticket_list != null && nextProps.ticket_list != undefined){
            this.setState({Tickets:nextProps.ticket_list,loading:false})
        }
        if(nextProps.history_list != null && nextProps.history_list != undefined){
            this.setState({History:nextProps.history_list,loading:false})
        }
        if(nextProps.error != null && nextProps.error != undefined){
            Alert.alert('',nextProps.error,[
                {
                    text: I18n.t('try_again'),
                    onPress: () => {
                        this.fetchData()
                    }
                }
            ],
            { cancelable: false })
        }
    }
    _onRefresh = () => {
        this.setState({loading: true});
        this.fetchData()
    }
    async fetchData(){
        var token = await AsyncStorage.getItem('token')
        if(token == '' || token == null){
            this.setState({loading:false})
        }else{
            var data = {
                language:I18n.locale
            }
            this.props.getbuffetTicket(data)
            // this.props.getTicket(data)
            this.props.getHistory(data)
        }
    }
    onChangeSubtab(index){
        this.setState({tab_index:index})
        this.props.setindexTickets(index)
    }
    rendertest = () => {
        console.log('rendertest')
        return(
            <View>
                <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:BG_1,width:320,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                    <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:BLACK,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
                        <Image
                            source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                            style={{
                                width:120,
                                alignSelf:'center',
                                // height:dim.height/100*5,
                            }}
                            resizeMode='contain'
                        />
                    </View>
            
                    <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
                        <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.movie')}</Text>
                        <Text numberOfLines={1} style={{color:BLACK,fontSize:20,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>TEST</Text>
                    </View>
            
                    <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
                        <View style={{width:160}}>
                            <View 
                                style={{
                                    height:65,
                                    borderBottomColor:BG_2,
                                    borderBottomWidth:1,
                                    flexDirection:'row',
                                    alignItems:'center'
                                }}
                            >
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                    <Text numberOfLines={2} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>HOUSE 3</Text>        
                                </View>
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>D1</Text>        
                                </View>
                            </View>
                            <View 
                                style={{
                                    height:65,
                                    borderBottomColor:BG_2,
                                    borderBottomWidth:1,
                                    flexDirection:'row',
                                    alignItems:'center'
                                }}
                            >
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>26 JAN</Text>        
                                </View>
                                <View style={{width:80}}>
                                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:TEXT_HEADER_EN_BOLD}}>00:00</Text>        
                                </View>
                            </View>
                        </View>
            
                        <View style={{width:160,alignItems:'center',justifyContent:'center'}}>
                            <Image
                                source={{uri:''}}
                                style={{width:130,height:130}}
                                resizeMode='contain'
                            />
                        </View>
                    
                    </View>
                    <View style={{height:30,backgroundColor:BLACK,overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:BG_1,width:360,height:270,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                    <View style={{flexDirection:'row'}}>
                        <View style={{width:200,backgroundColor:BLACK,height:270,borderTopLeftRadius:dim.width/100*3,borderBottomLeftRadius:dim.width/100*3}}>
                            <View style={{height:54}}>
                                <Image
                                    source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                                    style={{
                                        width:120,
                                        height:54,
                                        alignSelf:'center'
                                    }}
                                    resizeMode='contain'
                                />
                            </View>
                            <View style={{height:54,alignItems:'center',justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'center',color:'#ffffff'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                                <Text numberOfLines={1} style={{color:'#ffffff',fontSize:14,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>XXXXXXXXXXXXXX</Text>        
                            </View>
                            <View style={{height:54,alignItems:'center',justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'center',color:'#ffffff'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.time')}</Text>
                                <Text numberOfLines={1} style={{color:'#ffffff',fontSize:14,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>01 JAN - 30 JUN 2020</Text>        
                            </View>
                            <View style={{height:54,alignItems:'center',justifyContent:'center'}}>
                                <Text numberOfLines={1} style={{color:'#ffffff',fontSize:14,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`SIRIYAKON THAIKON`}</Text>        
                            </View>
                            <View style={{height:54,alignItems:'center',justifyContent:'center'}}>
                                <Text numberOfLines={1} style={{color:'#ffffff',fontSize:14,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${I18n.t('tab.tickets.ticket_buffet_movie.member_id')} 1212312121`}</Text>        
                            </View>
                        </View>
                        <View style={{width:200,height:270,borderBottomRightRadius:dim.width/100*3,borderBottomLeftRadius:dim.width/100*3}}>
                            <View style={{height:54,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${I18n.t('tab.tickets.ticket_buffet_movie.event')} XXXXXXXXXXXXXX`}</Text>        
                            </View>
                            <View style={{height:54,alignItems:'center',justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.movie')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>Title Test</Text>        
                            </View>
                            <View style={{width:200,height:162,flexDirection:'row'}}>
                                <View style={{width:100,height:162}}>
                                    <View style={{height:40.5,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                        <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>DATE 26 JAN</Text>        
                                    </View>
                                    <View style={{height:40.5,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                        <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>TIME 00:00</Text>        
                                    </View>
                                    <View style={{height:40.5,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                        <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>SEAT D1</Text>        
                                    </View>
                                    <View style={{height:40.5,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                                        <Text numberOfLines={2} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>XXXXXXXXXXXXXXXXX</Text>        
                                    </View>
                                </View>
                                <View style={{width:100,height:162,alignItems:'center',justifyContent:'center'}}>
                                    <Image
                                        source={{uri:''}}
                                        style={{width:95,height:95}}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                              
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
            
        );
}
    _renderItem1 = (item,item1,index,index1) => {
        // console.log(index)
        var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
        var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
        var date = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss')
        var time = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss').format('HH:mm')
        var day = date.date()
        var month = date.month()
        var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
        var year = date.year()
        var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
        return(
        <TouchableOpacity key={`${index}${index1}`} style={{flex:1,alignSelf:'center',backgroundColor:item.movie.ticket_color==null?BG_1:item.movie.ticket_color,width:dim.width/100*80,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3,}}>
            <View style={[styles.Content_View,{height:dim.width/100*20,justifyContent:'center',padding:0}]}>
                {item.movie.ticket_mobile_path!=null && item.movie.ticket_mobile_path != undefined?<View style={{height:dim.width/100*20,overflow:'hidden',width:dim.width/100*80,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3,backgroundColor:BG_1}}>
                    <Image 
                        source={{uri:item.movie.ticket_mobile_path}}
                        style={{height:dim.height/100*10,width:dim.width/100*80,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}
                        resizeMode='stretch'
                        
                    />
                </View>:
                <Image
                    source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                    style={{
                        width:dim.width/100*30,
                        alignSelf:'center',
                        height:dim.height/100*5,
                    }}
                    resizeMode='contain'
                />}
            </View>

            <View style={styles.Content_View}>
                <Text style={styles.textH}>{I18n.t('tab.tickets.tickets_each.movie')}</Text>
                <Text numberOfLines={1} style={{color:BLACK,fontSize:SIZE_5,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.movie.title}</Text>
            </View>

            <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0}]}>
                <View style={{flex:1.5}}>
                    <View style={styles.Content_detail}>
                        <Text style={[styles.textH,{transform:[{ rotate: "90deg" }] }]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                    </View>

                    <View style={styles.Content_detail}>
                        <Text style={[styles.textH,{transform:[{ rotate: "90deg" }] }]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                    </View>

                    <View style={styles.Content_detail}>
                        <Text style={[styles.textH,{transform:[{ rotate: "90deg" }] }]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                    </View>

                    <View style={[styles.Content_detail,{borderBottomWidth:0}]}>
                        <Text style={[styles.textH,{transform:[{ rotate: "90deg" }] }]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                    </View>
                </View>

                <View style={{flex:3.5}}>
                    <View style={styles.Content_detail1}>
                        <Text style={[styles.textD]}>{`${monthtxt} ${day}`}</Text>
                    </View>

                    <View style={styles.Content_detail1}>
                        <Text style={[styles.textD]}>{`${time}`}</Text>
                    </View>

                    <View style={styles.Content_detail1}>
                        {/*<Text style={[styles.textD]}>{item.theater.name}</Text>*/}
                        <Text style={[styles.textD]}>HOUSE 3</Text>
                    </View>

                    <View style={[styles.Content_detail1,{borderBottomWidth:0}]}>
                        <Text style={[styles.textD,{fontSize:SIZE_7}]}>{`${item1.row}${item1.col_no}`}</Text>
                    </View>
                </View>

                <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
                    <Image
                        source={{uri:item1.qrcode_path}}
                        style={{
                            width:dim.width/100*40,
                            height:dim.width/100*40
                        }}
                        resizeMode='contain'
                    />
                </View>
            </View>
        </TouchableOpacity>
    );
}
_renderItem = (item,item1,index,index1) => {
    console.warn(item)
    var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
    var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
    var date = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss')
    var time = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss').format('HH:mm')
    var day = date.date()
    var month = date.month()
    var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
    var year = date.year()
    var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
    if(item.is_buffet == true){
        var date_buffet_start = moment(item.event.start_date,'YYYY-MM-DD HH:mm:ss')
        var day_buffet_start = date_buffet_start.date()
        var month_buffet_start = date_buffet_start.month()
        var monthtxt_buffet_start = I18n.locale == 'th'?month_th[month_buffet_start]:month_en[month_buffet_start]
        var year_buffet_start = date_buffet_start.year()
        var yeartxt_buffet_start =  I18n.locale == 'th'?parseInt(year_buffet_start)+543:parseInt(year_buffet_start)

        var date_buffet_end = moment(item.event.end_date,'YYYY-MM-DD HH:mm:ss')
        var day_buffet_end = date_buffet_end.date()
        var month_buffet_end = date_buffet_end.month()
        var monthtxt_buffet_end = I18n.locale == 'th'?month_th[month_buffet_end]:month_en[month_buffet_end]
        var year_buffet_end = date_buffet_end.year()
        var yeartxt_buffet_end =  I18n.locale == 'th'?parseInt(year_buffet_end)+543:parseInt(year_buffet_end)
        var no = item.member.member_id
        var num = 9 - no.length
        var no_text = ''
        for(var i = 0 ; i <= num ; i++){
            if(i == num){
                no_text += no
            }else{
                no_text += '0'
            }
        }
        var num1 = no_text.substring(0, 3)
        var num2 = no_text.substring(3, 6)
        var num3 = no_text.substring(6, 9)
        var num_text = num1+' '+num2+' '+num3
        console.warn('poster_mobile_path',item.movie.poster_mobile_path)
        return(
            <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#fff8eb',width:360,height:270,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*2}}>
                <View style={{flexDirection:'row'}}>
                    <View style={{width:145,backgroundColor:'#fff8eb',borderRightWidth:0.5,borderRightColor:BLACK,height:270,borderTopLeftRadius:dim.width/100*2,borderBottomLeftRadius:dim.width/100*2,padding:10}}>
                        <View style={{height:50}}>
                            <Image
                                source={require('../../../asset/Images/g-logo-house-1024-300-tran.png')}
                                style={{
                                    width:120,
                                    height:54,
                                    alignSelf:'flex-start'
                                }}
                                resizeMode='contain'
                            />
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text style={[styles.textH,{textAlign:'left',color:BLACK,fontSize:10}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                            <Text numberOfLines={1} style={{color:BLACK,fontSize:14,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{item.event.title}</Text>        
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text style={[styles.textH,{textAlign:'left',color:BLACK,fontSize:10}]}>{I18n.t('tab.tickets.ticket_buffet_movie.time')}</Text>
                            <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`${day_buffet_start} ${monthtxt_buffet_start} - ${day_buffet_end} ${monthtxt_buffet_end} ${yeartxt_buffet_end}`}</Text>        
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text numberOfLines={2} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`${item.member.first_name}\n${item.member.last_name}`}</Text>        
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text numberOfLines={1} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${num_text}`}</Text>        
                        </View>
                    </View>
                    <View style={{width:215,height:270,borderBottomRightRadius:dim.width/100*3,borderBottomLeftRadius:dim.width/100*1,paddingTop:10,paddingHorizontal:5}}>
                        <View style={{height:50,alignItems:'flex-end',justifyContent:'flex-end',flexDirection:'row'}}>
                            <View style={{backgroundColor:'#78bfb5',height:50,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,alignItems:'center',justifyContent:'center'}}>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${item.event.title}`}</Text>        
                            </View>
                        </View>
                        <View style={{height:50,alignItems:'center',justifyContent:'center'}}>
                            <View style={{width:205,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.title')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{item.movie.title}</Text>        
                            </View>
                        </View>
                        <View style={{height:50,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <View style={{width:100,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${day} ${monthtxt}`}</Text>        
                            </View>
                            <View style={{width:100,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center',marginLeft:5}}>
                                <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                <Text numberOfLines={2} style={{color:BLACK,fontSize:8,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{item.theater.name}</Text>        
                            </View>
                        </View>
                        <View style={{width:215,flexDirection:'row',height:100}}>
                            <View style={{height:100,width:53,alignItems:'center',justifyContent:'center'}}>
                                <Image
                                    source={{uri:item.movie.poster_mobile_path}}
                                    style={{width:53,height:95,backgroundColor:'#fbc618'}}
                                    resizeMode='cover'
                                />
                            </View>
                            <View style={{height:100,width:60,marginLeft:2,alignItems:'center',justifyContent:'center'}}>
                                <View style={{width:60,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                    <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{time}</Text>        
                                </View>
                                <View style={{width:60,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center',marginTop:5}}>
                                    <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${item1.row}${item1.col_no}`}</Text>        
                                </View>
                            </View>
                            <View style={{height:90,width:90,alignItems:'center',justifyContent:'center'}}>
                                <Image
                                    source={{uri:item1.qrcode_path}}
                                    style={{width:90,height:90}}
                                    resizeMode='contain'
                                />
                            </View>
                        </View>
                            
                    </View>
                </View>
            </TouchableOpacity>
        )
    }else{
        return(
            <TouchableOpacity key={`${index}${index1}`} style={{flex:1,alignSelf:'center',backgroundColor:BG_1,width:320,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:item.movie.ticket_color==null?BLACK:item.movie.ticket_color,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
                    {item.movie.ticket_mobile_path!=null && item.movie.ticket_mobile_path != undefined?<View style={{height:dim.width/100*20,overflow:'hidden',width:dim.width/100*80,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}>
                        <Image 
                            source={{uri:item.movie.ticket_mobile_path}}
                            style={{height:60,width:280,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}
                            resizeMode='stretch'
                            
                        />
                    </View>:
                    <Image
                        source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                        style={{
                            width:120,
                            alignSelf:'center',
                            // height:dim.height/100*5,
                        }}
                        resizeMode='contain'
                    />}
                </View>
        
                <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.movie')}</Text>
                    <Text numberOfLines={1} style={{color:BLACK,fontSize:20,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.movie.title}</Text>
                </View>
        
                <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
                    <View style={{width:160}}>
                        <View 
                            style={{
                                height:65,
                                borderBottomColor:BG_2,
                                borderBottomWidth:1,
                                flexDirection:'row',
                                alignItems:'center'
                            }}
                        >
                            <View style={{width:80}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                <Text numberOfLines={2} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.theater.name}</Text>        
                            </View>
                            <View style={{width:80}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${item1.row}${item1.col_no}`}</Text>        
                            </View>
                        </View>
                        <View 
                            style={{
                                height:65,
                                borderBottomColor:BG_2,
                                borderBottomWidth:1,
                                flexDirection:'row',
                                alignItems:'center'
                            }}
                        >
                            <View style={{width:80}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${day} ${monthtxt}`}</Text>        
                            </View>
                            <View style={{width:80}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:TEXT_HEADER_EN_BOLD}}>{`${time}`}</Text>        
                            </View>
                        </View>
                    </View>
        
                    <View style={{width:160,alignItems:'center',justifyContent:'center'}}>
                        <Image
                            source={{uri:item1.qrcode_path}}
                            style={{width:130,height:130}}
                            resizeMode='contain'
                        />
                    </View>
                   
                </View>
                <View style={{height:30,backgroundColor:item.movie.ticket_color==null?BLACK:item.movie.ticket_color,overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
            </TouchableOpacity>
        );
    }
    
}
_renderItemHistory = (item,index) => {
    var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
    var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
    var date = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss')
    var time = moment(item.showtimes.start_time,'YYYY-MM-DD HH:mm:ss').format('HH:mm')
    var day = date.date()
    var month = date.month()
    var monthtxt = I18n.locale == 'th'?month_th[month]:month_en[month]
    var year = date.year()
    var yeartxt =  I18n.locale == 'th'?parseInt(year)+543:parseInt(year)
    var seattext = ''
    for(var i = 0 ; i < item.showtimes.seats.length ; i++){
        if(i == item.showtimes.seats.length-1){
            seattext += `${item.showtimes.seats[i].row}${item.showtimes.seats[i].col_no}`
        }else{
            seattext += `${item.showtimes.seats[i].row}${item.showtimes.seats[i].col_no},`
        }
    }
    if(item.is_buffet == true){
        var date_buffet_start = moment(item.event.start_date,'YYYY-MM-DD HH:mm:ss')
        var day_buffet_start = date_buffet_start.date()
        var month_buffet_start = date_buffet_start.month()
        var monthtxt_buffet_start = I18n.locale == 'th'?month_th[month_buffet_start]:month_en[month_buffet_start]
        var year_buffet_start = date_buffet_start.year()
        var yeartxt_buffet_start =  I18n.locale == 'th'?parseInt(year_buffet_start)+543:parseInt(year_buffet_start)

        var date_buffet_end = moment(item.event.end_date,'YYYY-MM-DD HH:mm:ss')
        var day_buffet_end = date_buffet_end.date()
        var month_buffet_end = date_buffet_end.month()
        var monthtxt_buffet_end = I18n.locale == 'th'?month_th[month_buffet_end]:month_en[month_buffet_end]
        var year_buffet_end = date_buffet_end.year()
        var yeartxt_buffet_end =  I18n.locale == 'th'?parseInt(year_buffet_end)+543:parseInt(year_buffet_end)
        console.warn('item',item)
        var no = item.member.member_id
        var num = 9 - no.length
        var no_text = ''
        for(var i = 0 ; i <= num ; i++){
            if(i == num){
                no_text += no
            }else{
                no_text += '0'
            }
        }
        var num1 = no_text.substring(0, 3)
        var num2 = no_text.substring(3, 6)
        var num3 = no_text.substring(6, 9)
        var num_text = num1+' '+num2+' '+num3
        console.warn(item.movie.poster_mobile_path)
        return(
            <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#fff8eb',width:360,height:270,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*2}}>
                <View style={{flexDirection:'row'}}>
                    <View style={{width:145,backgroundColor:'#fff8eb',borderRightWidth:0.5,borderRightColor:BLACK,height:270,borderTopLeftRadius:dim.width/100*2,borderBottomLeftRadius:dim.width/100*2,padding:10}}>
                        <View style={{height:50}}>
                            <Image
                                source={require('../../../asset/Images/g-logo-house-1024-300-tran.png')}
                                style={{
                                    width:120,
                                    height:54,
                                    alignSelf:'flex-start'
                                }}
                                resizeMode='contain'
                            />
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text style={[styles.textH,{textAlign:'left',color:BLACK,fontSize:10}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                            <Text numberOfLines={1} style={{color:BLACK,fontSize:14,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{item.event.title}</Text>        
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text style={[styles.textH,{textAlign:'left',color:BLACK,fontSize:10}]}>{I18n.t('tab.tickets.ticket_buffet_movie.time')}</Text>
                            <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`${day_buffet_start} ${monthtxt_buffet_start} - ${day_buffet_end} ${monthtxt_buffet_end} ${yeartxt_buffet_end}`}</Text>        
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text numberOfLines={2} style={{color:BLACK,fontSize:10,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`${item.member.first_name}\n${item.member.last_name}`}</Text>        
                        </View>
                        <View style={{height:50,justifyContent:'center'}}>
                            <Text numberOfLines={1} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${num_text}`}</Text>        
                        </View>
                    </View>
                    <View style={{width:215,height:270,borderBottomRightRadius:dim.width/100*3,borderBottomLeftRadius:dim.width/100*1,paddingTop:10,paddingHorizontal:5}}>
                        <View style={{height:50,alignItems:'flex-end',justifyContent:'flex-end',flexDirection:'row'}}>
                            <View style={{backgroundColor:'#78bfb5',height:50,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,alignItems:'center',justifyContent:'center'}}>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${item.event.title}`}</Text>        
                            </View>
                        </View>
                        <View style={{height:50,alignItems:'center',justifyContent:'center'}}>
                            <View style={{width:205,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.title')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{item.movie.title}</Text>        
                            </View>
                        </View>
                        <View style={{height:50,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <View style={{width:100,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{`${day} ${monthtxt}`}</Text>        
                            </View>
                            <View style={{width:100,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center',marginLeft:5}}>
                                <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                <Text numberOfLines={2} style={{color:BLACK,fontSize:8,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{item.theater.name}</Text>        
                            </View>
                        </View>
                        <View style={{width:215,flexDirection:'row',height:100}}>
                            <View style={{height:100,width:53,alignItems:'center',justifyContent:'center'}}>
                                <Image
                                    source={{uri:item.movie.poster_mobile_path}}
                                    style={{width:53,height:95,backgroundColor:'#fbc618'}}
                                    resizeMode='cover'
                                />
                            </View>
                            <View style={{height:100,width:60,marginLeft:2,alignItems:'center',justifyContent:'center'}}>
                                <View style={{width:60,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center'}}>
                                    <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{time}</Text>        
                                </View>
                                <View style={{width:60,backgroundColor:'#78bfb5',height:45,borderColor:BLACK,borderWidth:1,paddingHorizontal:10,justifyContent:'center',marginTop:5}}>
                                    <Text style={[styles.textH,{textAlign:'left',fontFamily:'SpaceMono-Regular'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                    <Text numberOfLines={1} style={{color:BLACK,fontSize:10,textAlign:'center',fontFamily:'SpaceMono-Bold'}}>{seattext}</Text>        
                                </View>
                            </View>
                            <View style={{height:90,width:90}}>
                                <Image
                                    source={{uri:item.qrcode_path}}
                                    style={{width:90,height:90}}
                                    resizeMode='contain'
                                />
                            </View>
                        </View>
                            
                    </View>
                </View>
            </TouchableOpacity>
        )
    }else{
        return(
            <TouchableOpacity key={`${index}`} style={{flex:1,alignSelf:'center',backgroundColor:BG_1,width:320,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:item.movie.ticket_color==null?BLACK:item.movie.ticket_color,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
                    {item.movie.ticket_mobile_path!=null && item.movie.ticket_mobile_path != undefined?<View style={{height:dim.width/100*20,overflow:'hidden',width:dim.width/100*80,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}>
                        <Image 
                            source={{uri:item.movie.ticket_mobile_path}}
                            style={{height:60,width:280,borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}}
                            resizeMode='stretch'
                            
                        />
                    </View>:
                    <Image
                        source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                        style={{
                            width:120,
                            alignSelf:'center',
                            // height:dim.height/100*5,
                        }}
                        resizeMode='contain'
                    />}
                </View>
        
                <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
                    <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.movie')}</Text>
                    <Text numberOfLines={1} style={{color:BLACK,fontSize:20,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.movie.title}</Text>
                </View>
        
                <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
                    <View style={{width:320}}>
                        <View 
                            style={{
                                height:65,
                                borderBottomColor:BG_2,
                                borderBottomWidth:1,
                                flexDirection:'row',
                                alignItems:'center'
                            }}
                        >
                            <View style={{width:90}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.theater.name}</Text>        
                            </View>
                            <View style={{width:230}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.seat')}</Text>
                                <View style={{flexDirection:'row',width:230,alignItems:'center',justifyContent:'center'}}>
                                    <Text style={{color:BLACK,fontSize:18,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD,textAlign:'center'}}>{seattext}</Text>        
                                </View>
                            </View>
                        </View>
                        <View 
                            style={{
                                height:65,
                                borderBottomColor:BG_2,
                                borderBottomWidth:1,
                                flexDirection:'row',
                                alignItems:'center'
                            }}
                        >
                            <View style={{width:160}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.date')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${day} ${monthtxt}`}</Text>        
                            </View>
                            <View style={{width:160}}>
                                <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                <Text numberOfLines={1} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:TEXT_HEADER_EN_BOLD}}>{`${time}`}</Text>        
                            </View>
                        </View>
                    </View>
                   
                </View>
                <View style={{height:30,backgroundColor:item.movie.ticket_color==null?BLACK:item.movie.ticket_color,overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
            </TouchableOpacity>
        );
    }
    
}
    _keyExtractor = (item, index) => item.id;
    render(){
        if(this.state.loading == true){
            return(
                <Loading black={false}/>
            )
        }else{
            return(
                <Content style={{marginTop:0}} refreshControl={
                    <RefreshControl
                      refreshing={this.state.loading}
                      onRefresh={this._onRefresh}
                    />
                  }>
                    <View style={{flexDirection:'row',justifyContent:'center',marginTop:dim.height/100*2.5}}>
                        <TouchableOpacity style={{marginRight:dim.width/100*2}} onPress={()=> this.onChangeSubtab(1)}>
                            <Text style={[this.state.tab_index==1?
                                {
                                    color:BG_1,
                                    fontSize:SIZE_5,
                                    fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD
                                }
                                :
                                {
                                    color:BG_1,
                                    fontSize:SIZE_5,
                                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN
                                }]}>{I18n.t('tab.tickets.tickets')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginLeft:dim.width/100*2}} onPress={()=> this.onChangeSubtab(2)}>
                            <Text  style={[this.state.tab_index==2?
                                {
                                    color:BG_1,
                                    fontSize:SIZE_5,
                                    fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD
                                }
                                :
                                {
                                    color:BG_1,
                                    fontSize:SIZE_5,
                                    fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN
                                }]}>{I18n.t('tab.tickets.history')}</Text>
                        </TouchableOpacity>
                    </View>
                    {
                        // this.rendertest()
                    }
                    {
                        this.state.tab_index==1 && this.state.Tickets.map((item,index)=>{
                            if(index == 0){
                                if(item != null){
                                    var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
                                    var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
                                    var start_date_buffet = moment(item.start_date,'YYYY-MM-DD')
                                    var start_date_buffet_day = start_date_buffet.date()
                                    var start_date_buffet_month = I18n.locale == 'th'?month_th[start_date_buffet.month()]:month_en[start_date_buffet.month()]
                                    var end_date_buffet = moment(item.end_date,'YYYY-MM-DD')
                                    var end_date_buffet_day = end_date_buffet.date()
                                    var end_date_buffet_month = I18n.locale == 'th'?month_th[start_date_buffet.month()]:month_en[start_date_buffet.month()]
                                    var year = I18n.locale == 'th'?parseInt(start_date_buffet.year())+543:parseInt(start_date_buffet.year())
                                    var diff_day = end_date_buffet.diff(start_date_buffet, 'days')+1
                                    console.warn('item buffet',item)
                                    var no = item.member.member_id
                                    var num = 9 - no.length
                                    var no_text = ''
                                    for(var i = 0 ; i <= num ; i++){
                                        if(i == num){
                                            no_text += no
                                        }else{
                                            no_text += '0'
                                        }
                                    }
                                    var num1 = no_text.substring(0, 3)
                                    var num2 = no_text.substring(3, 6)
                                    var num3 = no_text.substring(6, 9)
                                    var num_text = num1+' '+num2+' '+num3
                                    // var num_text = ''
                                    return(
                                        <View>
                                            <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#289f8b',width:360,height:255,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                                                <View style={[{height:85,justifyContent:'center',paddingRight:20,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3,alignItems:'center'}]}>
                                                    <Image
                                                        source={require('../../../asset/Images/buffet-logo-house-1024-300-tran.png')}
                                                        style={{
                                                            width:160,
                                                            alignSelf:'flex-end',
                                                            // height:dim.height/100*5,
                                                        }}
                                                        resizeMode='contain'
                                                    />
                                                </View>
                                        
                                                <View style={[{height:85,justifyContent:'flex-start',paddingHorizontal:20}]}>
                                                    <Text style={[styles.textH,{textAlign:'left',color:'#feec04',fontSize:22,fontFamily:'SpaceMono-Bold'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                                                    <Text numberOfLines={2} style={{color:'#feec04',fontSize:28,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{`${item.title}`}</Text>        
                                                </View>
                                                <View style={[{height:85,justifyContent:'flex-start',paddingHorizontal:20}]}>
                                                    <Text numberOfLines={1} style={{color:BG_1,fontSize:24,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{`${num_text}`}</Text>        
                                                    <Text numberOfLines={1} style={{color:BG_1,fontSize:16,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`DATE ${start_date_buffet_day} ${start_date_buffet_month} - ${end_date_buffet_day} ${end_date_buffet_month} ${year}`}</Text>        
                                                </View>

                                            </TouchableOpacity>
                                            {/* <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#f9f8f4',width:320,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                                                <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:'#e7b945',overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
                                                    <Image
                                                        source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                                                        style={{
                                                            width:120,
                                                            alignSelf:'center',
                                                            // height:dim.height/100*5,
                                                        }}
                                                        resizeMode='contain'
                                                    />
                                                </View>
                                        
                                                <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
                                                    <Image
                                                        source={require('../../../asset/Images/ticket-buffet.png')}
                                                        style={{
                                                            width:320,
                                                            height:70,
                                                            alignSelf:'center',
                                                            // height:dim.height/100*5,
                                                        }}
                                                        resizeMode='cover'
                                                    />
                                                </View>
                                        
                                                <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
                                                    <View style={{width:160}}>
                                                        <View 
                                                            style={{
                                                                height:65,
                                                                borderBottomColor:BG_2,
                                                                borderBottomWidth:1,
                                                                alignItems:'center',
                                                                justifyContent:'center'
                                                            }}
                                                        >
                                                            <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                                            <Text numberOfLines={2} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${item.total_movie} ${I18n.t('tab.tickets.ticket_buffet.movie')}\n${diff_day} ${I18n.t('tab.tickets.ticket_buffet.days')}`}</Text>        
                                                            
                                                        </View>
                                                        <View 
                                                            style={{
                                                                height:65,
                                                                borderBottomColor:BG_2,
                                                                borderBottomWidth:1,
                                                                alignItems:'center',
                                                                justifyContent:'center'
                                                            }}
                                                        >
                                                            <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                                            <Text numberOfLines={2} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${start_date_buffet_day} ${start_date_buffet_month} - ${end_date_buffet_day} ${end_date_buffet_month} ${year}`}</Text>
                                                                    
                                                        </View>
                                                    </View>
                                        
                                                    <View style={{width:160,alignItems:'center',justifyContent:'center'}}>
                                                        <Image
                                                            source={{uri:item.qrcode_buffet_path}}
                                                            style={{width:130,height:130}}
                                                            resizeMode='contain'
                                                        />
                                                    </View>
                                                
                                                </View>
                                                <View style={{height:30,backgroundColor:'#e7b945',overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
                                            </TouchableOpacity> */}
                                        </View>
                                    )
                                }else{
                                    return(
                                        <View>
                                        </View>
                                    )
                                }
                            }else{
                                return(
                                    <View key={index}>
                                    {item.showtimes.seats.map((item1,index1)=>{
                                        return(
                                            this._renderItem(item,item1,index,index1)
                                        )
                                        })}
                                    </View>
                                )
                            }
                            
                        })
                    }
                    {
                        this.state.tab_index!=1 && this.state.History.map((item,index)=>{
                            // console.log('item',item)
                            if(item.event1 == true){
                                if(item != null){
                                    var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
                                    var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
                                    var start_date_buffet = moment(item.start_date,'YYYY-MM-DD')
                                    var start_date_buffet_day = start_date_buffet.date()
                                    var start_date_buffet_month = I18n.locale == 'th'?month_th[start_date_buffet.month()]:month_en[start_date_buffet.month()]
                                    var end_date_buffet = moment(item.end_date,'YYYY-MM-DD')
                                    var end_date_buffet_day = end_date_buffet.date()
                                    var end_date_buffet_month = I18n.locale == 'th'?month_th[start_date_buffet.month()]:month_en[start_date_buffet.month()]
                                    var year = I18n.locale == 'th'?parseInt(start_date_buffet.year())+543:parseInt(start_date_buffet.year())
                                    var diff_day = end_date_buffet.diff(start_date_buffet, 'days')+1
                                    console.log('item',start_date_buffet,end_date_buffet)
                                    
                                    var no = item.member.member_id
                                    var num = 9 - no.length
                                    var no_text = ''
                                    for(var i = 0 ; i <= num ; i++){
                                        if(i == num){
                                            no_text += no
                                        }else{
                                            no_text += '0'
                                        }
                                    }
                                    var num1 = no_text.substring(0, 3)
                                    var num2 = no_text.substring(3, 6)
                                    var num3 = no_text.substring(6, 9)
                                    var num_text = num1+' '+num2+' '+num3
                                    return(
                                        <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#289f8b',width:360,height:255,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                                                <View style={[{height:85,justifyContent:'center',paddingRight:20,overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3,alignItems:'center'}]}>
                                                    <Image
                                                        source={require('../../../asset/Images/buffet-logo-house-1024-300-tran.png')}
                                                        style={{
                                                            width:160,
                                                            alignSelf:'flex-end',
                                                            // height:dim.height/100*5,
                                                        }}
                                                        resizeMode='contain'
                                                    />
                                                </View>
                                        
                                                <View style={[{height:85,justifyContent:'flex-start',paddingHorizontal:20}]}>
                                                    <Text style={[styles.textH,{textAlign:'left',color:'#feec04',fontSize:22,fontFamily:'SpaceMono-Bold'}]}>{I18n.t('tab.tickets.ticket_buffet_movie.event')}</Text>
                                                    <Text numberOfLines={2} style={{color:'#feec04',fontSize:28,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{`${item.title}`}</Text>        
                                                </View>
                                                <View style={[{height:85,justifyContent:'flex-start',paddingHorizontal:20}]}>
                                                    <Text numberOfLines={1} style={{color:BG_1,fontSize:24,textAlign:'left',fontFamily:'SpaceMono-Bold'}}>{`${num_text}`}</Text>        
                                                    <Text numberOfLines={1} style={{color:BG_1,fontSize:16,textAlign:'left',fontFamily:'SpaceMono-Regular'}}>{`DATE ${start_date_buffet_day} ${start_date_buffet_month} - ${end_date_buffet_day} ${end_date_buffet_month} ${year}`}</Text>        
                                                </View>

                                            </TouchableOpacity>
                                    // <TouchableOpacity style={{flex:1,alignSelf:'center',backgroundColor:'#f9f8f4',width:320,height:285,marginVertical:dim.width/100*2.5,borderRadius:dim.width/100*3}}>
                                    //     <View style={[styles.Content_View,{height:60,justifyContent:'center',padding:0,backgroundColor:'#e7b945',overflow:'hidden',borderTopLeftRadius:dim.width/100*3,borderTopRightRadius:dim.width/100*3}]}>
                                    //         <Image
                                    //             source={require('../../../asset/Images/w-logo-house-1024-300-tran.png')}
                                    //             style={{
                                    //                 width:120,
                                    //                 alignSelf:'center',
                                    //                 // height:dim.height/100*5,
                                    //             }}
                                    //             resizeMode='contain'
                                    //         />
                                    //     </View>
                                
                                    //     <View style={[styles.Content_View,{height:70,justifyContent:'center'}]}>
                                    //         <Image
                                    //             source={require('../../../asset/Images/ticket-buffet.png')}
                                    //             style={{
                                    //                 width:320,
                                    //                 height:70,
                                    //                 alignSelf:'center',
                                    //                 // height:dim.height/100*5,
                                    //             }}
                                    //             resizeMode='cover'
                                    //         />
                                    //     </View>
                                
                                    //     <View style={[styles.Content_View,{flexDirection:'row',borderBottomWidth:0,padding:0,height:130}]}>
                                    //         <View style={{width:160}}>
                                    //             <View 
                                    //                 style={{
                                    //                     height:65,
                                    //                     borderBottomColor:BG_2,
                                    //                     borderBottomWidth:1,
                                    //                     alignItems:'center',
                                    //                     justifyContent:'center'
                                    //                 }}
                                    //             >
                                    //                 <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.cinema')}</Text>
                                    //                 <Text numberOfLines={2} style={{color:BLACK,fontSize:18,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${item.total_movie} ${I18n.t('tab.tickets.ticket_buffet.movie')}\n${diff_day} ${I18n.t('tab.tickets.ticket_buffet.days')}`}</Text>        
                                                    
                                    //             </View>
                                    //             <View 
                                    //                 style={{
                                    //                     height:65,
                                    //                     borderBottomColor:BG_2,
                                    //                     borderBottomWidth:1,
                                    //                     alignItems:'center',
                                    //                     justifyContent:'center'
                                    //                 }}
                                    //             >
                                    //                 <Text style={[styles.textH,{textAlign:'center'}]}>{I18n.t('tab.tickets.tickets_each.time')}</Text>
                                    //                 <Text numberOfLines={2} style={{color:BLACK,fontSize:16,textAlign:'center',fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{`${start_date_buffet_day} ${start_date_buffet_month} - ${end_date_buffet_day} ${end_date_buffet_month} ${year}`}</Text>
                                                            
                                    //             </View>
                                    //         </View>
                                
                                    //         <View style={{width:160,alignItems:'center',justifyContent:'center'}}>
                                    //             <Image
                                    //                 source={{uri:item.qrcode_buffet_path}}
                                    //                 style={{width:130,height:130}}
                                    //                 resizeMode='contain'
                                    //             />
                                    //         </View>
                                           
                                    //     </View>
                                    //     <View style={{height:30,backgroundColor:'#e7b945',overflow:'hidden',borderBottomLeftRadius:dim.width/100*3,borderBottomRightRadius:dim.width/100*3}}/>
                                    // </TouchableOpacity>
                                    )
                                }else{
                                    return(
                                        <View>
                                        </View>
                                    )
                                }
                            }else{
                                console.log('item', item)
                                return(
                                    <View key={index}>
                                            {this._renderItemHistory(item,index)}
                                    </View>
                                )
                            }
                        })
                    }
                </Content>
            )
        }
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        ticket_list:state.ticket.tickets,
        history_list:state.ticket.history,
        error:state.ticket.error
    }
  }
  var styles = StyleSheet.create({
    textTab:{
        color:BG_1,
        fontSize:SIZE_5,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_HEADER_EN_BOLD
    }
    ,
    textTab_a:{
        color:BG_1,
        fontSize:SIZE_5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN
    },
    Content_View:{
        // padding:dim.width/100*2.5,
        borderBottomColor:BG_2,
        borderBottomWidth:1
    },
    textH:{
        color:BLACK,
        fontSize:12,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    Content_detail:{
        borderRightColor:BG_2,
        borderRightWidth:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomColor:BG_2,
        borderBottomWidth:1,
        height:dim.height/100*7
    },
    Content_detail1:{
        borderRightColor:BG_2,
        borderRightWidth:1,
        alignItems:'center',
        justifyContent:'center',
        borderBottomColor:BG_2,
        borderBottomWidth:1,
        height:dim.height/100*7
    },
    textD:{
        color:BLACK,
        fontSize:SIZE_3_5,
        fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD
    }
  });
  

export default connect(mapStateToProps,actions)(Tickets)