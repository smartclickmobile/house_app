import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,

  } from "react-native"
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BG_2 } from '../constant/color'
import { Header, Left, Body, Right, Icon,Button } from 'native-base';
import {Actions} from 'react-native-router-flux'
import I18n from '../../asset/languages/i18n'
import {TEXT_HEADER_EN,SIZE_5} from '../constant/font'
const dim = Dimensions.get('window');
class headerMovie extends Component {
    constructor(props){
        super(props)
        
    }
    cancelBooking(){
        var data = {
            purchase_transaction_id:this.props.purchase_transaction_id,
            language:I18n.locale
        }
        this.props.bookingCancel(data)
        
    }
    async onback(){
        var token = await AsyncStorage.getItem('token')
        if(token == '' || token == null){
           
        }else{
            var data={
                language:I18n.locale
            }
            this.props.getwish(data)
        }
        Actions.pop()
    }
    render(){
            return(
                <Header noShadow
                    style={{
                        backgroundColor:'transparent',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11,
                        borderColor:'transparent'
                    }} 
                    androidStatusBarColor={BG_2}
                >
                    {this.props.leftbutton?<Left style={{flex:1,alignItems:'center'}}>
                    <Button style={{width:dim.width/100*10,alignItems:'center',justifyContent:'center'}} transparent onPress={()=> this.props.purchase_transaction_id!=undefined?this.cancelBooking(): this.onback()}>
                        <Icon  name='chevron-left' type='FontAwesome5' style={{color:BG_1}}/>
                    </Button>
                    </Left>:<View style={{flex:1}}/>}
                    <Body style={{flex:5,flexDirection:'row',justifyContent:'center'}}>
                        <Text style={styles.text_btnT_a}>{I18n.t('header.movie')}</Text>
                    </Body>
                    <Right style={{flex:1}}/>
                </Header>
            )
        
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        index_menu:state.check.index_menu
    }
  }
  var styles = StyleSheet.create({
    text_btnT_a:{
        textAlign:'center',
        fontSize:SIZE_5,
        color:BG_1,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN
    }
  });

export default connect(mapStateToProps,actions)(headerMovie)