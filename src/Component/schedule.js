import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    ScrollView,
    Modal,
    RefreshControl,
    StyleSheet,
    StatusBar,
    Platform,
    Alert,

    Image
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2 } from '../constant/color'
import { Container, Header, Content, Footer, FooterTab, Button, Icon } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { TEXT_TH, TEXT_BODY_EN, TEXT_HEADER_EN, TEXT_TH_BOLD, TEXT_BODY_EN_BOLD,SIZE_3,SIZE_3_5, SIZE_2_5 } from '../constant/font'
import { Actions } from "react-native-router-flux";

const dim = Dimensions.get('window');
import Slick from 'react-native-slick';
import Swiper from 'react-native-swiper'
import Loading from '../Component/loading'
var flag_8 = false
class schedule extends Component {
    constructor(props){
        super(props)
        this.state = {
            movie:[],
            finish:false,
            loading:true,
            cheack:false,
            token:null,
            flag_8:false,
            flag:false,
            membertype:''
        }
    }
    async componentDidMount(){
        var membertype = await AsyncStorage.getItem('membertype')
        console.log('componentDidMount schedule')
        var token = await AsyncStorage.getItem('token')
        if(this.state.flag == false){
            this.fetchData()
        }
       
        this.setState({token:token,flag:true,membertype:membertype})
        
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.scheduleList != null && nextProps.scheduleList != undefined){
            var data = this.state.movie
            data.push(nextProps.scheduleList)
            this.setState({movie:data},()=>{
                if(this.state.finish == true){
                    var objs = this.state.movie
                    objs.sort((a,b) => (moment(a.date,"YYYY-MM-DD") > moment(b.date,"YYYY-MM-DD") ? 1 : ((moment(b.date,"YYYY-MM-DD") > moment(a.date,"YYYY-MM-DD")) ? -1 : 0)))
                    this.setState({movie:objs})
                }
            })
        }
        if(this.state.movie.length == 8){
            this.setState({loading:false,flag_8:true})

        }
        if(nextProps.error != null && nextProps.error != undefined && this.state.cheack == false){
            this.setState({cheack:true})
            Alert.alert('',nextProps.error,[
                {
                    text: I18n.t('try_again'),
                    onPress: () => {
                        this.setState({cheack:false})
                        this.fetchData()
                    }
                }
            ],
            { cancelable: false })
        }
    }
    _onRefresh = () => {
        this.setState({loading: true,movie:[]});
        this.fetchData()
      }
      async fetchData(){
          console.log('fetchdata schedule')
        var currentDate = moment()
        for(var i = 0; i<8;i++){
          var data = {
            language:I18n.locale,
            date:moment(currentDate).format('YYYY-MM-DD')
            }
            await this.props.getSchedule(data)
            currentDate = moment(currentDate).add(1, 'days');
            if(i == 7){
                this.setState({finish:true})
            }else{
                this.setState({finish:false})
            }
        }
    }
    _renderItem(item,all){
        console.log('is_buffet',item.item.is_buffet)
        var height_max = dim.height/100*70
        var d_height = height_max/17.5
        var end = moment(item.item.end_time,'YYYY-MM-DD HH:mm:ss')
        var start = moment(item.item.start_time,'YYYY-MM-DD HH:mm:ss')
        var start_time = moment(moment(item.item.start_time,'YYYY-MM-DD HH:mm:ss').format('HH:mm'),'HH:mm')
        
        var now = moment().subtract({minutes: 30})
        var duration = moment.duration(end.diff(start));
        var hours = duration.asHours();
        var margin_time = 0
        var open_time = moment('10:00','HH:mm')
        var show_time = parseInt(item.item.movie.show_time)
        var height = d_height*(show_time/60)

        if(item.index == 0){
            var duration_time = moment.duration(start_time.diff(open_time));
            margin_time = (duration_time.asHours()*d_height)
        }else{
            var end_time_before = moment(moment(all[item.index-1].end_time,'YYYY-MM-DD HH:mm:ss').format('HH:mm'),'HH:mm')
            var duration_time = moment.duration(start_time.diff(end_time_before));
            margin_time = (duration_time.asHours()*d_height)
        }
        var booking_date
        if(this.state.membertype == 1){
            booking_date = moment(item.item.booking_regular_date,"YYYY-MM-DD HH:mm:ss")
        }else if(this.state.membertype == 2){
            booking_date = moment(item.item.booking_member_date,"YYYY-MM-DD HH:mm:ss")
        }
        return(
        <TouchableOpacity 
            disabled={start < now}
            style={{
                flex:1,
                alignItems:'center',
                marginTop:margin_time
            }} 
            onPress={ async ()=> {
                var token = await AsyncStorage.getItem('token')
                console.warn('getItem token',token)
                if(token != '' && token != null){
                    if(item.item.is_buffet == true){
                        // console.warn('check booking',booking_date,now)
                        if(booking_date > now){
                            var booking_d = moment(booking_date).format('YYYY-MM-DD')
                            var booking_t = moment(booking_date).format('HH:mm:ss')
                            Alert.alert("",I18n.t('BookingSelectTimeScreen.error1')+booking_d+I18n.t('BookingSelectTimeScreen.time')+booking_t)    
                        }else{
                            Actions.BookingSelectSeatScreen({data:item.item.movie,time_data:item.item,start:item.item.start_time})
                        }
                    }else{
                        Actions.BookingSelectSeatScreen({data:item.item.movie,time_data:item.item,start:item.item.start_time})
                    }
                }else{
                    this.props.setindexmenu(1)
                    this.props.setindexfooter(1)
                    Actions.pleaselogin()
                }
                
            } }
        >
            <View style={{
                width:dim.width/100*40,
                height:height+5,
            }}>
                <View 
                    style={{
                        width:dim.width/100*32,
                        height:height,
                        padding:dim.width/100*1.5,
                        backgroundColor:item.item.is_buffet==true?'#289f8b':BG_1,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.22,
                        shadowRadius: 2.22,

                        elevation: 3,
                        borderWidth:0,
                        borderRadius:dim.width/100*2,
                        opacity:start < now ? 0.5 : 1
                    }}
                >
                    <Text numberOfLines={2} style={{fontSize:SIZE_2_5,color:BLACK,fontFamily:I18n.locale=='th'?TEXT_TH_BOLD:TEXT_BODY_EN_BOLD}}>{item.item.movie.title}</Text>
                    <Text style={{fontSize:SIZE_2_5,color:BLACK,fontFamily:TEXT_HEADER_EN}}>{moment(item.item.start_time,"YYYY-MM-DD HH:mm:ss").format('HH:mm')}</Text>
                </View>
            </View>
            
            
        </TouchableOpacity>
    );
}
    _keyExtractor = (item, index) => item.id;
    render(){
         if(this.state.loading == true && this.state.movie.length != 8){
            return(
                <Loading/>
            )
        }else{
            return(
                <Content scrollEnabled={false} refreshControl={
                    <RefreshControl
                      refreshing={this.state.loading}
                      onRefresh={this._onRefresh}
                    />
                  }>
                    
                    {this.state.movie.length == 8 || flag_8?
                        <Swiper showsPagination={false} showsButtons width={dim.width} height={dim.height/100*80} loop={false}>
                            {this.state.movie.map((item, key) => {
                                flag_8 = true
                                var month_th = ["ม.ค.","ก.พ.","มี.ค","เม.ย.","พ.ค","มิ.ย.","ก.ค","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]
                                var month_en = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
                                var day_th = ["อ.","จ.","อ.","พ.","พฤ.","ศ.","ส.","อ."]
                                var day_en = ["SUN","MON","TUE","WED","THU","FRI","SAT","SUN"]
                                var date_now = moment().format()
                                var date_now1 = moment(date_now,"YYYY-MM-DD").format("YYYY-MM-DD HH:mm:ss")
                                var date_list = moment(item.date,"YYYY-MM-DD").format("YYYY-MM-DD HH:mm:ss")
                                var date = moment(item.date,"YYYY-MM-DD")
                                var day = date.date()
                                var days = I18n.locale == 'th'?day_th[date.day()]:day_en[date.day()]
                                var month = I18n.locale == 'th'?month_th[date.month()]:month_en[date.month()]
                                var days_t = date.format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')?I18n.t('BookingSelectTimeScreen.to_day'):`${days} ${day} ${month}`
                                return(
                                    <View key={key} style={styles.slide1}>
                                        <View style={{width:dim.width,backgroundColor:BG_2,height:dim.height/100*5,justifyContent:'center'}}>
                                            <Text style={{color:BLACK,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,textAlign:'center'}}>{`${days_t}`}</Text>
                                        </View>
                                        <View horizontal style={{flexDirection:'row',paddingLeft:dim.width/100*0.7}}>
                                        {
                                            item.data.map((item1,index1)=>{
                                                // console.log('item1',item1.theater_icon_mobile_path)
                                                return(
                                                    <View key={index1} style={{width:dim.width/3,alignItems:'center',height:dim.height/100*75}}>
                                                        <View style={{width:dim.width/3,height:dim.height/100*5,justifyContent:'center'}}>
                                                            {
                                                                item1.theater_icon_mobile_path == '' || item1.theater_icon_mobile_path == null?
                                                                <Text numberOfLines={3} style={{color:BLACK,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,textAlign:'center'}}>{`${item1.theater_name}`}</Text>
                                                                :
                                                                <Image
                                                                    source={{uri:item1.theater_icon_mobile_path}}
                                                                    style={{
                                                                        width:(dim.width/3)-10,
                                                                        height:dim.height/100*4.5,
                                                                        marginVertical:dim.height/100*0.25
                                                                    }}
                                                                    resizeMode='contain'
                                                                />
                                                            }
                                                        </View>
                                                        <FlatList
                                                            data={item1.schedules}
                                                            extraData={this.state}
                                                            keyExtractor={this._keyExtractor}
                                                            renderItem={(item)=>this._renderItem(item,item1.schedules)}
                                                            scrollEnabled={false}
                                                        />
                                                    </View>
                                                )
                                            })
                                        }
                                        {
                                            item.data.length==0?
                                            <View style={{paddingBottom:dim.height/100*2,justifyContent:'center',width:dim.width,alignItems:'center',height:dim.height/100*75}}>
                                                <Text style={{color:BLACK,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.house.schedule.no_item')}</Text>
                                            </View>
                                            :
                                            <View/>
                
                                        }
                                        </View>
                                        
                                    </View>
                                )
                            })}
                        </Swiper>:<View/>
                    }
                            {
                                // this.state.movie.map((item,index)=>{
                                //     return(
                                //         <View>
                                //             <View style={{marginBottom:dim.height/100*2,width:dim.width,backgroundColor:BG_2,paddingVertical:dim.height/100*1.5}}>
                                //                 <Text style={{color:BLACK,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,textAlign:'center'}}>{item.date}</Text>
                                //             </View>
                                //             <ScrollView horizontal style={{flexDirection:'row',paddingLeft:dim.width/100*0.7}}>
                                //                 {
                                //                     item.data.map((item1,index1)=>{
                                //                         return(
                                //                             <View key={index1} style={{width:dim.width/3,alignItems:'center'}}>
                                //                                 <Text numberOfLines={3} style={{color:BLACK,fontSize:SIZE_3_5,marginBottom:dim.height/100*2,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{item1.theater_name}</Text>
                                //                                 <FlatList
                                //                                     data={item1.schedules}
                                //                                     extraData={this.state}
                                //                                     keyExtractor={this._keyExtractor}
                                //                                     renderItem={this._renderItem}
                                //                                     scrollEnabled={false}
                                //                                 />
                                //                             </View>
                                //                         )
                                //                     })
                                //                 }
                                //                 {
                                //                     item.data.length==0?
                                //                     <View style={{paddingBottom:dim.height/100*2,justifyContent:'center',width:dim.width,alignItems:'center'}}>
                                //                         <Text style={{color:BLACK,fontSize:SIZE_3_5,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{I18n.t('tab.house.schedule.no_item')}</Text>
                                //                     </View>
                                                    
                                //                     :
                                //                     <View/>
    
                                //                 }
                                //             </ScrollView>
                                //         </View>
                                //     )
                                // })
                            }
                </Content>
            )
        }
        
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        scheduleList:state.home.schedule,
        error:state.home.error,
        error_num:state.home.error_num
    }
  }
  var styles = StyleSheet.create({
    wrapper: {
        
    },
    slide1: {
        height:dim.height/100*80,
    }

  })

export default connect(mapStateToProps,actions)(schedule)