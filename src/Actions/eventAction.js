import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    EVENT_DEDAIL,
    EVENT_DEDAIL_ERROR
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import moment from 'moment'
export const getEventDetail = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        data.append(`${option.event_type_name}_id`, option.event_id)
        axios
            .post(API_IP + `${option.event_type_name}/detail`,data, CONFIG)
            .then(function (response) {
                console.log('getEventDetail',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: EVENT_DEDAIL ,payload:response.data.data})
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.log('getEventDetail error',error)
            })
    }
}



