import axios from "axios"
import { API_IP,CONFIG } from '../constant/constant'
import {
    PROFILE_INFO,
    PROFILE_INFO_ERROR,
    CHANGE_NUMBER,
    CHANGE_NUMBER_ERROR,
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_ERROR,
    MY_CARD,
    MY_CARD_ERROR,
    VOUCHER,
    VOUCHER_ERROR,
    RESET_INFO,
    EMEMBER_PRIVILEGE,
    EMEMBER_PRIVILEGE_ERROR,
    DELETE_USER,
    DELETE_USER_ERROR

} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk';
import {clear_auth} from './authAction'
export const getProfileInfo = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
        axios
            .post(API_IP + "member/getmemberinfo",data, CONFIG)
            .then(function (response) {
                console.log('getProfileInfo',response)
                if(response.data.code == '0x0000-00000'){
                    console.log('getProfileInfo',response.data.data)
                    AsyncStorage.setItem('membertype',response.data.data.member_type_id)
                    dispatch({ type: PROFILE_INFO ,payload:response.data.data})
                }else{
                    
                    dispatch({ type: PROFILE_INFO_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
              console.log('getProfileInfo error',error)
              dispatch({ type: PROFILE_INFO_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}

export const changeNumber = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("telephone", option.telephone)
        data.append("language", option.language)
        console.log('changeNumber',data)
        axios
            .post(API_IP + "member/updatetelephone",data, CONFIG)
            .then(function (response) {
                console.log('changeNumber',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: CHANGE_NUMBER })
                }else{
                    Alert.alert('ERROR',response.data.message)
                    // dispatch({ type: CHANGE_NUMBER_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
            //   console.log(error)
            })
    }
}
export const changePassword = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("password", option.password)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "member/updatepassword",data, CONFIG)
            .then(function (response) {
                console.log('changePassword',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: CHANGE_PASSWORD })
                }else{
                    dispatch({ type: CHANGE_PASSWORD_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('changePassword error',error)
            })
    }
}

export const GetCreditCard = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "creditcard/getall",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: MY_CARD ,payload:response.data.data})
                }else{
                    dispatch({ type: MY_CARD_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('GetCreditCard error',error)
            })
    }
}

export const deletecredit = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
        data.append("creditcard_id", option.creditcard_id)
        axios
            .post(API_IP + "creditcard/remove",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch(GetCreditCard({language:option.language}))
                }else{
                    
                }
            })
            .catch(function (error) {
              console.log('deletecredit error',error)
            })
    }
}

export const getVoucher = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
        data.append("platform_id", 2)
        data.append("telephone", option.telephone)
        axios
            .post(API_IP + "voucher/getvoucher",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: VOUCHER ,payload:response.data.data})
                }else{
                    dispatch({ type: VOUCHER_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
              console.log('getVoucher error',error)
            })
    }
}

export const resetInfoProfile = option => {
    return async dispatch => { 
        dispatch({ type: RESET_INFO })        
    }
}

export const member_privilege = option => {
    return async dispatch => {
       
        const data = new FormData()
    
        data.append("language", option.language)
    
        axios
            .post(API_IP + "emember_privilege/getall",data, CONFIG)
            .then(function (response) {
                // console.log('member_privilege response',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: EMEMBER_PRIVILEGE ,payload:response.data.data})
                }else{
                    dispatch({ type: EMEMBER_PRIVILEGE_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
              console.log('member_privilege error',error)
            })
    }
}

export const deleteUser = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const type = await AsyncStorage.getItem('login_type')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "Member/remove_member",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    
                    if(type == 'gm'){
                        console.log('logout gm')
                        try {
                            // GoogleSignin.revokeAccess();
                            GoogleSignin.signOut();
                          } catch (error) {
                            console.error(error);
                          }
                    }else if(type == 'fb'){
                        LoginManager.logOut()
                    }
                    AsyncStorage.setItem('token', '');
                    AsyncStorage.setItem('login_type', '');
                    dispatch(clear_auth())
                    
                    Actions.popTo('loginhome')
                    dispatch({ type: DELETE_USER ,payload:response.data.data})
                }else{
                    dispatch({ type: DELETE_USER_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
              console.log('getVoucher error',error)
            })
    }
}