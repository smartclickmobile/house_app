import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    SHOWTIME,
    SHOWTIME_ERROR,

    SHOWTIME_DETAIL,
    SHOWTIME_DETAIL_ERROR
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import moment from 'moment'
export const getShowtime = option => {
    return async dispatch => {
       
        const data = new FormData()
        data.append("language", option.language)
        data.append("movie_id", option.movie_id)
        data.append("search_date", option.search_date)
        console.log('getShowtime data',data)
        axios
            .post(API_IP + "movie/showtime",data, CONFIG)
            .then(function (response) {
                console.log('getShowtime',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: SHOWTIME ,payload:response.data.data})
                }else{
                    dispatch({ type: SHOWTIME_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('getShowtime error',error)
            })
    }
}
export const getShowtimeDetail = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("showtimes_id", option.showtimes_id)
        console.log('getShowtimeDetail data',data)
        axios
            .post(API_IP + "showtimes/detail",data, CONFIG)
            .then(function (response) {
                console.log('getShowtimeDetail response',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: SHOWTIME_DETAIL ,payload:response.data.data})
                }else{
                    dispatch({ type: SHOWTIME_DETAIL_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('getShowtimeDetail error',error)
            })
    }
}
export const bookingReserve = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("device_id", option.device_id)
        data.append("platform_id", 2)
        for(var i = 0; i< option.showtimes_seat_id.length ; i++){
            data.append(`showtimes_seat_ids[${i}]`, option.showtimes_seat_id[i])
        }
        axios
            .post(API_IP + "booking/reserve",data, CONFIG)
            .then(function (response) {
                console.log('bookingReserve',response)
                if(response.data.code == '0x0000-00000'){
                    console.log('bookingReserve success',response.data.data.purchase_transaction_id)
                    Actions.BookingSummeryScreen({
                        data:option.data,
                        time_data:option.time_data,
                        seat:option.seat,
                        purchase_transaction_id:response.data.data.purchase_transaction_id,
                        showtime_detail:option.showtime_detail,
                        price:option.price,
                    })
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.log('bookingReserve error',error)
            })
    }
}

export const bookingReserveBuffet = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("device_id", option.device_id)
        data.append("platform_id", 2)
        for(var i = 0; i< option.showtimes_seat_id.length ; i++){
            data.append(`showtimes_seat_ids[${i}]`, option.showtimes_seat_id[i])
        }
        console.warn('bookingReserveBuffet data',data)
        axios
            .post(API_IP + "buffet/bookingmovie",data, CONFIG)
            .then(function (response) {
                console.warn('bookingReserveBuffet',response)
                if(response.data.code == '0x0000-00000'){
                    console.warn('bookingReserveBuffet success',response.data.data)
                    Actions.BookingSuccessScreen({buffet_movie:true,data_success:response.data.data})
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.warn('bookingReserveBuffet error',error)
            })
    }
}

export const bookingCancel = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("platform_id", 2)
        data.append("purchase_transaction_id", option.purchase_transaction_id)
        // console.log('bookingCancel data',data)
        axios
            .post(API_IP + "booking/cancelbooking",data, CONFIG)
            .then(function (response) {
                console.log('bookingCancel',response)
                if(response.data.code == '0x0000-00000'){
                    Actions.popTo('BookingSelectSeatScreen',{re:true})
                    setTimeout(() => Actions.refresh({ re:true,re_num: Math.floor((Math.random() * 1000) + 1)}),1000);
                }else{
                    // Alert.alert('ERROR',response.data.message)
                    Actions.popTo('BookingSelectSeatScreen',{re:true})
                    setTimeout(() => Actions.refresh({ re:true,re_num: Math.floor((Math.random() * 1000) + 1)}),1000);
                }
            })
            .catch(function (error) {
              console.log('bookingCancel error',error)
            })
    }
}