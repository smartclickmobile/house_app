import axios from "axios"
import { API_IP, CONFIG, GB_API_IP, GB_CONFIG, GB_API_3D, GB_CONFIG_H,PUBLIC_KEY } from '../constant/constant'
import {
    GITF,
    GITF_ERROR,
    PACKET,
    PACKET_ERROR,
    E_CREDIT_SUCCESS,
    E_CREDIT_SUCCESS_ERROR,
    CLEAR_EMEMBER
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { getProfileInfo, GetCreditCard } from '../Actions/profileAction'
export const getGift = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        axios
            .post(API_IP + "gift/getall",data, CONFIG)
            .then(function (response) {
                // console.log('getGift',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: GITF ,payload:response.data.data})
                }else{
                    dispatch({ type: GITF_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('getGift error',error)
            })
    }
}
export const getPacket = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        axios
            .post(API_IP + "member/getpackage",data, CONFIG)
            .then(function (response) {
                // console.log('getPacket',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: PACKET ,payload:response.data.data})
                }else{
                    dispatch({ type: PACKET_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('getPacket error',error)
            })
    }
}

export const upgradeEmember = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        console.log('upgradeEmember ff',option.payment_id,option.creditcard_id)
        if( option.payment_id == 2 && (option.creditcard_id == undefined || option.creditcard_id == null)){
            ///////////////////////////////////////////////////////////////////////////////////  UAT โมบาย get token จาก GB เอง  ///////////////////////////////////////////
            // const data = new FormData()
            // var token = await AsyncStorage.getItem('token')
            // data.append("language", option.language)
            // data.append("token", token)
            // data.append("title", option.title)
            // data.append("first_name", option.first_name)
            // data.append("last_name", option.last_name)
            // data.append("telephone", option.telephone)
            // data.append("citizen_number", option.citizen_number)
            // data.append("birth_date", option.birth_date)
            // data.append("lineid", option.lineid)
            // data.append("gender", option.gender)
            // data.append("age", option.age)
            // data.append("gift_id", option.gift_id)
            // data.append("is_voucher", option.is_voucher)
            // data.append("is_buffet", option.is_buffet)
            // data.append("package_id", option.package_id)
            // data.append("price", option.price)
            // data.append("payment_id", option.payment_id)
            // if(option.creditcard_id != undefined){
            //     data.append("creditcard_id", option.creditcard_id)
            // }else{
            //     data.append("number", option.number)
            //     data.append("expiration_month", option.expiration_month)
            //     data.append("expiration_year", option.expiration_year)
            //     data.append("security_code", option.security_code)
            //     data.append("name", option.name)
            //     data.append("remember_card", option.remember_card)
            // }
            // axios
            //     .post(API_IP + "member/upgrade",data, CONFIG)
            //     .then(function (response) {
            //         if(response.data.code == '0x0000-00000'){
            //             console.log('upgradeEmember',option.payment_id,response.data.data)
            //             if(option.payment_id == 4 || option.payment_id == 3){
            //                 Actions.ShowQrcodeScreen({qr:response.data.data,title:option.payment_id == 4?I18n.t('ShowQrcodeScreen.credit'):I18n.t('ShowQrcodeScreen.debit')})
            //                 dispatch({ type: E_CREDIT_SUCCESS ,payload:response.data.message})
            //             }else{
            //                 var data = {
            //                     language: I18n.locale
            //                 }
            //                 dispatch(getProfileInfo(data))
            //                 dispatch(GetCreditCard(data))
            //                 Actions.popTo('home')
            //                 dispatch({ type: E_CREDIT_SUCCESS })
            //             }
            //         }else{
            //             dispatch({ type: E_CREDIT_SUCCESS_ERROR ,payload:response.data.message})
            //         }
            //     })
            //     .catch(function (error) {
            //       console.log('error upgradeEmember',error)
            //       dispatch({ type: E_CREDIT_SUCCESS_ERROR ,payload:I18n.t('networkerror')})
            //     })

            ///////////////////////////////////////////////////////////////////////////////////  production โมบาย get token จาก GB เอง  ///////////////////////////////////////////

            console.log('credis unre')
            var data_token = {
                rememberCard:option.remember_card,
                card:{
                    "number": option.number, 
                    "expirationMonth": option.expiration_month, 
                    "expirationYear": option.expiration_year, 
                    "securityCode": option.security_code
                }
            }
            console.log('data_token ff',data_token)
            axios
                .post(GB_API_IP,data_token, GB_CONFIG)
                .then(function (response) {
                    if(response.data.resultCode == '00'){
                        var data_card = response.data.card
                        const data = new FormData()
                        data.append("language", option.language)
                        data.append("token", token)
                        data.append("title", option.title)
                        data.append("first_name", option.first_name)
                        data.append("last_name", option.last_name)
                        data.append("telephone", option.telephone)
                        data.append("citizen_number", option.citizen_number)
                        data.append("birth_date", option.birth_date)
                        data.append("lineid", option.lineid)
                        data.append("gender", option.gender)
                        data.append("age", option.age)
                        data.append("gift_id", option.gift_id)
                        data.append("is_voucher", option.is_voucher)
                        data.append("is_buffet", option.is_buffet)
                        data.append("package_id", option.package_id)
                        data.append("price", option.price)
                        data.append("payment_id", option.payment_id)

                        data.append("creditcard_token", data_card.token)// new
                        data.append("number", data_card.number)
                        data.append("expiration_month", data_card.expirationMonth)
                        data.append("expiration_year", data_card.expirationYear)
                        // data.append("security_code", option.security_code)
                        data.append("name", option.name)
                        data.append("remember_card", option.remember_card)
                        console.log('data_card',data)
                        axios
                            .post(API_IP + "member/upgradechargeotp",data, CONFIG)
                            .then(function (response) {
                                var res = response.data
                                if(response.data.code == '0x0000-00000'){
                                    var data_3d = `publicKey=${PUBLIC_KEY}&gbpReferenceNo=${res.data}`
                                    console.log('data_3d',data_3d)
                                    axios
                                    .post(GB_API_3D,data_3d, GB_CONFIG_H)
                                    .then(function (response) {
                                        
                                        console.log('purchaseBycredit',response)
                                        dispatch({ type: E_CREDIT_SUCCESS})
                                        Actions.SentOTPscreen({data:JSON.stringify(response)})
                                    })
                                }else{
                                    // Alert.alert('ERROR',response.data.code)
                                    dispatch({ type: E_CREDIT_SUCCESS_ERROR ,payload:response.data.message})
                                }
                            })
                            .catch(function (error) {
                                console.log('error upgradeEmember',error)
                            })

                    }else{
                        // Alert.alert('CARD ERROR',resultCard[response.data.resultCode].message)
                        dispatch({ type: E_CREDIT_SUCCESS_ERROR ,payload:resultCard[response.data.resultCode].message})
                    }
            }).catch(function (error) {
                // Alert.alert('CARD ERROR','Please check all digits of card no.')
                dispatch({ type: E_CREDIT_SUCCESS_ERROR ,payload:'Please check all digits of card no.'})
            })
        }else if(option.payment_id == 2 && option.creditcard_id != undefined){
            console.log('credis re')
            const data = new FormData()
            data.append("language", option.language)
            data.append("token", token)
            data.append("title", option.title)
            data.append("first_name", option.first_name)
            data.append("last_name", option.last_name)
            data.append("telephone", option.telephone)
            data.append("citizen_number", option.citizen_number)
            data.append("birth_date", option.birth_date)
            data.append("lineid", option.lineid)
            data.append("gender", option.gender)
            data.append("age", option.age)
            data.append("gift_id", option.gift_id)
            data.append("is_voucher", option.is_voucher)
            data.append("is_buffet", option.is_buffet)
            data.append("package_id", option.package_id)
            data.append("price", option.price)
            data.append("payment_id", option.payment_id)
            data.append("creditcard_id", option.creditcard_id)
            axios
            .post(API_IP + "member/upgradechargeotp",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    var data_3d = `publicKey=${PUBLIC_KEY}&gbpReferenceNo=${res.data}`
                    console.log('data_3d',data_3d)
                    axios
                    .post(GB_API_3D,data_3d, GB_CONFIG_H)
                    .then(function (response) {
                        // console.log('purchaseBycredit',response)
                        dispatch({ type: E_CREDIT_SUCCESS})
                        Actions.SentOTPscreen({data:JSON.stringify(response)})
                    })
                }
            }).catch(function (error) {
              console.log('error upgradeEmember',error)
            })
        }else{
            
            const data = new FormData()
             data.append("language", option.language)
            data.append("token", token)
            data.append("title", option.title)
            data.append("first_name", option.first_name)
            data.append("last_name", option.last_name)
            data.append("telephone", option.telephone)
            data.append("citizen_number", option.citizen_number)
            data.append("birth_date", option.birth_date)
            data.append("lineid", option.lineid)
            data.append("gender", option.gender)
            data.append("age", option.age)
            data.append("gift_id", option.gift_id)
            data.append("is_voucher", option.is_voucher)
            data.append("is_buffet", option.is_buffet)
            data.append("package_id", option.package_id)
            data.append("price", option.price)
            data.append("payment_id", option.payment_id)
            if(option.creditcard_id != undefined){
                data.append("creditcard_id", option.creditcard_id)
            }
            console.log('no credit',data)
            axios
            .post(API_IP + "member/upgrade",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    console.log('upgradeEmember',option.payment_id,response.data.data)
                    if(option.payment_id == 4 || option.payment_id == 3){
                        Actions.replace('ShowQrcodeScreen',{qr:response.data.data,title:option.payment_id == 4?I18n.t('ShowQrcodeScreen.credit'):I18n.t('ShowQrcodeScreen.debit')})
                        // Actions.ShowQrcodeScreen({qr:response.data.data,title:option.payment_id == 4?I18n.t('ShowQrcodeScreen.credit'):I18n.t('ShowQrcodeScreen.debit')})
                    }else{
                        var data = {
                            language: I18n.locale
                        }
                        dispatch(getProfileInfo(data))
                        dispatch(GetCreditCard(data))
                        Actions.popTo('home')
                        dispatch({ type: E_CREDIT_SUCCESS })
                    }
                }else{
                    console.log('upgradeEmember',response.data)
                    dispatch({ type: E_CREDIT_SUCCESS_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
              console.log('error upgradeEmember',error)
            })
        }

        
    }
}

export const clearEmember = option => {
    return async dispatch => {
        dispatch({ type: CLEAR_EMEMBER })
    }
}


