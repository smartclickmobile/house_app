import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    EVENT_LIST,
    EVENT_LIST_ERROR,
    NOW_SCREENING,
    NOW_SCREENING_ERROR,
    COMINGSOON,
    COMINGSOON_ERROR,
    SCHEDULE,
    SCHEDULE_ERROR,
    BRANNER,
    BRANNER_ERROR
} from './type'

import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import moment from 'moment'
import I18n from '../../asset/languages/i18n'
export const getEvent = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)

        axios
            .post(API_IP + "event/getall",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: EVENT_LIST ,payload:response.data.data})
                }else{
                    dispatch({ type: EVENT_LIST_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('getEvent error',error)
            })
    }
}

export const getNowScreening = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        var token = await AsyncStorage.getItem('token')
        if(token!= null && token!= '' && token != undefined){
            data.append("token", token)
        }
       

        axios
            .post(API_IP + "movie/ongoing",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: NOW_SCREENING ,payload:response.data.data})
                }else{
                    dispatch({ type: NOW_SCREENING_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
                dispatch({ type: NOW_SCREENING_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}

export const getComingsoon = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        var token = await AsyncStorage.getItem('token')
        if(token!= null && token!= '' && token != undefined){
            data.append("token", token)
        }
        axios
            .post(API_IP + "movie/comingsoon",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: COMINGSOON ,payload:response.data.data})
                }else{
                    dispatch({ type: COMINGSOON_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
                dispatch({ type: COMINGSOON_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}

export const getSchedule = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        // data.append("search_date", moment().format('YYYY-MM-DD'))
        // data.append("search_date", '2019-07-17')
        data.append("search_date", option.date)
        var token = await AsyncStorage.getItem('token')
        if(token!= null && token!= '' && token != undefined){
            data.append("token", token)
        }
        axios
            .post(API_IP + "showtimes/schedule",data, CONFIG)
            .then(function (response) {
                // console.log('getSchedule',response,option.date)
                if(response.data.code == '0x0000-00000'){
                    var date = option.date
                    var temp = {}
                    temp['date'] = option.date
                    temp['data'] = response.data.data
                    dispatch({ type: SCHEDULE ,payload:temp})
                }else{
                    dispatch({ type: SCHEDULE_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
                console.log('getSchedule error',error)
                dispatch({ type: SCHEDULE_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}

export const getBraner = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)

        axios
            .post(API_IP + "banner/getall",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: BRANNER ,payload:response.data.data})
                }else{
                    dispatch({ type: BRANNER_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
            //   dispatch({ type: BRANNER_ERROR ,payload:'Network error'})
            })
    }
}