import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    WISH,
    WISH_ERROR,
    WISHLIST,
    WISHLIST_ERROR
} from './type'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'

export const getwish = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        axios
            .post(API_IP + "wish/getall",data, CONFIG)
            .then(function (response) {
                console.log('getwish response',response.data)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: WISHLIST ,payload:response.data.data})
                }else{
                    dispatch({ type: WISHLIST_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
                dispatch({ type: WISHLIST_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}
export const addwish = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("movie_id", option.movie_id)
        axios
            .post(API_IP + "wish/addwish",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    // dispatch({ type: WISH ,payload:true})
                }else{
                    // dispatch({ type: WISH_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('addwish error',error)
            })
    }
}
export const removewish = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("wish_id", option.wish_id)
        axios
            .post(API_IP + "wish/removewish",data, CONFIG)
            .then(function (response) {
                console.log('removewish',response)
                if(response.data.code == '0x0000-00000'){
                    // dispatch({ type: WISH ,payload:true})
                }else{
                    // dispatch({ type: WISH_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.log('removewish error',error)
            })
    }
}
