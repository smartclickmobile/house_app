import axios from "axios"
import { API_IP,CONFIG,GB_API_IP,GB_CONFIG,GB_API_3D,GB_CONFIG_H,PUBLIC_KEY } from '../constant/constant'
import {
   CREDIT_SUCCESS,
   CREDIT_ERROR,
   CLEAR_PURCHASE,
   PURCHASE_DETAIL,
   PURCHASE_DETAIL_ERROR
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import moment from 'moment'
import { resultCard } from '../constant/resultCard'
export const purchaseBycredit = option => {
    return async dispatch => {
        ///////////////////////////////////////////////////////////////////////////////////  production โมบาย get token จาก GB เอง  ///////////////////////////////////////////
        var data_token = {
            rememberCard:option.remember_card,
            card:{
                "number": option.number, 
                "expirationMonth": option.expiration_month, 
                "expirationYear": option.expiration_year, 
                "securityCode": option.security_code,
                "name":option.name
            }
        }
        var token = await AsyncStorage.getItem('token')
        if(option.creditcard_id != undefined && option.creditcard_id !=null && option.creditcard_id !=''){
            const data = new FormData()
           
            data.append("token", token)
            data.append("language", option.language)
            data.append("platform_id", 2)
            data.append("purchase_transaction_id", option.purchase_transaction_id)
            for(var i = 0; i< option.voucher_ids.length ; i++){
                data.append(`voucher_ids[${i}]`, option.voucher_ids[i])
            }
            data.append("price", option.price)
            // data.append("price", 1)
            data.append("creditcard_id", option.creditcard_id)
            axios
                .post(API_IP + "purchase/creditcharge",data, CONFIG)
                .then(function (response) {
                    console.log('response2',response.data)
                    if(response.data.code == '0x0000-00000'){
                        // console.log('response1',response.data)
                        var res = response.data
                        var data_3d = `publicKey=${PUBLIC_KEY}&gbpReferenceNo=${res.data}`
                        console.log('data_3d',data_3d)
                        axios
                        .post(GB_API_3D,data_3d, GB_CONFIG_H)
                        .then(function (response) {
                            console.log('purchaseBycredit',response)
                            dispatch({ type: CREDIT_SUCCESS})
                            Actions.SentOTPscreen({data:JSON.stringify(response),purchase_transaction_id:option.purchase_transaction_id})
                        })
                    }else{
                        
                        Alert.alert('ERROR remem',response.data.message)
                        // Alert.alert('ERROR remem',response.data.data)
                        dispatch({ type: CREDIT_ERROR ,payload:response.data.message})
                    }
                    
                })
                .catch(function (error) {
                console.log(error)
                })
        }else{
            console.log('data_token',data_token)
            axios
            .post(GB_API_IP,data_token, GB_CONFIG)
            .then(function (response) {
                console.log('data_token card',response.data)
                if(response.data.resultCode == '00'){
                    
                    var data_card = response.data.card
                    const data = new FormData()
                    data.append("token", token)
                    data.append("language", option.language)
                    data.append("platform_id", 2)
                    data.append("purchase_transaction_id", option.purchase_transaction_id)
                    for(var i = 0; i< option.voucher_ids.length ; i++){
                        data.append(`voucher_ids[${i}]`, option.voucher_ids[i])
                    }
                    data.append("price", option.price)
                    data.append("creditcard_id", '')
                    data.append("number", data_card.number)
                    data.append("expiration_month", data_card.expirationMonth)
                    data.append("expiration_year", data_card.expirationYear)
                    data.append("name", option.name)
                    data.append("remember_card", option.remember_card)
                    data.append("creditcard_token", data_card.token)
                    console.log('card data',data,data_card)
                    axios
                        .post(API_IP + "purchase/creditcharge",data, CONFIG)
                        .then(function (response) {
                            var res = response.data
                            console.log('res',res.data)
                            if(response.data.code == '0x0000-00000'){
                               
                                var data_3d = `publicKey=${PUBLIC_KEY}&gbpReferenceNo=${res.data}`
                                console.log('data_3d',data_3d)
                                axios
                                .post(GB_API_3D,data_3d, GB_CONFIG_H)
                                .then(function (response) {
                                    // console.log('purchaseBycredit',response)
                                    dispatch({ type: CREDIT_SUCCESS})
                                    Actions.SentOTPscreen({data:JSON.stringify(response),purchase_transaction_id:option.purchase_transaction_id})
                                })
                            }else{
                                Alert.alert('ERROR',response.data.message)
                                dispatch({ type: CREDIT_ERROR ,payload:response.data.message})
                            }
                            
                        })
                        .catch(function (error) {
                            dispatch({ type: CREDIT_ERROR ,payload:'Please check all digits of card no.'})
                        })
                }else{
                    // Alert.alert('CARD ERROR',resultCard[response.data.resultCode].message)
                    dispatch({ type: CREDIT_ERROR ,payload:resultCard[response.data.resultCode].message})
                }  
            })
            .catch(function (error) {
                dispatch({ type: CREDIT_ERROR ,payload:'Please check all digits of card no.'})
            })
        }
        ///////////////////////////////////////////////////////////////////////////////////  UAT โมบาย get token จาก GB เอง  ///////////////////////////////////////////
        
        // const data = new FormData()
        // var token = await AsyncStorage.getItem('token')
        // data.append("token", token)
        // data.append("language", option.language)
        // data.append("platform_id", 2)
        // data.append("purchase_transaction_id", option.purchase_transaction_id)
        // for(var i = 0; i< option.voucher_ids.length ; i++){
        //     data.append(`voucher_ids[${i}]`, option.voucher_ids[i])
        // }
        // data.append("price", option.price)
        // if(option.creditcard_id != undefined && option.creditcard_id !=null && option.creditcard_id !=''){
        //     data.append("creditcard_id", option.creditcard_id)
        // }else{
        //     data.append("creditcard_id", '')
        //     data.append("number", option.number)
        //     data.append("expiration_month", option.expiration_month)
        //     data.append("expiration_year", option.expiration_year)
        //     data.append("security_code", option.security_code)
        //     data.append("name", option.name)
        //     data.append("remember_card", option.remember_card)
        // }
        // console.log('purchaseBycredit data',data)
       
        // axios
        //     .post(API_IP + "purchase/credit",data, CONFIG)
        //     .then(function (response) {
        //         console.log('purchaseBycredit',response)
        //         if(response.data.code == '0x0000-00000'){
        //             dispatch({ type: CREDIT_SUCCESS})
        //             Actions.popTo('home')
        //             Actions.BookingSuccessScreen({data:response.data.data})
        //             // Actions.replace('BookingSuccessScreen',{data:response.data.data})
        //         }else{
                    
        //             dispatch({ type: CREDIT_ERROR ,payload:response.data.message})
        //         }
                
        //     })
        //     .catch(function (error) {
        //       console.log('purchaseBycredit error',error)
        //     })
    }
}
export const getPurchase_Detail = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("language", option.language)
        data.append("purchase_transaction_id", option.purchase_transaction_id)
        axios
        .post(API_IP + "purchase/detail",data, CONFIG)
        .then(function (response) {
            console.log('getPurchase_Detail',response)
            if(response.data.code == '0x0000-00000'){
                dispatch({ type: PURCHASE_DETAIL ,payload:response.data.data})
            }else{
                dispatch({ type: PURCHASE_DETAIL_ERROR ,payload:response.data.message})
            }
        })
    }
}
export const confirmPaymentFree = option => {
    return async dispatch => {
       
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("platform_id", 2)
        data.append("purchase_transaction_id", option.purchase_transaction_id)
        for(var i = 0; i< option.voucher_ids.length ; i++){
            data.append(`voucher_ids[${i}]`, option.voucher_ids[i].voucher_id)
        }
        data.append("price", option.price)
        // console.log('confirmPaymentFree data',data)
        axios
            .post(API_IP + "purchase/confirmpayment",data, CONFIG)
            .then(function (response) {
                // console.log('confirmPaymentFree',response)
                if(response.data.code == '0x0000-00000'){
                    Actions.replace('BookingSuccessScreen',{purchase_transaction_id:option.purchase_transaction_id})
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.log('confirmPaymentFree error',error)
            })
    }
}

export const getQRcredit = option => {
    return async dispatch => {
        
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        data.append("platform_id", 2)
        data.append("purchase_transaction_id", option.purchase_transaction_id)
        for(var i = 0; i< option.voucher_ids.length ; i++){
            data.append(`voucher_ids[${i}]`, option.voucher_ids[i].voucher_id)
        }
        data.append("price", option.price)
        console.log('getQRcredit',data)
        axios
            .post(API_IP + "purchase/getqrcredit",data, CONFIG)
            .then(function (response) {
                console.log('getQRcredit',response.data.data.qrcode_url)
                if(response.data.code == '0x0000-00000'){
                    // Actions.ShowQrcodeScreen({qr:response.data.data.qrcode_url,title:I18n.t('ShowQrcodeScreen.credit')})
                    Actions.replace('ShowQrcodeScreen',{qr:response.data.data.qrcode_url,title:I18n.t('ShowQrcodeScreen.credit')})
                }else{
                Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.log('getQRcredit error',error)
            })
    }
}

export const getQRdebit = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)

        data.append("platform_id", 2)
        data.append("purchase_transaction_id", option.purchase_transaction_id)
        for(var i = 0; i< option.voucher_ids.length ; i++){
            data.append(`voucher_ids[${i}]`, option.voucher_ids[i].voucher_id)
        }
        data.append("price", option.price)
        // console.log('getQRdebit data',data)
        axios
            .post(API_IP + "purchase/getqrcode",data, CONFIG)
            .then(function (response) {
                console.log('getQRdebit res',response)
                if(response.data.code == '0x0000-00000'){
                    Actions.replace('ShowQrcodeScreen',{qr:response.data.data.qrcode_url,title:I18n.t('ShowQrcodeScreen.debit')})

                    // Actions.ShowQrcodeScreen({qr:response.data.data.qrcode_url,title:I18n.t('ShowQrcodeScreen.debit')})
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.log('getQRdebit error',error)
            })
    }
}
export const clearPurchase = option => {
    return async dispatch => {
        dispatch({ type: CLEAR_PURCHASE})
    }
}
