import axios from "axios"
import { API_IP, CONFIG, GB_API_IP, GB_API_3D, GB_CONFIG, GB_CONFIG_H, PUBLIC_KEY } from '../constant/constant'
import {
    BUFFET_SUCCESS,
    BUFFET_ERROR,
    BUFFET_CLEAR_ERROR,
    BUFFET_CREDIT_SUCCESS,
    BUFFET_CREDIT_ERROR,
    BUFFET_QR_SUCCESS,
    BUFFET_QR_ERROR,
    BUFFET_CLEAR_PURCHASE,
    BUFFET_PURCHASE_DETAIL,
    BUFFET_PURCHASE_DETAIL_ERROR
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"
import I18n from '../../asset/languages/i18n'

export const getBuffet = option => {
    return async dispatch => {
        const data = new FormData()
        const value = await AsyncStorage.getItem('token')
        data.append("token", value)
        data.append("language", option.language)
        data.append("platform_id", 2)
        console.warn('getBuffet data',data)
        axios
            .post(API_IP + "buffet/getbuffet",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                   console.warn('getBuffet response', response.data)
                   dispatch({ type: BUFFET_SUCCESS ,payload:response.data.data})
                }
            })
            .catch(function (error) {
                console.warn('getBuffet error', error)
                Alert.alert('',I18n.t('networkerror'))
            })
    }
}
export const clear_buffet = option => {
    return async dispatch => {
        dispatch({ type: BUFFET_CLEAR_ERROR}) 
    }
}

export const getQRdebit_buffet = option => {
    return async dispatch => {
        const data = new FormData()
        const value = await AsyncStorage.getItem('token')
        data.append("token", value)
        data.append("language", option.language)
        data.append("device_id", option.device_id)
        data.append("first_name", option.first_name)
        data.append("last_name", option.last_name)
        data.append("telephone", option.telephone)
        data.append("event_buffet_id", option.event_buffet_id)
        data.append("payment_id", 3)
        data.append("platform_id", 2)
        data.append("price", option.price)

        console.warn('getQRdebit data',data)
        axios
            .post(API_IP + "buffet/buyticket",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    console.warn('getQRdebit response',response.data.data)
                    dispatch({ type: BUFFET_QR_SUCCESS})
                    Actions.ShowQrcodeScreen({qr:response.data.data,title:I18n.t('ShowQrcodeScreen.debit')})
                }else{
                    dispatch({ type: BUFFET_QR_ERROR,payload:response.data.message})
                }
            })
            .catch(function (error) {
                Alert.alert('',I18n.t('networkerror'))
            })
    }
}

export const getQRcredit_buffet = option => {
    return async dispatch => {
        const data = new FormData()
        const value = await AsyncStorage.getItem('token')
        data.append("token", value)
        data.append("language", option.language)
        data.append("device_id", option.device_id)
        data.append("first_name", option.first_name)
        data.append("last_name", option.last_name)
        data.append("telephone", option.telephone)
        data.append("event_buffet_id", option.event_buffet_id)
        data.append("payment_id", 4)
        data.append("platform_id", 2)
        data.append("price", option.price)

        console.warn('getQRcredit data',data)
        axios
            .post(API_IP + "buffet/buyticket",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    console.warn('getQRcredit response',response.data.data)
                    dispatch({ type: BUFFET_QR_SUCCESS})
                    Actions.ShowQrcodeScreen({qr:response.data.data,title:I18n.t('ShowQrcodeScreen.credit')})
                }else{
                    dispatch({ type: BUFFET_QR_ERROR,payload:response.data.message})
                }
            })
            .catch(function (error) {
                Alert.alert('',I18n.t('networkerror'))
            })
    }
}

export const payBycreditcard = option => {
    return async dispatch => {
        ///////////////////////////////////////////////////////////////////////////////////   โมบาย get token จาก GB เอง  ///////////////////////////////////////////
        var token = await AsyncStorage.getItem('token')
        console.log('payBycreditcard ff',option)
        if(option.creditcard_id == undefined || option.creditcard_id == null){
            console.log('credis unre')
            var data_token = {
                rememberCard:option.remember_card,
                card:{
                    "number": option.number, 
                    "expirationMonth": option.expiration_month, 
                    "expirationYear": option.expiration_year, 
                    "securityCode": option.security_code
                }
            }
            console.warn('data_token ff',data_token)
            axios
                .post(GB_API_IP,data_token, GB_CONFIG)
                .then(function (response) {
                    console.warn('GB_API_IP response',response)
                    if(response.data.resultCode == '00'){
                        var data_card = response.data.card
                        const data = new FormData()
                        data.append("language", option.language)
                        data.append("token", token)
                        data.append("first_name", option.first_name)
                        data.append("last_name", option.last_name)
                        data.append("telephone", option.telephone)
                        data.append("price", option.price)
                        data.append("platform_id", option.platform_id)
                        data.append("creditcard_id", '')
                        data.append("event_buffet_id", option.event_buffet_id)
                        data.append("device_id", option.device_id)

                        data.append("creditcard_token", data_card.token)
                        data.append("number", data_card.number)
                        data.append("expiration_month", data_card.expirationMonth)
                        data.append("expiration_year", data_card.expirationYear)
                        data.append("name", option.name)
                        data.append("remember_card", option.remember_card)
                        console.warn('data_card',data)
                        axios
                            .post(API_IP + "buffet/creditcharge",data, CONFIG)
                            .then(function (response) {
                                var res = response.data
                                console.warn('creditcharge',response)
                                if(response.data.code == '0x0000-00000'){
                                    var data_3d = `publicKey=${PUBLIC_KEY}&gbpReferenceNo=${res.data}`
                                    console.warn('data_3d',data_3d)
                                    axios
                                    .post(GB_API_3D,data_3d, GB_CONFIG_H)
                                    .then(function (response) {
                                        
                                        console.log('purchaseBycredit',response)
                                        dispatch({ type: BUFFET_CREDIT_SUCCESS})
                                        Actions.BuffetSentOTPScreen({data:JSON.stringify(response),event_buffet_id: option.event_buffet_id})
                                    })
                                }else{
                                    dispatch({ type: BUFFET_CREDIT_ERROR ,payload:response.data.message})
                                }
                            })
                            .catch(function (error) {
                                console.log('error payBycreditcard',error)
                            })

                    }else{
                        dispatch({ type: BUFFET_CREDIT_ERROR ,payload:resultCard[response.data.resultCode].message})
                    }
            }).catch(function (error) {
                console.warn('GB_API_IP error',error)
                dispatch({ type: BUFFET_CREDIT_ERROR ,payload:'Please check all digits of card no.'})
            })
        }else{
            console.log('credis re')
            const data = new FormData()
            data.append("language", option.language)
            data.append("token", token)
            data.append("first_name", option.first_name)
            data.append("last_name", option.last_name)
            data.append("telephone", option.telephone)
            data.append("price", option.price)
            data.append("platform_id", option.platform_id)
            data.append("creditcard_id", option.creditcard_id)
            data.append("event_buffet_id", option.event_buffet_id)
            data.append("device_id", option.device_id)
            console.warn('data_card',data)
            axios
            .post(API_IP + "buffet/creditcharge",data, CONFIG)
            .then(function (response) {
                console.warn('creditcharge response',response)
                var res = response.data
                if(response.data.code == '0x0000-00000'){
                    console.warn('creditcharge 0x0000-00000')
                    var data_3d = `publicKey=${PUBLIC_KEY}&gbpReferenceNo=${res.data}`
                    console.warn('data_3d',data_3d)
                    axios
                    .post(GB_API_3D,data_3d, GB_CONFIG_H)
                    .then(function (response) {
                        dispatch({ type: BUFFET_CREDIT_SUCCESS})
                        Actions.BuffetSentOTPScreen({data:JSON.stringify(response),event_buffet_id: option.event_buffet_id})
                    })
                }else{
                    console.warn('creditcharge response error',response.data.message)
                    dispatch({ type: BUFFET_CREDIT_ERROR ,payload:response.data.message})
                }
            }).catch(function (error) {
              console.log('error payBycreditcard',error)
            })
        }
        ///////////////////////////////////////////////////////////////////////////////////   โมบาย get token จาก GB เอง  ///////////////////////////////////////////
        // dispatch({ type: BUFFET_CREDIT_SUCCESS})
        // Actions.BuffetSentOTPScreen({data:'',event_buffet_id: option.event_buffet_id})
        
    }
}

export const buffetClearPurchase = option => {
    return async dispatch => {
        dispatch({ type: BUFFET_CLEAR_PURCHASE})
    }
}
export const getPurchaseBuffet_Detail = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("language", option.language)
        data.append("event_buffet_id", option.event_buffet_id)
        data.append("platform_id", 2)
        data.append("token", token)
        console.warn('getPurchaseBuffet_Detail data',data)
        axios
        .post(API_IP + "buffet/getbuffetdetail",data, CONFIG)
        .then(function (response) {
            console.warn('getPurchaseBuffet_Detail',response)
            if(response.data.code == '0x0000-00000'){
                dispatch({ type: BUFFET_PURCHASE_DETAIL ,payload:response.data.data})
            }else{
                dispatch({ type: BUFFET_PURCHASE_DETAIL_ERROR ,payload:response.data.message})
            }
        })
    }
}