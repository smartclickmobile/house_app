import {
    CHECKTYPEIPHONE,
    CHECKTYPEIPHONE_ERROR,
    INDEX_MENU,
    INDEX_FOOTER,
    INDEX_TICKET,
    INDEX_PROFILE,
    BOOKING_FLAG,
    BOOKING_FLAG_CLEAR,
    CHECK_APP_VERSION
} from './type'
import { API_IP, CONFIG } from '../constant/constant'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from "axios"
export const checkiphonex = (data) => {
    return async dispatch => {
        if(data.height==812&&data.width==375){
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'x' })
        }else if(data.height==896&&data.width==414){
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'xr' })
        }else{
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'nox' })
        }
    }
}

export const checkappversion = (data) => {
    return async dispatch => {
        let ver = await axios
            .get(API_IP + "Version/get_version", CONFIG)
            .then(function (response) {
                console.log('checkappversion',response.data)
                if(response.data.code == '0x0000-00000'){
                    return response.data.data
                }else{
                    return null
                }
            }).catch(function (error) {
                console.log('checkappversion error',error)
                return null
              })

        return ver
    }
}

export const moneyFormat = (data) => {
    return async dispatch => {
    var decimal = "."
    var thousands = ","
    var decimalCount1 = ''
    
    try {
        // console.log('moneyFormat',data)
        decimalCount1 = Math.abs(data.decimalCount);
        decimalCount1 = isNaN(data.decimalCount) ? 2 : data.decimalCount;
    
        const negativeSign = data.amount < 0 ? "-" : "";
        let i = parseInt(Math.abs(Number(data.amount) || 0).toFixed(decimalCount1)).toString();
        
        let j = (i.length > 3) ? i.length % 3 : 0;
        var p = negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount1 ? decimal + Math.abs(data.amount - i).toFixed(decimalCount1).slice(2) : "")
        // console.log('test',data.amount,p)
        return p
      } catch (e) {
        console.log(e)
      }
    }
}

export const creditcardFormat = (data) => {
    return async dispatch => {
        var num1 = data.substring(0, 4)
        var num2 = data.substring(4, 8)
        var num3 = data.substring(8, 12)
        var num4 = data.substring(12, 16)
        var sum_str = `${num2!=''?num1+'-':num1}${num3!=''?num2+'-':num2}${num4!=''?num3+'-':num3}${num4}`
        // console.log('creditcardFormat',sum_str)
        return sum_str
    }
}

export const convertTime = (data) => {
    return async dispatch => {
        var num = data;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + " hour " + rminutes + " min";
    }
}
export const setindexmenu = (data) => {
    return async dispatch => {
        return dispatch({ type: INDEX_MENU, payload: data })
    }
}
export const setindexfooter = (data) => {
    return async dispatch => {
        return dispatch({ type: INDEX_FOOTER, payload: data })
    }
}
export const setindexTickets = (data) => {
    return async dispatch => {
        return dispatch({ type: INDEX_TICKET, payload: data })
    }
}

export const resetProfile = (data) => {
    return async dispatch => {
        return dispatch({ type: INDEX_PROFILE, payload: data })
    }
}

export const setBooking = (data) => {
    return async dispatch => {
        return dispatch({ type: BOOKING_FLAG, payload: data, flag:true })
    }
}
export const clearBooking = (data) => {
    return async dispatch => {
        return dispatch({ type: BOOKING_FLAG_CLEAR,flag:false })
    }
}


   