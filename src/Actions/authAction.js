import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    REGISTER_SUCCESS,
    REGISTER_ERROR,
    REGISTER_E_MEMBER_SUCCESS,
    REGISTER_E_MEMBER_ERROR,

    LOGIN_SUCCESS,
    LOGIN_ERROR,

    FORGET_PASSWORD_SUCCESS,
    FORGET_PASSWORD_ERROR,
    CLEAR_AUTH
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk';
import I18n from '../../asset/languages/i18n'
export const registerUser = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("email", option.email)
        data.append("telephone", option.telephone)
        data.append("device_id", option.device_id)
        data.append("device_name", option.device_name)
        data.append("platform_id", 2)
        data.append("member_type_id", option.member_type_id)
        data.append("language", option.language)
        
        if(option.type == 1){
            data.append("token_facebook", option.token_facebook)
        }else if(option.type == 2){
            data.append("token_gmail", option.token_gmail)
        }else if(option.type == 3){
            data.append("password", option.password)
        }
        axios
            .post(API_IP + "member/register",data, CONFIG)
            .then(function (response) {
               
                if(response.data.code == '0x0000-00000'){
                    if(option.member_type_id == 1){
                        if(option.type == 2 ){
                        }
                        dispatch({ type: REGISTER_SUCCESS })
                    }else{
                        dispatch({ type: REGISTER_E_MEMBER_SUCCESS })
                    }
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
                Alert.alert('',I18n.t('networkerror'))
            })
    }

    
}

export const login = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("email", option.email)
        data.append("device_id", option.device_id)
        data.append("device_name", option.device_name)
        data.append("platform_id", 2)
        data.append("language", option.language)
        data.append("token_firebase", option.token_firebase)
        var type = ''
        if(option.type == 1){
            data.append("telephone", option.telephone != undefined?option.telephone:'')
            data.append("token_facebook", option.token_facebook)
            data.append("facebook_id", option.facebook_id)
            data.append("member_type_id", 1)
            type = 'fb'
        }else if(option.type == 2){
            data.append("telephone", option.telephone != undefined?option.telephone:'')
            data.append("token_gmail", option.token_gmail)
            data.append("member_type_id", 1)
            type = 'gm'
        }else if(option.type == 3){
            data.append("password", option.password)
            data.append("member_type_id", 1)
            type = 'r'
        }else if(option.type == 4){
            data.append("telephone", option.telephone != undefined?option.telephone:'')
            data.append("token_appleid", option.token_appleid)
            data.append("member_type_id", 1)
            type = 'ap'
        }
            console.log('auth data',data)
            axios
            .post(API_IP + "member/login",data, CONFIG)
            .then(async (response)=> {
                console.log('member/login',response.data.code)
                if(response.data.code == '0x0000-00000'){
                    await AsyncStorage.setItem('token', response.data.data);
                    console.log('member token',response.data.data)
                    await AsyncStorage.setItem('login_type', type);
                    if(option.type == 1){
                        await   AsyncStorage.setItem('token_fb', option.token_facebook);
                    }else if(option.type == 2){
                        await  AsyncStorage.setItem('token_gm', option.token_gmail);
                    }else if(option.type == 4){
                        await AsyncStorage.setItem('token_ap', option.token_appleid);
                    }
                    console.warn('fromdetail',option.fromdetail)
                    if(option.fromdetail){
                        Actions.home({login_flag:true,fromdetail:option.fromdetail,data_detail:option.data_detail})
                    }else{
                        if(option.login){
                            Actions.popTo('loginhome',{login_flag:true})
                            Actions.home({login_flag:true})
                        }else{
                            Actions.home({login_flag:true})
                        }
                    }
                }else if(response.data.code == '0x0API-00004'){
                    if(option.type == 1){
                        var val = {
                            email:option.email,
                            token_facebook:option.token_facebook
                        }
                    }else if(option.type == 2){
                        var val = {
                            email:option.email,
                            token_gmail:option.token_gmail
                        }
                    }else if(option.type == 4){
                        var val = {
                            email:option.email,
                            token_appleid:option.token_appleid
                        }

                    }
                    Actions.confirmmobile({data:val,login_t:type,facebook_id:option.facebook_id,fromdetail:option.fromdetail,data_detail:option.data_detail})
                }
                else{
                    // GoogleSignin.revokeAccess();
                    GoogleSignin.signOut();
                    LoginManager.logOut()
                    Alert.alert('ERROR',response.data.message)
                    
                }
            })
            .catch(function (error) {
                console.log('login error',error)
                Alert.alert('',I18n.t('networkerror'))
            })
        // }
        
    } 
}

export const logout = option => {
    return async dispatch => {
        const data = new FormData()
        const value = await AsyncStorage.getItem('token')
        const type = await AsyncStorage.getItem('login_type')
        data.append("token", value)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "member/logout",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    if(type == 'gm'){
                        console.log('logout gm')
                        try {
                            // GoogleSignin.revokeAccess();
                            GoogleSignin.signOut();
                          } catch (error) {
                            console.error(error);
                          }
                    }else if(type == 'fb'){
                        LoginManager.logOut()
                    }
                    AsyncStorage.setItem('token', '');
                    AsyncStorage.setItem('login_type', '');
                    dispatch(clear_auth())
                    
                    Actions.popTo('loginhome')
                    // Actions.loginhome()
                    
                }
            })
            .catch(function (error) {
                Alert.alert('',I18n.t('networkerror'))
            })
    }
}

export const forgetpassword = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("email", option.email)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "member/forgetpassword",data, CONFIG)
            .then(function (response) {
                console.log('forgetpassword',response.data)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: FORGET_PASSWORD_SUCCESS })
                }else{
                    dispatch({ type: FORGET_PASSWORD_ERROR,payload:response.data.message })
                }
            })
            .catch(function (error) {
                Alert.alert('',I18n.t('networkerror'))
            })
    }
}

export const chackmail = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("email", option.email)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "member/login",data, CONFIG)
            .then(function (response) {
                console.log('chackmail',response)
                // if(response.data.code == '0x0000-00000'){
                //     dispatch({ type: FORGET_PASSWORD_SUCCESS })
                // }else{
                //     dispatch({ type: FORGET_PASSWORD_ERROR })
                // }
            })
            .catch(function (error) {
                Alert.alert('',I18n.t('networkerror'))
            })
    }
}

export const clear_auth = option => {
    return async dispatch => {
      
    dispatch({ type: CLEAR_AUTH })
    }      
}