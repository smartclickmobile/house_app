import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    STUDENT_SUCCESS,
    STUDENT_ERROR,

    STUDENT_LIST,
    STUDENT_LIST_ERROR,

    STUDENT_CLEAR
} from './type'
import {  Alert } from 'react-native' 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import moment from 'moment'
export const registerStudent = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("language", option.language)
        
        data.append("token", token)
        data.append("title", option.title)
        data.append("first_name", option.first_name)
        data.append("last_name", option.last_name)
        data.append("citizen_number", option.citizen_number)
        data.append("student_number", option.student_number)
        data.append("school_name", option.school_name)
        data.append("expire_month", option.expire_month)
        data.append("expire_year", option.expire_year)

        for(var i = 0 ; i < option.document_file.length ; i++){
            var photo = {
                uri: option.document_file[i],
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
            }
            data.append("document_file[" + i + "]", photo)
        }
        console.warn("registerStudent data",data)
        // dispatch({ type: STUDENT_SUCCESS})
        // dispatch({ type: STUDENT_ERROR ,payload:'testtest'})
        axios
            .post(API_IP + "student/insertstudent",data, CONFIG)
            .then(function (response) {
                console.log('registerStudent res',response)
                 
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: STUDENT_SUCCESS})
                }else{
                    dispatch({ type: STUDENT_ERROR ,payload:response.data.message})
                }
            })
            .catch(function (error) {
              console.log('registerStudent error',error)
            })
    }
}
export const clearStudent = option => {
    return async dispatch => {
        dispatch({ type: STUDENT_CLEAR})
    }
}
