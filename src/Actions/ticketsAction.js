import axios from "axios"
import { API_IP,CONFIG } from '../constant/constant'
import {
    HISTORY,
    HISTORY_ERROR,
    TICKET,
    TICKET_ERROR
} from './type'

import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux"
import moment from 'moment'
import I18n from '../../asset/languages/i18n'
export const getTicket = option => {
    return async dispatch => { 
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        axios
            .post(API_IP + "purchase/getpurchaseall",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                   
                    dispatch({ type: TICKET ,payload:response.data.data})
                }else{
                    dispatch({ type: TICKET_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
                console.log('getTicket error')
                dispatch({ type: TICKET_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}
export const getbuffetTicket = option => {
    return async dispatch => { 
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        
        axios
            .post(API_IP + "event/getbuffetticket",data, CONFIG)
            .then(function (response) {
                console.warn('getbuffetTicket',response)
                if(response.data.code == '0x0000-00000'){
    
                   var buffet = response.data.data

                   axios
                   .post(API_IP + "purchase/getpurchaseall",data, CONFIG)
                   .then(function (response1) {
                    console.warn('getpurchaseall',response1)
                       if(response1.data.code == '0x0000-00000'){
                           var ticket = response1.data.data
                           var data_ticket = []
                           data_ticket.push(buffet)
                            for(var i=0;i<ticket.length;i++){
                                data_ticket.push(ticket[i])
                            }
                            // console.log('data_ticket',data_ticket)
                           dispatch({ type: TICKET ,payload:data_ticket})
                       }else{
                           dispatch({ type: TICKET_ERROR ,payload:response1.data.data})
                       }
                   })
                   .catch(function (error) {
                        console.log('getbuffetTicket error')
                       dispatch({ type: TICKET_ERROR ,payload:I18n.t('networkerror')})
                   })
                    

                }else{
                    
                }
            })
            .catch(function (error) {
                console.log('getbuffetTicket error')
                dispatch({ type: TICKET_ERROR ,payload:I18n.t('networkerror')})
            })
    }
}
export const getHistory = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("language", option.language)
        console.log('getHistory data',data)
        axios
            .post(API_IP + "event/getbuffetticketHistory",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    // console.log('getHistory',response)
                   var buffet = response.data.data

                   axios
                   .post(API_IP + "history/getall",data, CONFIG)
                   .then(function (response1) {
                    console.warn('getHistory',response1.data)
                       if(response1.data.code == '0x0000-00000'){
                           var ticket = response1.data.data
                           var data_ticket = []
                           if(buffet != null){
                                for(var j=0;j<buffet.length;j++){
                                    buffet[j].event1 = true
                                    data_ticket.push(buffet[j])
                                }
                            
                                for(var i=0;i<ticket.length;i++){
                                    ticket[i].event1 = false
                                    data_ticket.push(ticket[i])
                                }
                                console.log('data_ticket',data_ticket.length)
                            dispatch({ type: HISTORY ,payload:data_ticket})
                           }else{
                                for(var i=0;i<ticket.length;i++){
                                    ticket[i].event1 = false
                                    data_ticket.push(ticket[i])
                                }
                                console.log('data_ticket',data_ticket)
                                dispatch({ type: HISTORY ,payload:data_ticket})
                           }
                        
                       }else{
                           dispatch({ type: HISTORY_ERROR ,payload:response1.data.data})
                       }
                   })
                   .catch(function (error) {
                    console.log('getbuffetTicket error 1')
                       dispatch({ type: HISTORY_ERROR ,payload:I18n.t('networkerror')})
                   })
                    

                }else{
                    
                }
            })
            .catch(function (error) {
                console.log('getbuffetTicket error 2')
                dispatch({ type: HISTORY_ERROR ,payload:I18n.t('networkerror')})
            })

        // axios
        //     .post(API_IP + "history/getall",data, CONFIG)
        //     .then(function (response) {
        //         // console.log('getHistory',response)
        //         if(response.data.code == '0x0000-00000'){
        //             dispatch({ type: HISTORY ,payload:response.data.data})
        //         }else{
        //             dispatch({ type: HISTORY_ERROR ,payload:response.data.data})
        //         }
        //     })
        //     .catch(function (error) {
        //       console.log('getHistory error',error)
        //     })
    }
}

