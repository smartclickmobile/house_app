import axios from "axios"
import { API_IP, CONFIG } from '../constant/constant'
import {
    EXCHANGE_POINT,
    EXCHANGE_POINT_ERROR
} from './type'
import {  Alert } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'; 
import { Actions } from "react-native-router-flux"
import moment from 'moment'
export const exchangePoint = option => {
    return async dispatch => {
        const data = new FormData()
        var token = await AsyncStorage.getItem('token')
        data.append("language", option.language)
        data.append("token", token)
        data.append("total_voucher", option.total_voucher)
        axios
            .post(API_IP + `voucher/changvoucher`,data, CONFIG)
            .then(function (response) {
                console.log('exchangePoint',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: EXCHANGE_POINT })
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.log('exchangePoint error',error)
            })
    }
}



