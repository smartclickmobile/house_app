import { 
   CREDIT_SUCCESS,
   CREDIT_ERROR,
   CLEAR_PURCHASE,
   PURCHASE_DETAIL,
   PURCHASE_DETAIL_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   credit_success:null,
   credit_success_num:null,
   purchase_detail:null,
   error_num:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case CREDIT_SUCCESS:
        return {
          ...state,
          credit_success: true,
          credit_success_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case CREDIT_ERROR:
        return {
          ...state,
          credit_success: null,
          error_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
    case CLEAR_PURCHASE:
        return {
          ...state,
          credit_success:null,
            credit_success_num:null,
            error:null
        }
      case PURCHASE_DETAIL:
          return {
            ...state,
            purchase_detail:action.payload
          }
      case PURCHASE_DETAIL_ERROR:
          return {
            ...state,
            purchase_detail:action.payload
          }
      default:
        return state
    }
  }
  