import { 
    TICKET,
    TICKET_ERROR,
    HISTORY,
    HISTORY_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   tickets:null,
   history:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case TICKET:
        return {
          ...state,
          tickets: action.payload,
          error: null,
        }
      case TICKET_ERROR:
        return {
          ...state,
          tickets: null,
          error: action.payload,
        }
      case HISTORY:
        return {
          ...state,
          history: action.payload,
          error: null,
        }
      case HISTORY_ERROR:
        return {
          ...state,
          history: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  