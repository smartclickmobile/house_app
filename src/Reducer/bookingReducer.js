import { 
    SHOWTIME,
    SHOWTIME_ERROR,
    SHOWTIME_DETAIL,
    SHOWTIME_DETAIL_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   showtime:null,
   showtimedetail:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case SHOWTIME:
        return {
          ...state,
          showtime: action.payload,
          error: null,
        }
      case SHOWTIME_ERROR:
        return {
          ...state,
          showtime: null,
          error: action.payload,
        }
      case SHOWTIME_DETAIL:
        return {
          ...state,
          showtimedetail: action.payload,
          error: null,
        }
      case SHOWTIME_DETAIL_ERROR:
        return {
          ...state,
          showtimedetail: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  