import { 
    WISH,
    WISH_ERROR,
    WISHLIST,
    WISHLIST_ERROR
} from '../Actions/type'
const INITIAL_STATE = {

   wishlist:null,
   wish_success:null,
   wish_success_num:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case WISH:
        return {
          ...state,
          wish_success: action.payload,
          wish_success_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case WISH_ERROR:
        return {
          ...state,
          wish_success: null,
          error: action.payload,
        }
      case WISHLIST:
        return {
          ...state,
          wishlist: action.payload,
          error: null,
        }
      case WISHLIST_ERROR:
        return {
          ...state,
          wishlist: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  