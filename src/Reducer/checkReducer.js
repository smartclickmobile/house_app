import { 
    CHECKTYPEIPHONE,
    CHECKTYPEIPHONE_ERROR,
    INDEX_MENU,
    INDEX_FOOTER,
    INDEX_TICKET,
    INDEX_PROFILE,
    BOOKING_FLAG,
    BOOKING_FLAG_CLEAR
} from '../Actions/type'
const INITIAL_STATE = {
   checkIpnoe:null,
   index_menu:1,
   index_footer:1,
   error:null,
   index_ticket:1,
   index_profile:0,
   index_profile_num:null,
   booking_flag:0,
   booking_flag_num:null,
   booking:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case CHECKTYPEIPHONE:
        return {
          ...state,
          checkIpnoe: action.payload,
          error: null,
        }
      case CHECKTYPEIPHONE_ERROR:
        return {
          ...state,
          checkIpnoe: null,
          error: null,
        }
      case INDEX_MENU:
        return {
          ...state,
          index_menu: action.payload,
          error: null,
        }
      case INDEX_FOOTER:
        return {
          ...state,
          index_footer: action.payload,
          error: null,
        }
      case INDEX_TICKET:
        return {
          ...state,
          index_ticket: action.payload,
          error: null,
        }
      case INDEX_PROFILE:
        return {
          ...state,
          index_profile: action.payload,
          index_profile_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case BOOKING_FLAG:
        return {
          ...state,
          booking_flag: action.flag,
          booking:action.payload,
          booking_flag_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case BOOKING_FLAG_CLEAR:
        return {
          ...state,
          booking_flag:  action.flag,
          booking:null,
          booking_flag_num: null,
          error: null,
        }
      default:
        return state
    }
  }
  