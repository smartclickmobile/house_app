import { 
    GITF,
    GITF_ERROR,
    PACKET,
    PACKET_ERROR,
    E_CREDIT_SUCCESS,
    E_CREDIT_SUCCESS_ERROR,
    CLEAR_EMEMBER
} from '../Actions/type'
const INITIAL_STATE = {
   GitfList:null,
   packetList:null,
   credit_success:null,
   credit_success_num:null,
   error:null,
   error_num:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case GITF:
        return {
          ...state,
          GitfList: action.payload,
          error: null,
        }
      case GITF_ERROR:
        return {
          ...state,
          GitfList: null,
          error: action.payload,
        }
      case PACKET:
        return {
          ...state,
          packetList: action.payload,
          error: null,
        }
      case PACKET_ERROR:
        return {
          ...state,
          packetList: null,
          error: action.payload,
        }
      case E_CREDIT_SUCCESS:
          return {
            ...state,
            credit_success: true,
            credit_success_num:Math.floor((Math.random() * 1000) + 1),
            error: null,
          }
      case E_CREDIT_SUCCESS_ERROR:
        console.log('E_CREDIT_SUCCESS_ERROR')
          return {
            ...state,
            credit_success: null,
            error_num:Math.floor((Math.random() * 1000) + 1),
            error: action.payload,
          }
      case CLEAR_EMEMBER:
          return {
            ...state,
            credit_success: null,
            credit_success_num:null,
            error: null,
            error_num: null
      }
      default:
        return state
    }
  }
  