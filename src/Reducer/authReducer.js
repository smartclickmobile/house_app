import { 
    REGISTER_SUCCESS,
    REGISTER_ERROR,
    REGISTER_E_MEMBER_SUCCESS,
    REGISTER_E_MEMBER_ERROR,

    LOGIN_SUCCESS,
    LOGIN_ERROR,

    FORGET_PASSWORD_SUCCESS,
    FORGET_PASSWORD_ERROR,

    CLEAR_AUTH
} from '../Actions/type'
const INITIAL_STATE = {
   register:null,
   register_random:null,
   register_e_member:null,
   register_e_member_random:null,
   error_register:null,
   error_e_member:null,

   login:null,
   login_random:null,
   login_error:null,

   forget_success:null,
   forget_success_random:null,
   forget_error_num:null,
   forget_error:null

}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case REGISTER_SUCCESS:
        return {
          ...state,
          register: true,
          register_random: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case REGISTER_ERROR:
        return {
          ...state,
          register: null,
          error: null,
        }
      case REGISTER_E_MEMBER_SUCCESS:
        return {
          ...state,
          register_e_member: true,
          register_e_member_random:Math.floor((Math.random() * 1000) + 1),
          error_e_member: null,
        }
      case REGISTER_E_MEMBER_ERROR:
        return {
          ...state,
          register_e_member: null,
          error_e_member: action.payload,
        }
      
      case LOGIN_SUCCESS:
        return {
          ...state,
          login: true,
          login_error: null,
        }
      case LOGIN_ERROR:
        return {
          ...state,
          login: null,
          login_error: action.payload,
        }

      case FORGET_PASSWORD_SUCCESS:
        return {
          ...state,
          forget_success: true,
          forget_success_random:Math.floor((Math.random() * 1000) + 1),
          forget_error: null,
        }
      case FORGET_PASSWORD_ERROR:
        return {
          ...state,
          forget_error: action.payload,
          forget_error_num:Math.floor((Math.random() * 1000) + 1),
          forget_success: null,
        }
      case CLEAR_AUTH:
        return{
          ...state,
          register:null,
          register_random:null,
          register_e_member:null,
          register_e_member_random:null,
          error_register:null,
          error_e_member:null,
       
          login:null,
          login_random:null,
          login_error:null,
       
          forget_success:null,
          forget_success_random:null,
          forget_error:null
        }
      
      default:
        return state
    }
  }
  