import { 
    STUDENT_LIST,
    STUDENT_LIST_ERROR,
    STUDENT_SUCCESS,
    STUDENT_ERROR,
    STUDENT_CLEAR
} from '../Actions/type'
const INITIAL_STATE = {
   student:null,
   success:null,
   success_num:null,
   error:null,
   error_num:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case STUDENT_LIST:
        return {
          ...state,
          student: action.payload,
          error: null,
        }
      case STUDENT_LIST_ERROR:
        return {
          ...state,
          student: null,
          error: action.payload,
        }
      case STUDENT_SUCCESS:
        return {
          ...state,
          success: true,
          success_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case STUDENT_ERROR:
        return {
          ...state,
          success: null,
          error: action.payload,
          error_num: Math.floor((Math.random() * 1000) + 1)
        }
      case STUDENT_CLEAR:
        return {
          ...state,
          success:null,
          success_num:null,
          error:null,
          error_num:null
        } 
      default:
        return state
    }
  }
  