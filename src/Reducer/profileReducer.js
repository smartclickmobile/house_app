import { 
    PROFILE_INFO,
    PROFILE_INFO_ERROR,

    CHANGE_NUMBER,
    CHANGE_NUMBER_ERROR,

    CHANGE_PASSWORD,
    CHANGE_PASSWORD_ERROR,

    MY_CARD,
    MY_CARD_ERROR,

    VOUCHER,
    VOUCHER_ERROR,
    RESET_INFO,

    EMEMBER_PRIVILEGE,
    EMEMBER_PRIVILEGE_ERROR,

    DELETE_USER,
    DELETE_USER_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   profile_info:null,
   profile_info_num:null,
   change_number:null,
   change_number_num:null,
   change_password:null,
   change_password_num:null,
   my_card:null,
   my_card_num:null,
   voucher:null,
   voucher_num:null,
   emember_privilege:null,
   emember_privilege_num:null,
   delete_user:null,
   delete_user_num:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case PROFILE_INFO:
        return {
          ...state,
          profile_info: action.payload,
          profile_info_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case PROFILE_INFO_ERROR:
        console.log('PROFILE_INFO_ERROR')
        return {
          ...state,
          profile_info: null,
          error: action.payload,
        }
      case CHANGE_NUMBER:
        return {
          ...state,
          change_number: true,
          change_number_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case CHANGE_NUMBER_ERROR:
        return {
          ...state,
          change_number: null,
          error: action.payload,
        }
      case CHANGE_PASSWORD:
        return {
          ...state,
          change_password: true,
          change_password_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case CHANGE_PASSWORD_ERROR:
        return {
          ...state,
          change_password: null,
          error: action.payload,
        }
      case MY_CARD:
        return {
          ...state,
          my_card: action.payload,
          my_card_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case MY_CARD_ERROR:
        return {
          ...state,
          my_card: null,
          error: action.payload,
        }
      case VOUCHER:
        return {
          ...state,
          voucher: action.payload,
          voucher_num: Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case VOUCHER_ERROR:
        return {
          ...state,
          voucher: null,
          error: action.payload,
        }
      case EMEMBER_PRIVILEGE:
          return {
            ...state,
            emember_privilege: action.payload,
            emember_privilege_num: Math.floor((Math.random() * 1000) + 1),
            error: null,
          }
        case EMEMBER_PRIVILEGE_ERROR:
          return {
            ...state,
            emember_privilege: null,
            error: action.payload,
          }
          case DELETE_USER:
            return {
              ...state,
              delete_user: action.payload,
              delete_user_num: Math.floor((Math.random() * 1000) + 1),
              error: null,
            }
          case DELETE_USER_ERROR:
            return {
              ...state,
              delete_user: null,
              error: action.payload,
            }
      case RESET_INFO:
        return {
          ...state,
          change_password: null,
          change_password_num:null,
          error: null,
        }
      default:
        return state
    }
  }
  