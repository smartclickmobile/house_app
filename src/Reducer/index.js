import { combineReducers } from "redux"
import checkReducer from './checkReducer'
import authReducer from './authReducer'
import profileReducer from './profileReducer'
import homeReducer from './homeReducer'
import bookingReducer from './bookingReducer'
import wiahReducer from './wiahReducer'
import ticketReducer from './ticketReducer'
import eMemberReducer from './eMemberReducer'
import eventReducer from './eventReducer'
import voucherReducer from './voucherReducer'
import purchaseReducer from './purchaseReducer'
import buffetReducer from './buffetReducer'
import studentReducer from './studentReducer'
export default combineReducers({
    check: checkReducer,
    auth: authReducer,
    profile: profileReducer,
    home: homeReducer,
    booking: bookingReducer,
    wish:wiahReducer,
    ticket:ticketReducer,
    eMember: eMemberReducer,
    event:eventReducer,
    voucher:voucherReducer,
    purchase:purchaseReducer,
    buffet:buffetReducer,
    student:studentReducer
  })