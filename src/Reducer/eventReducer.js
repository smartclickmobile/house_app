import { 
    EVENT_DEDAIL,
    EVENT_DEDAIL_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   event_detail:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case EVENT_DEDAIL:
        return {
          ...state,
          event_detail: action.payload,
          error: null,
        }
      case EVENT_DEDAIL_ERROR:
        return {
          ...state,
          event_detail: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  