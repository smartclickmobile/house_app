import { 
    EXCHANGE_POINT,
    EXCHANGE_POINT_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   exchange_success:null,
   exchange_success_num:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case EXCHANGE_POINT:
        console.log('EXCHANGE_POINT')
        return {
          ...state,
          exchange_success:true,
          exchange_success_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case EXCHANGE_POINT_ERROR:
        return {
          ...state,
          exchange_success: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  