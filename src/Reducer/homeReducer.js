import { 
    EVENT_LIST,
    EVENT_LIST_ERROR,
    NOW_SCREENING,
    NOW_SCREENING_ERROR,
    COMINGSOON,
    COMINGSOON_ERROR,
    SCHEDULE,
    SCHEDULE_ERROR,
    BRANNER,
    BRANNER_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   event_list:null,
   now_screening:null,
   comingsoon_list:null,
   schedule:null,
   branner:null,
   error:null,
   error_num:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case EVENT_LIST:
        return {
          ...state,
          event_list: action.payload,
          error: null,
        }
      case EVENT_LIST_ERROR:
        return {
          ...state,
          event_list: null,
          error: action.payload,
        }
        case NOW_SCREENING:
        return {
          ...state,
          now_screening: action.payload,
          error: null,
        }
      case NOW_SCREENING_ERROR:
        return {
          ...state,
          now_screening: null,
          error_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
        case COMINGSOON:
        return {
          ...state,
          comingsoon_list: action.payload,
          error: null,
        }
      case COMINGSOON_ERROR:
        return {
          ...state,
          comingsoon_list: null,
          error_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
      case SCHEDULE:
        return {
          ...state,
          schedule: action.payload,
          error: null,
        }
      case SCHEDULE_ERROR:
        return {
          ...state,
          schedule: null,
          error_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
      case BRANNER:
        return {
          ...state,
          branner: action.payload,
          error: null,
        }
      case BRANNER_ERROR:
        return {
          ...state,
          branner: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  