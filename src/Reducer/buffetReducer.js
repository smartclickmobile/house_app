import { 
    BUFFET_SUCCESS,
    BUFFET_ERROR,
    BUFFET_CLEAR_ERROR,
    BUFFET_CREDIT_SUCCESS,
    BUFFET_CREDIT_ERROR,
    BUFFET_QR_SUCCESS,
    BUFFET_QR_ERROR,
    BUFFET_CLEAR_PURCHASE,
    BUFFET_PURCHASE_DETAIL,
    BUFFET_PURCHASE_DETAIL_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   buffet:null,
   buffet_success_num:null,
   error:null,
   credit_success:null,
   credit_success_num:null,
   qr_success:null,
   qr_success_num:null,
   error_num:null,
   purchase_detail:null,
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case BUFFET_SUCCESS:
        return {
          ...state,
          buffet:action.payload,
          buffet_success_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case BUFFET_ERROR:
        return {
          ...state,
          buffet: null,
          error: action.payload,
        }
      case BUFFET_CLEAR_ERROR:
        return {
          ...state,
          buffet: null,
          buffet_success_num: null,
          error: action.payload,
        }
      case BUFFET_CREDIT_SUCCESS:
        return {
          ...state,
          credit_success: true,
          credit_success_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case BUFFET_CREDIT_ERROR:
        return {
          ...state,
          credit_success: null,
          error_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
      case BUFFET_QR_SUCCESS:
        return {
          ...state,
          qr_success: true,
          qr_success_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case BUFFET_QR_ERROR:
        return {
          ...state,
          qr_success: null,
          error_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
      case BUFFET_CLEAR_PURCHASE:
        return {
          ...state,
          credit_success:null,
          credit_success_num:null,
          qr_success: null,
          qr_success_num:null,
          error:null
        }
      case BUFFET_PURCHASE_DETAIL:
          return {
            ...state,
            purchase_detail:action.payload
          }
      case BUFFET_PURCHASE_DETAIL_ERROR:
          return {
            ...state,
            purchase_detail:null,
            error:action.payload
          }
      default:
        return state
    }
  }
  