import React, { Component } from "react"
import { View } from "react-native"
import { Scene, Router, Stack, Actions } from "react-native-router-flux"
import Tester from './Tester'
import Splash from './src/Screen/Splash'
import LoginhomeScreen from './src/Screen/loginhomeScreen'
import HomeScreen from './src/Screen/mainScreen'
import LoginScreen from './src/Screen/loginScreen'
import RegisterScreen from './src/Screen/registerScreen'
import ForgetpasswordScreen from './src/Screen/forgetpasswordScreen'
import ConfirmnumberScreen from './src/Screen/comfirmnumberScreen'
import BookingSelectTimeScreen from './src/Screen/BookingSelectTimeScreen'
import BookingSelectSeatScreen from './src/Screen/BookingSelectSeatScreen'
import BookingSummeryScreen from './src/Screen/BookingSummeryScreen'
import PaymentScreen from './src/Screen/PaymentScreen'
import PleaseLoginScreen from './src/Screen/PleaseLoginScreen'
import ConfirmMobileScreen from './src/Screen/ConfirmMobileScreen'
import DetailbannerScreen from './src/Screen/DetailbannerScreen'
import MycardScreen from './src/Screen/MycardScreen'
import MycradEmemberscreen from './src/Screen/MycradEmemberscreen'
import BookingSuccessScreen from './src/Screen/BookingSuccessScreen'

import Ehome from './src/Screen/Ehome'
import Eregister from './src/Screen/Eregister'
import Econfirm from './src/Screen/Econfirm'
import Epaymenthome from './src/Screen/Epaymenthome'
import Epayment from './src/Screen/Epayment'

import ShowQrcodeScreen from './src/Screen/ShowQrcodeScreen'

import ProfileChangemobile from './src/Screen/ProfileChangemobile'
import ProfileChangePassword from './src/Screen/ProfileChangePassword'
import ProfilePoint from './src/Screen/ProfilePoint'
import ProfileSetting from './src/Screen/ProfileSetting'
import Profilepage from './src/Screen/Profilepage'

import EmemberprivilegeScreen from './src/Screen/EmemberprivilegeScreen'

import SentOTPscreen from './src/Screen/SentOTPscreen'

import BuffetScreen from './src/Screen/BuffetScreen'
import BuffetSummeryScreen from './src/Screen/BuffetSummeryScreen'
import MycardBuffet from './src/Screen/MycardBuffet'
import BuffetPayment from './src/Screen/BuffetPayment'
import BuffetSentOTPScreen from './src/Screen/BuffetSentOTPScreen'
import BuffetSuccessScreen from './src/Screen/BuffetSuccessScreen'

import FromStudentScreen from './src/Screen/FromStudentScreen'
export default class Routers extends Component {
    render(){
        return(
            <Router>
                <Stack key='root'>
                {/* {<Scene key="Tester" component={Tester}  />}  */}
                    {<Scene key="splash" component={Splash} title="App" hideNavBar/>}

                    {<Scene key="loginhome" component={LoginhomeScreen} gesturesEnabled={false} hideNavBar/>}
                    {<Scene key="pleaselogin" component={PleaseLoginScreen} hideNavBar/>}
                    {<Scene key="login" component={LoginScreen} hideNavBar/>}
                    {<Scene key="confirmmobile" component={ConfirmMobileScreen} hideNavBar/>}

                    {<Scene key="register" component={RegisterScreen} hideNavBar/>}
                    {<Scene key="forgetpassword" component={ForgetpasswordScreen} hideNavBar/>}
                    {<Scene key="confirmnumber" component={ConfirmnumberScreen} hideNavBar/>}

                    {<Scene key="home" gesturesEnabled={false} component={HomeScreen} hideNavBar/>}
                    {<Scene key="BookingSelectTimeScreen" gesturesEnabled={false} component={BookingSelectTimeScreen} hideNavBar/>}
                    {<Scene key="BookingSelectSeatScreen" gesturesEnabled={false} component={BookingSelectSeatScreen} hideNavBar/>}
                    {<Scene key="BookingSummeryScreen" gesturesEnabled={false} component={BookingSummeryScreen} hideNavBar/>}
                    {<Scene key="ShowQrcodeScreen" gesturesEnabled={false} component={ShowQrcodeScreen} hideNavBar/>}
                    {<Scene key="payment" gesturesEnabled={false} component={PaymentScreen} hideNavBar/>}
                    {<Scene key="MycradEmemberscreen" gesturesEnabled={false} component={MycradEmemberscreen} hideNavBar/>}
                    {<Scene key="mycard" gesturesEnabled={false} component={MycardScreen} hideNavBar/>}
                    {<Scene key="BookingSuccessScreen" gesturesEnabled={false} component={BookingSuccessScreen} hideNavBar/>}

                    {<Scene key="detailbaner" component={DetailbannerScreen} hideNavBar/>}

                    {<Scene key="buffet_home" component={BuffetScreen} hideNavBar/>}
                    {<Scene key="buffet_summery" component={BuffetSummeryScreen} hideNavBar/>}
                    {<Scene key="MycardBuffet" component={MycardBuffet} hideNavBar/>}
                    {<Scene key="BuffetPayment" component={BuffetPayment} hideNavBar/>}
                    {<Scene key="BuffetSentOTPScreen" component={BuffetSentOTPScreen} hideNavBar/>}
                    {<Scene key="BuffetSuccessScreen" component={BuffetSuccessScreen} hideNavBar/>}

                    {<Scene key="Ehome" component={Ehome} hideNavBar/>}
                    {<Scene key="Eregister" component={Eregister} hideNavBar/>}
                    {<Scene key="Econfirm" component={Econfirm} hideNavBar/>}
                    {<Scene key="Epaymenthome" component={Epaymenthome} hideNavBar/>}
                    {<Scene key="Epayment" component={Epayment} hideNavBar/>}

                    {<Scene key="ProfileChangemobile" component={ProfileChangemobile} hideNavBar/>}
                    {<Scene key="ProfileChangePassword" component={ProfileChangePassword} hideNavBar/>}
                    {<Scene key="ProfilePoint" component={ProfilePoint} hideNavBar/>}
                    {<Scene key="ProfileSetting" component={ProfileSetting} hideNavBar/>}
                    {<Scene key="Profilepage" component={Profilepage} hideNavBar/>}

                    {<Scene key="Ememberprivilege" component={EmemberprivilegeScreen} hideNavBar/>}

                    {<Scene key="SentOTPscreen" component={SentOTPscreen} hideNavBar/>}
                    
                    {<Scene key="FromStudentScreen" component={FromStudentScreen} hideNavBar/>}
                </Stack>
            </Router>
        )
    }
}