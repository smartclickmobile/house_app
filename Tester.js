import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import firebase from '@react-native-firebase/app';
import {WebView} from 'react-native-webview';
import {Container, Header, Tab, Tabs, Text, Content} from 'native-base';

export default class Tester extends Component {
  state = {
    showweb: true,
  };
  show = () => {
    this.setState({showweb: !this.state.showweb});
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity style={{height: 30}} onPress={this.show}>
          <Text>back</Text>
        </TouchableOpacity>
        {this.state.showweb == true && (
          <WebView source={{uri: 'https://reactnative.dev/'}} />
        )}
      </View>
    );
  }
}
